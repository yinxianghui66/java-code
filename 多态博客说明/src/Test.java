class  People{
    String name;
    int age;
    int height;


    public People(String name, int age, int height) {
        this.name = name;
        this.age = age;
        this.height = height;
    }
    public void eat(){
        System.out.println("人正在吃饭");

    }
}
class Boy extends People{

    public Boy(String name, int age, int height) {
        super(name, age, height);
    }
    @Override
    public void eat(){
        System.out.println(name+"正在吃饭");
    }

}
class Girl extends  People{
    public Girl(String name, int age, int height) {
        super(name, age, height);
    }

    @Override
    public void eat() {
        System.out.println(name+"正在吃饭");
    }
}

public class Test {
    public static void eat(People people){
        people.eat();
    }
    public static void main(String[] args) {
//        People people=new Boy("小明",19,180);
//        people.eat();
//        People people1=new Girl("小红",18,165);
//        people1.eat();
          Girl girl=new Girl("小红",18,165);
          Boy boy=new Boy("小明",19,180);
          eat(boy);
          eat(girl);

    }
}
