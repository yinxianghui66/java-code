import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
//这里必须使用MultipartConfig 这个注解 否则无法使用getPart 方法
@MultipartConfig
@WebServlet("/upload")
public class UploadServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取客户端发送的文件  参数对应前端input的name属性
        Part part= req.getPart("MyImage");
        //获取提交的文件类型
        System.out.println(part.getContentType());
        //获取提交的文件名
        System.out.println(part.getSubmittedFileName());
        //获取提交的文件的大小
        System.out.println(part.getSize());
        //将获取的文件写入磁盘
        part.write("d:/code file/file.jpg");
        resp.getWriter().write("upload ok");
    }
}
