package Login;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

//验证用户名密码
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.从请求中拿到用户名和密码
        //保证读取的数据支持中文 设置编码方式为utf8
        req.setCharacterEncoding("utf8");
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        if(username==null||password==null||username.equals("")||password.equals("")){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前输入的用户名或密码不能为空");
            return;
        }
        //此时假定用户名为 zhangsan 或者lisi  密码都为123
//        if(username.equals("zhangsan")||username.equals("lisi")){
//            if(password.equals("123")){
//                //此时用户名密码都正确 跳转到主页
//            }else {
//                //密码错误
//            }
//        }else {
//            //用户错误
//        }
        if(!username.equals("zhangsan")&&!username.equals("lisi")){
            //用户名错误
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("用户名或者密码错误");
            return;
        }
        if(!password.equals("123")){
            //密码错误
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("用户名或者密码错误");
            return;
        }
        //都正确继续执行 创建一个对话 跳转到主页
        //创建一个对话 此时没有Session 这里参数为true就会自动创建一个Session对象
        //和sessionid 并且自动将session 对象和sessionid存储到哈希表中
        //同时返回session 对象 并且在接下里的响应中通过Set-cookie 把这个sessionid返回给客户端浏览器
        HttpSession session=req.getSession(true);
        //可以将刚创建好的session 对象存储我们自定义的数据 就可以在这个对象中存储用户的身份信息
        //该方法使用指定的名称绑定一个对象到该 session对话
        //相当于将用户的登录信息绑定到 对话中 存储到全局的一个哈希表中
        //使用 getAttribute 方法之后可以随时取出
        session.setAttribute("username",username);
        //重定到主页
        resp.sendRedirect("index");
    }
}
