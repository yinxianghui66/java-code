package Login;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

//动态生成主页内容
@WebServlet("/index")
public class indexServlet extends HttpServlet {
    //统计访问次数
    int visit=0;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取之前的session对象 设置为false 如果没找到就说明为未登录
        HttpSession session=req.getSession(false);
        if(session==null){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("用户未登录");
            return;
        }
        //取出之前绑定的username和password属性  getAttribute返回的是Object类 强转为String
        //之前登录页面会返回sessionid给浏览器 到这里是浏览器拿着sessionid 回来
        //此时取出的就是之前存储的属性 因为是同一个sessionid 所以取出的内容相同
        String username = (String) session.getAttribute("username");

        if(username==null){
            //如果有会话 但是里面没有属性 也认为未登录
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("用户未登录");
            visit=0;
            return;
        }

        //如果验证都通过 则动态生成一个动态页面
        resp.setContentType("text/html;charset=utf8");
        visit++;
        resp.getWriter().write("欢迎你"+username+"访问次数为"+visit);
    }
}
