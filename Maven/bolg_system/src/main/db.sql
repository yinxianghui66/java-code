--一般对于建表的 sql 都会单独写个 .sql 文件保存
--后续程序可能在不同的电脑上运行部署 部署的时候也需要运行 创建对应的数据库
--把建表的 sql 保存好 方便在不同的主机上进行建表建库

--建库 如果库不存在就建立
--create database if not exists java107_blog_system;
--
--use java107_blog_system;
----当前是否存在blog  如果存在就删掉
--drop table if exists blog;
----重新建立bolg表
--create table blog(
--    --博客id作为自增逐渐
--    blogId int primary key auto_increment,
--    title varchar(128),
--    content varchar(4096),
--    userid int,
--    postTime datetime
--);
--
--drop table if exists user;
----重新建立bolg表
--create table user(
--    userid int primary key auto_increment,
--    username varchar(50),
--    password varchar(50)
--);
--这里是是正式 复制粘贴到mysql 执行的代码
create database if not exists java107_blog_system charset utf8mb4;

use java107_blog_system;

drop table if exists blog;

create table blog(

    blogId int primary key auto_increment,
    title varchar(128),
    content varchar(4096),
    userid int,
    postTime datetime
);

drop table if exists user;

create table user(
    userid int primary key auto_increment,
    username varchar(50) unique,
    password varchar(50)
);

insert into user values(null,'zhangsan','123'),(null,'lisi',123);

insert into blog values(null,'我的第一篇博客','正文',1,'2023-06-15 12:00:00');
insert into blog values(null,'我的第二篇博客','正文',1,'2023-06-16 12:21:00');
insert into blog values(null,'我的第三篇博客','不必说碧绿的菜畦，光滑的石井栏，高大的皂荚树，紫红的桑椹;也不必说鸣蝉在树叶里长吟，肥胖的黄蜂伏在菜花上，轻捷的叫天子(云雀)忽然从草间直窜向云霄里去了。单是周围的短短的泥墙根一带，就有无限趣味。油蛉在这里低唱，蟋蟀们在这里弹琴。翻开断砖来，有时会遇见蜈蚣;还有斑蝥，倘若用手指按住它的脊梁，便会拍的一声，从后窍喷出一阵烟雾。何首乌藤和木莲藤缠络着，木莲有莲房一般的果实，何首乌有拥肿的根。有人说，何首乌根是有象人形的，吃了便可以成仙，我于是常常拔它起来，牵连不断地拔起来，也曾因此弄坏了泥墙，却从来没有见过有一块根象人样。如果不怕刺，还可以摘到覆盆子，象小珊瑚珠攒成的小球，又酸又甜，色味都比桑椹要好得远。',1,'2023-06-17 12:21:00');
--删库
drop database java107_blog_system;