package model;

import model.DBUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
    //删除用户
    //注册用户 （上面俩暂时没实现）
    //查找用户
    //根据id查找用户!
    public User selectUserById(int userId){
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        User user=new User();
        try {
            //建立连接
            connection= DBUtil.getConnection();
            //构造sql语句
            String sql="select * from user where userId=?";
            statement=connection.prepareStatement(sql);
            statement.setInt(1,userId);
            //执行sql语句
            resultSet=statement.executeQuery();
            if (resultSet.next()){
                user.setUserId(resultSet.getInt("userId"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return user;
    }
    //根据name查找用户
    public User selectUserByName(String username){
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        User user=new User();
        try {
            //建立连接
            connection= DBUtil.getConnection();
            //构造sql语句
            String sql="select * from user where username=?";
            statement=connection.prepareStatement(sql);
            statement.setString(1,username);
            //执行sql语句
            resultSet=statement.executeQuery();
            if (resultSet.next()){
                user.setUserId(resultSet.getInt("userId"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return user;
    }
}
