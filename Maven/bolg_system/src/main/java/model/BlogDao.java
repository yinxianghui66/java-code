package model;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//Dao表示数据访问对象
public class BlogDao {
    //把一个Blog 对象插入数据库中
    public void insert(Blog blog){
        Connection connection=null;
        PreparedStatement statement=null;
        try{
            //我们已经封装好连接操作 可以直接调用
            //1.建立连接
           connection=DBUtil.getConnection();
            //2.构造sql语句
            String sql="insert into blog values(null,?,?,?,?)";
            statement=connection.prepareStatement(sql);
            statement.setString(1,blog.getTitle());
            statement.setString(2,blog.getContent());
            statement.setInt(3,blog.getUserId());
            statement.setString(4,blog.getPostTime());
            //3.执行sql语句
            statement.executeLargeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            //关闭连接放到finally更好 前面异常也不会影响关闭
        }finally {
            //4.关闭连接
            DBUtil.close(connection,statement,null);
        }
    }
    //查询数据库中Blog表中的所有数据
    public List<Blog> selectAll(){
        //将查询的数据存储到List中
        List<Blog> blogs=new ArrayList<>();
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        try{
            //我们已经封装好连接操作 可以直接调用
            //1.建立连接
            connection=DBUtil.getConnection();
            //2.构造sql语句  这里博客都是新发布的在上面 这里我们按照时间降序查询即可
            String sql="select * from blog order by postTime desc";
            statement=connection.prepareStatement(sql);
            //3.执行sql语句
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                Blog blog=new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                //这里我们显示的只需要一部分摘要即可 只需要显示一部分(0-100)即可
                String content=resultSet.getString("content");
                //如果超出100长度就只取前100
                if(content.length()>100){
                    content=content.substring(0,100)+"...";
                }
                blog.setContent(content);
                blog.setUserId(resultSet.getInt("userid"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                //遍历查询到的数据 存储到list中
                blogs.add(blog);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            //关闭连接放到finally更好 前面异常也不会影响关闭
        }finally {
            //4.关闭连接
            DBUtil.close(connection,statement,resultSet);
        }
        return blogs;
    }
    //给定一个博客id查询对应的数据
    public Blog selectOne(int blogId){
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        Blog blog=new Blog();
        try {
            //建立连接
           connection=DBUtil.getConnection();
           //构造sql语句
           String sql="select * from blog where blogId=?";
           statement=connection.prepareStatement(sql);
           statement.setInt(1,blogId);
            //执行sql语句
           resultSet=statement.executeQuery();
           if(resultSet.next()) {
               //赋值消息
               blog.setBlogId(resultSet.getInt("blogId"));
               blog.setTitle(resultSet.getString("title"));
               blog.setContent(resultSet.getString("content"));
               blog.setUserId(resultSet.getInt("userid"));
               blog.setPostTime(resultSet.getTimestamp("postTime"));
           }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
             //执行关闭
            DBUtil.close(connection,statement,resultSet);
        }
        return blog;
    }
    //给定一个博客id删除对应的数据
    public void delete(int blogId){
        Connection connection=null;
        PreparedStatement statement=null;
        try {
            //建立连接
            connection=DBUtil.getConnection();
            //构造sql语句
            String sql="delete from blog where blogId = ?";
            statement=connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            //执行
            statement.executeLargeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }

}
