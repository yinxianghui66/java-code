package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Blog;
import model.BlogDao;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

//实现后端提供的登陆
@WebServlet("/blog")
public class BolgServlet extends HttpServlet {
    protected ObjectMapper objectMapper=new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //从query string 中查询是否有blogId  如果有就要查询指定博客 如果没有就是正常查询所有数据
        String blogId=req.getParameter("blogId");
        BlogDao blogDao = new BlogDao();
        if(blogId==null) {
            //从数据库查询数据存储到list
            List<Blog> blogs = blogDao.selectAll();
            //将list数据转换成json格式
            String respString = objectMapper.writeValueAsString(blogs);
            //写回给客户端 数据格式
            resp.setContentType("application/json;charset=utf8");
            resp.getWriter().write(respString);
        }else {
            Blog blog=blogDao.selectOne(Integer.parseInt(blogId));
            String respString = objectMapper.writeValueAsString(blog);
            //写回给客户端 数据格式
            resp.setContentType("application/json;charset=utf8");
            resp.getWriter().write(respString);
        }
    }
    //用来发布博客 实现
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      //从前端拿到数据
        req.setCharacterEncoding("utf8");
        String title=req.getParameter("title");
        String content=req.getParameter("content");
        if(title==null||content==null||title.equals("")||content.equals("")){
            //数据不存在
            String html="<h3>标题或者正文为空  发布失败</h3>";
            resp.setContentType("text/html ; charset=utf8");
            resp.getWriter().write(html);
            return;
        }
        //从会话中拿到作者的id
        HttpSession session=req.getSession(false);
        if(session==null){
            //未登录
            String html="<h3>当前用户未登录</h3>";
            resp.setContentType("text/html ; charset=utf8");
            resp.getWriter().write(html);
            return;
        }
        User user= (User) session.getAttribute("user");
        if(user==null){
            //未登录
            String html="<h3>当前用户未登录</h3>";
            resp.setContentType("text/html ; charset=utf8");
            resp.getWriter().write(html);
            return;
        }
        //构造blog对象 插入到数据库
        Blog blog=new Blog();
        blog.setUserId(user.getUserId());
        blog.setTitle(title);
        blog.setContent(content);
        blog.setPostTime(new Timestamp(System.currentTimeMillis()));
        //插入到数据库 blog对象
        BlogDao blogDao=new BlogDao();
        blogDao.insert(blog);
        //跳转到登录页
        resp.sendRedirect("blog_list.html");
    }
}
