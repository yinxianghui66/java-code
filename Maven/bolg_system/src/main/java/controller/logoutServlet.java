package controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/logout")
public class logoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //通过a标签发送get请求 删除对应的会话
        //删除后重定向到登陆页
        HttpSession session= req.getSession(false);
        if(session==null){
            //用户本来就没登录  //跳转到登录页即可
            resp.sendRedirect("blog_login.html");
        }
        //如果user存在 就删除这个对话 不存在也不会有影响
        session.removeAttribute("user");
        resp.sendRedirect("blog_login.html");
    }
}
