package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Blog;
import model.BlogDao;
import model.User;
import model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/user")
public class UserServlet extends HttpServlet {
    private ObjectMapper objectMapper=new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //读取出blogId  拿到id查找对应的文章
        //找到文章作者 返回前端
        resp.setContentType("application/json;charset=utf8");
        String blogId=req.getParameter("blogId");
        if(blogId==null||blogId.equals("")){
            //如果不存在 返回一个userId为0的对象 最终返回json数据
            //返回一个空的对象
            String respJson=objectMapper.writeValueAsString(new User());
            resp.getWriter().write(respJson);
            System.out.println("参数给定blogId为空");
            return;
        }
        //查询数据库 找到文章
        BlogDao blogDao=new BlogDao();
        Blog blog=blogDao.selectOne(Integer.parseInt(blogId));
        if(blog==null){
            String respJson=objectMapper.writeValueAsString(new User());
            resp.getWriter().write(respJson);
            System.out.println("数据库中找不到对应的文章");
            return;
        }
        UserDao userDao=new UserDao();
        User user=userDao.selectUserById(blog.getUserId());
        if(user==null){
            String respJson=objectMapper.writeValueAsString(new User());
            resp.getWriter().write(respJson);
            System.out.println("该博客对应的作者不存在");
            return;
        }
        //如果都通过 返回数据给前端
        String respJson=objectMapper.writeValueAsString(user);
        resp.getWriter().write(respJson);
    }
}
