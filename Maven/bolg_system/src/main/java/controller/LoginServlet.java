package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.User;
import model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
//实现登陆逻辑
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private ObjectMapper objectMapper=new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置前端的数据编码方式为uft8  防止用户名出现中文导致出现问题
        req.setCharacterEncoding("utf8");
        //从前端form表单中取出数据
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        //用户名密码残缺 登陆失败 返回错误html
        if(username==null||username.equals("")||password==null||password.equals("")){
            String html="<h3> 登陆失败 缺少用户名或者密码！</h3>";
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write(html);
            return;
        }
        //校验数据库账户
        UserDao userDao=new UserDao();
        User user=userDao.selectUserByName(username);
        if(user.getUsername()==null){
            //用户名不存在
            String html="<h3> 登陆失败 用户名不存在 </h3>";
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write(html);
            return;
        }
        if(!password.equals(user.getPassword())){
            //密码错误
            String html="<h3> 用户名或者密码错误</h3>";
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write(html);
            return;
        }
        //都通过就说明登陆成功 创建一个对话 重定向到 blog_list.html
        HttpSession session=req.getSession(true);
        //把用户对象存储到session中 下次访问不需要访问其他页面  就可以直接拿到之前的user对象了
        session.setAttribute("user",user);
        //重定向到blog_list.html
        resp.sendRedirect("blog_list.html");
    }
    //通过这个方法 判定当前用户登录状态 已登录返回200  未登录返回403 状态码
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //看当前请求是否存在会话  并且当前对话是否存在user对象
        HttpSession session=req.getSession(false);
        if(session==null){
            //会话不存在 未登录状态
            resp.setStatus(403);
            return;
        }
        User user= (User) session.getAttribute("user");
        if(user==null){
            //会话存在 但是不存在User  也是未登录
            resp.setStatus(403);
            return;
        }
        //都通过 返回200 默认返回也是可以的
        resp.setStatus(200);
        //登陆成功后 返回对应账户的用户名 用户登陆列表页等显示当前账户
        //将密码设置为空 加强安全系数 (掩耳盗铃式的)
        user.setPassword("");
        resp.setContentType("application/json;charset=utf8");
        String respJson=objectMapper.writeValueAsString(user);
        resp.getWriter().write(respJson);
    }
}
