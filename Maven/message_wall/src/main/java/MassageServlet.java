import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


class Message{
    //必须设置public
    // 如果是private 必须生成getter和setter！！
    public String from;
    public String to;
    public String message;
    //方便打印
    @Override
    public String toString() {
        return "Message{" +
                "form='" + from + '\'' +
                ", to='" + to + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}

@WebServlet("/message")
public class MassageServlet extends HttpServlet {
    private  ObjectMapper objectMapper=new ObjectMapper();
//    private List<Message> messageList=new ArrayList<>();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       //通过这个方法来获取所有信息
        //将list转换成json字符串数组 返回一个json字符串数组
        List<Message> messageList=load();
        String respString=objectMapper.writeValueAsString(messageList);
        resp.setContentType("application/json; charset=utf8");
        resp.getWriter().write(respString);
    }
    //从数据库中读取数据
    private List<Message> load() {
        //存储结果
        List<Message> list=new ArrayList<>();
        DataSource dataSource=new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/yxh?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("20020401yxh");
        try {
            Connection connection=dataSource.getConnection();
            String sql="select * from message";
            PreparedStatement statement=connection.prepareStatement(sql);
            //执行
            ResultSet resultSet=statement.executeQuery();
            //遍历返回的resultSet 对应每一条消息进行拆分赋值
            while (resultSet.next()){
                Message message=new Message();
                message.from=resultSet.getString("from");
                message.to=resultSet.getString("to");
                message.message=resultSet.getString("message");
                list.add(message);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //通过这个方法新增数据
        //解析请求的body转成Message对象 将这个对象的信息存储起来
        Message message=objectMapper.readValue(req.getInputStream(),Message.class);
         //添加到存储器里面
//         messageList.add(message);
        save(message);
        //响应成功默认就会返回200  这里不用额外设置
        System.out.println("消息添加成功 message=" + message);
    }
    //往数据库写一条消息
    private void save(Message message)  {
        DataSource dataSource=new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/yxh?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("20020401yxh");
        //
        try {
            Connection connection=dataSource.getConnection();
            //编辑语句 插入一条数据
            String sql="insert into message values(?,?,?)";
            PreparedStatement statement=connection.prepareStatement(sql);
            statement.setString(1,message.from);
            statement.setString(2,message.to);
            statement.setString(3,message.message);
            //执行sql语句
            statement.executeUpdate();
            //关闭资源
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
