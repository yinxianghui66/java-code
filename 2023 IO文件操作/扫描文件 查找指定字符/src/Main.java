import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入要查找的目录");
        String path= scanner.next();
        File rootDir=new File(path);  //创建File对象
        if(!rootDir.isDirectory()){
            System.out.println("输入错误");
            return;
        }
        System.out.println("输入要查找的关键字");
        String word= scanner.next();
        scanDirWith(rootDir,word);
    }

    private static void scanDirWith(File rootDir, String word) throws IOException {
        File[] files=rootDir.listFiles();   //把File对象里的内容全部列出
        if(files==null||files.length==0){
            return;
        }
        for (File file:files) {   //遍历这些文件/目录
            System.out.println(file.getAbsolutePath()+"查找到这里了");
            if(file.isFile()){ //如果是文件就打开并且查找，如果相同返回
                String count=readFile(file);
                if(count.contains(word)){  //如果存在就返回
                    System.out.println(file.getAbsolutePath()+"包含要找的关键字");
                }
            }else if(file.isDirectory()){  //如果当前的File是目录，那就以当前目录为下一个目录，查找这个目录里的所有文件
                scanDirWith(file,word);
            }else {  //俩都不是 结束这次循环
                continue;
            }
        }
    }

    private static String readFile(File file) {  //打开文件查找是否有关键字
        StringBuffer stringBuffer=new StringBuffer();   //存储读取结果
        try(Reader reader=new FileReader(file)) {  //使用字符流读取
            while (true){
            int ret=reader.read();
                 if(ret==-1){
                        break;
                }
                stringBuffer.append((char) ret);  //这里需要强转成char来存入
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuffer.toString();  //将stringBuffer内容转换成字符串返回
    }
}
