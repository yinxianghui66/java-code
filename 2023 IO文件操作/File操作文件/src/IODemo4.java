import java.io.File;
import java.util.Arrays;

public class IODemo4 {
    public static void main(String[] args) {
        File file=new File("test-dir");
        String[] arr=file.list();    //返回 File 对象代表的目录下的所有文件名
        System.out.println(Arrays.toString(arr));
        File[] arr1=file.listFiles();   //返回 File 对象代表的目录下的所有文件，以 File 对象表示
        System.out.println(Arrays.toString(arr1));
    }
}
