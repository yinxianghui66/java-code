package IO;

import java.io.File;
import java.io.IOException;

public class IODemo1 {
    public static void main(String[] args) throws IOException {
        File file=new File("./哲学家.png");
        System.out.println(file.getParent());  //返回File对象的父目录文件路径
        System.out.println(file.getName());    //返回File对象的纯文件名称
        System.out.println(file.getPath());   //返回File对象的文件路径
        System.out.println(file.getAbsoluteFile());   //返回File对象的绝对路径
        System.out.println(file.getCanonicalPath());  //返回File对象修饰过的绝对路径
    }
}
