package IO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class IODemo9 {
    public static void main(String[] args) {
        try(Writer writer=new FileWriter("d:/text.txt")){
            writer.write("abdc");  //字符流输入
            writer.write("efg");
            writer.write("hig");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
