package IO;

import java.io.File;
import java.io.IOException;

public class IODemo2 {
    public static void main(String[] args) throws IOException {
        //相对路径中./可以省略
        File file=new File("./hello_word.txt");   //IDEA 的工作目录就是这个项目所在的目录 写相对路径就是以该项目的目录为工作目录
        System.out.println(file.exists());  //文件是否存在
        System.out.println(file.isDirectory());  //判断当前File的对象代表的文件是不是目录
        System.out.println(file.isFile());   //判断File对象代表的是不是一个普通文件

        file.createNewFile();  //创建该文件
        System.out.println(file.exists());  //文件是否存在
        System.out.println(file.isDirectory());  //判断当前File的对象代表的文件是不是目录
        System.out.println(file.isFile());   //判断File对象代表的是不是一个普通文件

        file.delete();  //删除文件
        System.out.println("删除文件");
        System.out.println(file.exists());   //判断文件是否存在
    }
}
