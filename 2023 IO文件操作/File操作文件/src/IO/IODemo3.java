package IO;

import java.io.File;

public class IODemo3 {
    public static void main(String[] args) {
        File file=new File("test-dir/123/456");
        file.mkdir();   //创建目录  只能创建一级
        file.mkdirs();   //创建多级目录
    }
}
