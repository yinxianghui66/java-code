package IO;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class IODemo7 {
    public static void main(String[] args) {
        try(OutputStream outputStream=new FileOutputStream("d:/test.txt")){
            outputStream.write(97);
            outputStream.write(98);
            outputStream.write(99);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
