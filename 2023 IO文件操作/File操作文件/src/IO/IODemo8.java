package IO;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class IODemo8 {
    public static void main(String[] args) {
        try(Reader reader=new FileReader("d:/test.txt")){
            while (true){
                int ret=reader.read();
                if(ret==-1){
                    break;
                }
                char ch=(char)ret;
                System.out.println(ch);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
