package IO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class IODemo6 {
    //事先在目录中准备了文件，内容为abc
    public static void main(String[] args) throws IOException {   //使当前对象和硬盘上的文件关联起来
        try(InputStream inputStream=new FileInputStream("d:/test.txt")) {  //相当于打开文件，如果不存在就抛出异常
            while (true) {                              //try代码块的作用可以在代码块结束的时候自动执行close操作
                int ret = inputStream.read();  //读取文件，一次读取一个字节，一直到文件读取结束返回-1  这里使用int接收
                if (ret == -1) {                 // （需要-1来确定是否读取完）
                    break;
                }

                System.out.printf("%x\n",ret);
            }
        }
//        inputStream.close();  //有打开就要有关闭操作~  如果不关闭，文件描述符如果超出上限就会发生严重BUG
    }
}
