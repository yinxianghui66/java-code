//链接:https://leetcode.cn/problems/missing-number/
//使用异或的办法 因为只缺失一个数 所以 nums[0] ~ nums[n] 这些位置 异或的结果
//和 从 0 ~ n 位置异或的结果 就差缺失是那个数 所以异或到最后 剩下的就是缺失的数
class Solution {
    public int missingNumber(int[] nums) {
        int sum=0;
        for (int i = 0; i <nums.length; i++) {
            sum^=nums[i];
        }
        for (int i = 0; i <=nums.length; i++) {
            sum^=i;
        }
        return sum;
    }
}