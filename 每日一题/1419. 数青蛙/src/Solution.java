//https://leetcode.cn/problems/minimum-number-of-frogs-croaking/submissions/477152831/
//模拟+哈希表  使用哈希表 来快速找到哈希数组上一位的下标位置 使用哈希数组 统计当前蛙叫到个位置
//每次遍历 只需要找这个位置的前一位 存不存在数组 存在说明可以继续蛙叫 不存在即不合法 返回-1
//细节问题 当我们遍历完字符串 如果前面有未叫完的蛙叫 说明不合法 直接返回-1
//当遍历到第一声蛙叫 此时两种情况 如果最后位置有值 说明可以用之前叫完的青蛙叫这一声 达到最小青蛙的目的
//如果没有值 此时就新来一个青蛙叫
//最后返回最后一声蛙叫的次数 也就是 k 出现的次数即可!
import java.util.HashMap;
import java.util.Map;
class Solution {
    public int minNumberOfFrogs(String croakOfFrogs) {
        char[] arr=croakOfFrogs.toCharArray();
        String s="croak";
        int n= s.length();
        //使用数组 模拟哈希表
        int[] hash=new int[n];
        //使用字符 映射下标
        Map<Character,Integer> index=new HashMap<>(); //[x,x这个这个字符对应的下标]
        for (int i = 0; i <n; i++) {
            index.put(s.charAt(i),i);
        }
        for(char ch:arr){
            if(ch==s.charAt(0)){
                //如果是 c 的话 分两种清空 如果k位置有值 将位置k-- c++  如果没值 k++
                if(hash[n-1]!=0) hash[n-1]--;
                hash[0]++;
            }else {
                //遍历到当前字符的 对应哈希数组的下标
                int i=index.get(ch);
                if(hash[i-1]==0) return -1; //说明前面并没有青蛙叫 直接返回
                hash[i-1]--;
                hash[i]++;   //对应这个位置++
            }
        }
        //特殊情况处理
        for (int i=0;i<n-1;i++){
            if(hash[i]!=0) return -1;
        }
        return hash[n-1];
    }
}