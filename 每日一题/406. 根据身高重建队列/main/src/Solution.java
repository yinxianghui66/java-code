import java.util.Arrays;
import java.util.LinkedList;

//假设有打乱顺序的一群人站成一个队列，
// 数组 people 表示队列中一些人的属性（不一定按顺序）。每个 people[i] = [hi, ki] 表示第 i 个人的身高为 hi
// 前面 正好 有 ki 个身高大于或等于 hi 的人。
//请你重新构造并返回输入数组 people 所表示的队列。返回的队列应该格式化为数组 queue ，其中 queue[j] = [hj, kj]
// 是队列中第 j 个人的属性（queue[0] 是排在队列前面的人）。
//
class Solution {
    public static int[][] reconstructQueue(int[][] people) {
        //把数组 按照身高来排序 这样每个位置 之前的值 身高一定是大于当前位置的
        //所以 你插入到前面的位置 你这个值一定是比前面的值小的 所以不会影响后面的顺序
        Arrays.sort(people,(a,b)->{
            //如果两个身高相等 则将身高比较小的排到前面
            if(a[0]==b[0]) return a[1]-b[1];
            //不相等时 则按照降序排序
            return b[0]-a[0];
        });
        //排序完后 身高较高的排在前面 遍历这个数组 将k的位置插入到指定的位置
        LinkedList<int[]> que = new LinkedList<>();
        for(int[] p:people){
            //根据k的下标位置 插入元素
            que.add(p[1],p);
        }
        return que.toArray(new int[people.length][]);
    }

    public static void main(String[] args) {
        int[][] s ={{7,0},{4,4},{7,1},{5,0},{6,1},{5,2}};
        System.out.println(reconstructQueue(s));
    }
}