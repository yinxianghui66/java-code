//给你一个 只包含正整数 的 非空 数组 nums 。
// 请你判断是否可以将这个数组分割成两个子集，使得两个子集的元素和相等
//输入：nums = [1,5,11,5]
//输出：true
//解释：数组可以分割成 [1, 5, 5] 和 [11] 。
class Solution {
    public static boolean canPartition(int[] nums) {
        //定义dp数组 dp[j] 为容量为j时 最大的容量 如果容量 == 背包容量 则可以
        //这里我们的目标就是数组的和/2 也就是容量为sum/2 的背包 看存不存在
        if(nums==null||nums.length==0) return false;
        int len = nums.length;
        int sum = 0;
        for (int i = 0; i <len; i++) {
           sum+=nums[i];
        }
        if(sum%2!=0) return false;
        int target = sum/2;
        //这里dp的长度 为target+1 要找到容量为target的位置的元素 该位置的元素的值 是不是预期的目标值
        int[] dp = new int[target+1];
        //初始化dp数组 这里也是默认为0
        //遍历顺序 直接套01背包的公式 先便利物品 再便利背包
        for (int i = 0; i < len; i++) {
            for (int j = target; j >=nums[i] ; j--) {
                dp[j] = Math.max(dp[j],dp[j-nums[i]]+nums[i]);
            }
            if(dp[target]==target) return true;
        }
        return dp[target] ==target;
    }

    public static void main(String[] args) {
        int[] num = {1,5,11,5};
        System.out.println(canPartition(num));
    }
}