import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode() {}
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
  }

class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        //返回一个二维数组 每个数组代表一层的元素
        //使用队列来模拟 并且记录每一层的要弹出的元素个数
        //每次弹出一个非空的节点 则给队列加入该节点的左右节点
        Queue<TreeNode> queue = new LinkedList<>();
        List<List<Integer>> res= new ArrayList<>();
        if(root==null) return res;
        queue.add(root);
        while (!queue.isEmpty()){
            List<Integer> item = new ArrayList<>();
            //记录节点个数
            int len = queue.size();
            for(int i=0;i<len;i++){
                //弹出对应个数的元素 并且存储
                TreeNode node = queue.poll();
                item.add(node.val);
                //弹出之后 加入该节点的左右孩子到队列后
                //如果该节点的左右节点位空 则无需加入
                if(node.left!=null){
                    queue.add(node.left);
                }
                if(node.right!=null){
                    queue.add(node.right);
                }
            }
            res.add(item);
        }
        return res;
    }
}