//斐波那契数 （通常用 F(n) 表示）形成的序列称为 斐波那契数列 。
// 该数列由 0 和 1 开始，后面的每一项数字都是前面两项数字的和。也就是
//输入：n = 2
//输出：1
//解释：F(2) = F(1) + F(0) = 1 + 0 = 1
class Solution {
    public static int fib(int n) {
        if(n<=1) return n;
        //用动态规划来做
        //1.确定dp[i] 代表的含义 本题为 第i个斐波那契数
        int[] dp = new int[n+1];
        //2.确定递推公式 本题已经给出了 即dp[i] = dp[i-1]+dp[i-2]
        //3.dp数组如何初始化 可以得到 dp[0] = 1 dp[1] =1;
        //4.dp数组的遍历顺序 本体因为要依靠前两个数 所以为 从前到后
        dp[0] = 1;
        dp[1] = 1;
        for (int i = 2; i <=n; i++) {
            dp[i] = dp[i-1]+dp[i-2];
        }
        //5.打印dp数组 进行调试
        for (int i = 0; i <dp.length;i++) {
            System.out.print(dp[i]+" ");
        }
        return dp[n-1];
    }

    public static void main(String[] args) {
        System.out.println(fib(4));
    }
}