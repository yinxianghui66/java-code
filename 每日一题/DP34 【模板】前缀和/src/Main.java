import java.util.Scanner;

//链接:https://www.nowcoder.com/practice/acead2f4c28c401889915da98ecdc6bf?tpId=230&tqId=2021480&qru
//前缀和算法(小型的动态规划)
//提前准备前缀和的数组 数组的元素 对应 1-n 位置的和
//找到初始化数组的公式 不需要从头统计  将前一位的和加上当前位置即为当前dp数组下标的元素 (dp[i]=dp[i-1]+arr[i])
//找到l r区域的和  只要找到公式 dp[r]-dp[l-1]
public class Main {
    public static void main(String[] args) {
        //处理输入
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int q=scanner.nextInt();
        int arr[]=new int[n+1];
        //下标从1开始
        for(int i=1;i<=n;i++) arr[i]=scanner.nextInt();
        //初始化前缀和数组 使用long 防止越界
        long dp[]=new long[n+1];
        //下标从1开始  是因为 如果从0开始 因为需要用到l-1 如果为0 就需要处理边界情况
        for(int i=1;i<=n;i++) dp[i]=dp[i-1]+arr[i];
        //循环计算结果
        while(q>0){
            int l=scanner.nextInt();
            int r=scanner.nextInt();
            System.out.println(dp[r]-dp[l-1]);
            q--;
        }
    }
}
