//链接：https://leetcode.cn/problems/find-first-and-last-position-of-element-in-sorted-array/
//使用二分查找的算法
//分为两部步 找到左端点
//如果当前位置小于 目标值 因为数组是有序的 所以可以直接mid+1从后面继续找目标值
//如果大于等于 说明后面的目标在后面的区间(因为是等于 所以当前位置也可能是目标值)  right=mid值 继续寻找
//需要注意的就是细节问题
//1.取中间值的方法 (如下 根据你left和right的加减决定)  (下面出现-1的时候 上面就+1)
//2.left < right  不能等于 因为等于就是目标位置
//3.判断之后 left和right如何移动
class Solution {
    public int[] searchRange(int[] nums, int target) {
        int ret[]=new int[2];
        ret[0]=ret[1]=-1;
        //处理边界情况
        if(nums.length==0){
            return  ret;
        }
        int left=0,right=nums.length-1;
        //先找左侧端点  注意这里不能是等于 因为我们最后查找的就是等于 最后left和right相遇 表示找到这个端点了
        while(left<right){
            int mid=left+(right-left)/2;   //使用这种求中策略 当数据数为偶数时 取得是左边的数据 如果取的是右边的数据
            if(nums[mid]<target) left=mid+1;  //就会造成死循环 因为如果只有两个数据时 right是一直不变的 造成死循环
            else{
                right=mid;  //不能是mid-1 因为这个位置也可能是端点
            }
        }
        if(nums[left]!=target){
            return ret;
        }
        ret[0]=left;
        //找右侧端点
        left=0;right=nums.length-1;
        while(left<right){
            int mid=left+(right-left+1)/2;   //使用不同的求中策略 防止死循环  原因如上
            if(nums[mid]<=target){
                left=mid;
            }else{
                right=mid-1;
            }
        }
        //如果找到 最后left和right一定是在一个位置的
        if(nums[left]!=target){
           return ret;
        }
        ret[1]=left;
        return ret;
    }
}