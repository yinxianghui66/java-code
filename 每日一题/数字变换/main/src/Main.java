import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long a = in.nextInt();
        long b = in.nextInt();
        System.out.println(getNum(a, b));
    }
    public static long getNum(long a,long b){
        //特殊情况 直接输出
        if(b%a!=0){
            return -1;
        }
        long k = b/a;
        long cnt = 0;
        while (k>1){
            boolean divded = false;
            int[] v = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,
                    103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,
                    199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293
            };
            for (int i:v){
                if(k%i==0){
                    k/=i;
                    cnt++;
                    divded=true;
                    break;
                }
            }
            //如何k不能被任何
            if(!divded){
                return cnt+1;
            }
        }
        return cnt;
    }

}