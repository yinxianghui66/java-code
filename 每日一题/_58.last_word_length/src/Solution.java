class Solution {
    public int lengthOfLastWord(String s) {
        int count=0;
        if(s.length()==0||s==null) return 0;
        for(int i=s.length()-1;i>=0;i--){
            if(s.charAt(i)==' '){
                //找到第一个不为空格的单词 进行统计
                if(count==0) continue;
                break;
            }
            count++;
        }
        return count;
    }
}