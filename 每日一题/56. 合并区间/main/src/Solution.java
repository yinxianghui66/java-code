import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

//以数组 intervals 表示若干个区间的集合，其中单个区间为 intervals[i] = [starti, endi] 。
// 请你合并所有重叠的区间，并返回 一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间 。
class Solution {
    public int[][] merge(int[][] intervals) {
        LinkedList<int[]> res = new LinkedList<>();
        if(intervals.length==0) return res.toArray(new int[res.size()][]);
        //找重叠区间 合并
        //先将数组排序
        Arrays.sort(intervals,(a,b)->Integer.compare(a[0],b[0]));
        //遍历数组 找重叠的区间
        res.add(intervals[0]);
        for (int i = 1; i <intervals.length ; i++) {
            //当前位置的左区间 小于上一个位置的右区间 则为重复区间 需要合并
            if(intervals[i][0]<=res.getLast()[1]){
                //左边界不变 右边界取最大值  //合并区间
                int start = res.getLast()[0];
                int end = Math.max(intervals[i][1],res.getLast()[1]);
                //删除最后位置的元素 加入更新后的操作
                res.removeLast();
                //更新右区间
                res.add(new int[]{start,end});
            }else {
                //不重复的区间 直接加入结果
                res.add(new int[]{intervals[i][0],intervals[i][1]});
            }
        }
        return res.toArray(new int[res.size()][]);
    }
}