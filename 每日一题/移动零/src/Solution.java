//https://leetcode.cn/problems/move-zeroes/description/
//双指针的算法 保持三个区间 0-dest 全为0区间 dest+1-cur-1 为非零区间 cur-nums.length 为待处理数据区间
//当然cur=nums.length 时 待处理数据区间为0 即为0在前 非零在后
class Solution {
    public void moveZeroes(int[] nums) {
        //双指针
        //cur 遍历数组  遇到0元素 直接++ 遇到非零元素 dest+1 交换cur和dest元素
        //cur ++  dest++
        int cur=0;
        //dest标识非零元素的最后一个位置
        int dest=-1;
        while(cur<nums.length){
            if(nums[cur]!=0){
                dest++;
                int tmp=nums[cur];
                nums[cur]=nums[dest];
                nums[dest]=tmp;
            }
            cur++;
        }
    }
}
