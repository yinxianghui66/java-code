//编写一个函数来查找字符串数组中的最长公共前缀。
//
//如果不存在公共前缀，返回空字符串 ""。
//链接:https://leetcode.cn/problems/longest-common-prefix/description/
//输入：strs = ["flower","flow","flight"]
//输出："fl"

//解法1 两两比较  找到两个字符串的公共前缀 依次比较

//解法2 纵向比较 一次比较一列 如果相同 继续向后 如果不相同 则公共前缀为 0 - i 位置
//如果发现一列中有字符不同 则公共前缀为 0 - i 位置 结束
class Solution {
    //解法1
    public String longestCommonPrefix(String[] strs) {

        if(strs.length==0) return "";
        String ret = strs[0];
        for(int i=1; i<strs.length;i++){
            //两个字符串的公共前缀
           ret = commonPrefix(ret, strs[i]);
        }
        return  ret;
     }

    private String commonPrefix(String s1, String s2) {
        int i =0;
        //只要不越界，就继续比较 并且当前位置的字符相等 就一直向后
        while (i<Math.min(s1.length(), s2.length())&&s1.charAt(i)==s2.charAt(i)){
            i++;
        }
        return s1.substring(0,i);
    }

    //解法2
    public String longestCommonPrefix2(String[] strs) {
        //统一比较
        if (strs.length == 0) return "";
        String ret = strs[0];
        for (int i = 0; i < strs[0].length(); i++) {
            char tmp = ret.charAt(i);
            //当前长度未越界 并且这一列的字符和ret的字符相同 则继续比较
            for (int j = 1; j< strs.length; j++) {
                if (i == strs[j].length() || strs[j].charAt(i) != tmp) {
                    return ret.substring(0, i);
                }
            }

        }
        return ret;
    }


}