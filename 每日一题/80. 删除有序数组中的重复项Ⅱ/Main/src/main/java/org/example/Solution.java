package org.example;
//给你一个有序数组 nums ，请你 原地 删除重复出现的元素，使得出现次数超过两次的元素只出现两次 ，返回删除后数组的新长度。
//不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
//输入：nums = [1,1,1,2,2,3]
//输出：5, nums = [1,1,2,2,3]
//解释：函数应返回新长度 length = 5, 并且原数组的前五个元素被修改为 1, 1, 2, 2, 3。
// 不需要考虑数组中超出新长度后面的元素。
//此题可以改为出现次数超过k次的问题 直接将slow-2改为slow-k
//并将指针的初始值改为k 判断条件改为<=k 即可
class Solution {
    public int removeDuplicates(int[] nums) {
        if(nums.length<=2) return  nums.length;
        //双指针直接从2位置开始即可
        int slow = 2, fast = 2;
        //fast表示遍历过的位置 slow表示处理过的长度
        //需要替换fast到slow位置 此时的slow位置为无效数据
        //如果间隔两位还相等 则直接跳过 一直找到不相同的位置 替换位置即可
        while(fast<nums.length){
            if(nums[slow-2]!=nums[fast]){
                nums[slow]=nums[fast];
                slow++;
            }
            fast++;
        }
        return slow;
    }
}