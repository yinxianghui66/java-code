import java.util.Stack;

//给你一个字符串数组 tokens ，表示一个根据 逆波兰表示法 表示的算术表达式。
//请你计算该表达式。返回一个表示表达式值的整数。
//输入：tokens = ["2","1","+","3","*"]
//输出：9
//解释：该算式转化为常见的中缀算术表达式为：((2 + 1) * 3) = 9
class Solution {
    public static  int evalRPN(String[] tokens) {
        //定义一个栈 遵循一个规则
        //遇到数字就加入栈 遇到操作符 就取出栈顶的两个元素 进行运算
        //将结果在加入栈 最后栈留下的即为结果
        Stack<Integer> stack  = new Stack<>();
        for(String s :tokens){
           //判断是否为运算符
            if(s.equals("+")){
                //为+ 则弹出两个元素 进行运算
                //注意- 和 / 需要特殊处理 因为第一个数是先进入栈的 所以计算是也应该是第一个数在前
                int tmp = stack.pop()+stack.pop();
                stack.push(tmp);
            }else if(s.equals("-")){
                int tmp = -stack.pop()+stack.pop();
                stack.push(tmp);
            }else if(s.equals("*")){
                int tmp = stack.pop()*stack.pop();
                stack.push(tmp);
            }else if(s.equals("/")){
                int tmp1= stack.pop();
                int tmp2= stack.pop();
                stack.push(tmp2/tmp1);
            }else {
                //为数字 加入栈
                stack.push(Integer.valueOf(s));
            }
        }
        //最后在栈中的元素 极为结果
        return stack.pop();
    }

}