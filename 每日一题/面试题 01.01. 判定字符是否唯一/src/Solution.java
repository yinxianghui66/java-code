//链接:https://leetcode.cn/problems/is-unique-lcci/
//使用位运算 把一个int类型 二进制位数为32位 每一位看作一个数组里的一位 里面 0 表示一个信息 1表示一个信息
//在此题中 用前 25个 二进制 第0位 表示 字符a是否出现 第1位表示 字符b是否出现(从后往前看 最后一位为 0 位置)
//所以我们只需计算出对应索引 判断这个位置是 0 还是 1 如果为1 说明重复 返回false
//如果为0 我们继续执行 将这个索引位置 改为1
//直到遍历完 如果没false 就返回 true
class Solution {
    public boolean isUnique(String astr) {
        //鸽巢思想 优化
        if(astr.length()>26) return false;

        int bitMap=0;
        for (int i = 0; i <astr.length(); i++) {
            //将每一位字符转成对应的索引 a 是 0 位置 b 是 1 位置
            int index=astr.charAt(i)-'a';
            //判断此位置的值是 0 还是 1 如果为 1 直接返回false
            if(((bitMap>>index)&1)==1) return false;
            //将对应位置变为 1
            bitMap |= 1<<index;
        }
        return true;
    }
}