//链接：https://leetcode.cn/problems/find-pivot-index/
//使用前缀和思想
//前缀和数组每个下标代表从 0 - i-1的位置的和
//后缀和数组每个下标代表从 i+1 - n-1 位置的和
//除去i本身的位置的值
//对比i位置的前缀和和后缀和如果一样代表当前位置为中心下标
//注意细节
//1.前缀和是从前向后初始化 后缀和是从后向前初始化
//2.f[0] 位置和 g[n-1] 位置 初始值为0(为最左边和为0 和最右边和为0)
public class Solution {
    public int pivotIndex(int[] nums) {
        int n=nums.length;
        int f[]=new int[n];
        int g[]=new int[n];
        //初始化前缀和数组(除去i本身 注意从1开始)
        for (int i = 1; i < n; i++) {
            f[i]=f[i-1]+nums[i-1];
        }
        //初始化后缀和数组  从后向前遍历
        for (int i = n-2; i >=0 ; i--) {
            g[i]=g[i+1]+nums[i+1];
        }
        for (int i = 0; i <n; i++) {
            if(f[i]==g[i]){
                return i;
            }
        }
        return -1;
    }
}
