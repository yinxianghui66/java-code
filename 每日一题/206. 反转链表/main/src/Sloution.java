//https://leetcode.cn/problems/reverse-linked-list/
//给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
class ListNode {
      int val;
      ListNode next;
     ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }

class Solution {
    public ListNode reverseList(ListNode head) {
        //使用双指针的思路 pre指向cur的前一位
        //然后把链表两个两个的反转指向
        ListNode cur = head;
        //注意这里的初始值为null 反转后 cur为尾节点 他的后一位 那不就是null吗
        ListNode pre = null;
        //注意这里是移动cur 和pre 所以cur在前 所以终止条件为cur ==null
        while(cur!=null){
            //提前存储下一位的位置 好让cur和pre后移
            ListNode ret = cur.next;
            //逆转位置 让他指向前面的节点
            cur.next = pre;
            //pre 和cur 的位置往后移一位
            pre = cur;
            cur = ret;
        }
        //最后返回头节点 此时链表已经反转了 所以 最后位置的节点既为头节点
        return pre;
    }
}