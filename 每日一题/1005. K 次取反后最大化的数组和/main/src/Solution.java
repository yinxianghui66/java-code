import java.util.Arrays;

//给你一个整数数组 nums 和一个整数 k ，按以下方法修改该数组：
//选择某个下标 i 并将 nums[i] 替换为 -nums[i] 。
//重复这个过程恰好 k 次。可以多次选择同一个下标 i 。
//以这种方式修改数组后，返回数组 可能的最大和
//输入：nums = [4,2,3], k = 1
//输出：5
//解释：选择下标 1 ，nums 变为 [4,-2,3] 。
class Solution {
    public static int largestSumAfterKNegations(int[] nums, int k) {
        int res =0;
        Arrays.sort(nums);
        int size =0;
        for (int i = 0;i<nums.length; i++) {
            if(nums[i]<0){
                size++;
            }else break;
        }
        //统计出负数的个数
        if(k<=size){
            //把前k个位置的负数都变成正数
            for (int i = 0; i <k; i++) {
                nums[i] = -nums[i];
            }
        }else {
            //把前k个位置的负数都变成正数
            for (int i = 0; i <size; i++) {
                nums[i] = -nums[i];
            }
            Arrays.sort(nums);
            k-=size;
            //此时剩余的k此 都为正数
            if(k%2==0){
                //如果k为偶数 则可以将正数变负变正 也就是无需操作
            }else{
                //如果k为奇数 则将该位置的值直接变成负数 这样损失小
                //因为执行到这里 剩下的位置 都是正数了 所以取反k次 和直接将第一位置为负数是一样的
                nums[0]  = -nums[0];
            }
        }

        for (int i = 0; i <nums.length; i++) {
            res+=nums[i];
        }
        return res;
    }

    public static void main(String[] args) {
        int[] num = {5,6,9,-3,3};
        System.out.println(largestSumAfterKNegations(num, 2));
    }
}