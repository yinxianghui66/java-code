//给定一个整数数组 nums，将数组中的元素向右轮转 k 个位置，其中 k 是非负数。
//输入: nums = [1,2,3,4,5,6,7], k = 3
//输出: [5,6,7,1,2,3,4]
//解释:
//向右轮转 1 步: [7,1,2,3,4,5,6]
//向右轮转 2 步: [6,7,1,2,3,4,5]
//向右轮转 3 步: [5,6,7,1,2,3,4]

public class Main {
    public void rotate1(int[] nums, int k) {
        //时间复杂度太高 直接超时了!!!!!
        //这个思路是一步一步的来的 一次轮转就要On2 所以时间复杂度是 On2k
        int  n =nums.length-1;
        int ret=0;
        if(n==0) return;
        for(;k>0;k--){
            ret=nums[n];
            for(int i=n;i>0;i--){
                nums[i]=nums[i-1];
                if(i-1==0){
                    nums[0]=ret;
                }
            }
        }
    }
    //使用翻转的思想 首先将整个数组进行翻转 拿上面的例子进行举例 既结果为
    //[7,6,5,4,3,2,1]  //此时 我们要翻转k个数组 那么将数组划分为两个区间 一个是 从 0 - k-1位置 另一个为k-n位置
    //[7,6,5] [4,3,2,1]
    //再将这两个区间翻转 既可以得到最终结果[5,6,7,1,2,3,4]
    public void rotate(int[] nums, int k) {
        int n = nums.length-1;
        //这里要对k进行取模 因为轮转一次和轮转三次结果是一样的
        k%=nums.length;
        //翻转
        reverse(nums,0,n);
        reverse(nums,0,k-1);
        reverse(nums,k,n);
    }
    //翻转从start到end位置的元素
    public void reverse(int[] nums,int start,int end){
        //对应位置进行一一替换 进行翻转
        while (start<end){
            int ret = nums[start];
            nums[start] = nums[end];
            nums[end] = ret;
            start++;
            end--;
        }
    }
}
