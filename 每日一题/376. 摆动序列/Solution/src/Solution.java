//如果连续数字之间的差严格地在正数和负数之间交替，则数字序列称为 摆动序列 。第一个差（如果存在的话）可能是正数或负数。
// 仅有一个元素或者含两个不等元素的序列也视作摆动序列。
//例如， [1, 7, 4, 9, 2, 5] 是一个 摆动序列 ，因为差值 (6, -3, 5, -7, 3) 是正负交替出现的。
//相反，[1, 4, 7, 2, 5] 和 [1, 7, 4, 5, 5] 不是摆动序列，第一个序列是因为它的前两个差值都是正数，第二个序列是因为它的最后一个差值为零。
//子序列 可以通过从原始序列中删除一些（也可以不删除）元素来获得，剩下的元素保持其原始顺序。
//给你一个整数数组 nums ，返回 nums 中作为 摆动序列 的 最长子序列的长度 。
//示例 1：
//输入：nums = [1,7,4,9,2,5]
//输出：6
//解释：整个序列均为摆动序列，各元素之间的差值为 (6, -3, 5, -7, 3) 。
//示例 2：
//输入：nums = [1,17,5,10,13,15,10,5,16, 8]
//输出：7
//解释：这个序列包含几个长度为 7 摆动序列。
//其中一个是 [1, 17, 10, 13, 10, 16, 8] ，各元素之间的差值为 (16, -7, 3, -3, 6, -8) 。
//示例 3：
//输入：nums = [1,2,3,4,5,6,7,8,9]
//输出：2
//https://leetcode.cn/problems/wiggle-subsequence/description/
class Solution {
    public int wiggleMaxLength(int[] nums) {
        //把数组元素画成图就很好想了 找到顶峰元素 删除路上的节点 最后判断这个位置是不是顶峰即可
        //这样遍历一次数组后 就可以得到所有的顶峰元素个数 并且是符合  - + || + - 条件的
        //初始化为0 是为了考虑最少需要有三个节点 所以模拟在最前面一个位置是平坡的元素
        int preDiff = 0;
        int curDiff=0;
        //默认最后一个节点是摆动
        int res = 1;
        //1.上下坡有平坡
        //2.首位元素的计算
        //3.单调平坡
        //去掉最后一位 因为已经记录
        for (int i = 0; i <nums.length-1; i++) {
            //遍历数组
            curDiff = nums[i+1]-nums[i];
            if((preDiff>=0&&curDiff<0)||(preDiff<=0&&curDiff>0)){
                //符合抖动条件 一正一负 或者一负一正
                //注意这里是有一侧需要包含= 因为第一个节点 要考虑平坡的情况 也就是 preDiff == curDiff 此时为平坡 所以不统计
                res++;
                //使用完后 因为向后遍历 所以下一个位置的preDiff == curDiff
                preDiff=curDiff;
            }
            //如果这个更新放在外面 当遇到单调平坡的时候 就会多统计一次 所以只需要坡度发生变化的时候更新就可以
            //每次从上坡->下坡 或者 下坡->到上坡时 记录初始位置的即可
            //preDiff=curDiff;
        }
        return res;
    }
}