//假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
//每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
//输入：n = 2
//输出：2
//解释：有两种方法可以爬到楼顶。
//1. 1 阶 + 1 阶
//2. 2 阶
class Solution {
    public static int climbStairs(int n) {
        if(n<=2) return n;
        //1.定义dp数组的每一位代表什么  dp[i] 代表 到第i阶楼梯 有i种方法
        int[] dp = new int[n+1];
        //2.定义状态方向 计算可以发现 每一位的方法数量 依赖于前两位 即dp[i] = dp[i-1]+dp[i-2];
        //3.初始话dp数组 这里dp[0] 是没有意义的 到第0 既原地不动 则为0中方法 题目中标志了n一定大于0
        //则初始化dp[1]和dp[2]
        dp[1] = 1;
        dp[2] = 2;
        //4.dp数组的遍历顺序 此题从后向前 因为后面的元素依赖前面
        for (int i = 3; i <=n; i++) {
            dp[i] = dp[i-1]+dp[i-2];
        }
        //5.打印dp数组 调试查看
        return dp[n];
    }

    public static void main(String[] args) {
        System.out.println(climbStairs(1));
    }
}