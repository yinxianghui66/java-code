import java.util.Random;
//给定整数数组 nums 和整数 k，请返回数组中第 k 个最大的元素。
//
//请注意，你需要找的是数组排序后的第 k 个最大的元素，而不是第 k 个不同的元素。
//
//你必须设计并实现时间复杂度为 O(n) 的算法解决此问题。

//快排+快排选择
//在快排的基础上加上选择 可以更快的找到结果
class Solution {
    public int findKthLargest(int[] nums, int k) {
        return qsort(nums,0,nums.length-1,k);
    }

    public int qsort(int[] nums,int l,int r,int k){
        if(l==r) return nums[l];
        //1.随机选择一个基准元素(在l - r区间)
        int key=nums[new Random().nextInt(r-l+1)+l];
        //2.根据基准元素 将数组分为三块
        int left=l-1,right=r+1,i=l;
        while(i<right){
            if(nums[i]<key) swap(nums,++left,i++);
            else if(nums[i]==key) i++;
            else swap(nums,--right,i);
        }
        //3.分类讨论
        //[l,left] [left+1,right-1] [right,r]
        int b=right-left-1,c=r-right+1;
        //此时第k的的元素一定不在 [right,r] 这个区间内
        if(c>=k) return qsort(nums,right,r,k);
        //此时这个区间所有元素都是第k大
        else if(b+c>=k) return key;
        //这里应该减去后面大的数的个数
        else return qsort(nums,l,left,k-b-c);
    }
    public void swap(int[] nums,int left,int right){
        int t=nums[left];
        nums[left]=nums[right];
        nums[right]=t;
    }
}