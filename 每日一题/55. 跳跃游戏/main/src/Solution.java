//给你一个非负整数数组 nums ，你最初位于数组的 第一个下标 。数组中的每个元素代表你在该位置可以跳跃的最大长度。
//判断你是否能够到达最后一个下标，如果可以，返回 true ；否则，返回 false 。
//输入：nums = [2,3,1,1,4]
//输出：true
//解释：可以先跳 1 步，从下标 0 到达下标 1, 然后再从下标 1 跳 3 步到达最后一个下标。
class Solution {
    public static boolean canJump(int[] nums) {
        if(nums.length==1) return true;
        //每次都跳最大值 判断距离末尾的距离 如果找到当前位置的值>距离末尾的距离 则可以
        //每次跳跃 取数组的最大值去跳 判断距离
//        int index =nums.length-1;
//        for (int i = 0; i <nums.length;) {
//            if(nums[i]==0) return false;
//            //跳到对应位置
//            if(nums[i]>=index){
//                return true;
//            }
//            i = nums[i];
//            index-=nums[i];
//        }
        //这里的思路 会固定取第一位的最大值来走 没有考虑到取小值的情况
//        int index =nums.length-1;
//        for (int i = 0; i <nums.length;) {
//            if(nums[i]==0) return false;
//            //从覆盖范围中 也就是可以跳到的范围中 找到一个最大值 进行跳跃
//            if(nums[i]>=index){
//                return true;
//            }
//            int max = getMax(nums,i,i+nums[i]);
//
//            index -= max;
//            i =max;
//
//        }
        //找遍历覆盖范围
        int cover = 0;
        if(nums.length==1) return true;

        for (int i = 0; i <=cover; i++) {
            //取最大的覆盖范围
            cover = Math.max(cover,i+nums[i]);
            //最大的覆盖范围覆盖到终点 则直接返回
            if(cover>=nums.length-1) return true;
        }
        return false;
    }

    public static int getMax(int[] num,int start,int end){
        int max=Integer.MIN_VALUE;
        for (int i = start; i <end; i++) {
            max=Math.max(max,num[i]);
        }
        return max;
    }


    public static void main(String[] args) {
        int[] s = {1,3,2};
        System.out.println(canJump(s));
    }
}