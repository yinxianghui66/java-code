//给你一个非负整数数组 nums ，你最初位于数组的 第一个下标 。数组中的每个元素代表你在该位置可以跳跃的最大长度
//判断你是否能够到达最后一个下标，如果可以，返回 true ；否则，返回 false
//输入:nums = [2,3,1,1,4]
//输出：true
//解释：可以先跳 1 步，从下标 0 到达下标 1, 然后再从下标 1 跳 3 步到达最后一个下标
class Solution {
    public boolean canJump(int[] nums) {
        //使用贪心算法 转换思想 不要考虑每一个走多少步 转换为最大跳越范围 只要最大跳越范围大于最后位置 则返回true
        int cur = 0;
        if(nums.length == 1) return true;
        for(int i= 0; i<=cur ;i++){  //注意这里的i应该小于cur 只能取最大范围内的步数
            cur = Math.max(i+nums[i],cur);  //取可得的最大范围
            if(cur>=nums.length-1) return true;
        }
        return false;
    }
}