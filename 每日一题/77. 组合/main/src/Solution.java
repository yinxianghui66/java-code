import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

//给定两个整数 n 和 k，返回范围 [1, n] 中所有可能的 k 个数的组合
//你可以按 任何顺序 返回答案。
class Solution {
    public List<List<Integer>> combine(int n, int k) {
        //使用回溯算法
        backtracking(n,k,1);
        return  res;
    }
    LinkedList<Integer> path = new LinkedList<>();
    List<List<Integer>> res = new ArrayList<>();
    public void backtracking(int n,int k,int startIndex){
        //1.确定终止条件 也就是 找到结果长度为目标长度时
        if(path.size() == k){
            //找到结果 存储一次 并结束递归
            res.add(new ArrayList<>(path));
            return;
        }
        //2.处理单层的处理逻辑
        //额外进行减枝判断 如果此时 后面的个数都加起来 还不能满足我的目标个数 则后面无需再往下处理
        //用目标个数 - 已经存储的个数 得到还需要的个数  注意需要+1 因为要包含起始位置
        for(int i=startIndex;i<=n-(k-path.size())+1;i++){
            //从传入的参数开始 存入结果
            path.add(i);

            //递归处理下一个位置 从下一个位置开始找
            backtracking(n,k,i+1);
            //3.回溯结果 因为这里不需要之前的数据 所以存储了一次后 需要将元素弹出 保证不会有多余元素
            path.removeLast();
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        //转换成字符数组
        char[] ss= s.toCharArray();
        int  x =0,y = 0;
        //搞一个指南针 每次操作 更改方向 根据不同的方向 进行不同的操作
        char direction = 'N';
        for (int i = 0; i <ss.length;i++) {
            //识别操作步骤
            if(ss[i] == 'W'){
                //需要先判断目前的方向
                if(direction == 'N'){
                    //目前方向是朝上
                    y++;
                }else if(direction == 'E'){
                    x++;
                }else if(direction == 'S'){
                    y--;
                }else {
                    x--;
                }
                direction = 'N';
            }else if (ss[i] == 'A'){
                //向左旋转90度
                if(direction=='N') direction = 'W';
                else if(direction =='S') direction = 'E';
                else if(direction =='E') direction = 'N';
                else  direction ='S';
            }else if (ss[i] == 'D'){
                //向右旋转90度
                if(direction=='N') direction = 'E';
                else if(direction =='S') direction = 'W';
                else if(direction =='E') direction = 'S';
                else  direction ='N';
            }
            //s 为在原地不动 不用处理
        }
        System.out.printf( x+" "+y);
    }
}