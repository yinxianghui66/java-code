import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i <n; i++) {
            arr[i] = sc.nextInt();
        }
        System.out.println(maxProduct(arr));
    }
    public static int maxProduct(int[] nums){
        int maxProduct = Integer.MIN_VALUE;
        int minProduct = Integer.MAX_VALUE;
        int curMax = 1;
        int curMin = 1;
        for (int num:nums){
            if(num==0){
                maxProduct = Math.max(maxProduct,0);
                curMax =1;
                curMin =1;
                continue;
            }
            int temp = curMax*num;
            curMax =Math.max(num,Math.max(temp,curMin*num));
            curMin =Math.min(num,Math.min(temp,curMin*num));
            maxProduct = Math.max(maxProduct,curMax);
        }
        return maxProduct;
    }
}