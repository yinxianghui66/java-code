import java.util.Arrays;
import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String player1 = scanner.nextLine().trim();
        String player2 = scanner.nextLine().trim();
        System.out.println(hasWinning(player1)?"Y":"N");
        System.out.println(hasWinning(player2)?"Y":"N");
        scanner.close();
    }

    private static boolean hasWinning(String cards) {
        int[] points = new int[cards.length()/2];
        int len2 =cards.length();
        for (int i = 0; i <cards.length()-1; i+=2) {
            points[i/2] = getCardValue(cards.substring(i,i+2));
        }
        //排序数组
        Arrays.sort(points);
        //动态规划
        int n =points.length;
        int[][] dp = new int[n][n];
        for (int i = 0; i <n; i++) {
            dp[i][i] = points[i];
        }
        for (int len = 2; len <=n ; len++) {
            for (int i = 0; i <=n-len; i++) {
                int j = i+len-1;
                dp[i][j] = Math.max(points[i]-dp[i+1][j],points[j]-dp[i][j-1]);
            }
        }
        return dp[0][n-1]>0;
    }



    private static int getCardValue(String card) {
        char rank = card.charAt(1);
        switch (rank){
            case '2':return 2;
            case '3':return 3;
            case '4':return 4;
            case '5':return 5;
            case '6':return 6;
            case '7':return 7;
            case '8':return 8;
            case '9':return 9;
            case 'T':return 10;
            case 'J':return 11;
            case 'Q':return 12;
            case 'K':return 13;
            case 'A':return 14;
            default: return 0;
        }
    }
}