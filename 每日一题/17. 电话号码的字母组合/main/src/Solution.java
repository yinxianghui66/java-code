import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

//给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。答案可以按 任意顺序 返回。
//给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。
//输入：digits = "23"
//输出：["ad","ae","af","bd","be","bf","cd","ce","cf"]
//组合问题 就用回溯
class Solution {
    public List<String> letterCombinations(String digits) {
        if(digits.length()==0||digits==null){
            return res;
        }
        //定义数组 对应各个位置的字符串
        String[] numString ={
                "",
                "",
                "abc",
                "def",
                "ghi",
                "jkl",
                "mno",
                "pqrs",
                "tuv",
                "wxyz",
        };
        backTracking(digits,numString,0);
        return res;
    }
    LinkedList<String> res = new LinkedList<>();
    StringBuffer s  = new StringBuffer();
    public  void  backTracking(String digits,String[] numString,int num){
        //1.确定终止条件
        if(num==digits.length()){
            res.add(s.toString());
            return;
        }
        //2.确定单层循环处理
        //找到输入的对应的元素字符串
        String str = numString[digits.charAt(num)-'0'];
        for (int i = 0; i <str.length() ; i++) {
            //每一位加入进来
            s.append(str.charAt(i));
            //注意这里是从num 的下一位搜索
            backTracking(digits,numString,num+1);
            //回溯 删除最后一位
            s.deleteCharAt(s.length()-1);
        }
    }



}