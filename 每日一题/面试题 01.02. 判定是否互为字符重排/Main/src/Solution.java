//给定两个由小写字母组成的字符串 s1 和 s2，请编写一个程序，确定其中一个字符串的字符重新排列后，能否变成另一个字符串
//链接: https://leetcode.cn/problems/check-permutation-lcci/description/
//使用哈希表 统计字符出现的个数 如果两个字符串的字符出现的个数相同，则它们是彼此的排列。
//在上面思想中优化 使用一个哈希表 统计s1字符出现的个数 然后遍历s2字符串 碰到的字符减去哈希表中该字符的计数 如果出现负数 则返回false 最后判断所有位置是不是 0 即可判断两个字符串是否是彼此的排列
class Solution {
    public boolean CheckPermutation(String s1, String s2) {
        //使用数组模拟哈希表
        int[] hash=new int[26];
        //使用一个哈希表 统计s1字符出现的个数
        //遍历第二个字符串时 碰到的字符 减去哈希表中该字符的计数
        //最后判断所有位置是不是 0 即可判断两个字符串是否是彼此的排列
        //如果遍历s2时 出现负数 说明出现额外的字符 直接返回false
        if(s1.length()!=s2.length()) return false;
        for(int i=0; i<s1.length();i++){
            hash[s1.charAt(i)-'a']++;
        }
        for(int i=0; i<s2.length();i++){
            hash[s2.charAt(i)-'a']--;
            if(hash[s2.charAt(i)-'a']<0) return false;
        }
        return true;
    }
}