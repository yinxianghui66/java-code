//给你一个字符串 s，由若干单词组成，单词前后用一些空格字符隔开。返回字符串中 最后一个 单词的长度。
//单词 是指仅由字母组成、不包含任何空格字符的最大子字符串

class Solution {
    public int lengthOfLastWord(String s) {
        int n = s.length()-1;
        int count=0,flag=0;
        //先去除最后的空格
        while(n>=0&&s.charAt(n)==' '){
            n--;
        }
        //然后第一次出现空格 说明单词结束  //犯的错误 没有判断n的终止条件 并且这个判断条件应该在判断之前！！
        while(n>=0&&s.charAt(n)!=' '){
            n--;
            count++;
        }

        return count;
    }
}