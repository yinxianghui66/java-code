//在股票交易中，如果前一天的股价高于后一天的股价，则可以认为存在一个「交易逆序对」。
//请设计一个程序，输入一段时间内的股票交易记录 record，返回其中存在的「交易逆序对」总数。
//使用归并排序 在排序的时候 统计逆序对的个数
class Solution {
    int[] tmp;
    public int reversePairs(int[] record) {
       tmp=new int[record.length];
       return mergeSort(record,0,record.length-1);
    }
    public int mergeSort(int[] nums,int left,int right){
        if(left>=right) return 0; //此时逆序对为0 递归结束
        //统计逆序对个数
        int ret=0;
        //1.寻找中间元素
        int mid=(left+right)/2;
        //2.排序区间 左边和右边的逆序对数量相加
        ret+=mergeSort(nums,left,mid);
        ret+=mergeSort(nums,mid+1,right);
        //3.寻找逆序对并排序 将数组分成两部分，左边部分为[left,mid]，右边部分为[mid+1,right]
        int cur1=left,cur2=mid+1,i=0;
        while(cur1<=mid&&cur2<=right){
            if(nums[cur1]<=nums[cur2]){
                tmp[i++]=nums[cur1++];
            }else {
                //统计逆序对数量并排序
                ret+=mid-cur1+1;
                tmp[i++]=nums[cur2++];
            }
        }
        //4.将剩余部分拷贝到临时数组中
        while(cur1<=mid) tmp[i++]=nums[cur1++];
        while(cur2<=right) tmp[i++]=nums[cur2++];
        //5.将临时数组拷贝到原数组中
        for(int j=left;j<=right;j++){
            nums[j]=tmp[j-left];
        }
        return ret;
    }
}