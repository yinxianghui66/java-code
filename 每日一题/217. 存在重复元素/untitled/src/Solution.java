//给你一个整数数组 nums 和一个整数 k ，判断数组中是否存在两个 不同的索引 i 和 j ，
// 满足 nums[i] == nums[j] 且 abs(i - j) <= k 。如果存在，返回 true ；否则，返回 false 。
//使用哈希表 存储数据 和该数据的下标 来判断是否存在重复的数
//固定一个数 去哈希表中找另一个数 如果没找到 则将这位的数据和下标加入哈希表
//如果找到了 则判断两个位置的差值是否小于等于k
//注意细节问题 如果有多个重复的数据 则只存储最近的一位下标即可
import java.util.HashMap;



class Solution {
    public boolean containsNearbyDuplicate(int[] nums, int k) {

        HashMap<Integer,Integer> hash= new HashMap<>();
        for(int i=0;i<nums.length;i++){
            if(hash.containsKey(nums[i])){
                //找到相同的数 判断你下表的差值是否小于等于k
                if(Math.abs(i-hash.get(nums[i]))<=k) return true;
            }
            //小细节 如果有多个相同的数 只需存储最近的一位的下标即可
            hash.put(nums[i],i);  //不存在相同的数 加入哈希表
        }
        return false;
    }
}
