//给你一个整数数组 nums 。如果任一值在数组中出现 至少两次 ，返回 true ；如果数组中每个元素互不相同，返回 false
//思路跟两数之和差不多 固定一个数 去前面找 也没有这个数 如果有 返回ture
//如果没有 就加入这个数 一直到遍历完
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

class Solution {

    //只需存储一个元素即可 无需存储下标
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> hash=new HashSet<>();
        for(int i:nums){
            if(hash.contains(i)) return true;
            hash.add(i);
        }
        return false;
    }
    //使用封装好的哈希表
    public boolean containsDuplicate2(int[] nums) {


        HashMap hashMap = new HashMap<>();
        for (int i = 0; i <nums.length; i++) {
            if(hashMap.containsKey(nums[i])) return true;
            else hashMap.put(nums[i], i);
        }
        return false;
    }
}