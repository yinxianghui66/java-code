//给你一个字符串 s ，请你反转字符串中 单词 的顺序。
//单词 是由非空格字符组成的字符串。s 中使用至少一个空格将字符串中的 单词 分隔开。
//返回 单词 顺序颠倒且 单词 之间用单个空格连接的结果字符串。
//注意：输入字符串 s中可能会存在前导空格、尾随空格或者单词间的多个空格。返回的结果字符串中，单词间应当仅用单个空格分隔，且不包含任何额外的空格
//输入：s = "the sky is blue"
//输出："blue is sky the"
//示例 2
//输入：s = "  hello world  "
//输出："world hello"
//解释：反转后的字符串中不能存在前导空格和尾随空格。
//示例 3
//输入：s = "a good   example"
//输出："example good a"
//解释：如果两个单词间有多余的空格，反转后的字符串需要将单词间的空格减少到仅有一个。
class Solution {
    //去除多余的空格后 对整个字符串反转 再对单个单词反转即可
    public String reverseWords(String s) {
        //先进行去空格的操作 使用快慢指针 覆盖字符数组
        //使用快慢指针 找到符合条件的元素
        char[] ss = s.toCharArray();
        int end =0;
        for(int fast = 0,slow = 0;fast<s.length();fast++){
            //快指针找符合条件的元素 慢指针 找符合放置的位置
            if(ss[fast]!=' ') {
                //只要不等于空 则为单词 赋值给对应的慢指针位置
                //并且每次slow位置不为第一位的时候 在对应位置前加个空格
                if (slow != 0) {
                    ss[slow++] = ' ';
                }
                //一直找到fast为 空的位置 退出 进行添加空格操作 一次while循环相当于
                //一直找到空格位置 也就是一个单词结束 一个单词结束 加上一个空格 除去第一个位置
                while (fast < s.length() && ss[fast] != ' ') {
                    ss[slow] = ss[fast];
                    slow++;
                    fast++;
                }
            }
            end = slow;
        }
        //最后 手动截取掉多余的后部分 最后slow位置即为有效的字符长度
        String res =  new String(ss).substring(0,end);
        //对整个字符串进行反转
        ss = res.toCharArray();
        ss = revese(new String(ss),0,ss.length-1);
        //再对每个单词进行反转
        for(int left =0,right = 0;right<ss.length;right++){
            if(ss[right]==' '){
                ss = revese(new String(ss),left,right-1);
                left=right+1;
            }
            if (right==ss.length-1) {
                //将最后一位的单词全部反转
                ss = revese(new String(ss),left,right);
            }
        }
        return new String(ss);
    }
    //去除无效空格后 对整体进行反转 再对每个单词进行反转即可
    public static char[] revese(String s,int left,int right){
        char[] ss = s.toCharArray();
        while(left<right){
            char tmp = ss[left];
            ss[left] = ss[right];
            ss[right] = tmp;
            left++;
            right--;
        }
        return ss;
    }
}

