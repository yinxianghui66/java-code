//在一条环路上有 n 个加油站，其中第 i 个加油站有汽油 gas[i] 升。
//你有一辆油箱容量无限的的汽车，从第 i 个加油站开往第 i+1 个加油站需要消耗汽油 cost[i] 升。你从其中的一个加油站出发，开始时油箱为空。
//给定两个整数数组 gas 和 cost ，如果你可以按顺序绕环路行驶一周，则返回出发时加油站的编号，否则返回 -1 。如果存在解，则 保证 它是 唯一 的。
//输入: gas = [1,2,3,4,5], cost = [3,4,5,1,2]
//输出: 3
//解释:
//从 3 号加油站(索引为 3 处)出发，可获得 4 升汽油。此时油箱有 = 0 + 4 = 4 升汽油
//开往 4 号加油站，此时油箱有 4 - 1 + 5 = 8 升汽油
//开往 0 号加油站，此时油箱有 8 - 2 + 1 = 7 升汽油
//开往 1 号加油站，此时油箱有 7 - 3 + 2 = 6 升汽油
//开往 2 号加油站，此时油箱有 6 - 4 + 3 = 5 升汽油
//开往 3 号加油站，你需要消耗 5 升汽油，正好足够你返回到 3 号加油站。
//因此，3 可为起始索引。
class Solution2 {
    public static int canCompleteCircuit(int[] gas, int[] cost) {
        int res = 0;
        int curSum =0;
        int totalSum =0;
        int[] result = new int[gas.length];
        for (int i = 0; i <gas.length; i++) {
            //取到达一个加油站 消耗和补给的差值
           result[i] = gas[i]-cost[i];
           //用来判断是否可以走一圈
           totalSum+=result[i];
        }
        //遍历这个数组 如果油为负数 则这个位置和这个位置之前的位置 都不可以作为起始位置了 因为没油了
        for (int i = 0; i < result.length; i++) {
            curSum+=result[i];
            if(curSum<0) {
                //如果油量为负 则该位置 包括之前的位置开始 都是不可以的 从下个位置开始 继续遍历
                curSum=0;
                res = i+1;
            }
        }
        if(totalSum < 0) return -1;
        return res;
    }

    public static void main(String[] args) {
        int[] gas={3,1,1};
        int[] cost = {1,2,2};
        System.out.println(canCompleteCircuit(gas, cost));
    }
}