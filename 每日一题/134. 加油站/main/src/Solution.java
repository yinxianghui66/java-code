//在一条环路上有 n 个加油站，其中第 i 个加油站有汽油 gas[i] 升
//你有一辆油箱容量无限的的汽车，从第 i 个加油站开往第 i+1 个加油站需要消耗汽油 cost[i] 升。你从其中的一个加油站出发，开始时油箱为空
//给定两个整数数组 gas 和 cost ，如果你可以按顺序绕环路行驶一周，则返回出发时加油站的编号，否则返回 -1 如果存在解 则 保证 它是 唯一的
//https://leetcode.cn/problems/gas-station/description/?envType=study-plan-v2&envId=top-interview-150
class Solution {
    public int canCompleteCircuit1(int[] gas, int[] cost) {
        //先模拟这个过程试试 挨个试一下
        int num = 0;
        for (int i = 0; i < gas.length; i++) {
            num+=gas[i]; //从该位置开始 获得这个位置的汽油
            //减去到达下一个加油站需要耗费的汽油
            num-=cost[i];
            if(num<0) continue;
            num+=gas[i+1]; //到达下个加油站 补充汽油
        }
        //想了一下 这样没法让他回到之前的加油站 也就是如果从3开始 如果到头了 再没办法回到0开始走了(其实可以用while)
        return num;
    }
    public int canCompleteCircuit(int[] gas, int[] cost) {
        //使用贪心的算法 从头开始 计算每个站点位置的剩余油量!
        int curSum = 0,stat=0,totalSum=0;
        for(int i=0;i<gas.length;i++){
            //统计这个位置的剩余油量
            curSum+=gas[i]-cost[i];
            totalSum+=gas[i]-cost[i];
            //如果剩余油量累加小于0 则从这个位置i+1开始尝试作为起点 说明这个位置之前的位置 都无法围绕一圈
            if(curSum<0) {
                //重新
                stat = i+1;
                //重置计算油量
                curSum=0;
            }
        }
        //如果总油耗大于补充的油量 则一定不能围绕一圈
        if(totalSum<0) return -1;
        return stat;
    }
}