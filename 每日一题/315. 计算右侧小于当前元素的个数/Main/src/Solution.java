import java.util.ArrayList;
import java.util.List;
//给你一个整数数组 nums ，按要求返回一个新数组 counts 。
// 数组 counts 有该性质： counts[i] 的值是  nums[i] 右侧小于 nums[i] 的元素的数量。
//跟逆序对一样，我们使用归并排序
// 此题的难点在于如何将原数组的元素和其对应的下标进行对应
//我们用两个数组 来分别记录原数组的元素和其对应的下标
class Solution {
    int[] ret;
    int[] index; //标记当前元素的原始下标
    int[] tmpIndex;
    int[] tmpNums;
    public List<Integer> countSmaller(int[] nums) {
        ret=new int[nums.length];
        index=new int[nums.length];
        tmpIndex=new int[nums.length];
        tmpNums=new int[nums.length];
        //初始化index数组
        for(int i=0;i<nums.length;i++){
            index[i]=i;
        }
        mergeSort(nums,0,nums.length-1);
        List<Integer> list=new ArrayList<Integer>();
        for(int i=0;i<ret.length;i++){
            list.add(ret[i]);
        }
        return list;
    }
    public void mergeSort(int[] nums,int left,int right){
        if(left>=right) return;
        //1.定义中间元素
        int mid=(left+right)/2;
        //2.递归左边和右边
        //将数组分为 [left,mid] [mid+1,right]
        mergeSort(nums,left,mid);
        mergeSort(nums,mid+1,right);
        //3.排序
        int cur1=left,cur2=mid+1,i=left;
        while(cur1<=mid&&cur2<=right){
            if(nums[cur1]<=nums[cur2]){
                tmpNums[i]=nums[cur1];
                tmpIndex[i]=index[cur1];
                //统计结果
                ret[index[cur1]]+=(cur2-mid-1);
                cur1++;
                i++;
            }else{
                tmpNums[i]=nums[cur2];
                tmpIndex[i]=index[cur2];
                cur2++;
                i++;
            }
        }
        //处理剩余的元素
        while(cur1<=mid){
            tmpNums[i]=nums[cur1];
            tmpIndex[i]=index[cur1];
            ret[index[cur1]]+=(cur2-mid-1);
            i++;
            cur1++;
        }
        while(cur2<=right){
            tmpNums[i]=nums[cur2];
            tmpIndex[i]=index[cur2];
            i++;
            cur2++;
        }
        //将排序后的数组复制回原数组
        for(int k=left;k<=right;k++){
            index[k]=tmpIndex[k];
            nums[k]=tmpNums[k];
        }
    }
}