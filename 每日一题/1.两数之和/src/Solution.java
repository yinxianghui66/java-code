import jdk.nashorn.internal.ir.IdentNode;

import java.util.HashMap;
import java.util.Map;

//https://leetcode.cn/problems/two-sum/description/
//力扣第一题
//给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。
//你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
//你可以按任意顺序返回答案
class Solution {
    //时间复杂度On2 太慢了。。
    //暴力枚举
    public int[] twoSum(int[] nums, int target) {
        int res[]=new int[2];
        for(int i=0;i<nums.length;i++){
            for(int  j=i+1;j<nums.length;j++){
                if(nums[i]+nums[j]==target){
                    res[0]=i;
                    res[1]=j;
                    return res;
                }
            }
        }
        return res;
    }
    //暴力枚举的优化版本 从后向前找数据相加
    public int[] twoSum2(int[] nums, int target) {
        int res[] = new int[2];
        int n = nums.length;
        for(int i=0;i<n;i++){
            for(int j=i-1;j>=0;j--) {
                if(nums[i]+nums[j]==target){
                    res[0]=j;
                    res[1]=i;
                    return res;
                }
            }
        }
        return res;
    }
    public int[] twoSum3(int[] nums, int target) {
        //int[] res =new int[2];
        //使用哈希表存储 元素和下标
        //遍历数组元素 x
        //target-x 如果哈希表中不存在 就存入这个
        //如果target-x 在哈希表中存在 就说明当前位置和哈希表中元素相加等于target
        int[] res = new int[2];
        Map<Integer,Integer> map=new HashMap<>();
        for (int i = 0; i <nums.length; i++) {
            if(map.containsKey(target-nums[i])){
               return new int[]{map.get(target-nums[i]),i};
//                res[0]=map.get(target-nums[i]);
//                res[1]=i;
//                return res;
            }
            map.put(nums[i],i);
        }
        return new int[0];
    }
}