//给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。
//注意：若 s 和 t 中每个字符出现的次数都相同，则称 s 和 t 互为字母异位词。
//https://leetcode.cn/problems/valid-anagram/description/
//使用数组哈希表来表示 0 -26个字母 遍历第一个字符串 给对应位置++
//遍历第二个字符串 给对应位置-- 最后判断数组值是不是都为0
class Solution {
    public boolean isAnagram(String s, String t) {
        int[] hash = new int[26];
        for(int i=0;i<s.length();i++){
            //统计出现的次数
            hash[s.charAt(i)-'a']++;
        }
        for(int i =0;i<t.length();i++){
            hash[t.charAt(i)-'a']--;
        }
        for(int i=0;i<hash.length;i++){
            if(hash[i]!=0) return false;
        }
        return true;
    }
}