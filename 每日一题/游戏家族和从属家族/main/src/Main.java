

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        List<Character> famliy = new ArrayList<>();
        String s =in.nextLine();
        for (int i = 0; i <s.length(); i++) {
            if(s.charAt(i)>='0'&&s.charAt(i)<='9'){
                famliy.add(s.charAt(i));
            }
        }
        if(famliy.size()==0){
            System.out.println(true);
        }
        System.out.println(isBalanced(famliy));
    }

    private static boolean isBalanced(List<Character> famliy) {
        if(checkBlance(famliy,0)==-1){
            return false;
        }else return true;
    }

    private static int checkBlance(List<Character> famliy, int index) {
        if(index>=famliy.size()||famliy.get(index)==null){
            //节点不存在
            return -1;
        }
        int leftHeight = checkBlance(famliy,2*index+1);
        int rightHeight = checkBlance(famliy,2*index+2);
        if(Math.abs(leftHeight-rightHeight)>1){
            return Integer.MAX_VALUE;
        }
        return Math.max(leftHeight,rightHeight)+1;
    }
}