import java.util.Stack;

class MyQueue {

    Stack<Integer> inStack;
    Stack<Integer> outStack;
    public MyQueue() {
        inStack = new Stack<>();
        outStack = new Stack<>();
    }

    public void push(int x) {
        //入队列
        inStack.push(x);
    }

    public int pop() {
        //出队列 把inStack的元素 全部输入到outStack 再出栈 outStack即可
        dumpstackIn();
        return outStack.pop();
    }

    public int peek() {
        //返回队列头部的元素
        dumpstackIn();
        return outStack.peek();
    }

    public boolean empty() {
        return inStack.isEmpty() && outStack.isEmpty();
    }
    private  void  dumpstackIn(){
        //只要出栈的队列不为空 说明outStack已经存好了数据 直接pop/peek就行
        if(!outStack.isEmpty()) return;
        while (!inStack.isEmpty()){
            outStack.push(inStack.pop());
        }
    }

    public static void main(String[] args) {
        MyQueue obj = new MyQueue();
        obj.push(1);
        int param_2 = obj.pop();
        boolean param_4 = obj.empty();
    }
}



