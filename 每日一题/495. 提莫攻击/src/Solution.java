//https://leetcode.cn/problems/teemo-attacking/description/
//使用模拟算法 这个题 画图找一下规律 可以得到 如果后一位-前一位的值大于 duration
//说明这个区间累计都会中毒 duration 秒 如果小于 duration 那就只会中毒 后一位-前一位的秒数
//最后记得 我们数组最后一位的中毒时间是没计算的 所以直接加上中毒时间 duration 即可
class Solution {
    public int findPoisonedDuration(int[] timeSeries, int duration) {
        int ret=0;
        for(int i=0;i<timeSeries.length-1;i++){
            if((timeSeries[i+1]-timeSeries[i])>=duration){
                ret+=duration;
            }else {
                ret+=timeSeries[i+1]-timeSeries[i];
            }
        }
        //最后一位中毒的秒数
        ret+=duration;
        return ret;
    }
}