//链接:https://leetcode.cn/problems/container-with-most-water/description/
//双指针  主要是总结规律 找到单调性!
class Solution {
    public int maxArea(int[] height) {
        //单调性  主要是观察单调性 发现
        //如果在一个区间中 left和right 较小的哪一个 和区间中的任意数枚举
        //得出的容积 一定是比left和right区间的容积小
        //所以我们可以直接舍弃较小的值的区间
        //计算多次区间 找到最大值
        int left=0;
        int right=height.length-1;
        int ret=0;
        while(left<right){
            int v=Math.min(height[left],height[right])*(right-left);
            ret=Math.max(ret,v);
            if(height[left] <= height[right]){
                left++;
            }else{
                right--;
            }
        }

        return ret;
    }
}