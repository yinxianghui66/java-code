import java.util.Stack;

//https://leetcode.cn/problems/valid-parentheses/
//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
//        有效字符串需满足：
//        左括号必须用相同类型的右括号闭合。
//        左括号必须以正确的顺序闭合。
//        每个右括号都有一个对应的相同类型的左括号。
class Solution {
    public boolean isValid(String s) {
        if(s.length()%2!=0){
            return false;
        }
        Stack<Character> stack=new Stack<>();
        for (int i = 0; i <s.length(); i++) {
            char c=s.charAt(i);
            //如果是左边符号 直接入栈
            if(c=='['||c=='{'||c=='('){
                stack.push(c);
            }else {
                //如果不是左边符号(当前符号为右边符号) 就出栈一个 跟三种符号匹配 如果匹配的
                //上 出栈一个 和当前未入栈的匹配 如果三种都不匹配 直接返回false
                //如果开始就是右边符号 则前面不可能有匹配的括号 直接返回false
                if(stack.isEmpty()){
                    return false;
                }
                char top=stack.pop();
                //三个判断 其实只会判断其中一个
                if(c==')'&&top!='(')
                    return false;
                if(c=='}'&&top!='{')
                    return false;
                if(c==']'&&top!='[')
                    return false;
            }
        }
        //如果全部匹配 此时栈为空 返回正确
        return stack.isEmpty();
    }
}