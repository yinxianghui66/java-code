import com.sun.org.apache.bcel.internal.generic.NEW;
import sun.reflect.generics.tree.Tree;

import java.util.*;

//给你二叉树的根节点 root ，返回它节点值的 前序 遍历。
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class Solution {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        tarversal(root,list);
        return list;
    }
    //其他的遍历都是一样的 取决于顺序
    public static void tarversal(TreeNode node,List<Integer> list){
        if(node==null) return;
        //存储节点元素
        list.add(node.val);
        //遍历左节点
        tarversal(node.left,list);
        //遍历右节点
        tarversal(node.right,list);
    }
    //使用迭代法来实现
    //使用栈来模拟递归的过程
    public List<Integer> preorderTraversal2(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        if(root==null) return list;
        Stack<TreeNode> stack = new Stack<>();
        //先加入跟结点
        stack.push(root);
        while (!stack.isEmpty()){
            TreeNode node = stack.pop();
            //如果该位置的节点为空 则直接结束这次循环 直到栈为空
            if(node!=null){
                list.add(node.val);
            }else {
                continue;
            }
            //加入栈的左结点和右节点
            //注意先加入右节点 因为出栈的顺序
            stack.push(node.right);
            stack.push(node.left);
            //后序遍历只需要调转一下入栈的顺序 再将结果集合反转即可得到
//            stack.push(node.left);
//            stack.push(node.right);
        }
//        Collections.reverse(list);
        return list;
    }

}