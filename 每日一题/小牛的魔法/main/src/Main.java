import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        //先保证 a切的每一段 都是最大的值 所以需要进行排序
        System.out.println(getMax(a, b));
    }
    public static int getMax(int a,int b){
        String strA = Integer.toString(a);
        char[] digits = strA.toCharArray();
        //排序
        for (int i = 0; i <digits.length-1 ; i++) {
            for (int j = i+1; j <digits.length ; j++) {
                if(digits[i]<digits[j]){
                    char tmp = digits[i];
                    digits[i] = digits[j];
                    digits[j] =tmp;
                }
            }
        }
        int sum = 0;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < digits.length; i+=b) {

            for (int j = i; j <Math.min(i+b,digits.length) ; j++) {
                sb.append(digits[i]);
            }
            sum+=Integer.parseInt(sb.toString());
        }
       if(b==1) return Integer.parseInt(sb.toString());
        return sum;
    }

}