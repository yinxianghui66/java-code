
//小A每天都要吃a,b两种面包各一个。而他有n个不同的面包机，不同面包机制作面包的时间各不相同。第i台面包机制作a面包需要花费ai的时间，
// 制作b面包则需要花费bi的时间。为能尽快吃到这两种面包，小A可以选择两个不同的面包机x,y同时工作，
// 并分别制作a,b两种面包，花费的时间将是max(ax,by)。当然，小A也可以选择其中一个面包机x制作a,b两种面包，
// 花费的时间将是ax+bx。为能尽快吃到面包，请你帮小A计算一下，至少需要花费多少时间才能完成这两种面包的制作。
//第一行一个正整数n，表示面包机的个数。
//
//第二行n个正整数ai，表示面包机制作面包a的时间。
//
//第三行n个正整数bi，表示面包机制作面包b的时间。

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int  res= 0;
        //处理输入
        int n = scanner.nextInt();
        int ai[] = new int[n];
        for (int i = 0; i <n; i++) {
            ai[0] = scanner.nextInt();
        }
        int bi[] = new int[n];
        for (int i = 0; i <n; i++) {
            bi[i] = scanner.nextInt();
        }
        int minTime = Integer.MAX_VALUE;
        //从ai 和 bi中 找出值最小的数 并且校验下标是否一致
        //一致的话则存储结果 再去找第二小的值 和ax+bx 比较 得出小的值返回
        //如果不一致 则直接返回结果 min(ax,by)

        //下面是暴力 遍历全部的两种结果 取最小值
        for (int i = 0; i <n; i++) {
            for(int j = 0;j<n;j++){
                //第一种情况 全都遍历一遍
                minTime = Math.min(minTime,Math.max(ai[i],bi[i]));
            }
        }
        //遍历
        for (int i = 0; i <n; i++) {
            minTime = Math.min(minTime,ai[i]+bi[i]);
        }
        System.out.println(minTime);
    }
}

