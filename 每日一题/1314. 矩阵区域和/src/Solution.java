//链接:https://leetcode.cn/problems/matrix-block-sum/
//二维前缀和矩阵 需要额外注意两个细节
//矩阵的右下角存储的是整个矩阵的和
//如果要拓展出 要计算出对应矩阵的左上角下标 和右下角下标 使用二维前缀和 就可以求出整个矩阵的和
//1.计算对应矩阵的下标
//2.初始数组和返回的数组下标都是从 0 开始 dp数组是从 1 开始 需要对应映射下标位置
class Solution {
    public int[][] matrixBlockSum(int[][] mat, int k) {
        //1.初始化前缀和矩阵
        int m=mat.length,n=mat[0].length;
        int [][] dp=new int[m+1][n+1];  //这里 +1 是为了处理边界情况
        for (int i = 1; i <=m; i++) {  //从1开始赋值 为了防止越界
            for (int j = 1; j <=n; j++) {
                dp[i][j]=dp[i][j-1]+dp[i-1][j]-dp[i-1][j-1]+mat[i-1][j-1];  //正常这里的mat应该是 i j 但是因为mat数组是从
            }                                                               //0开始赋值的 为了对应到mat数组的位置 将下标-1
        }
        //2.使用前缀和矩阵
        int [][] answer=new int[m][n];
        for (int i = 0; i <m; i++) {
            for (int j = 0; j <n; j++) {
                //计算拓展出的对应下标
                int x1= Math.max(0,i-k)+1,y1=Math.max(0,j-k)+1;  //这里的计算下标的公式 推导 因为不能越界 所以如果越界 就将位置缩到
                int x2= Math.min(m-1,i+k)+1,y2=Math.min(n-1,j+k)+1;//0,0 位置 或者 m-1 n-1 位置
                //并且因为我们新建的数组是从0 0 开始赋值的 所以对应到dp的位置 每一位都应该+1
                answer[i][j]=dp[x2][y2]-dp[x1-1][y2]-dp[x2][y1-1]+dp[x1-1][y1-1];
            }
        }
        return answer;
    }
}