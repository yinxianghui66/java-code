import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//给你一个由 n 个整数组成的数组 nums ，和一个目标值 target
// 请你找出并返回满足下述全部条件且不重复的四元组
// [nums[a], nums[b], nums[c], nums[d]]
// （若两个四元组元素一一对应，则认为两个四元组重复）
public class Solution {
    public List<List<Integer>> fourSum(int[] nums, int target) {
        //在三数之和的基础上 再加一层 进行控制
        List<List<Integer>> res = new ArrayList<>();
        //对数组进行排序
        Arrays.sort(nums);
        //再套一层k并且进行去重和剪枝操作
        for(int k = 0;k<nums.length;k++){
            //进行减枝 但是要保证是正数的情况 因为目标target
            if(nums[k]>target&&nums[k]>0){
                return res;
            }
            //对k进行去重
            if(k>0&&nums[k]==nums[k-1]) continue;
            for(int i= k+1;i<nums.length;i++){
                //进行二次剪枝
                if(nums[i]+nums[k]>target&&nums[i]+nums[k]>0){
                    break;
                }
                //进行去重
                if(i>k+1&&nums[i]==nums[i-1]) continue;
                //定义双指针
                int left = i+1;
                int right = nums.length-1;
                while (left<right){
                    int sum = nums[k]+nums[i]+nums[left]+nums[right];
                    if(sum<target){
                        left++;
                    }else if(sum>target){
                        right--;
                    }else {
                        //找到结果 进行存储
                        res.add(Arrays.asList(nums[k],nums[i],nums[left],nums[right]));
                        //对left和right进行去重
                        while (left<right&&nums[left]==nums[left+1]) left++;
                        while (left<right&&nums[right]==nums[right-1]) right--;
                        //操作完后 各进一步
                        left++;
                        right--;
                    }
                }
            }
        }
        return res;
    }
}

