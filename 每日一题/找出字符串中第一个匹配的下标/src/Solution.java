class Solution {
    public static int strStr(String haystack, String needle) {
        if(needle.length()>haystack.length()){
            return -1;
        }
        char ret=needle.charAt(0);
        //找到第一个出现needle的位置
        //找到后 从i截取neele长度的字符串 和neele对比 如果相同 返回当前i
        //如果不相同 继续往后遍历
        for(int i=0;i<haystack.length();i++){
            //找到第一次出现的位置
            if(haystack.charAt(i)==ret){
                //防止i+needle.length大于haystack的长度 导致越界
                if(i+needle.length()>haystack.length()){
                    return -1;
                }
                //截取从第一个字符 到needle.length长度的字符串
                String sub=haystack.substring(i,i+needle.length());
                //如果相同 直接返回下标
                if(sub.equals(needle)){
                    return i;
                }
            }
        }
        return -1;

    }
    public static void main(String[] args) {
        System.out.println(strStr("sadbutsad", "sad"));
    }
}