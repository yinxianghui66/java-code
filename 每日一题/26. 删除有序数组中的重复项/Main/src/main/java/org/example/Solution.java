package org.example;
//给你一个 非严格递增排列 的数组 nums ，请你 原地 删除重复出现的元素，使每个元素 只出现一次 ，返回删除后数组的新长度。元素的 相对顺序 应该保持 一致 。然后返回 nums 中唯一元素的个数。
//考虑 nums 的唯一元素的数量为 k 你需要做以下事情确保你的题解可以被通过：
//更改数组 nums ，使 nums 的前 k 个元素包含唯一元素，并按照它们最初在 nums 中出现的顺序排列。nums 的其余元素与 nums 的大小不重要。
//返回 k
//输入：nums = [1,1,2]
//输出：2, nums = [1,2,]
//解释：函数应该返回新的长度 2 ，并且原数组 nums 的前两个元素被修改为 1, 2 不需要考虑数组中超出新长度后面的元素
class Solution {
    public int removeDuplicates(int[] nums) {
        //使用双指针即可 找到不重复的位置和一定重复的位置交换
        int left = 0 , right = 0;
        while(right<nums.length){
            if(nums[left]==nums[right]) right++;
            else{
                //此时说明right不重复了
                //left+1位置一定是无用的位置 也就是right走过的位置
                nums[left+1] = nums[right];
                left++;
                right++;
            }
        }
        return left+1;
    }
}