import java.util.*;


public class Solution {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param n int整型 
     * @return int整型
     */
    public int bitwiseComplement (int n) {
        // write code here
        String binaryStr = Integer.toBinaryString(n);
        //转换为字符串 方便遍历
        StringBuffer complementStr = new StringBuffer();
        for(char c:binaryStr.toCharArray()){
            //处理反码
            if(c=='1'){
                complementStr.append('0');
            }else {
                complementStr.append('1');
            }
        }
        //返回十进制的数
        return Integer.parseInt(complementStr.toString(),2);
    }
}
