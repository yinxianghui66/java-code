//链接:https://leetcode.cn/problems/single-number-ii/
//位运算 因为只有一位出现一次 其他数字出现三次 所以 将这些数的每一位(32位)
//统计每一位 出现1的次数 %3 如果为1 说明这一位是出现一次的数字的结果(最好画图看一下) 将这一位设置成1 设置32位后 即为结果
class Solution {
    public int singleNumber(int[] nums) {
        int ans=0;
        for (int i = 0; i <32; i++) {
            int sum=0;
            for (int x:nums){  //遍历数数组 统计这一比特为出现1的 次数
                if(((x>>i)&1)==1) sum++;
            }
            if(sum%3==1) { //说明这一位是出现一次的结果
                ans |= 1<<i;  //将这一位设置成 1
            }
        }
        return ans;
    }
}