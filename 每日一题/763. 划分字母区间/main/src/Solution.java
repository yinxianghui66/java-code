import java.util.ArrayList;
import java.util.List;
//给你一个字符串 s 。我们要把这个字符串划分为尽可能多的片段，同一字母最多出现在一个片段中。
//注意，划分结果需要满足：将所有划分结果按顺序连接，得到的字符串仍然是 s 。
//返回一个表示每个字符串片段的长度的列表。
class Solution {
    public List<Integer> partitionLabels(String s) {
        //使用hash统计 每个字符 出现在字符串的最远位置
        //然后使用区间来判断 每个位置的最远位置
        int[] hash = new int[27];
        for (int i = 0; i <s.length(); i++) {
            //此处 因为不断遍历 所以 最后元素的值 一定是最远位置
            hash[s.charAt(i)-'a'] =i;
        }
        //
        List<Integer> res = new ArrayList<>();
        int left=0,right=0;
        for (int i = 0; i <s.length();i++) {
            //每次 取最远出现的距离
            right = Math.max(right,hash[s.charAt(i)-'a']);
            if(right == i){ //当前位置遍历到最远位置 则这个区间为一个合法区间
                res.add(right-left+1);
                //更新此时的左区间 为当前位置的下一位
                left=i+1;
            }
        }
        return res;
    }
}