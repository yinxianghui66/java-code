//链接:https://leetcode.cn/problems/peak-index-in-a-mountain-array/
//典型的二分 只要分析出二段性 总结出 必要条件 arr[mid] > arr[mid-1]
//如果上述成立 则表示当前区间合法 更新left=mid
//如果< 说明在右边 更新right=mid-1
public class Solution {
    public int peakIndexInMountainArray(int[] arr) {
        int left=0,right=arr.length-1;
        while (left<right){
            int mid=left+(right-left+1)/2;
            if(arr[mid]>arr[mid-1])  left=mid;
            else right=mid-1;
        }
        return left;
    }
}
