import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        int n = scanner.nextInt();
        int k =scanner.nextInt();
        scanner.nextLine();
        String s = scanner.nextLine();
        char[] chars = s.toCharArray();
        for (int i = 0; i <chars.length; i++) {
            if(chars[i]=='1'){
                --k;
                chars[i]='0';
                if(k==0){
                    break;
                }
            }
        }
        if(k>0){
            if(k%2==1){
                chars[chars.length-1] ='1';
            }
        }
        System.out.println(new String(chars));
    }
}