//链接:https://leetcode.cn/problems/3sum/description/
//假设数组长度为 n
//使用双指针的思想 主要思路是 固定一个数 a(下标s)  在 s+1 - n-1这个区间内 找到值为 -a的两个数 三个数组合起来就为0
//在s+1 --  n-1这个区间 使用双指针算法 找到两个数
//两个重点
//1.去重 对于重复数字 结果也是相同的 对 nums[left] 和 nums[right]的前一个数和后一个数 如果相同的话 直接跳到下一个位置 (保证不会越界)的情况下
//还需要对 s 位置进行去重 如果前一个位置和现在的位置 大小相同 直接停止这次循环 到下个位置继续 (保证不会越界)
//2.继续: 找到一对数 不用停下 要找到所有符合条件的数
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution {
    public static List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> ret=new ArrayList<>();
        //先排序
        Arrays.sort(nums);
        int n=nums.length;
        //使用双指针算法
        for(int i=0;i<n;){
            //当第一个固定的数为正数时 后面两个数相加需要为负数才
            //可能符合 因为已经排序了 所以后面不可能有负数了 直接跳出
            if(nums[i]>0){
                break;
            }
            int left=i+1;;
            int right=n-1;
            int target=-nums[i];
            while(left<right){
                int sum=nums[left]+nums[right];
                if(sum<target){
                    left++;
                }else if(sum>target){
                    right--;
                }else{
                    //找到一种之后 继续找 找到所有情况
                    ret.add(new ArrayList<>(Arrays.asList(nums[i],nums[left],nums[right])));
                    //缩小区间 继续寻找
                    left++;
                    right--;
                    //对区间内元素 去重操作  left<right 保证不会越界
                    while(nums[left]==nums[left-1]&&left<right){
                        left++;
                    }
                    while(nums[right]==nums[right+1]&&left<right){
                        right--;
                    }
                }
            }
            i++;
            //对固定数 a 进行去重操作  注意 使用while  !!! 不要再用if了!! 多次去重啊
            while (i<n&&nums[i]==nums[i-1]){
                i++;
            }
        }
        return ret;
    }

}