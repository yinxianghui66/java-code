class Solution {
    public static String longestCommonPrefix(String[] strs) {
        if(strs.length==0){
            return "";
        }
        //设置第一位为最短公共前缀
        String res=strs[0];
        //遍历对比
        for (int i = 0; i <strs.length; i++) {
            //如果当前字符串中不存在res这个最短前缀 就一次-1长度 直到最短
            //因为是公共前缀 所以 只要查找位置不是0（你也找不出来其他位置） 就说明字符串中不包含这个公共前缀
            //我们就减一长度 再次对比
            while (strs[i].indexOf(res)!=0){
                //如果减完了还没有 就返回""
                if(res.length()==0){
                    return "";
                }
                //
                res=res.substring(0,res.length()-1);
            }
        }
        return res;
    }

    public static void main(String[] args) {
        String num[]={"flower","flow","flight"};
        System.out.println(longestCommonPrefix(num));
    }
}
