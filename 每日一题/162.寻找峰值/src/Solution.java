//链接:https://leetcode.cn/problems/find-peak-element/！
//二分查找 找到关系 关键信息 假设 nums[-1] = nums[n] = -∞
//并且 nums[i] != nums[i + 1]  不可能存在两个相同的相邻元素
//说明数组两边都是负无穷  如果后一位直大于前一位 说明是由升序  也就是从小到大 那么右边的区间一定有一个峰值(因为nums[n] = -∞) 所以left=mid+1
//如果后一位直小于前一位 说明是降序 也就是从大到小 那么右边一定是没有峰值的(因为nums[n] = -∞) 峰值在左边区间 所以right=mid  //不确定当前位置是否是峰值
//最后返回结果

public class Solution {
    public int findPeakElement(int[] nums) {
        int left=0,right=nums.length-1;
        while (left<right){
            int mid=left+(right-left)/2;
            if(nums[mid]<nums[mid+1]) left=mid+1;
            else right=mid;
        }
        return left;
    }
}
