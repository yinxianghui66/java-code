//给你两个 非空 的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，并且每个节点只能存储 一位 数字。
//请你将两个数相加，并以相同形式返回一个表示和的链表。
//你可以假设除了数字 0 之外，这两个数都不会以 0 开头
//链接:https://leetcode.cn/problems/add-two-numbers/submissions/494782030/
//链表题目 使用模拟的思想 链表每一位相加
//使用t来模拟进位 每次取t的个位数 如果有进位 t/10 去掉这次结果 下次相加时 十位就变成个位 再加上这里的数 完成进位
  class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 }

 public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        //定义双指针 依次相加
        ListNode cur1 = l1, cur2 = l2;
        //创建虚拟头节点 方便操作
        ListNode newHead = new ListNode();
        //记录尾节点
        ListNode prev=newHead;
        //记录进位
        int t = 0;
        while (cur1 != null || cur2 != null || t != 0) {
            if (cur1 != null) {
                t += cur1.val;
                cur1 = cur1.next;
            }
            if (cur2 != null) {
                t += cur2.val;
                cur2 = cur2.next;
            }
            //尾插计算结果
            //取t的个位数为结果，并将其添加到链表中
            prev.next=new ListNode(t%10);
            prev=prev.next;
            //每次加完 除去上一次的个位结果
            t/=10;
        }
            //返回头结点 也就是虚拟头结点的下一位
            return newHead.next;
        }
}
