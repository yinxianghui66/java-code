
//给你一棵二叉树的根节点 root ，翻转这棵二叉树，并返回其根节点。
class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode() {}
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
         this.right = right;
      }
  }

class Solution {
    public TreeNode invertTree(TreeNode root) {
        //递归处理交换两个节点
        if(root==null) return null;
        //交换节点的左右孩子
        swapChildren(root);
        //递归处理左节点和右节点
        invertTree(root.left);
        invertTree(root.right);
        return root;
    }
    public static  void swapChildren(TreeNode node){
        //交换节点的左右孩子
        TreeNode ret = node.left;
        node.left = node.right;
        node.right = ret;
    }
}