//https://leetcode.cn/problems/search-insert-position/description/
//分析题目 可以得出 找出有效区间的左端点即可
//使用左端点模板 并对特殊情况进行处理
class Solution {
    public static int searchInsert(int[] nums, int target) {
        //使用二分查找的方法 稍作修改
        //这个是朴素二分查找
//        int left=0;
//        int right=nums.length-1;
//        //默认找不到的情况 插入位置为最后一位
//        int ans=nums.length;
//        while (left<=right){
//            int mid=(right-left)/2+left;
//            if(nums[mid]<target){
//                left=mid+1;
//            }else if(nums[mid]>=target){
//                //不断更新ans下标
//                ans=mid;
//                right=mid-1;
//            }
//        }
//        return ans;
        int left=0,right=nums.length-1;
        while (left<right){
            int mid=left+(right-left)/2;
            if(nums[mid]<target){
                left=mid+1;
            }else {
                //当大于等于目标值为 为有效区间
                right=mid;
            }
        }
        //需要针对第三种情况特殊判断  (此时left和right已经相遇)
        if(nums[left]<target){
            return left+1;
        }
        return left;
    }


}
