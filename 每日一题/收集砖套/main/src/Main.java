import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int T = in.nextInt();
        for (int i = 0; i <T; i++) {
            int a = in.nextInt(); //红砖数
            int b =in.nextInt();
            int c = in.nextInt();
            int x =in.nextInt();
            int y = in.nextInt();
            int max = 0;
            for(int redToBlue =0;redToBlue<=a/x;redToBlue++){
                int remainingRed = a-redToBlue*x;
                int remainingBlue = b+redToBlue;
                int blueToGreen = Math.min(remainingBlue,c/y);
                int sets = Math.min(remainingRed,Math.min(remainingBlue-blueToGreen,c-blueToGreen*y));
                max = Math.max(max,sets);
            }
            System.out.println(max);
        }

    }
}