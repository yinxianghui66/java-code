//链接:https://leetcode.cn/problems/binary-search
//使用二分查找的算法 利用数组是有序的  这样我们取一个中间数 和目标值对比 就可以一下排除一半不符合条件的数据
//需要注意的几个点
//1.跳出循环的条件 是left<=right  当这个区间只剩下一个数 也是需要判断的
//2.计算中间值时  为了防止数据量很大 right+left超过Integer的数据范围  使用(right-left)/2+left 减法 防止数据溢出
//3.时间复杂度 logN  非常快
//4.为什么是正确的?  因为他和暴力解法一样 排除了所有不可能的数据  一次可以排除一般的无效数据
//写之前要定义好区间 这里是左闭由闭的区间
class Solution {
    public int search(int[] nums, int target) {
        int left = 0 ;
        int right = nums.length-1;
        //因为是 左闭右闭 所以 left == right 也是合法的区间
        while(left<=right){
            //取中间值
            int mid = left+(right-left) /2;
            //定义左闭右闭的区间
            if(nums[mid]>target){
                //mid位置的值已经明确不为目标值 所以下一次不需要包含mid本身
                right = mid-1;
            }else if (nums[mid]<target){
                //mid位置的值已经明确不为目标值 所以下一次不需要包含mid本身
                left = mid+1;
            }else{
                return mid;
            }
        }
        return -1;
    }
}