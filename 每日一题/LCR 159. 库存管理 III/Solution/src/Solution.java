import java.util.Random;
//仓库管理员以数组 stock 形式记录商品库存表，其中 stock[i] 表示对应商品库存余量。请返回库存余量最少的 cnt 个商品余量，返回 顺序不限
//链接:https://leetcode.cn/problems/zui-xiao-de-kge-shu-lcof/description/
//topk问题 使用快速选择算法
//        输入：stock = [2,5,7,4], cnt = 1
//        输出：[2]
//        输入：stock = [0,2,3,6], cnt = 2
//        输出：[0,2] 或 [2,0]
//生成随机基准元素 将数组分为三份
//进行分类讨论
public class Solution {
    public int[] inventoryManagement(int[] nums, int k) {
        qsort(nums,0,nums.length-1,k);
        int[] ret=new int[k];
        for(int i=0;i<k;i++){
            ret[i]=nums[i];
        }
        return  ret;
    }
    public void qsort(int[] nums,int l,int r,int k){
        //1.生成随机基准元素
        int key=nums[new Random().nextInt(r-l+1)+l];
        //2.将数组分为三份
        int left=l-1,right=r+1,i=l;
        while(i<right){
            if(nums[i]<key) swap(nums,++left,i++);
            else  if(nums[i]==key) i++;
            else  swap(nums,--right,i);
        }
        //3.分类讨论
        int a=left-l+1,b=right-left-1;
        if(a>k) qsort(nums,l,left,k);
        else if(a+b>=k) return;
        else  qsort(nums,right,r,k-a-b);
    }
    public void swap(int[] nums,int i,int j){
        int ret=nums[i];
        nums[i]=nums[j];
        nums[j]=ret;
    }
}
