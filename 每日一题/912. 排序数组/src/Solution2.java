//给你一个整数数组 nums，请你将该数组升序排列。
//将数组分成两份 去左边排下序 右边排下序 然后合并两个数组
//归并排序  递归处理
class Solution2 {
    int tmp[];
    public int[] sortArray(int[] nums) {
        tmp=new int[nums.length];
        mgrgeSort(nums, 0, nums.length - 1);
        return nums;
    }
    public void mgrgeSort(int[] nums, int left, int right) {
        if(left>=right) return;
        //1.根据中间点 划分区间
        int mid=(left+right)/2;
        //[left,mid] [mid+1,right]
        //2.把左右区间排序
        mgrgeSort(nums,left,mid);
        mgrgeSort(nums,mid+1,right);
        //3.合并两个有序数组
        int cur1=left,cur2=mid+1,i=0;
        while (cur1<=mid&&cur2<=right){
            tmp[i++]=nums[cur1]<=nums[cur2]?nums[cur1++]:nums[cur2++];
        }
        //处理完没有遍历完的数组
        while(cur1<=mid) tmp[i++]=nums[cur1++];
        while(cur2<=right) tmp[i++]=nums[cur2++];
        //4.数据还原到nums中
        for (int j = left; j <=right ; j++) {
            nums[j]=tmp[j-left];
        }
    }
}