//链接: https://leetcode.cn/problems/sort-an-array/
//也是采用快排的思想 这里我们使用三段分法 根据75 颜色分类问题差不多
//将数组分为三份 第一份全是小于当前值的 第二份全是等于当前值的 第三份全是大于当前值的
//我们使用随机生成的数组 优化算法
//将数组分为三份后 再根据每一份进行左边和右边的递归再次分组 直到所有数据排序完成
import java.util.Random;
class Solution {
    public static int[] sortArray(int[] nums) {
        //先根据基准元素分三份 随机生成基准数
        qsort(nums,0,nums.length-1);
        return nums;
    }
    public static void qsort(int[] nums, int l, int r){
        //注意这里的终止条件 当右指针大于等于左指针时 区间重合 排序完成
        if(l>=r) return;
        //生成随机位置的元素 进行优化
        int key=nums[new Random().nextInt(r-l+1)+l];  //注意这里的公式是 右坐标-左坐标 +1 表示当前长度 +左下标为随机位置的下标
        int i=l,left=l-1,right=r+1;
        while(i<right){
            if(nums[i]<key){
                swap(nums,++left,i++);
            }else if(nums[i]>key){
                swap(nums,--right,i);
            }else{
                i++;
            }
        }
        //递归执行左边和右边区域
        qsort(nums,l,left);
        qsort(nums,right,r);

    }
    public static void swap(int[] nums, int i, int j){
        int ret=nums[i];
        nums[i]=nums[j];
        nums[j]=ret;
    }

}
