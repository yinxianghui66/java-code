import com.sun.org.apache.bcel.internal.generic.NEW;
import sun.reflect.generics.tree.Tree;

import java.util.*;

//给你二叉树的根节点 root ，返回它节点值的 中序 遍历。
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        tarversal(root,list);
        return list;
    }
    //其他的遍历都是一样的 取决于顺序
    public static void tarversal(TreeNode node,List<Integer> list){
        if(node==null) return;
        //遍历左节点
        tarversal(node.left,list);
        //存储节点元素
        list.add(node.val);
        //遍历右节点
        tarversal(node.right,list);
    }
    //使用迭代法来实现
    //使用栈来模拟递归的过程
    //需要一路走到左节点 的头 和前序和后序不太一样
    public List<Integer> inorderTraversal2(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        while (!stack.isEmpty()||cur!=null){
            //先去处理左节点 如果为空 则存储结果 并且看右节点
            if(cur!=null){
                //一直向左遍历直到cur为空 用栈存储遍历过的元素
                stack.push(cur);
                cur = cur.left;
            }else {
                //当节点为空的时候 存储结果元素(上一个不为空的元素的左节点) 然后判断 并且遍历上一个不为空的位置的右边节点
                //节点为空 弹出元素 存储结果
                cur = stack.pop();
                list.add(cur.val);
                //遍历右孩子
                cur = cur.right;
            }
        }
        return list;
    }

}