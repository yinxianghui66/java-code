//https://leetcode.cn/problems/que-shi-de-shu-zi-lcof/description/
//五种解法  前四种复杂度都为ON
//1. 哈希表
//2. 暴力循环
//3. 异或
//4. 数学方法(高斯求和)
//5. 二分查找
// 找到二段性  是下标和位置大小的二段性  如果下标和当前位置相同 说明左边没有确实的数据 left=mid+1
//如果不相同 说明结果就在这个下标的左边 包含其本身  right=mid
//最后处理边界情况 如果缺的是最后一位 需要特殊处理 返回left+1
public class Solution {
    public int takeAttendance(int[] records) {
        int left=0,right=records.length-1;
        while (left<right){
            int mid=left+(right-left)/2;
            if(records[mid]==mid){
                left=mid+1;
            }else{
                right=mid;
            }
        }
        if(records[left]==left){  //说明是边界情况 返回的应该是left+1
            return left+1;
        }
        return left;
    }
}
