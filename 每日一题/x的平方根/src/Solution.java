//链接：https://leetcode.cn/problems/sqrtx/
//典型的二分查找类型
//求中间值的平方 判断位置与x关系
//注意处理x<1 时 二分不成立 直接返回0即可
public class Solution {
    public int mySqrt(int x) {
        //处理边界情况
        if(x<1){
            return 0;
        }
        long left=0,right=x;
        while (left<right){
            long mid=left+(right-left+1)/2;
            if(mid*mid<=x) left=mid;
            else right=mid-1;
        }
        return (int) left;
    }
}
