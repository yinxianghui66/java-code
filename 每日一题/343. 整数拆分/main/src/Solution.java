//给定一个正整数 n ，将其拆分为 k 个 正整数 的和（ k >= 2 ），并使这些整数的乘积最大化
//返回 你可以获得的最大乘积
//输入: n = 10
//输出: 36
//解释: 10 = 3 + 3 + 4, 3 × 3 × 4 = 36。
class Solution {
    public int integerBreak(int n) {
        //dp数组的定义  拆分n这个值后 得到的最大的乘积
        int[] dp = new int[n+1];
        //初始化dp数组 + 状态方程
        dp[0] = 0;
        dp[1] =0;
        dp[2] =1;
        for (int i = 3; i <=n; i++) {
            for (int j = 1; j <=i/2; j++) {
                //拆分成近似相等的数
                //j*dp[i-j] 再往下拆分成3/ 3+个数
                //这里还要再取 dp[i] 的最大值 因为j循环是一整轮 取一整轮的最大值
                //j*(i-j) 为拆分成两位的情况
                dp[i] =Math.max(dp[i],Math.max(j*dp[i-j],j*(i-j)));
            }
        }
        return dp[n];
    }
}