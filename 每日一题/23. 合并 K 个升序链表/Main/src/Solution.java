import java.util.PriorityQueue;
//给你一个链表数组，每个链表都已经按升序排列。
//请你将所有链表合并到一个升序链表中，返回合并后的链表。
//使用优先级队列 建立一个小根堆
//将每个头结点 放入小根堆 小根堆的堆顶就是下一个要合并的链表
//合并完后 将该位置的下一个节点 再次放入小根堆 继续维护
//直到小根堆为空 结束合并
//时间复杂度为 O(k n×logk)
class ListNode {
     int val;
     ListNode next;
     ListNode() {}
     ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }

 class Solution {

     public ListNode mergeKLists(ListNode[] lists) {
         //1.创建一个小根堆 传入比较方法 比较两个节点的大小 建立小根堆
         PriorityQueue<ListNode> heap=new PriorityQueue<>((v1,v2)->v1.val-v2.val);
         //2.把所有的头结点 放入小根堆中
         for(ListNode head:lists){
             //只要头结点不为空 就把它放入小根堆中
             if(head!=null){
                 heap.offer(head);
             }
         }
         //3.合并链表
         ListNode ret=new ListNode(0);
         ListNode prev=ret;
         while(!heap.isEmpty()){  //只要堆不为空 就继续合并
             //取出最小的节点 连接到链表后
             ListNode t=heap.poll();
             prev.next=t;
             //继续向后
             prev=t;
             //移动指针 让后面的进入小根堆
             if(t.next!=null) heap.offer(t.next);
         }
         //返回虚拟头结点的下一个节点
         return ret.next;
     }
}