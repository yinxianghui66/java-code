//使用递归 分治来解决
//跟归并排序差不多 这里是合并两个有序链表
//如果left>right 说明此时是个空链表 直接返回
//如果left==right 说明是个只有一个链表的列表 返回任意一个链表即可
//找到中间值 分为两半 递归处理左右部分  [left, mid],[mid+1, right]
//然后合并两个有序链表 两个指针 挨个遍历 找到其中一个小的 挨个插入到结果链表中
//最后返回虚拟头结点的next即可!
public class Solution2 {
    public ListNode mergeKLists(ListNode[] lists) {
        return  merge(lists,0,lists.length-1);
    }

    public ListNode merge(ListNode[] lists, int left, int right) {
        //1.处理边界情况
        if(left > right) return  null;
        if(left == right) return lists[left];
        //找到中间点
        int mid = (left + right) / 2;
        //[left, mid],[mid+1, right]
        //2.递归处理左右两部分
        ListNode l1=merge(lists,left,mid);
        ListNode l2=merge(lists,mid+1,right);
        //3.合并两个有序链表
        return mergeTwoLists(l1,l2);
    }
    public ListNode mergeTwoLists(ListNode l1, ListNode l2){
        if(l1==null) return l2;
        if(l2==null) return l1;
        //合并两个有序链表
        ListNode head=new ListNode(0);
        ListNode cur1=l1,cur2=l2,prev=head;
        while(cur1!=null&&cur2!=null){
            if(cur1.val<=cur2.val){
                prev.next=cur1;
                prev=cur1;
                cur1=cur1.next;
            }else {
                prev.next=cur2;
                prev=cur2;
                cur2=cur2.next;
            }
        }
        //处理链表长度不一样的问题
        if(cur1!=null) prev.next=cur1;
        if(cur2!=null) prev.next=cur2;
        return head.next;
    }

}
