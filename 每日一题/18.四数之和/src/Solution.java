//链接:https://leetcode.cn/problems/4sum/
//还是双指针算法  本质上和三数之和(也是双指针)是一样的 多固定一个数字 再多去重一次即可
//唯一不同的是 我们在双指针的区间里找 target-前两个数的和
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
class Solution {
    public  List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> ret=new ArrayList<>();
        //排序
        Arrays.sort(nums);
        //固定一个数 a
        int n=nums.length;
        for(int i=0;i<n;){
            //三数之和 解决
            for(int j=i+1;j<n;){  //固定数b
                int left=j+1;
                int right=n-1;
                long aim=(long)target-nums[i]-nums[j];
                while(left<right){
                    int sum=nums[left]+nums[right];
                    if(sum>aim){
                        right--;
                    }else if(sum<aim){
                        left++;
                    }else{
                        //找到元素 存入结果集
                        ret.add(new ArrayList<Integer>(Arrays.asList(nums[i],nums[j],
                                nums[left],nums[right])));
                        //不漏 继续寻找其他结果
                        left++;
                        right--;
                        //去重操作1
                        while(left<right&&nums[left]==nums[left-1]){
                            left++;
                        }
                        while(left<right&&nums[right]==nums[right+1]){
                            right--;
                        }
                    }
                }
                j++;
                //去重操作2  注意使用while 去重多个 并且 不能越界
                while (j<n&&nums[j]==nums[j-1]){
                    j++;
                }
            }
            //去重操作3
            i++;
            while (i<n&&nums[i]==nums[i-1]){
                i++;
            }
        }
        return ret;
    }
}