import java.util.*;
//实现RandomizedSet 类
//RandomizedSet() 初始化 RandomizedSet 对象
//bool insert(int val) 当元素 val 不存在时，向集合中插入该项，并返回 true ；否则，返回 false!
//bool remove(int val) 当元素 val 存在时，从集合中移除该项，并返回 true ；否则，返回 false!
//int getRandom() 随机返回现有集合中的一项（测试用例保证调用此方法时集合中至少存在一个元素）。每个元素应该有 相同的概率 被返回。
//你必须实现类的所有函数，并满足每个函数的 平均 时间复杂度为 O(1) 
//这题要求插入和删除都为0(1) 复杂度 可以考虑使用哈希表来进行插入跟删除
//对于取随机元素 可以使用链表 直接访问一个随机下标获取
class RandomizedSet {
    List<Integer> list;
    HashMap<Integer,Integer> map;
    Random random;
    public RandomizedSet() {
        //初始化
        list = new ArrayList<>();
        map = new HashMap<>();
        random = new Random();
    }

    public boolean insert(int val) {
        //使用哈希表插入 并维护链表
        if(map.containsKey(val)) return false;
        //数组存储元素 哈希表存储元素和这个元素在数组中的下标
        int size = list.size();
        //因为这个size是在执行add之前调用的 所以不需要-1 因为
        //链表的下标从0开始 所以size得到的长度本来就是下标位置+1 因为新增一个元素
        //所以新增元素的下标刚好就是size
        list.add(val);
        map.put(val,size);
        return true;
    }

    public boolean remove(int val) {
        if(!map.containsKey(val)) return  false;
        //这里链表的删除不能直接调用方法 要考虑时间复杂度
        int index = map.get(val); //找到要删除位置的元素下标
        int end = list.get(list.size()-1); //链表最后一位的元素
        //用最后一位的数据覆盖要删除数据的下标位置的元素即可
        list.set(index,end);
        //将替换的元素和他的新下标(覆盖掉的元素的下标)存入哈希表
        map.put(end,index);
        //删除多余的元素
        list.remove(list.size()-1);
        //删除哈希表中的元素
        map.remove(val);
        return true;
    }

    public int getRandom() {
        //生成随机数 根据下标获取数据
        int  randomIndex = random.nextInt(list.size());
        return list.get(randomIndex);
    }
}

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet obj = new RandomizedSet();
 * boolean param_1 = obj.insert(val);
 * boolean param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */