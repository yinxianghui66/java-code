//给你一个字符串 s，找到 s 中最长的回文子串。
//
//如果字符串的反序与原始字符串相同，则该字符串称为回文字符串。
//示例 1：
//输入：s = "babad"
//输出："bab"
//解释："aba" 同样是符合题意的答案。
//示例 2：
//输入：s = "cbbd"
//输出："bb"
//中心拓展算法
//1.固定中心点
//2.从中心点向两边扩展  注意:奇数和偶数长度都需要考虑
//奇数长度 left=right 向两边推展
//偶数长度 right=left+1 向两边推展
//奇数长度的 判断left 和right不越级 并且字符相等 找到最长的子串 统计起始位置和长度!
class Solution {
    public String longestPalindrome(String s) {
        int begin=0, len=0;
        //固定所有的中间点
        for(int i=0; i<s.length();i++){
            //拓展奇数长度的字串
            int left=i,right = i;
            while(left>=0&&right<s.length()&&s.charAt(left)==s.charAt(right)){
                left--;
                right++;
            }
            //更新最长子串结果
            if(right-left-1 > len){
                begin = left + 1;
                len=right-left-1;
            }
            //拓展偶数长度的子串
            left = i;right = i + 1;
            while(left>=0&&right<s.length()&&s.charAt(left)==s.charAt(right)){
                left--;
                right++;
            }
            if(right-left-1 > len){
                begin = left + 1;
                len=right-left-1;
            }
        }
        return s.substring(begin,begin+len);
    }
}