//给你一个整数 n ，求恰由 n 个节点组成且节点值从 1 到 n 互不相同的 二叉搜索树 有多少种？
// 返回满足题意的二叉搜索树的种数
class Solution {
    public static int numTrees(int n) {
        //1.定义dp数组含义 dp[i] 为 由i个结点组成节点值 从1-i 有dp[i] 种结果
        int[] dp = new int[n+1];
        //2.初始化dp数组 并推导状态方程
        if(n<2) return 1;
        dp[0] = 1;
        dp[1] =1;

        //推导方程 画图 根据结构一样得出
        for (int i = 2; i <=n; i++) {
            for (int j = 1; j <=i ; j++) {
                //这里是+= 要统计所有的结果 每次遍历一次是一种结果
                dp[i] += dp[j-1]*dp[i-j];
            }
        }
        //4.确定遍历顺序 后依赖前 从前向后遍历
        //5.打印dp数组 调试结果
        return dp[n];
    }

    public static void main(String[] args) {
        System.out.println(numTrees(3));
    }
}