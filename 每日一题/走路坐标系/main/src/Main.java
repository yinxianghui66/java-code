//牛牛昨晚喝醉了，走路开始疯狂摇摆，方向也分不清了。假设他所在的地方是一个二维平面，开始它位于坐标为(0,0)的地方，并且面朝北方即y轴正方向。
// W表示牛牛向前走，A表示牛牛把当前方向向左转90度，D表示牛牛把方向向右转90度，S表示牛牛呆在原地。给出一个字符串表示牛牛的酒后行为方式，你能告诉他走完后它位于哪个坐标点吗？

//WAW
//样例输出
//-1 1
//说明
//第一步后牛牛面朝上位于(0,1)，第二步后牛牛面朝西位于(0,1)，第三步后牛牛面朝西位于(-1,1)；

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        //转换成字符数组
        char[] ss= s.toCharArray();
        int  x =0,y = 0;
        //搞一个指南针 每次操作 更改方向 根据不同的方向 进行不同的操作
        char direction = 'N';
        for (int i = 0; i <ss.length;i++) {
            //识别操作步骤
            if(ss[i] == 'W'){
                //需要先判断目前的方向
                if(direction == 'N'){
                    //目前方向是朝上
                    y++;
                }else if(direction == 'E'){
                    x++;
                }else if(direction == 'S'){
                    y--;
                }else {
                    x--;
                }
                direction = 'N';
            }else if (ss[i] == 'A'){
                //向左旋转90度
                if(direction=='N') direction = 'W';
                else if(direction =='S') direction = 'E';
                else if(direction =='E') direction = 'N';
                else  direction ='S';
            }else if (ss[i] == 'D'){
                //向右旋转90度
                if(direction=='N') direction = 'E';
                else if(direction =='S') direction = 'W';
                else if(direction =='E') direction = 'S';
                else  direction ='N';
            }
            //s 为在原地不动 不用处理
        }
        System.out.printf( x+" "+y);
    }
}
