import java.util.HashSet;
import java.util.Set;

class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> res = new HashSet<>();
        //遍历数组 加入set1中
        for(int i = 0;i<nums1.length;i++){
            set1.add(nums1[i]);
        }
        //遍历数组二 判断数组2里的数据是否在哈利表里出现 如果出现就塞到res里
        for(int i = 0;i<nums2.length;i++){
            if(set1.contains(nums2[i])) res.add(nums2[i]);
        }
        //返回res
        int[] arr = new int[res.size()];
        int j=0;
        for(int i :res){
            arr[j++] = i;
        }
        return  arr;
    }
}