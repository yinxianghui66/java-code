import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//第一个困难题!
//和串联所有单词的子串 及其相似  我们需要对细节进行修改
//将这里的每一个长度为words字符串长度的字符串 看作一个字母  这样就和上一题一模一样了
//我们需要对细节进行修改
//1.滑动窗口的次数  我们需要将一个字符串每len 个字符都可能有结果 挨个保存 所以要执行len次滑动窗口
//2.滑动窗口的步长  一次滑动的大小应该是一个len 的长度
//3.哈希表的区别  定义一个<String,Integer>  String 表示每一个字符串 Integer表示这个字符串出现的次数
public class Solution {
    public List<Integer> findSubstring(String s, String[] words) {
        List<Integer> ret=new ArrayList<>();
        Map<String,Integer>  hash1=new HashMap<>();
        for(String str:words) hash1.put(str,hash1.getOrDefault(str,0)+1);  //统计words 每个字符串出现的次数
        int len=words[0].length(),m=words.length;
        for(int i=0;i<len;i++){  //滑动窗口执行的次数
            Map<String,Integer>  hash2=new HashMap<>();
            for (int left=i,right=i,count=0;right+len<=s.length();right+=len){  //注意滑动一次一个字符串的长度 //注意判断条件是right+len<=s.length()
                //进窗口 +维护count
                String in=s.substring(right,right+len);
                hash2.put(in,hash2.getOrDefault(in,0)+1);  //hash2 in位置可能为值0 所以如果找不到就默认为0
                if(hash2.get(in)<=hash1.getOrDefault(in,0)) count++;
                //判断
                if(right-left+1>len*m){  //这里滑动窗口的大小应该小于len*m的长度
                    //维护count+出窗口
                    String out=s.substring(left,left+len);
                    if(hash2.get(out)<=hash1.getOrDefault(out,0)) count--;
                    //出窗口
                    hash2.put(out,hash2.getOrDefault(out,0)-1);
                    left+=len;
                }
                //保存结果
                if(count==m){
                    ret.add(left);
                }
            }
        }
        return ret;
    }
}
