//当且仅当每个相邻位数上的数字 x 和 y 满足 x <= y 时，我们称这个整数是单调递增的。
//给定一个整数 n ，返回 小于或等于 n 的最大数字，且数字呈 单调递增 。
//输入: n = 10
//输出: 9
class Solution {
    public static int monotoneIncreasingDigits(int n) {
        //从后向前遍历 判断 如果前一位 大于当前位置 则将前一位-1 当前位置置为9
        String[] s =(n+"").split("");
        int start =s.length;
        for (int i = s.length-1; i >0 ; i--) {
            if(Integer.parseInt(s[i-1])>Integer.parseInt(s[i])){
                //不符合单调递增 将前一位-1 当前位置置为最大值9
                //注意这里字符串和数字之间的转换
               s[i-1] = (Integer.parseInt(s[i-1])-1)+"";
               //注意这里不能直接变成9 直接修改 会影响后面的大小比较 例如
                //100 0 0 不符合条件 10  变成000
                //此时赋值为9  结果为90 后面的最后一位的结果在之前判断过了 后面无法再修改
                //所以直接存储 需要从哪一位 后面的全都变为9 后面统一处理
               start = i;
            }
        }
        for (int i = start; i <s.length; i++) {
            s[i] = "9";
        }
        return Integer.parseInt(String.join("",s));
    }

    public static void main(String[] args) {
        int n = 100;
        System.out.println(monotoneIncreasingDigits2(n));
    }
    public static int monotoneIncreasingDigits2(int n) {
        //从后向前遍历 判断 如果前一位 大于当前位置 则将前一位-1 当前位置置为9
        String s= String.valueOf(n);
        char[] chars = s.toCharArray();
        int start =chars.length;
        for (int i = chars.length-1; i >0 ; i--) {
            if(chars[i-1]>chars[i]){
                //不符合单调递增 将前一位-1 当前位置置为最大值9
                //注意这里字符串和数字之间的转换
                chars[i-1]--;
                //注意这里不能直接变成9 直接修改 会影响后面的大小比较 例如
                //100 0 0 不符合条件 10  变成000
                //此时赋值为9  结果为90 后面的最后一位的结果在之前判断过了 后面无法再修改
                //所以直接存储 需要从哪一位 后面的全都变为9 后面统一处理
                start = i;
            }
        }
        for (int i = start; i <chars.length; i++) {
            chars[i] = '9';
        }
        return Integer.parseInt(String.valueOf(chars));
    }
}