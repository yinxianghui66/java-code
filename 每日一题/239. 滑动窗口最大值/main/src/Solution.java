//给你一个整数数组 nums，有一个大小为 k 的滑动窗口从数组的最左侧移动到数组的最右侧。你只可以看到在滑动窗口内的 k 个数字。滑动窗口每次只向右移动一位。
//返回 滑动窗口中的最大值

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

class Solution {
    public int[] maxSlidingWindow(int[] nums, int k) {
        int len = nums.length-k+1;
        int[] res = new int[len];
        int num =0;
        MyQueue myQueue = new MyQueue();
        //先将k个元素 放入队列
        for(int i=0;i<k;i++){
            myQueue.add(nums[i]);
        }
        //存储初始窗口的最大值
        res[num++] = myQueue.peek();
        //遍历数组 模拟滑动窗口 左边出一个 右边进一个
        for(int i=k;i<nums.length;i++){
            //滑动窗口 移除最前面的元素
            myQueue.poll(nums[i-k]);
            //进入最右边的元素
            myQueue.add(nums[i]);
            //记录此时窗口的最大值
            res[num++]=myQueue.peek();
        }
        return res;
    }
}
class MyQueue{
    //自定义实现一个单调队列
    Deque<Integer> queue = new LinkedList<>();
    public void add(int num){
        //添加元素时，如果要添加的元素大于入口处的元素，就将入口元素弹出
        //保证队列元素单调递减
        //比如此时队列元素3,1，2将要入队，比1大，所以1弹出，此时队列：3,2
        while (!queue.isEmpty() && num > queue.getLast()) {
            queue.removeLast();
        }
        queue.add(num);
    }
    //弹出元素
    public void poll(int num){
        //弹出队列 如果这个值为队列顶的元素 则真正出队列
        if(!queue.isEmpty()&&queue.peek()==num){
            queue.poll();
        }
        //如果不大于队顶元素 则不做任何操作
    }
    //获取队列头部元素 也就是最大值
    public int peek(){
        return queue.peek();
    }
}