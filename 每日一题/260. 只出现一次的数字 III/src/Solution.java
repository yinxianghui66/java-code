class Solution {
    public int[] singleNumber(int[] nums) {
        int ans=nums[0];
        for(int i=1;i<nums.length;i++){
            ans^=nums[i];
        }
        //得到的结果即为  a  ^ b  根据第一次出现1的位置 分组 分组后 a 和 b一定是在两个不同的组 再根据这个进行异或 得到结果
        int diff=0;
        for(int i=0;i<32;i++){
            if(((ans>>i)&1)==1) diff=i;
        }
        //对两组数据异或
        int[] ret=new int[2];
        for(int i=0;i<nums.length;i++){
            if(((nums[i]>>diff)&1)==1) ret[1]^=nums[i];
            else ret[0]^=nums[i];
        }
        return ret;
    }
}