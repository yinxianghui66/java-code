//https://leetcode.cn/problems/valid-triangle-number/description/
//双指针思想  a=nums[left]  b=nums[rigth]  c=当前数组最大值
//引入数学思想 三角形 a b c 三条边 如果是有序的 a<b<c
//那么只需要判断一次 a+b>c 那么a+c>b 那么 b+a>c 都直接成立  都无需比较 直接可以得出可以组成三角形
//先排序 根据单调性  固定一个最大的数 里面使用双指针思想进行判断
// 如果a+b>c 那么 right-left区间内的所有数都比c大 都可以统计  统计完后 right-- 再次比较下一个区间
//如果 a+b < c  那么 right-left区间内的所有数都比c小 组成不了三角形 直接让left++ 让下一个更大的数 判断下一个区间
import java.util.Arrays;
class Solution {
    public int triangleNumber(int[] nums) {
        //对数组进行排序
        Arrays.sort(nums);
        int left;
        int right;
        int count=0;
        //固定最大的数   这里i表示 三个数 无需再去前面找数 最大的数只能从2开始
        for(int i=nums.length-1;i>=2;i--){
            right=i-1;
            left=0;
            while(left<right){
                // a+b>c  如果  利用单调性
                if(nums[left]+nums[right]>nums[i]){
                    //a b 这个区间可以满足条件   那么 left-right区间都可以满足 统计区间内的所有
                    count+=right-left;
                    right--;
                }else{
                    //a+b<c
                    left++;
                }
            }

        }
        return count;
    }
}