
//力扣9.https://leetcode.cn/problems/palindrome-number/description/
//这种简单题都没写出来 我是垃圾= =
class Solution {
    public static boolean isPalindrome(int x) {
//        String s=String.valueOf(x);
//        //通过reverse将其倒置 判断是否相同
//        StringBuffer stringBuffer=new StringBuffer(s);
//        String s1= String.valueOf(stringBuffer.reverse());
//        if(!s.equals(s1)){
//            return false;
//        }
//        return true;
        //负数一定不为回文数
        if(x<0){
            return false;
        }
        //定义一个倒转字符串存储结果
        int or=x;
        int reversed=0;
        //每次%10取最后一位
        //本身*10+最后一位 完成倒置
        //最后判断倒置的数和原来的数是否相等
        while (x!=0){
            int digit=x%10;
            reversed=reversed*10+digit;
            x/=10;
        }
        return or==reversed;

    }
    public static void main(String[] args) {
        System.out.println(isPalindrome(123));
    }
}