//给定一个数组 nums ，如果 i < j 且 nums[i] > 2*nums[j] 我们就将 (i, j) 称作一个重要翻转对。
//你需要返回给定数组中的重要翻转对的数量。
//链接:https://leetcode.cn/problems/reverse-pairs/description/
//归并排序+分治
//这个题和逆序对不同 不能在合并数组的时候统计
//需要在合并之前统计反转对的个数才行
class Solution {
    int[] tmp;
    public int reversePairs(int[] nums) {
        int n=nums.length;
        tmp=new int[n];
        return mergeSort(nums,0,n-1);
    }
    public  int mergeSort(int[] nums,int left,int right){
        //1.处理边界
        if(left>=right) return 0;
        int ret=0;
        //2.取中间元素
        int mid=(left+right)/2;
        //3.排序求左右区间翻转对个数
        //[left,mid] [mid+1,right]
        ret+=mergeSort(nums,left,mid);
        ret+=mergeSort(nums,mid+1,right);
        //4.在合并之前 查找翻转对的数量
        int cur1=left,cur2=mid+1,i=left;
        while (cur1<=mid){
            while (cur2<=right&&nums[cur2]>=nums[cur1]/2.0) cur2++;
            if(cur1>right) break;
            //此时统计翻转对
            ret+=right-cur2+1;
            cur1++;
        }
        //5.合并两个有序数组
        cur1=left;cur2=mid+1;
        while (cur1<=mid&&cur2<=right){
            tmp[i++]=nums[cur1]<=nums[cur2]?nums[cur2++]:nums[cur1++];
        }
        //6.处理未遍历完的数组
        while (cur1<=mid) tmp[i++]=nums[cur1++];
        while (cur2<=right) tmp[i++]=nums[cur2++];
        //7.还原数组
        for(int j=left;j<=right;j++){
            nums[j]=tmp[j];
        }
        return ret;
    }

}