    //一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为 “Start” ）。
    //机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish” ）。
    //问总共有多少条不同的路径
    //输入：m = 3, n = 7
    //输出：28
    class Solution {
        public static int uniquePaths(int m, int n) {
            //1.定义dp数组的每一位的含义 dp[i][j] 达到这个位置 可以走的路径个数
            int[][] dp = new int[m+1][n+1];
            //2.初始化dp数组 并写状态方程
            dp[0][0] = 1;
            //3.确定遍历顺序 dp[i][j]依赖于前置数据 所以是从勾后向前
            for (int i = 0; i <m+1; i++) {
                for (int j = 0; j <n+1; j++) {
                    if(i==0&&j!=0){
                        dp[i][j] = dp[i][j-1];
                        continue;
                    }
                    if(j==0&&i!=0){
                        dp[i][j] = dp[i-1][j];
                        continue;
                    }
                    if(i==0&&j==0){
                        continue;
                    }
                    //想当于 这个位置的数量 因为只有两种方法可以到达 向右 或向左 所以这两种情况的位置相加
                    dp[i][j] = dp[i-1][j]+dp[i][j-1];
                }
            }
            //4.打印dp数组 调试 看是否符合预期
            return dp[m-1][n-1];
         }

        public static void main(String[] args) {
            System.out.println(uniquePaths(3, 7));
        }
    }