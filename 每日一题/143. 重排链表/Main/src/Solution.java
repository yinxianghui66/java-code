//给定一个单链表 L 的头节点 head ，单链表 L 表示为：
//L0 → L1 → … → Ln - 1 → Ln
//请将其重新排列后变为：
//L0 → Ln → L1 → Ln - 1 → L2 → Ln - 2 → …
//不能只是单纯的改变节点内部的值，而是需要实际的进行节点交换。
//模拟 ...  链表昏头了...
//画图!!!!!  这里的头插法 是借助head2来存储cur信息 然后在原位链表中进行头插..也就是连接
//先找到中间节点，然后将后半部分链表反转，然后将前半部分链表与后半部分链表连接起来
  class ListNode {
     int val;
     ListNode next;
     ListNode() {}
     ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }

class Solution {
    public static void reorderList(ListNode head) {
        //处理边界情况
        if(head==null||head.next==null||head.next.next==null) return;
        //1.找到链表的中间节点 ->快慢双指针
        ListNode slow =head,fast=head;
        while(fast!=null&&fast.next!=null){
            //执行完后 slow所处的就是中间节点
            slow=slow.next;
            fast=fast.next.next;
        }
        //2.把slow后面的部分逆序->头插法
        ListNode head2=new ListNode(0);
        ListNode cur=slow.next;
        slow.next=null;  //把两个链表分离
        while(cur!=null){
            //头插法
            ListNode next=cur.next;
            cur.next=head2.next;
            head2.next=cur;
            cur=next;
        }
        //3.合并两个链表 ->双指针
        ListNode cur1=head,cur2=head2.next;
        ListNode ret=new ListNode(0);
        ListNode prev=ret;
        while(cur1!=null){
            //先放第一个链表 再合并第二个链表
            prev.next=cur1;
            prev=cur1;
            cur1=cur1.next;
            //可能第二个链表会比第一个短
            if(cur2!=null){
                prev.next=cur2;
                prev=cur2;
                cur2=cur2.next;
            }
        }
    }
}