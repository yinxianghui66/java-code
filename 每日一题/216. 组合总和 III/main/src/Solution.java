import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

//找出所有相加之和为 n 的 k 个数的组合，且满足下列条件：
//只使用数字1到9
//每个数字 最多使用一次
//返回 所有可能的有效组合的列表 。该列表不能包含相同的组合两次，组合可以以任何顺序返回
//输入: k = 3, n = 7
//输出: [[1,2,4]]
//解释:
//1 + 2 + 4 = 7

//输入: k = 3, n = 9
//输出: [[1,2,6], [1,3,5], [2,3,4]]
//解释:
//1 + 2 + 6 = 9
//1 + 3 + 5 = 9
//2 + 3 + 4 = 9
//没有其他符合的组合了。
class Solution {

    List<List<Integer>> res = new ArrayList<>();
    LinkedList<Integer> path = new LinkedList<>();
    public List<List<Integer>> combinationSum3(int k, int n) {
        backtracking(k,n,0,1);
        return  res;
    }

    public void backtracking(int k,int n,int sum,int startIndex){
        //如果目标和已经大于了 就没必要继续了
        if(sum>n) return;
        if(path.size()==k){
            //存储结果
            if(sum==n){
                res.add(new ArrayList<>(path));
            }
            return;
        }
        //确定单层循环逻辑 这里也可以进行剪枝操作 使用sum来计算目前组合里的和
        for (int i = startIndex; i <= 9-(k-path.size())+1; i++) {
            path.add(i);
            sum+=i;
            backtracking(k, n, sum,i+1);
            //回溯
            sum-=i;
            path.removeLast();
        }
    }
}