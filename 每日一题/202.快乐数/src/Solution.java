
//链接:https://leetcode.cn/problems/happy-number/description/
//使用双指针的思想
//本题明确说了  重复这个过程直到这个数变为 1，也可能是 无限循环 但始终变不到 1。
//把这个过程看作一个链表 因为题目说了 所以一定有环 我们只需要判断他俩相遇时的值 是否为1 即为快乐数
//slow一次走一步(把求这个数的每一位平方和看作一步) fast一次走两部
//因为有环 所以一定会相遇
class Solution {
    public int bitSum(int n){
        //返回n这个数每一位的平方和
        int sum=0;
        while(n!=0){
            //找到最后一位 平方 去除最后一位
            int t=n%10;
            sum+=t*t;
            n/=10;
        }
        return sum;
    }
    public boolean isHappy(int n) {
        int slow=n;
        int fast=bitSum(n);
        while(slow!=fast){
            //快慢指针 slow一次走一步
            slow=bitSum(slow);
            fast=bitSum(bitSum(fast));
        }
        //根据题目可以得出 他俩一定会相遇  判断相遇处的值 如果为1即为快乐数
        return slow==1?true:false;
    }
}