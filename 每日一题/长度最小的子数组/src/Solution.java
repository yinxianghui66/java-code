//链接https://leetcode.cn/problems/minimum-size-subarray-sum/
//因为题目说了 是个正整数数组 所以 可以理由单调性 如果找到第一个区间
//此时的sum>=target  那么后面的数据 无论怎么加 都是比target大的 所以可以直接舍去
//也就是出窗口操作 此时left++ 区间的sum值需要减去nums[left]的值
//继续判断 再次找到大于target的区间 更新结果 判断 直到right>=n 时 停止寻找
//每次更新结果都取最小值
class Solution {
    public static int minSubArrayLen(int target, int[] nums) {
        //滑动窗口
        int n=nums.length;
        int sum=0;
        int len=Integer.MAX_VALUE;
        for(int left=0,right=0;right<n;right++){
            sum+=nums[right]; //进窗口
            while (sum>=target){
                //更新结果
                len=Math.min(len,right-left+1);
                sum-=nums[left];
                left++;  //出窗口
            }
        }
        //有可能没有结果 判断此时如果len还是初值的话就返回0
        return len==Integer.MAX_VALUE?0:len;
    }

    public static void main(String[] args) {
        int arr[]={2,3,1,2,4,3};
        System.out.println(minSubArrayLen(7,arr));
    }
}