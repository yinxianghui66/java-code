//https://leetcode.cn/problems/largest-odd-number-in-string/description/
class Solution {
    public static String largestOddNumber(String num) {
        for(int i=num.length()-1;i>=0;i--){
            //从后往前 找到第一个结尾为奇数的数 返回0-该长度 即为最大奇数
            if((num.charAt(i)-'0')%2!=0){
                return num.substring(0,i+1);
            }
        }
        return "";
    }

    public static void main(String[] args) {
        System.out.println(largestOddNumber("35427"));
    }
}