import java.util.Arrays;
import java.util.stream.Collectors;
//https://leetcode.cn/problems/yuan-quan-zhong-zui-hou-sheng-xia-de-shu-zi-lcof/description/
class Solution {
    public static int lastRemaining(int n, int m) {
        //勾八纯数学题 自己看解析吧 ...
        int ans=0;
        for(int i=2;i<=n;i++){
            ans=(ans+m)%i;
        }
        return ans;
    }

    public static void main(String[] args) {
        System.out.println(lastRemaining(5, 3));
    }
}