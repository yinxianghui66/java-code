import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
//给你一个整数数组 nums ，判断是否存在三元组 [nums[i], nums[j], nums[k]]
// 满足 i != j、i != k 且 j != k ，
// 同时还满足 nums[i] + nums[j] + nums[k] == 0 。
// 请你返回所有和为 0 且不重复的三元组。
//注意：答案中不可以包含重复的三元组。
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        //对数组进行排序 方便去重 相同元素相邻
        Arrays.sort(nums);
        //固定i位置 定义双指针 寻找结果 并注意去重
        for(int i = 0;i<nums.length;i++){
            if(nums[i]>0) return res;
            //对i进行去重
            //注意是i 和他的前一位位置
            if(i>0&&nums[i] == nums[i-1]) continue;
            //left 为i的前一位 right为数组末尾
            int left = i+1;
            int right = nums.length-1;
            //不能等于 因为要找三个数字
            while(left<right){
                //如果小于0  因为数组排序过 所以移动指针 缩小范围
                if (nums[i]+nums[left]+nums[right]<0){
                    left++;
                }else if (nums[i]+nums[left]+nums[right]>0){
                    right--;
                }else{
                    //找到结果 存储答案 并且去重 left和right
                    res.add(Arrays.asList(nums[i],nums[left],nums[right]));
                    //去重left 和right 应该在找到一个结果后去重
                    while(left<right &&nums[left] == nums[left+1]) left++;
                    while(left<right &&nums[right] == nums[right-1]) right--;
                    //判断为一步 继续前进
                    left++;
                    right--;
                }
            }
        }
        return res;
    }
}
