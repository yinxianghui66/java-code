import java.util.Arrays;
import java.util.Scanner;

//假设你是一位很棒的家长，想要给你的孩子们一些小饼干。但是，每个孩子最多只能给一块饼干
//对每个孩子 i，都有一个胃口值 g[i]，这是能让孩子们满足胃口的饼干的最小尺寸；并且每块饼干 j，都有一个尺寸 s[j] 。
// 如果 s[j] >= g[i]，我们可以将这个饼干 j 分配给孩子 i ，这个孩子会得到满足。你的目标是满足尽可能多的孩子，并输出这个最大数值。
//输入: g = [1,2,1], s = [1,1]
//输出: 2
class Solution {
    public static int findContentChildren(int[] g, int[] s) {
        int count = 0;
        //进行排序后 小的值 是在前面 这样只要满足条件 就可以尽可能的分多
        Arrays.sort(g);
        Arrays.sort(s);
        //用另一个索引 控制饼干尺寸的下标 如果不符合条件 则向前
        int index = s.length-1;
        for(int i=g.length-1;i>=0;i--){
            //尽可能用大的 满足大的
            if (index>=0&&s[index]>=g[i]){
                //如果当前位置不满足 就去下一个位置 直到找到符合的
                count++;
                index--;
            }

        }
        return count;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] g = new int[3];
        int[] s = new int[1];
        for(int i=0;i<3;i++){
            g[i] = sc.nextInt();
        }
        for(int i=0;i<1;i++){
            s[i] = sc.nextInt();
        }
        System.out.println(findContentChildren(g, s));
    }
}