//https://leetcode.cn/problems/remove-duplicates-from-sorted-array/solutions/34033/shuang-zhi-zhen-shan-chu-zhong-fu-xiang-dai-you-hu/
//用双指针 画图更好理解一点
//一个在前 一个在后 判断是否相同 如果相同 后一个指针往后+1
//p是挨个往后 每一次肯定能保证p+1是有值 会从后面找一个不重复的数字赋值
//赋值后 p++ q++
class Solution {
    public int removeDuplicates(int[] nums) {
        if(nums==null||nums.length==0){
            return 0;
        }
        int p=0;
        int q=1;
        while (q<nums.length){
            if(nums[p]==nums[q]){
                q++;
            }else {
                nums[p+1]=nums[q];
                q++;
                p++;
            }
        }
        return p+1;
    }
}