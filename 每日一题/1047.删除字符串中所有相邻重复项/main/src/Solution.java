import java.util.Stack;

//给出由小写字母组成的字符串 s，重复项删除操作会选择两个相邻且相同的字母，并删除它们。
//在 s 上反复执行重复项删除操作，直到无法继续删除。
//在完成所有重复项删除操作后返回最终的字符串。答案保证唯一
//输入："abbaca"
//输出："ca"
//解释：
//例如，在 "abbaca" 中，我们可以删除 "bb" 由于两字母相邻且相同，
// 这是此时唯一可以执行删除操作的重复项。
// 之后我们得到字符串 "aaca"，其中又只有 "aa" 可以执行重复项删除操作，所以最后的字符串为 "ca"
class Solution {
    public static String removeDuplicates(String s) {
        //来一个元素就入栈 和栈顶元素对比 如果不同 则弹出元素
        //如果相同 则入栈
        //使用字符串 模拟栈
        StringBuffer ss = new StringBuffer();
        //top 为字符串的长度
        int top = -1;
        for(int i=0;i<s.length();i++){
            if(top>=0&&s.charAt(i)==ss.charAt(top)){
                //元素相同 出栈
                ss.deleteCharAt(top);
                top--;
            }else {
                //入栈
                ss.append(s.charAt(i));
                top++;
            }
        }
        return ss.toString();
    }


}