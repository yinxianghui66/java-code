import java.util.Stack;

//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
//有效字符串需满足：
//左括号必须用相同类型的右括号闭合。
//左括号必须以正确的顺序闭合。
//每个右括号都有一个对应的相同类型的左括号。
class Solution {
    public boolean isValid(String s) {
        //三种情况 1.左括号数量不匹配
        //2.左右括号类型不匹配
        //3.右括号数量不匹配
        //使用栈来匹配
        Stack<Character> stack = new Stack<>();
        for(int i=0;i<s.length();i++){
            //遇到 左括号方向的 就入栈右方向 方便后面直接对比
            if(s.charAt(i)=='{'){
                stack.push('}');
            }else if(s.charAt(i)=='['){
                stack.push(']');
            }else if(s.charAt(i)=='('){
                stack.push(')');
            }else {
                //如果栈为空 则直接返回
                if(stack.isEmpty()) return false;
                //如果不是左方向的 则栈出栈 和该位置匹配
                char tmp = stack.pop();
                //和对应位置不匹配 直接返回
                if(tmp!=s.charAt(i)) return false;
            }
        }
        //如果栈为空 则匹配成功
        return stack.isEmpty();
    }
}