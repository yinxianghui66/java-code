package org.example;

//给定两个以字符串形式表示的非负整数 num1 和 num2，返回 num1 和 num2 的乘积，它们的乘积也表示为字符串形式。
//示例 1:
//输入: num1 = "2", num2 = "3"
//输出: "6"
//示例 2:
//输入: num1 = "123", num2 = "456"
//输出: "56088"
//解法1
//细节: 高位相乘时 需要补零 建议先将字符串逆序 这样就可以根据下标来计算需要补几个0
//2.处理前导0 如果和0 相乘 那么结果直接为0即可 去掉前置的多余的0
//3.注意计算结果的顺序
//解法2
//对解法1做一下优化 无进位相乘再相加 最后处理进位
//使用数组来存储中间结果 大小为  m+n - 1!
public class Solution {
    public String multiply(String num1, String num2) {
        //解法2 无进位相乘再相加
        // 反转字符串
        int m =num1.length();
        int n = num2.length();
        char [] n1 = new StringBuffer(num1).reverse().toString().toCharArray();
        char [] n2 = new StringBuffer(num2).reverse().toString().toCharArray();
        int[] tmp = new int[m+n-1];
        //1.无进位相乘
        for(int i=0; i < m; i++){
            for(int j=0; j<n; j++){
                //固定一位和每一位相乘
                tmp[i+j] += (n1[i] - '0') * (n2[j] -'0');
            }
        }
        //2.处理进位
        int cur = 0, t=0;
        StringBuffer ret = new StringBuffer();
        while (cur < m + n -1 || t!=0){
            if(cur<m+n-1) t+=tmp[cur++];
            //取个位作为结果 十位进位
            ret.append((char) ((char)(t%10)+'0'));
            t/=10;
        }
        //3.处理前导0
        while(ret.length()>1&&ret.charAt(ret.length()-1)=='0'){
            //去除前导0
            ret.deleteCharAt(ret.length()-1);
        }

        return ret.reverse().toString();

    }
}