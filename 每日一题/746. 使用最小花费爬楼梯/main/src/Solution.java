//给你一个整数数组 cost ，其中 cost[i] 是从楼梯第 i 个台阶向上爬需要支付的费用。
// 一旦你支付此费用，即可选择向上爬一个或者两个台阶。
//你可以选择从下标为 0 或下标为 1 的台阶开始爬楼梯。
//请你计算并返回达到楼梯顶部的最低花费
//输入：cost = [10,15,20]
//输出：15
//解释：你将从下标为 1 的台阶开始。
//- 支付 15 ，向上爬两个台阶，到达楼梯顶部。
//总花费为 15
class Solution {
    public static int minCostClimbingStairs(int[] cost) {
        //1.定义dp数组 每一位的含义为 dp[i] 为到第i位台阶 需要花费最小的钱数
        //这里+1 是因为 是需要跳到cost长度外的位置 所以需要跳到长度外一位
        int[] dp = new int[cost.length+1];
        //2.初始化dp数组 并列出状态方程 因为可以从0 或者1位置开始跳
        dp[0] = 0;
        dp[1] =0;
        //3.状态方程 dp数组的遍历顺序 从后向前 因为后面的数据依赖前面
        //这里也需要对应的 多算一位
        for (int i = 2; i <cost.length+1 ; i++) {
            //第i为的值 可以通过i-1 跳一步 或者i-2跳两步得到 并且到i位置 需要花费对应的cost的值
            dp[i] = Math.min(dp[i-1]+cost[i-1],dp[i-2]+cost[i-2]);
        }

        //4.打印dp数组 调试看结果
        return dp[cost.length];
    }

    public static void main(String[] args) {
        int[] s= {10,15,20};
        System.out.println(minCostClimbingStairs(s));
    }
}