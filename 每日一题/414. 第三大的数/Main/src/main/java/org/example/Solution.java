package org.example;
//给你一个非空数组，返回此数组中 第三大的数 。如果不存在，则返回数组中最大的数。
//注意数据范围 使用long类型 来防止整数溢出。
//创建三个变量 维护大小顺序 如果新数据比最大的数大 则更新数据 最大的数为新数据 第二大的为刚才的第一大 第三大的为刚才的第二大
//如果新数据比第二大的数据大 比第一大的数据小 则第二大的数据变为新数据 第三大的数据变为刚才的第二大
//如果新数据比第三大的数据大 比第二大的数据小 则第三的数据直接变为新数据
public class Solution {
    public int thirdMax(int[] nums) {
        long num1=Long.MIN_VALUE,num2=Long.MIN_VALUE,num3=Long.MIN_VALUE; // 使用 long 类型以防止整数溢出
        for(int i=0;i<nums.length;i++){
            if(nums[i]>num1){
                num3=num2;
                num2=num1;
                num1=nums[i];
            }else if(nums[i]>num2 && nums[i]<num1){
                num3=num2;
                num2=nums[i];
            }else if(nums[i]>num3 && nums[i]<num2){
                num3=nums[i];
            }
        }
        //如果第三的数不存在 也就是值为 Long.MIN_VALUE 则应该返回最大的数num1
        return (num3==Long.MIN_VALUE || num3==num2) ? (int)num1 : (int)num3; // 将 long 类型转换为 int 类型返回
    }
}