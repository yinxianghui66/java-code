import java.util.ArrayList;
import java.util.List;

class Solution {
    private char [] path;
    private List<String> ans=new ArrayList<>();
    private int n;
    public List<String> generateParenthesis(int n) {
        this.n=n;
        path=new char[n*2];
        dfs(0,0);
        return  ans;
    }
    //open为左括号个数   i为当前位置
    private void dfs(int i, int open) {
        //当位置和括号数相同时 直接返回
        if(i==n*2){
            ans.add(new String(path));
            return;
        }
        //如果左括号个数小于n  就可以加一个左括号 n为左右括号的的个数 括号总数为n*2
        if(open<n){
            path[i]='(';
            //加完一个递归到下一个位置
            dfs(i+1,open+1);
        }
        //i为当前位置  剩余括号数-左括号数 就为剩余右括号数 如果右括号个数小于左括号了 此时可以加一个右括号
        if(i-open<open){
            path[i]=')';
            dfs(i+1,open);
        }
    }
}