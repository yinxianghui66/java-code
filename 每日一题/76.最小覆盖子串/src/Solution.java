//链接:https://leetcode.cn/problems/minimum-window-substring/description/
//滑动窗口 + 哈希表 +count优化
//也是困难题目!
//但是没有那么难 也是和统计字符串中所有字母异位词相似
//需要注意的是 这个题count需要统计的是种类 不是每个有效字符的数量 《子字符串中该字符数量必须不少于 t 中该字符数量》
//还有就是出窗口的判断 这个题的窗口大小是不固定的  找到一个当前合法的窗口(合法种类相同) 开始出窗口 一直到不合法
//因为是判断之后是合法的 所以更新结果是再判断完后直接判断 再出窗口
public class Solution {
    public String minWindow(String ss, String tt) {
        //转换字符串为字符数组
        char[] s=ss.toCharArray();
        char[] t=tt.toCharArray();
        //统计t中字符出现的次数
        int[] hash1=new int[128];
        int[] hash2=new int[128];  //统计滑动窗口中字符出现的次数
        int kinds=0;
        int begin=-1;
        int minlen=Integer.MAX_VALUE;
        for(char ch: t){
            if(hash1[ch]==0) kinds++;  //统计t中 合法种类的 数量
            hash1[ch]++;  //出现字符数量+1;
        }
        for(int left=0,right=0,count=0;right<s.length;right++){  //count是统计当前合法字符种类的个数
            // 注意这个题是子字符串中该字符数量必须不少于 t 中该字符数量  所以是统计种类 不是每个个数
            //进窗口 +维护count
            char in=s[right];
            hash2[in]++;  //字符出现次数 +1
            if(hash2[in]==hash1[in]) count++;
            //判断   当当前窗口的有效种类和t中有效种类相同时  出窗口 一直到当前窗口不合法 继续right++
            while (count==kinds){
                //更新结果  要返回字符串 找到最小字符长度和起始位置
                if(minlen>right-left+1){
                    begin=left;
                    minlen=right-left+1;
                }
                //维护count+出窗口
                char out=s[left];
                if(hash2[out]==hash1[out]) count--;
                hash2[out]--;
                left++;
            }
        }
        //如果begin为-1 说明一个合法的都没找到 返回空字符串
        if(begin==-1){
            return "";
        }else {
            return ss.substring(begin,begin+minlen);
        }
    }
}
