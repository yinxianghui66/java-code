//给你一个非负整数数组 nums 和一个整数 target
//向数组中的每个整数前添加 '+' 或 '-' ，然后串联起所有整数，可以构造一个 表达式
//例如，nums = [2, 1] ，可以在 2 之前添加 '+' ，在 1 之前添加 '-' ，然后串联起来得到表达式 "+2-1" 。
//返回可以通过上述方法构造的、运算结果等于 target 的不同 表达式 的数目
//输入：nums = [1,1,1,1,1], target = 3
//输出：5
//解释：一共有 5 种方法让最终目标和为 3
//-1 + 1 + 1 + 1 + 1 = 3
//+1 - 1 + 1 + 1 + 1 = 3
//+1 + 1 - 1 + 1 + 1 = 3
//+1 + 1 + 1 - 1 + 1 = 3
//+1 + 1 + 1 + 1 - 1 = 3
class Solution {
    public static int findTargetSumWays(int[] nums, int target) {
        //转换思路 数组总和为sum 设加法的总和是x 则减法的总和为sum-x
        //所以我们要求的就是 x-(sum-x) =target  所以x = (target+sum)/2
        //本题就转换为 装满容量为x的背包 有多少种方法
        //本题的递推公式为 dp[j] +=dp[j-nums[i]] 为统计装满容量为x 的背包 有多少种方法
        int sum=0;
        for (int i = 0; i <nums.length; i++) {
            sum+=nums[i];
        }
        int x = (target+sum)/2;
        //如果不能被整除 则没有方案 dp数组的含义 dp[j] 为装满容量为j的背包 可以有的多少种方法
        if(Math.abs(target)>sum) return 0;
        //如果不能整除 则说明不能分成两组 找到target值
        if((target+sum)%2==1) return 0;
        int[] dp = new int[x+1];
        //2.初始化dp数组 状态转移方程 注意01背包的便利顺序
        //注意这里的初始化 方法最少有一种
        dp[0] = 1;
        for (int i = 0; i <nums.length ; i++) {
            for (int j = x; j >=nums[i] ; j--) {
                dp[j]+= dp[j-nums[i]];
            }
        }
        //打印dp数组
        return dp[x];
    }

    public static void main(String[] args) {
        int[] s = {5,2,2,7,3,7,9,0,2,3};
        System.out.println(findTargetSumWays(s, 9));
    }
}