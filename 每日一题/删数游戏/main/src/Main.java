import java.io.BufferedReader;
import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long n = in.nextInt();
        long k = in.nextInt();
        long res = findKthNumber(n, k);
        System.out.println(res);
    }
    public static long findKthNumber(long n,long k){
        long left = 1;
        long right =n;
        while (left<right){
            long mid = left+(right-left)/2;
            long count = countNumbers(mid);
            if(count<k){
                left=mid+1;
            }else {
                right = mid;
            }
        }
        return left;
    }

    private static long countNumbers(long x) {
        long count = 0;
        long power= 1;
        while (x/power!=0){
            count+=(x/(power*10))*power;
            if((x/power)%10>1){
                count+=power;
            }else if((x/power)%10==1){
                count+=(x%power)+1;
            }
            power*=10;
        }
        return count;
    }
}