//给你一个正整数 n ，生成一个包含 1 到 n2 所有元素，且元素按顺时针顺序螺旋排列的 n x n 正方形矩阵 matrix
//https://leetcode.cn/problems/spiral-matrix-ii/description/
class Solution {
    public int[][] generateMatrix(int n) {
        //先画图看 挨个处理四条边
        //定义不变量 每次处理边 都是左闭右开的 这样刚好转一圈
        //需要几圈才能遍历完
        //(X,Y) 对应(i,j)
        int[][] res = new int[n][n];
        int loop = 1;
        int count =1;
        //定义起始位置
        int startX = 0, startY = 0;
        int i,j;
        //记录右边的结束位置
        int offset = 1;
        while (loop <= n/2){
            //处理第一条边 注意左闭右开
            for(j = startY; j< n -offset;j++){
                res[startX][j] = count++;
            }
            //往下处理第二条边
            for(i=startX; i< n -offset;i++){
                //纵左标变 横坐标不变
                res[i][j] = count++;
            }
            //处理第三条边
            for(;j>startY;j--){
                res[i][j] = count++;
            }
            //处理第四条
            for(;i>startX;i--){
                res[i][j] = count++;
            }
            //处理完后 向里收缩 变换值
            startX++;
            startY++;
            loop++;
            offset++;
        }
        //如果n为奇数 还需要手动填入最后一个值
        if(n%2!=0){
            res[startX][startY] = count;
        }
        return res;
    }
}