//编写一个高效的算法来搜索 m x n 矩阵 matrix 中的一个目标值 target 。该矩阵具有以下特性
//每行的元素从左到右升序排列。
//每列的元素从上到下升序排列。
class Solution {
    public boolean searchMatrix(int[][] matrix, int target) {
        //每次找右上角的元素 和目标元素对比 如果大于目标值 则这一列的数据都可以排除
        //如果小于 这一行的数据可以排除
        int i = 0;
        int j = matrix[0].length-1;
        while (i<=matrix.length&&j>=0){
            //从右上角位置开始判断
            if(matrix[i][j]>target){
                //这一列都可以排除
                j--;
            }else if(matrix[i][j]<target){
                i++;
            }else {
                //找到结果 返回true
                return true;
            }
        }
        return false;
    }
}