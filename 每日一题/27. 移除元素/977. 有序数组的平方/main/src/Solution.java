//给你一个按 非递减顺序 排序的整数数组 nums，返回 每个数字的平方 组成的新数组，要求也按 非递减顺序 排序。
//https://leetcode.cn/problems/squares-of-a-sorted-array/description/
//直接给每个位置的平方 是不对的 因为要考虑负数的情况
//因为是降序的 所以负数的最小值 一定是 0 位置 他平方后的值 一定是负数里最大的
//那么正数里最大的就是数组的最后一位 比较这两个值 找到较大的 放到新数组里即可 遍历完
class Solution {
    public int[] sortedSquares(int[] nums) {
        int[] res = new int[nums.length];
        //因为这样找的数据是最大的 但是题目要求返回为从小到打 所以赋值的时候从后往前赋值即可
        int k = res.length-1;
        //两个指针重合的时候就结束了
        for(int i = 0, j =nums.length-1;i<=j;){
            if(nums[i]*nums[i]<nums[j]*nums[j]){
                //此时说明正数的值大一点 赋值后 位置前移
                res[k--] = nums[j]*nums[j];
                j--;
            }else {
                //此时说明负数的的平方值大一点 赋值后 位置后移
                //如果相等 那么任意一个位置的值给他都可以 所以小于跟等于的逻辑是一样的
                res[k--] = nums[i]*nums[i];
                i++;
            }
            //不要一次性操作两个位置 这个思路是一次更新一个的
            //操作两个 会越界
//            else {
//                //如果一样大的话 就放入两个
//                res[k] = nums[j]*nums[j];
//                res[--k] = nums[i]*nums[i];
//                k--;
//                j--;
//                i++;
//            }
        }
        //最后res里的值即为结果
        return res;
    }
}