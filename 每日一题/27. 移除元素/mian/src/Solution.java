//https://leetcode.cn/problems/remove-element/
//给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素。元素的顺序可能发生改变。然后返回 nums 中与 val 不同的元素的数量
//假设 nums 中不等于 val 的元素数量为 k，要通过此题，您需要执行以下操作：
//更改 nums 数组，使 nums 的前 k 个元素包含不等于 val 的元素。nums 的其余元素和 nums 的大小并不重要。
//返回 k
class Solution {
    public int removeElement(int[] nums, int val) {
        //定义快慢指针 快指针指向新数组需要的元素(这里也就是不为val的)
        //慢指针指向 可以赋值给新数组的位置 也就是从0 开始
        int slow = 0;
        for(int fast = 0; fast<nums.length;fast++){
            //只要不是删除的元素 就为新数组的元素
            if(nums[fast]!=val){
                nums[slow] = nums[fast];
                //这个位置已经填入 进入下一个位置
                slow++;
            }
        }
        //最后返回填了多少个新值 即为结果
        return slow;
    }
}