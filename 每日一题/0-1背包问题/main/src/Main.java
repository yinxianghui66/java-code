import java.util.Scanner;

//需要带一些研究材料，它们各自占据不同的空间，并且具有不同的价值。
//小明的行李空间为 N，问小明应该如何抉择，才能携带最大价值的研究材料，
// 每种研究材料只能选择一次，并且只有选与不选两种选择，不能进行切割。
//第一行包含两个正整数，第一个整数 M 代表研究材料的种类，第二个正整数 N，代表小明的行李空间。
//第二行包含 M 个正整数，代表每种研究材料的所占空间。
//第三行包含 M 个正整数，代表每种研究材料的价值。
//输出一个整数，代表小明能够携带的研究材料的最大价值
//6 1
//2 2 3 1 5 2
//2 3 1 5 4 3
//输出 5
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n =scanner.nextInt();
        int bagweight = scanner.nextInt();
        int[] with = new int[n];
        int[] value = new int[n];
        for (int i = 0; i <n; i++) {
            with[i] = scanner.nextInt();
        }
        for (int i = 0; i <n; i++) {
            value[i] = scanner.nextInt();
        }
        //定义dp数组含义 dp[i][j] 代表 从[0,i] 位置容量为j的最大价值
        int[][] dp = new int[n][bagweight+1];
        //初始化行 这里的行 应该为 从背包容量 >= 0位置的物品的容量开始放
        for (int i = with[0]; i <=bagweight ; i++) {
            dp[0][i] = value[0];
        }
        //初始化dp数组 并且推导方程
        //状态方程 确认遍历顺序 每个不为0位置的值 都是由上方 和上方推导而来
        //二维的数组 在这里 遍历背包和物品的顺序可以颠倒 因为不管是先遍历那个 都不影响dp的推导
        for (int i = 1; i <n; i++) {
            for (int j = 0; j <=bagweight; j++) {
                //每个位置有两种状态 拿和不拿
                //不拿的话 结果就为 dp[i-1][j] 位置的值 相当于不加这个位置的值 直接取i前面的值
                //拿的话 想当于要加上这个位置的值 则 减去当前位置的重量 并且加上当前位置的价值
                //初始化列
                if(j==0){
                    dp[i][j] = 0;
                    continue;
                }
                if(j-with[i]<0){
                    //如果背包容量 放不下当前位置的值 则只取 不取当前位置的值
                    dp[i][j] = dp[i-1][j];
                    continue;
                }
                dp[i][j] = Math.max(dp[i-1][j],dp[i-1][j-with[i]]+value[i]);
            }
        }
        //打印dp数组 调试
        System.out.println(dp[n-1][bagweight]);

        //使用一维dp数组来解决 dp[j] 为容量为j的背包的最大价值 这里变为一维 要变成滚动数组
        //想当于每一位的值 等于她的上方的值 每次都进行覆盖
        //这里的dp数组的大小 为容量的大小+1
        int[] dp2 = new int[bagweight+1];
        //初始化数据 默认全部为0
        //状态转移方程
        //注意先遍历物品 再遍历背包
        for (int i = 0; i <n ; i++) {
            //这里是倒叙遍历 因为要保证 物品只被使用过一次
            for (int j = bagweight; j>=with[i]; j--) {
                //注意这里的with 和value 取得都是物品的价值和容量
                dp2[j] = Math.max(dp2[j],dp2[j-with[i]]+value[i]);
            }
        }
        System.out.println(dp2[bagweight]);
    }
}
