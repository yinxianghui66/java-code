
public class Sort {
    //1.直接插入排序
    //理解为 将一个元素 插入到有序的数组中 找到合适的位置排序
    //把第一个元素视为有序 从第二个元素开始 依次将后面的元素进行插入排序
    //时间复杂度O(N^2) 最好情况下 O(N) //适用于数据趋于有序的情况下
    //具有稳定性
    public static void insertSort(int[] nums){
        //定义i j j每次都在i的前一位 判断num[j]和 tmp的值大小关系
        //如果 > 则 将该位置的值 赋值到后一位
        //如果 < 则 原地不动
        for (int i = 1; i <nums.length; i++) {
            int j =i-1;
            int temp = nums[i];
            //单趟排序
            while (j>=0){
                if(nums[j]>temp){
                    //将num[j]的值给到j+1位置 也就是这个值应该在原本的后面
                    nums[j+1] = nums[j];
                }else {
                    //如果小于 则可以不动 或者重新赋下值
                    //因为前面都是排序好的 所以前面的数值 都是比temp小的 则直接退出循环
                    break;
                }
                j--;
            }
            //到这 j找到合适位置了 直接将元素插入
            nums[j+1] = temp;
        }
    }
    //2.希尔排序
    //是插入排序的一种优化先将数据分为多组 每组数据少 再对每组数据进行直接插入排序
    //再减少组数 每组数据多(但是前面已经分多组排序过了 所以即使数据多 因为数据趋于有序 所以直接插入就会快)
    //直到组数为1 也就是对全部数据进行插入排序 但是此时经过多组排序 数据已经趋于有序了 所以效率要更高
    //对于组数的取值 目前没有固定的最好办法 尚未有人证明 所以我们就取最基本的 数据长度/2 为组数
    //时间复杂度 不太好分析 因为没法确定每组分组后 数组还是不是逆序的 也就是模拟最坏情况
    //所以这里一般记 O(N^1.3~1.5)
    //这个排序是不稳定的
    public static void shellSort(int[] nums){
        int gap = nums.length;
        while (gap>1){
            shell(nums,gap);
            gap/=2;
        }
        //此时gap<=1 则对整体进行排序 分组数量为1
        shell(nums,1);
    }
    public static void shell(int[] nums,int gap){
        //i从分组的第一位开始
        for (int i = gap; i <nums.length; i++) {
            //j为i的上一位 也就是分组的上一位 这里不是淡出的上一位
            int j =i-gap;
            int temp = nums[i];
            //单趟排序
            while (j>=0){
                if(nums[j]>temp){
                    //将num[j]的值给到j+gap位置 也就是这个值应该在原本的后面
                    nums[j+gap] = nums[j];
                }else {
                    //如果小于 则可以不动 或者重新赋下值
                    //因为前面都是排序好的 所以前面的数值 都是比temp小的 则直接退出循环
                    break;
                }
                //减到分组的上一位
                j-=gap;
            }
            //到这 j找到合适位置了 直接将元素插入
            nums[j+gap] = temp;
        }
    }
    //3.选择排序
    //每次从待排序数组中 选出一个最小值 排序到当前下标 如果找到了 则交换元素位置 如果没找到 则说明这个位置排序完成
    //则继续遍历下一个位置 直到遍历完全部待排序序列
    //时间复杂度O(N^2) 并且不稳定
    public static void selectSort(int[] nums){
        //从初始位置开始 每次从后面 也就是待排数组里找到最小的值 如果找到 则交换位置 如果没找到 则继续下一位
        for (int i = 0; i <nums.length; i++) {
            int minIndex = i;
            int j =i+1;
            for (; j <nums.length; j++) {
                //找到后面最小的数据
                if(nums[minIndex]>nums[j]){
                    //存储最小元素的下标志
                    minIndex =j;
                }
            }
            if(minIndex!=i){
                //此时minIndex为i后面的最小元素的下标志 交换两个位置的值
                swap(nums,i,minIndex);
            }
        }
    }
    public static void swap(int[] nums,int i,int j){
        //交换两个位置的值
        int tmep = nums[i];
        nums[i] = nums[j];
        nums[j] = tmep;
    }
    //4.堆排序  使用向下调整 忘记了 = = 时间复杂度O(N*logn) 并且是不稳定的

    //5.冒泡排序 每两个元素挨个比较 如果前一位大于后一位 则交换数据 直到末尾 交换n遍
    //时间复杂度O(N^2)  这个排序也是稳定的
    public static void bubbleSort(int[] nums){
        for (int i = 0; i <nums.length;i++) {
            boolean falg = false;
            //这里额外-i 因为每一次遍历 都能让最后以为确定为正确位置 所以后面已经排序好的就无需排序了
            for (int j = 0; j <nums.length-1-i; j++) {
                if(nums[j]>nums[j+1]){
                    //交换元素
                    swap(nums,j,j+1);
                    falg=true;
                }else {
                    continue;
                }
                if(falg==false) return;
            }
        }
    }


}
