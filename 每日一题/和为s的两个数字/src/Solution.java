
//链接：https://leetcode.cn/problems/he-wei-sde-liang-ge-shu-zi-lcof/description/
//可以暴力枚举  复杂度为O(N2)
//使用双指针的思想 题目中明确说了是个递增数组 是有序的
//利用单调性  left指向数组最小的数 right指向数组最大的数
//如果left+right>target  说明right最大 [left  right] 这个区间里 无论是那个数 都是大于 target的 可以直接去掉
// 此时right-- 找下一个较小的数 再次比较
//如果left+right<target 说明此时left最小的数 [left  right] 这个区间里 无论是那个数 都是小于 target的
//此时left++ 找下一个较大的数 再次比较
//找到符合条件的就直接返回
//时间复杂度  最坏为每个都遍历一遍 O(N)
class Solution {
    public int[] twoSum(int[] nums, int target) {
        int  left=0;
        int right=nums.length-1;
        while(left<right){
            int sum=nums[left]+nums[right];
            if(sum<target){
                left++;
            }else if(sum>target){
                right--;
            }else{
                return new int[]{nums[left],nums[right]};
            }
        }
        //照顾编译器
        return new int[]{0};
    }
}