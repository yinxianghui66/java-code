//链接:https://leetcode.cn/problems/count-and-say/submissions/476899853/
//使用模拟+双指针的方法 找到每一个要解释的区间 拼接字符串即可
//注意细节 第一位为1 所以只需模拟 n-1 次即可 在写入后 要同步 left 到下一个区间
class Solution {
    public String countAndSay(int n) {
        String ret="1";  //第一个数据即为1
        for(int i=1;i<n;i++){  //解释n-1次ret
            StringBuilder tmp=new StringBuilder();
            int len=ret.length();
            for(int left=0,right=0;right<len;){    //使用双指针 确定每个要解释的区间
               while(right< len && ret.charAt(left)==ret.charAt(right)) right++;
               tmp.append(right-left);  //找到区间边界 解释 有几个 数据
               tmp.append(ret.charAt(left));  //写入数据本身
               left=right;  //写入完后 left 跳出这个区间 找下一个区间
            }
            ret=tmp.toString();
        }
        return ret;
    }
}