//在柠檬水摊上，每一杯柠檬水的售价为 5 美元。顾客排队购买你的产品，（按账单 bills 支付的顺序）一次购买一杯。
//每位顾客只买一杯柠檬水，然后向你付 5 美元、10 美元或 20 美元。
// 你必须给每个顾客正确找零，也就是说净交易是每位顾客向你支付 5 美元。
//注意，一开始你手头没有任何零钱。
//给你一个整数数组 bills ，其中 bills[i] 是第 i 位顾客付的账
// 如果你能给每位顾客正确找零，返回 true ，否则返回 false 。
class Solution {
    public static boolean lemonadeChange(int[] bills) {
        //统计钱币的数量即可 针对不同的钱 找不同的策略
        //注意使用贪心 在20面额时 优先用10+5的组合 这样才能尽可能省下5 来应对10
        int five = 0;
        int ten =0;
        for (int i = 0; i <bills.length ; i++) {
            if(bills[i]==5){
                //为5 则直接手下

                five++;
            }else if(bills[i]==10){
                //遇到10 则需要一张5找零 并且加上得到的10元
                five--;
                ten++;
            }else {

                //遇到20 优先用10+5;
                if(ten>0){
                    ten--;
                    five--;
                }else {
                    //没有10 用三个5
                    five-=3;
                }
            }
            //注意这里是每层都判断 不能出现-的情况
            if(ten<0||five<0) return false;
        }

        return true;
    }

    public static void main(String[] args) {
        int[] s = {5,5,5,5,20,20,5,5,5,5};
        System.out.println(lemonadeChange(s));
    }
}