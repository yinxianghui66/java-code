//给你两个字符串 haystack 和 needle ，
// 请你在 haystack 字符串中找出 needle 字符串的第一个匹配项的下标（下标从 0 开始）。如果 needle 不是 haystack 的一部分，则返回  -1
// 输入：haystack = "sadbutsad", needle = "sad"
//输出：0
//解释："sad" 在下标 0 和 6 处匹配。
//第一个匹配项的下标是 0 ，所以返回 0 。
//https://leetcode.cn/problems/find-the-index-of-the-first-occurrence-in-a-string/description/?envType=study-plan-v2&envId=top-interview-150
class Solution {
    public static int strStr(String haystack, String needle) {
        //取needle的第一个位置 去字符串中查找 然后取相同长度的字符串 比较
        char first = needle.charAt(0);
        String s= "";
        int ret=-1;
        for(int i=0;i<haystack.length();i++){
            //如果最后截取长度大于字符串长度 则直接返回
            if(i+needle.length()>haystack.length()) return -1;
            //找到needle第一个出现的位置 并且更新出现的位置
            if(haystack.charAt(i)==first){
                s=haystack.substring(i,i+needle.length());
                ret=i;
            }
            //比较长度 如果相同直接返回
            if(s.equals(needle)) return ret;
        }
        return -1;
    }

}