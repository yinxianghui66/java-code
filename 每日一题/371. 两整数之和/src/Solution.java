//链接:https://leetcode.cn/problems/sum-of-two-integers/submissions/475276423/
//使用位运算 计算结果 因为异或的无进位的相加 所以我们只需要找出哪里需要进位
//并且将其<<1 代表进位 再次异或 一直到 进位为0 此时说明相加完成
class Solution {
    public int getSum(int a, int b) {
        //重复过程 一直到进位为 0 即相加完成
        while (b!=0){
            //计算出无进位相加的结果
            int x=a^b;
            //找到进位 并且进位(<<1)
            int carry=(a&b)<<1;
            a=x;
            b=carry;
        }
        return a;
    }
}