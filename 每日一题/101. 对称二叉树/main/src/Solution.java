//给你一个二叉树的根节点 root ， 检查它是否轴对称。
class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode() {}
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
  }

class Solution {
    public boolean isSymmetric(TreeNode root) {
        return compare(root.left,root.right);
    }
    public boolean compare(TreeNode left,TreeNode right){
        //使用后序遍历 递归判断左右子数是否可以反转 也就是值 是否一致
        //判断 左子树的左子树 和右子数的的右字数 val是否都一致
        //左子树的右子树和右子树的左子树 val是否一致
        //全部满足 则可以对称
        if(left==null&&right!=null){
            return false;
        }else  if(left!=null&&right==null){
            return false;
        }else if(left==null&&right==null){
            return true;
        }else if(left.val!=right.val){
            return false;
        }
        boolean outside = compare(left.left,right.right);
        boolean inside = compare(left.right,right.left);
        return outside&&inside;
    }
}