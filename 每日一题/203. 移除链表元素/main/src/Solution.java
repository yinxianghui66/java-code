//https://leetcode.cn/problems/remove-linked-list-elements/
//删除链表的思路就是 找到要删除的位置 找到它的前一位 让他的前一位 指向当前位置的下一位即刻
//使用虚拟头节点 可以更方便的删除和新增 让他为头节点的前一个
 class ListNode {
     int val;
 ListNode next;
 ListNode() {}
     ListNode(int val) { this.val = val; }
     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}

class Solution {
    public ListNode removeElements(ListNode head, int val) {
        ListNode dummy = new ListNode();
        dummy.next = head;
        ListNode cur = dummy;
        while(cur.next!=null){
            if(cur.next.val==val){
                cur.next = cur.next.next;
            }else {
                cur=cur.next;
            }
        }
        return dummy.next;
    }
}