//链接 ：https://leetcode.cn/problems/find-minimum-in-rotated-sorted-array/
//找到二段性 找中间值和最后值进行比较 如果大于  说明最小值在右边
//如果小于 说明最小值在左边区间   题目明确说明了不会有等于的情况

public class Solution {
    public int findMin(int[] nums) {
        int left=0,right=nums.length-1;
        int last=nums[right]; //最后位置的值
        while (left<right){
            int mid=left+(right-left)/2;
            if(nums[mid]>last){
                left=mid+1;
            }else {
                right=mid;
            }
        }
        return nums[left];
    }
}
