import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

class test {


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n =scanner.nextInt();
        int q = scanner.nextInt();
        int a[] = new int[n];
        for(int i=0;i<n;i++){
            a[i] = scanner.nextInt();
        }
        int w[] = new int[q];
        for(int i=0;i<q;i++){
            w[i] = scanner.nextInt();
        }
        List<String> res = solve(n,q,a,w);
        String[] s = res.toArray(new String[0]);
        for (int i=0;i<s.length;i++){
            System.out.print(s[i]+" ");
        }
    }
    public static List<String> solve(int n,int q,int[] a,int[] w){
        int[] prefixSum= new int[n+1];
        for(int i=1;i<=n;i++){
            prefixSum[i] = prefixSum[i-1]+a[i-1];
        }
        List<String> answers = new ArrayList<>();
        for(int target:w){
            int left =1,right=1;
            boolean found = false;
            while (right<=n){
                int sum = prefixSum[right]-prefixSum[left-1];
                if(sum==target){
                    answers.add("Yes");
                    found = true;
                    break;
                }else if(sum<target){
                    right++;
                }else {
                    left++;
                }
            }
            if(!found){
                answers.add("No");
            }
        }
        return answers;
    }
}
