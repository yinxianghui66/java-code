import java.util.Scanner;

 class Main {


    public static void main(String[] args) {
        int mod = 100000007;
        Scanner scanner = new Scanner(System.in);
        int n =scanner.nextInt();
        int[] apples = new int[n];
        for(int i=0;i<n;i++){
            apples[i]=scanner.nextInt();
        }
        int[][] dp = new int[n+1][n];
        //初始化第一天
        for(int j=0;j<n;j++){
            dp[1][j] = apples[j];
        }
        //计算后续天数
        for(int i=2;i<=n;i++){
            for(int j =0;j<n;j++){
                int max=0;
                for (int k=0;k<n;k++){
                    if(k!=j){
                        max = Math.max(max,dp[i-1][k]+(int) ((long)apples[j]*Math.pow(2,i-1)%mod));
                    }
                }
                dp[i][j] = max%mod;
            }
        }
        int res =0;
        for(int j=0;j<n;j++){
            res = Math.max(res,dp[n][j]);
        }
        System.out.println(res);
    }
}
