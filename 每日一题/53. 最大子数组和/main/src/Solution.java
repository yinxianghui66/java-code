//给你一个整数数组 nums ，请你找出一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
//子数组是数组中的一个连续部分。
//输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
//输出：6
//解释：连续子数组 [4,-1,2,1] 的和最大，为 6 。
class Solution {
    public static int maxSubArray(int[] nums) {
        //主要思路 每次取结果和  如果和为负数 则不管后面是啥 都会对这个和做减小的操作
        //所以遇到和为负数 直接丢弃 从下一位开始 继续累加 记录最大和
        int max = Integer.MIN_VALUE;
        int sum =0 ;
        for (int i = 0; i <nums.length; i++) {
            if(sum<0){
                //丢弃该位置 直接从下一位开始
                sum = nums[i];
                max = Math.max(max,sum);
                continue;
            }
            sum+=nums[i];
            max = Math.max(max,sum);
        }
        return max;
    }

    public static void main(String[] args) {
        int[] nums ={-2,1};
        System.out.println(maxSubArray(nums));
    }
}