
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        List list=new ArrayList<>();
        int n=scanner.nextInt();
        int k=scanner.nextInt();
        int arr1[]=new int[n];
        int arr2[]=new int[n];
        for (int i = 0; i <n; i++) {
            arr1[i]=scanner.nextInt();
        }
        for (int i = 0; i <n; i++) {
            arr2[i]=scanner.nextInt();
        }
        int res[]=new int[n*n];
        int t=0;
        for (int i = 0; i <n ; i++) {
            for (int j = 0; j <n; j++) {
                res[t]=arr1[i]+arr2[j];
                t++;
            }
        }
        Arrays.sort(res);
        for (int i = res.length-1; i >=0; i--) {
                System.out.print(res[i]+" ");
                k--;
                if(k==0){
                    break;
                }
        }
    }
}
