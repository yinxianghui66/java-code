//链接:https://leetcode.cn/problems/missing-two-lcci/
//这个题目就是 找到丢失的数字 + 只出现一次的数字Ⅲ
//首先 将数组的中的数异或的结果
// 再和 1~n+2 的数 全部异或 得到的结果就是只出现一次的数 a 和 b 的异或结果  a ^ b (此时问题就转换为只出现一次数字的Ⅲ)
//(也就是除了 a 和 b 其他的数字都出现了两次 找出出现一次的 a 和 b)
//因为异或运算是 相同为 0 相异为1 所以我们找到ans中第一位异或结果为1的数 就说明 a 和 b 原来的二进制对应的位值的值是相异的
//所以我们根据这一位的值 给数组中所有的数 和 1 ~ n+2 这一位的值分成两组 这一位为 0 的一组 这一位为 1 的一组 此时 a 和 b 一定是分在两组的
//所以此时 这两组中 再进行异或(只出现一次的数字Ⅰ) 剩下的值 即为 a 和 b
class Solution {
    public int[] missingTwo(int[] nums) {
        int ans=0;
        for (int i = 0; i <nums.length; i++) {
            ans^=nums[i];
        }
        //因为缺失了两个数 所以这里要+2
        for (int i = 1; i <=nums.length+2; i++) {
            ans^=i;
        }
        //
        int diff=0;
        for (int i = 0; i < 32; i++) {
            if(((ans>>i)&1)==1){ //找到a^b结果为1 的二进制位置
                diff=i;
            }
        }
        int[] ret=new int[2];
        //将值分为两组进行异或
        for (int i = 0; i <nums.length; i++) {
            if(((nums[i]>>diff)&1)==1) ret[1]^=nums[i];
            else ret[0]^=nums[i];
        }
        //再异或 1 ~ n+2 得到结果
        for (int i = 1; i <=nums.length+2 ; i++) {
            if(((i>>diff)&1)==1) ret[1]^=i;
            else ret[0]^=i;
        }
        return ret;
    }
}