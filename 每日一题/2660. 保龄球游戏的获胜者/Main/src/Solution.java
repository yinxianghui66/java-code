//给你两个下标从 0 开始的整数数组 player1 和 player2 ，分别表示玩家 1 和玩家 2 击中的瓶数。
//保龄球比赛由 n 轮组成，每轮的瓶数恰好为 10 。
//假设玩家在第 i 轮中击中 xi 个瓶子。玩家第 i 轮的价值为：
//如果玩家在该轮的前两轮的任何一轮中击中了 10 个瓶子，则为 2xi 。
//否则，为 xi 。
//玩家的得分是其 n 轮价值的总和。
//返回
//如果玩家 1 的得分高于玩家 2 的得分，则为 1 ；
//如果玩家 2 的得分高于玩家 1 的得分，则为 2 ；
//如果平局，则为 0 。
//... 模拟即可 主要看思路
//tempPos 和tenPos 注意初始值必须为-3 否则会出错
//使用一个变量记录上次出现10环的位置 判断距离是否大于等于2 是的话就将这个位置的值翻倍相加
//两个人 就写一个方法 计算每个人的分数
class Solution {
    public static int isWinner(int[] player1, int[] player2) {
        int count1=getPoint(player1);
        int count2=getPoint(player2);
        if(count1>count2) return 1;
        else if(count1<count2) return 2;
        else return 0;
    }
    //计算得分总数
    public static int getPoint(int[] player){
        int ans=0;
        //记录上一次出现十环的位置 初始值从-3开始
        int tenPos=-3;
        for(int i=0;i<player.length;i++){
            int curPoint=player[i];
            int tempPos=tenPos;
            if(curPoint==10){
                tenPos=i;

            }
            //这里判断不能是之前的tenPos 而是赋值之前的tempPos
            if(i-tempPos<=2){
                //是否能翻倍
                curPoint*=2;
            }
            ans+=curPoint;
        }
        return ans;
    }


}