
//定一个非空的字符串 s 检查是否可以通过由它的一个子串重复多次构成

class Solution {
    public boolean repeatedSubstringPattern(String s) {
        //给s 加上s 得到ss 去掉ss的首位位置 判断ss中是否出现s 即可判断是否由字串构成
        int len =s.length();
        StringBuffer stringBuffer = new StringBuffer(s);
        stringBuffer.append(s);
        stringBuffer.deleteCharAt(stringBuffer.length()-1);
        stringBuffer.deleteCharAt(0);
        stringBuffer.delete(stringBuffer.length(),stringBuffer.length());
//        return  stringBuffer.toString().contains(s);
        //使用kmp算法 求s的前缀和数组 如果字符串长度的 对应的next数组  可以被整除 则一定出现了
        int[] next =getNext(s);
        //注意需要加上该位置不等0的情况 才能返回true 否则为0 肯定是不可以组成的
        return  next[len-1] !=0 &&len % (len - next[len-1]) ==0;
    }
    public static  int[] getNext(String ss){
        //求s的前缀和数组
        //1.初始化数据
        char[] s= ss.toCharArray();
        //j指向前缀的末尾位置 i指向后缀的末尾 也就是模式串中 每个前缀共有的最后一个字符的位置 一般都为 j = 0 i =1
        int j = 0;
        int[] next = new int[s.length];
        //数组第一位为 0 所有的都是
        next[0] = 0;
        //2.处理不匹配的情况 回退到该位置的上一位 继续
        for(int i=1;i<s.length;i++){
            while ((j>0)&&(s[i]!=s[j])){
                //不匹配的情况 j 找到他的上一位 进行回退 注意 这里是while 可能会回退多次
                //最后一直回退到 0位置 位置 此时j为0 则找不到相同的结果
                j= next[j-1];
            }
            //3.处理匹配的情况 j 位置移动到下一位 i会跟着循环走
            if(s[i]==s[j]){
                j++;
            }
            //4.赋值next数组 j位置最长相等前后缀的长度
            next[i] = j;
        }
        return next;
    }
}