class ListNode{
    int val;
    ListNode next;
    ListNode(){}
    ListNode(int val){
        this.val= val;
    }
}
class MyLinkedList {
    int size;
    ListNode head;
    public MyLinkedList() {
        size = 0;
        //此时这里的head已经是虚拟头节点了！！！！
        head = new ListNode(0);
    }
    //获取第n个节点
    public int get(int index) {
        //判断位置合法性
        if(index<0||index>=size){
            return -1;
        }
        ListNode cur = head;
        //循环index 找到该位置的值
        for(int i= 0;i<=index;i++){
            cur = cur.next;
        }
        return cur.val;
    }

    public void addAtHead(int val) {
        //头插法
        //使用虚拟头节点 指向节点的前面
        ListNode cur = head;
        //创建新节点 将新节点 插入到虚拟头节点后 head前即可
        ListNode newNode = new ListNode(val);
        //注意顺序 先吧head的值保存 也就是虚拟头节点的下一个值
        //然后再链接头部
        newNode.next = cur.next;
        cur.next = newNode;
        size++;
    }

    public void addAtTail(int val) {
        //尾插法 先找到尾部节点的位置 然后直接插入
        ListNode cur = head;
        while (cur.next!=null){
            cur = cur.next;
        }
        //此时cur的位置即为最后节点
        cur.next = new ListNode(val);
        size++;
    }

    public void addAtIndex(int index, int val) {
        if(index>size) return;
        if(index<0) index = 0;

        //第n个节点前插入结点
        //首先要找到n-1个节点的位置 才能插入
        ListNode cur = head;
        //找到n-1位置 这里不是等于
        for(int i = 0;i<index;i++){
            cur = cur.next;
        }
        //找到对应位置 插入元素
        ListNode newAdd = new ListNode(val);
        //注意赋值顺序
        newAdd.next = cur.next;
        cur.next = newAdd;
        size++;
    }

    public void deleteAtIndex(int index) {
        if(size<0||index>=size) return;
        //创建虚拟头节点
        ListNode cur = head;
        //找到 index - 1位置
        for(int i =0 ;i<index;i++){
            cur = cur.next;
        }
        //找到位置 删除元素
        //直接断开连接即可
        cur.next = cur.next.next;
        size--;
    }
}
//下面为错误示范的bug版本 自己多创建一次虚拟头节点 导致错误

//class ListNode{
//    int val;
//    ListNode next;
//    ListNode(){}
//    ListNode(int val){
//        this.val= val;
//    }
//}
//class MyLinkedList {
//    int size;
//    ListNode head;
//    public MyLinkedList() {
//        size = 0;
//        head = new ListNode(0);
//    }
//    //获取第n个节点
//    public int get(int index) {
//        //判断位置合法性
//        if(index<0||index>=size){
//            return -1;
//        }
//        ListNode currentNode = new ListNode();
//        currentNode.next = head;
//        ListNode cur = currentNode;
//        //循环index 找到该位置的值
//        for(int i= 0;i<=index;i++){
//            cur = cur.next;
//        }
//        return cur.val;
//    }
//
//    public void addAtHead(int val) {
//        //头插法
//        //使用虚拟头节点 指向节点的前面
//        ListNode cur = head;
//        //创建新节点 将新节点 插入到虚拟头节点后 head前即可
//        ListNode newNode = new ListNode(val);
//        //注意顺序 先吧head的值保存 也就是虚拟头节点的下一个值
//        //然后再链接头部
//        newNode.next = cur.next;
//        cur.next = newNode;
//        size++;
//    }
//
//    public void addAtTail(int val) {
//        //尾插法 先找到尾部节点的位置 然后直接插入
//        ListNode dummyNode = new ListNode();
//        dummyNode.next = head;
//        ListNode cur = dummyNode;
//        while (cur.next!=null){
//            cur = cur.next;
//        }
//        //此时cur的位置即为最后节点
//        cur.next = new ListNode(val);
//        size++;
//    }
//
//    public void addAtIndex(int index, int val) {
//        if(index>size) return;
//        if(index<0) index = 0;
//
//        //第n个节点前插入结点
//        //首先要找到n-1个节点的位置 才能插入
//        ListNode dummyNode = new ListNode();
//        dummyNode.next = head;
//        ListNode cur = dummyNode;
//        //找到n-1位置 这里不是等于
//        for(int i = 0;i<index;i++){
//            cur = cur.next;
//        }
//        //找到对应位置 插入元素
//        ListNode newAdd = new ListNode(val);
//        //注意赋值顺序
//        newAdd.next = cur.next;
//        cur.next = newAdd;
//        size++;
//    }
//
//    public void deleteAtIndex(int index) {
//        if(size<0||index>=size) return;
//        //创建虚拟头节点
//        ListNode dummyNode = new ListNode();
//        dummyNode.next =head;
//        ListNode cur = dummyNode;
//        //找到 index - 1位置
//        for(int i =0 ;i<index;i++){
//            cur = cur.next;
//        }
//        //找到位置 删除元素
//        //直接断开连接即可
//        cur.next = cur.next.next;
//        size--;
//    }
//}