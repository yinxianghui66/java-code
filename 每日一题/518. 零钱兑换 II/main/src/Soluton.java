//给你一个整数数组 coins 表示不同面额的硬币，另给一个整数 amount 表示总金额
//请你计算并返回可以凑成总金额的硬币组合数。如果任何硬币组合都无法凑出总金额，返回 0
//假设每一种面额的硬币有无限个
//题目数据保证结果符合 32 位带符号整数
//输入：amount = 5, coins = [1, 2, 5]
//输出：4
//解释：有四种方式可以凑成总金额：
//5=5
//5=2+2+1
//5=2+1+1+1
//5=1+1+1+1+1
class Solution {
    public static int change(int amount, int[] coins) {
        //本题明确可以使用无限次背包里的硬币 则为完全背包问题
        //1.定义dp[j] 的含义为 装满容量为j的背包 所可以得到的最大价值
        int[] dp = new int[amount+1];
        //2.初始化dp数组 dp[0] 容量为0 也是一种情况
        dp[0] = 1; //其他不为0的位置 也初始化为0即可
        //3.状态转移方程
        for (int i = 0; i <coins.length; i++) {
            for (int j = coins[i]; j <=amount; j++) {
                //求的是方法的个数 则递推公式为
                //dp[j] = Math.max(dp[j],dp[j-i]+coins[i]);
                dp[j]+=dp[j-coins[i]];
            }
        }
        //4.遍历顺序 从前向后
        return dp[amount];
        //5.打印dp数组
    }

    public static void main(String[] args) {
        int[] s= {1,2,5};
        System.out.println(change(5, s));
    }
}