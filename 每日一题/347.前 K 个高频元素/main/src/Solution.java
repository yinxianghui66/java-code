
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

//给你一个整数数组 nums 和一个整数 k ，请你返回其中出现频率前 k 高的元素。你可以按 任意顺序 返回答案
//想用哈希表 统计个数 并且存储出现的次数 最后返回k个
class Solution {
    public int[] topKFrequent(int[] nums, int k) {
        HashMap<Integer,Integer> map = new HashMap<>();
        int[] res = new int[k];
        //遍历数组 统计各个数字出现的次数
        for(int  i =0;i<nums.length;i++){
            //以下标为标准 该位置出现次数++
            map.put(nums[i],map.getOrDefault(nums[i],0)+1);
        }
        //维护一个k个的优先级队列 也就是小顶堆
        PriorityQueue<int[]> pq = new PriorityQueue<>((s2,s1)->s2[1]-s1[1]);
        //遍历哈希表
        for(Map.Entry<Integer,Integer> entry:map.entrySet()){
            if(pq.size()<k){
                //堆元素个数小与要求的个数 直接加入
                pq.add(new int[]{entry.getKey(),entry.getValue()});
            }else {
                if(entry.getValue()>pq.peek()[1]){ //当前位置出现的次数 大于堆顶的元素
                    //弹出堆顶
                    pq.poll();
                    //加入堆
                    pq.add(new int[]{entry.getKey(),entry.getValue()});
                }
            }
        }
        System.out.println(map);
        //最后弹出堆里的k个元素 取出对应的key 小跟堆 先弹出堆是出现频率最小的 所以需要逆序赋值 才能让 0 - k 为出现次数从大到校
        for(int i = k-1;i>=0;i--){
            res[i] = pq.poll()[0];
        }

        return res;
    }

}