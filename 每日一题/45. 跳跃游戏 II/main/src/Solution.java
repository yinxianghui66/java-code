//给定一个长度为 n 的 0 索引整数数组 nums。初始位置为 nums[0]。
//每个元素 nums[i] 表示从索引 i 向前跳转的最大长度。换句话说，如果你在 nums[i] 处，你可以跳转到任意 nums[i + j] 处:
//返回到达 nums[n - 1] 的最小跳跃次数。生成的测试用例可以到达 nums[n - 1]。
//输入: nums = [2,3,1,1,4]
//输出: 2
//解释: 跳到最后一个位置的最小跳跃数是 2。
//     从下标为 0 跳到下标为 1 的位置，跳 1 步，然后跳 3 步到达数组的最后一个位置。
class Solution {
    public int jump(int[] nums) {
        //转换思维 看最大的跳跃范围 如果大于等于最后位置即可
        //在跳跃范围中 找到最大的数 以此来保证跳跃的最小步数
        if(nums.length==1) return 0;
        int  cur =0;
        //下一步的覆盖范围
        int next = 0;
        //结果
        int result = 0;
        for(int i = 0;i<nums.length;i++){
            //记录下一步可以跳多远 取下一步跳的最大值
            next = Math.max(i+nums[i],next);
            //如果i遍历到当前覆盖范围的终点 但是并没有到终点 既需要再走一步
            if(i==cur&&cur!=nums.length-1){
                result++;
                //并且更新下一步的覆盖范围
                cur = next;
                //如果下一步范围可以到达终点 既返回结果
                if(cur >=nums.length-1) return result;

            }
        }
        return result;
    }
}