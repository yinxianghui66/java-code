import com.sun.org.apache.bcel.internal.generic.NEW;

//给定一个长度为 n 的 0 索引整数数组 nums。初始位置为 nums[0]。
//每个元素 nums[i] 表示从索引 i 向前跳转的最大长度。换句话说，如果你在 nums[i] 处，你可以跳转到任意 nums[i + j] 处:
//0 <= j <= nums[i]
//i + j < n
//返回到达 nums[n - 1] 的最小跳跃次数。生成的测试用例可以到达 nums[n - 1]。
class Solution2 {
    public int jump(int[] nums) {
        if(nums.length==1) return 0;
        //覆盖范围 这个用来判断是否走到终点 保证他是范围区间里取的最远的
        int index = 0;
        //下一步的覆盖范围 每一步的覆盖范围 最后取区间的最大覆盖范围
        int next= Integer.MIN_VALUE;
        int res =0;
        for (int i = 0; i <nums.length; i++) {
            //每次取最大的覆盖范围
            next = Math.max(next,i+nums[i]);
            //i走到了覆盖范围的终点 注意这里重点 i== index 走到了覆盖范围 则必须要继续走下一步
            if(index!=nums.length-1&&i==index){
                //当前覆盖范围还未到重点 则继续到下一位 更新最大的覆盖范围
                res+=1;
                //增加覆盖范围 注意这里取最大的
                index=next;
                if(index>=nums.length-1) return res;
            }
        }
        return res;
    }
}