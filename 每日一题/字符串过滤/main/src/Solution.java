import java.util.*;


public class Solution {
    /**
     * 删除输入字符串中所有在模式字符串中出现的字符
     * @param strSrc string字符串 源字符串
     * @param strPat string字符串 模式字符串
     * @return string字符串
     */
    public static String stringFilter (String strSrc, String strPat) {
        // write code here
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i <strSrc.length(); i++) {
            if(!get(strSrc.charAt(i),strPat)){
                stringBuffer.append(strSrc.charAt(i));
            }
        }
        return stringBuffer.toString();
    }
    public static boolean get(char i,String strPat){
        for (int j = 0; j <strPat.length(); j++) {
            if(strPat.charAt(j)==i){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        String strSrc = "welcome to amazing seasun";
        String strPat = "waecdf";
        stringFilter(strSrc,strPat);
    }

}