import java.util.Scanner;
import java.util.Stack;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        int[] heights = new int[10000];
        int j=0;
        String s = in.nextLine();
        for (int i = 0; i <s.length(); i++) {
            if(s.charAt(i)>='0'&&s.charAt(i)<='9'){
             heights[j++] = s.charAt(i)-'0';
            }
        }
        System.out.println(maxArea(heights,j));
    }
    public static int maxArea(int[] heights,int s){
        int n = s;
        if(n<2) return 0;

        Stack<Integer> stack = new Stack<>();
        int max = 0;
        for (int i = 0; i <= n; i++) {
            while (!stack.isEmpty()&&(i==n||heights[stack.peek()]>=heights[i])){
                int height = heights[stack.pop()];
                int width = stack.isEmpty()?i:stack.peek();
                max = Math.max(max,height*width);
            }
            stack.push(i);
        }
        return max;
    }
}