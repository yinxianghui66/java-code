import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        String s = in.nextLine();
        int[] nums = new int[100];
        int j =0;
        int target = 0;
        boolean isTarget = false;
        for (int i = 0; i <s.length(); i++) {
            if(s.charAt(i)==']'){
                isTarget = true;
            }
            if(s.charAt(i)>='0'&&s.charAt(i)<='9'&&isTarget==false){
                nums[j++] = s.charAt(i)-'0';
            }
            if(isTarget&&s.charAt(i)>='0'&&s.charAt(i)<='9'){
                target = s.charAt(i)-'0';
            }

        }

        int[] res =twoSum(nums, target,j);
        for (int i = 0; i < res.length; i++) {
            System.out.print(res[i]+" ");
        }
    }
    public static int[] twoSum(int[] nums,int target,int j){
        Map<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i < j; i++) {
            int complement = target - nums[i];
            if(map.containsKey(complement)){
                return new int[]{map.get(complement),i};
            }
            map.put(nums[i],i);
        }
        return new int[]{1,1};
    }

}