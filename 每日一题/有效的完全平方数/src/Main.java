//https://leetcode.cn/problems/valid-perfect-square/
//暴力求太慢了 使用二分查找
public class Main {
    public static boolean isPerfectSquare2(int num) {
        long a=0;
       while (a*a<num){
           a++;
       }
       if(a*a==num){
           return true;
       }
        return false;
    }
    //忘干净了 二分查找 再看！！
    public static boolean isPerfectSquare(int num) {
        long left=0;
        long right=num;
        while (left<=right){
            long mid=(right-left)/2+left;
            long ret=mid*mid;
            if(ret<num){
                left=mid+1;
            }else if(ret>num){
                right=mid-1;
            }else {
                return true;
            }
        }
        return false;
    }
    public static void main(String[] args) {
        System.out.println(isPerfectSquare(14));
    }
}
