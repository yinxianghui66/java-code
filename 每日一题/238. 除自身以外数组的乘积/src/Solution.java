//链接:https://leetcode.cn/problems/product-of-array-except-self/description/
//和上一题差不多 使用前缀积和后缀积数组
//注意细节
//1.f[0] 和g[n-1] 位置 因为是乘法 所以默认值应该为1
//2.注意前缀积和后缀积数组的初始化顺序 前缀积是从前往后( 0 - i-1 区间的积) 后缀积为从后往前( i+1 - n-1 区间的积)
//结果为 前缀积 * 后缀积 !
public class Solution {
    public int[] productExceptSelf(int[] nums) {
        int n=nums.length;
        int f[]=new int[n];
        int g[]=new int[n];
        //预处理前缀积数组
        f[0]=g[n-1]=1; //注意这里是乘积 使用1为默认值才行
        //从1 开始 默认值已经给过了
        for (int i = 1; i <n; i++) {
            //每个位置的前缀积 = 上一位的前缀积 * 上一位本身的值
            f[i]=f[i-1]*nums[i-1];
        }
        //预处理后缀积数组 (注意从后往前) 并且已经给初值 所以从n-2开始
        for (int i = n-2; i >=0 ; i--) {
            //后缀积同理
            g[i]=g[i+1]*nums[i+1];
        }
        //使用数组
        int res[]=new int[n];
        for (int i = 0; i <n; i++) {
            res[i]=f[i]*g[i];
        }
        return  res;
    }
}
