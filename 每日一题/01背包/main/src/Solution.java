import java.util.*;


public class Solution {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 计算01背包问题的结果
     * @param V int整型 背包的体积
     * @param n int整型 物品的个数
     * @param vw int整型二维数组 第一维度为n,第二维度为2的二维数组,vw[i][0],vw[i][1]分别描述i+1个物品的vi,wi
     * @return int整型
     */
    public static int knapsack (int V, int n, int[][] vw) {
        // write code here
        //定义dp数组的含义 dp[j] 为装满容量背包为j的最大价值
        int[] dp = new int[V+1];
        //2.初始化dp数组 这里默认为0即可
        dp[0] = 0;
        //3.状态转移方程 确定遍历顺序 这里使用滚动数组 所以为了保证每个位置的值只用一次 所以是从后向前遍历
        for (int i = 0; i < n; i++) {
            for (int j = V; j>=vw[i][0] ; j--) {
                if(j>=vw[i][0]){
                    dp[j] = Math.max(dp[j],dp[j-vw[i][0]]+vw[i][1]);
                }
            }
        }
        return dp[V];
    }

    public static void main(String[] args) {
        int[][] s = {{1,3},{10,4}};
        System.out.println(knapsack(10, 2, s));
    }
}