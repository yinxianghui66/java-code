package org.example;
//给你链表的头节点 head ，每 k 个节点一组进行翻转，请你返回修改后的链表。
//k 是一个正整数，它的值小于或等于链表的长度。如果节点总数不是 k 的整数倍，那么请将最后剩余的节点保持原有顺序。
//你不能只是单纯的改变节点内部的值，而是需要实际进行节点交换。
//1.计算出需要逆序几组数据(遍历链表 找出长度)
//2.使用变量记录 每一组逆序完的尾节点 下次逆序 从尾节点继续逆序下一组 使用头插法逆序链表
//3.直到逆序完n组 剩下不足k的长度的节点直接连接到 尾节点的后面
class ListNode {
      int val;
      ListNode next;
      ListNode() {}
     ListNode(int val) { this.val = val; }
     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 }

class Solution {
    public ListNode reverseKGroup(ListNode head, int k) {
        //1.计算需要逆序的组数
        int n=0;
        ListNode cur=head;
        while(cur!=null){
            cur=cur.next;
            n++;
        }
        n/=k;
        ListNode newHead=new ListNode(0);
        ListNode prev=newHead;
        cur=head;
        //2.逆序n次数组 每次逆序k个节点 注意记录每组逆序的尾节点 下次逆序从尾节点继续逆序下一组
        for(int i=0; i < n ;i++){
            ListNode tmp=cur;
            for(int j= 0; j < k ;j++){
                //头插法
                ListNode next=cur.next;
                cur.next=prev.next;
                prev.next=cur;
                cur=next;
            }
            prev=tmp;
        }
        //3.连接剩余的节点
        prev.next=cur;
        return newHead.next;
    }
}