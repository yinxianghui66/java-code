//给你两个二进制字符串 a 和 b ，以二进制字符串的形式返回它们的和。
//高精度算术
//链接:https://leetcode.cn/problems/add-binary/description/
//示例 1：
//输入:a = "11", b = "1"
//输出："100"
//示例 2：
//输入：a = "1010", b = "1011"
//输出："10101"
//模拟列数运算的过程 用t计算进位 从后向前 用进位和当前位进行加法运算
class Solution {
    public String addBinary(String a, String b) {
        int cur1= a.length() - 1, cur2 = b.length() -1;
        int t=0;
        StringBuilder ret = new StringBuilder();
        while (cur1>=0 || cur2 >=0 || t!=0){
            if(cur1>=0) t+= a.charAt(cur1--) - '0';
            if(cur2 >= 0) t+= b.charAt(cur2--) - '0';
            //当有一个长度不够时 相当于自动补0计算
            ret.append((char)((char) t %2 + '0'));
            t/=2;
        }
        return ret.reverse().toString();
    }
}