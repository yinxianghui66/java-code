import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

class Solution {
    public int eraseOverlapIntervals(int[][] intervals) {
        if(intervals.length==0) return 0;
        //注意 先将元素 按照左区间排序
        Arrays.sort(intervals,(a, b)->Integer.compare(a[0],b[0]));
        int res =1;
        for (int i = 1; i <intervals.length ; i++) {
            //统计重叠的元素 用总元素- 重叠元素的个数 = 不重叠的元素
            //此时说明两个区间不重合 则更新区间的范围
            if(intervals[i][0]<intervals[i-1][1]){
                intervals[i][1] = Math.min(intervals[i][1],intervals[i-1][1]);
            }else {
                //统计重叠的区间个数
                res++;
            }
        }
        return intervals.length-res;
    }
}