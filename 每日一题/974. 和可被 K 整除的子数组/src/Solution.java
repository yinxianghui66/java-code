import java.util.HashMap;
import java.util.Map;

//链接:https://leetcode.cn/problems/subarray-sums-divisible-by-k/description/
//前缀和+哈希表+同余定理
//这题和上一题细节问题大致一致
//最主要考察同余定理 即 如果 (a-b) % p=k....0  则  a%p == b%p
//加上如果是正数%负数 我们需要对其进行修正 则 (a % p + p )% p
//哈希表中存储[0 - n-1] 前缀和的余数  也就是找到这个区间有多少个余数 等于 (sum % k +k)%k
class Solution {
    public int subarraysDivByK(int[] nums, int k) {
        Map<Integer,Integer> hash=new HashMap<>();
        //注意默认边界位置存储(这里存的是前缀和的余数)
        hash.put(0%k,1);
        int sum=0,ret=0;
        for (int i = 0; i <nums.length; i++) {
            sum+=nums[i];  //计算前缀和
            int r=(sum%k+k)%k;   //同余定理 && java中如果负数和正数取余 修正
            ret+=hash.getOrDefault(r,0);  //统计结果
            hash.put(r,hash.getOrDefault(r,0)+1);  //存入前缀和的余数到哈希表
        }
        return ret;
    }
}