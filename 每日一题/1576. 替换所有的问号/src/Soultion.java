//链接:https://leetcode.cn/problems/replace-all-s-to-avoid-consecutive-repeating-characters/
//使用模拟的思想 考虑边界情况 使用代码展示出来即可
//本题 替换问号 需要考虑替换的字符 不能和这一位的前一位和后一位相同
//并且考虑边界情况 当 ? 为第一位时 只需要考虑后一位即可
//当 ? 为最后一位时 只需要考虑前一位即可
class Solution {
    public String modifyString(String ss) {
        char[] s=ss.toCharArray();  //转换成字符数组
        int n=s.length;
        for(int i=0;i<n;i++){
            if(s[i]=='?'){
                for(char ch='a';ch<='z';ch++){
                    //三种情况 当?为第一位时 只需要比较后面一位 当?为最后一位时 只需要比较前一位
                    //当为中间时 就要求前一位和后一位都不相同
                   if((i==0||ch!=s[i-1])&&((i==n-1)||ch!=s[i+1])){
                       s[i]=ch;
                       break;
                   }
                }
            }
        }
        return String.valueOf(s);
    }
}