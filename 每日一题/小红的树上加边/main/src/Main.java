import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        long n =scanner.nextLong();
       ArrayList<ArrayList<Integer>> inkTable = new ArrayList<>();
        for (long i = 0; i <n ; i++) {
            inkTable.add(new ArrayList<>());
        }
       for(long i =1;i<n;++i){
           long u = scanner.nextLong();
           long v = scanner.nextLong();
           u--;
           v--;
           inkTable.get((int)v).add((int)u);
           inkTable.get((int)u).add((int)v);
       }
       long ans = 0;
       for (ArrayList<Integer> v:inkTable){
           if(v.size()<1) break;
           long sz =v.size();
           ans+=sz*(sz-1)/2;
       }
        System.out.println(ans);

    }


}