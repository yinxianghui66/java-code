//给你一个链表，两两交换其中相邻的节点，并返回交换后链表的头节点
// 你必须在不修改节点内部的值的情况下完成本题（即，只能进行节点交换）。
//使用递归解决非常简单...
//这里使用循环+模拟来解决 最好画图
//交换两个链表需要修改四个地方 所以直接定义四个变量来
//需要注意处理边界情况
//https://leetcode.cn/problems/swap-nodes-in-pairs/description/
  class ListNode {
    int val;
     ListNode next;
     ListNode() {}
     ListNode(int val) { this.val = val; }
     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 }


class Solution {
    public ListNode swapPairs(ListNode head) {
        ListNode dummyNode = new ListNode();
        dummyNode.next = head;
        //cur从头节点的前一位开始 才能操作两个节点
        ListNode cur = dummyNode;
        //注意这里先判断next 再判断next.next否则可能空指针异常
        while(cur.next!=null&&cur.next.next!=null){
            //保存节点1
            ListNode node1 = cur.next;
            //保存节点3
            ListNode node3 = cur.next.next.next;
            //连接节点2
            //这里交换节点 画个图更好理解 不需要保存node2 因为可以直接指向
            cur.next = cur.next.next;
            cur.next.next = node1;
            node1.next = node3;
            //移动cur到下两个数据的前一位
            cur = cur.next.next;
        }
        return dummyNode.next;
    }
    public ListNode swapPairs2(ListNode head) {
        //边界情况处理 如果传入空链表 或者只有一个的链表 无需交换
        if(head==null||head.next==null) return head;
        //定义虚拟头结点
        ListNode newHead=new ListNode(0);
        newHead.next=head;
        //定义四个节点 进行交换
        ListNode prev=newHead;
        ListNode cur=prev.next;
        ListNode next=cur.next;
        ListNode nnext=next.next;
        while(cur!=null&&next!=null){
            //交换节点
            prev.next=next;
            next.next=cur;
            cur.next=nnext;

            //交换完节点 指针进行移动 画图理解吧...
            prev=cur;
            cur=nnext;
            if(cur!=null) next=cur.next;
            if(next!=null) nnext=next.next;  //处理边界情况... 如果是奇数/偶数个时 会空指针
        }
        return newHead.next;
    }
}


