import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

//给你一个字符串数组，请你将 字母异位词 组合在一起。可以按任意顺序返回结果列表。
//
//字母异位词 是由重新排列源单词的所有字母得到的一个新单词。

//1.判断两个字符串是否为字母异位词(哈希表或排序)
//首先对字符串排序 然后比较 如果是异位词
// 判断 如果是异位词 则排序后 他俩一定相等
//则判断 哈希表之前也没有这个字符串 如果有说明是异位词 则加入这个哈希表的后面的字符串链表
//如果不是异位词 则新加入哈希表 并加入其字符串链表中
//2.如果两个字符串是字母异位词，则将它们放入同一个组中
class Solution {
    public List<List<String>> groupAnagrams(String[] strs) {
        HashMap<String,List<String>> hash=new HashMap<>();
        //1.将所以的字母异位词分组
        //将字符串中的字符排序
        for(String s:strs){
            char[] tmp= s.toCharArray();
            Arrays.sort(tmp);
            String key = new String(tmp);
            //如果当前字符串不存在 则丢进去 并新添加一个链表
            if(!hash.containsKey(key)){
                hash.put(key,new ArrayList<>());
            }
            //如果存在 说明此时的s 是同一个异位词 所以直接添加到链表中
            hash.get(key).add(s);
        }
        //2.提取结果
        return new ArrayList(hash.values());
    }
}