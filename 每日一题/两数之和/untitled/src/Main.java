import java.util.*;


public class Solution {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 无
     * @param nums int整型一维数组 
     * @param target int整型 
     * @return int整型一维数组
     */
    public int[] twosum (int[] nums, int target) {
        //使用哈希表 存储之前的结果
        if(nums==null||nums.length==0) return null;
        HashMap<Integer,Integer> hash = new HashMap<>();
        for(int i=0;i<nums.length;i++){
            int  sum = nums[i];
            int key = target-nums[i];
            if(hash.containsKey(sum)){
                //找到符合 直接返回
                return new int[]{hash.get(sum),i};
            }
            hash.put(key,i);
        }
        return null;
    }
}