//有一堆石头，用整数数组 stones 表示。其中 stones[i] 表示第 i 块石头的重量。
//每一回合，从中选出任意两块石头，然后将它们一起粉碎。假设石头的重量分别为 x 和 y，且 x <= y。那么粉碎的可能结果如下：
//如果 x == y，那么两块石头都会被完全粉碎；
//如果 x != y，那么重量为 x 的石头将会完全粉碎，而重量为 y 的石头新重量为 y-x。
//最后，最多只会剩下一块 石头。返回此石头 最小的可能重量 。如果没有石头剩下，就返回 0
//输入：stones = [2,7,4,1,8,1]
//输出：1
//解释：
//组合 2 和 4，得到 2，所以数组转化为 [2,7,1,8,1]，
//组合 7 和 8，得到 1，所以数组转化为 [2,1,1,1]，
//组合 2 和 1，得到 1，所以数组转化为 [1,1,1]，
//组合 1 和 1，得到 0，所以数组转化为 [1]，这就是最优值。
class Solution {
    public static int lastStoneWeightII(int[] stones) {
        //和上一题差不多 重点在与如何转变思想到01背包上
        //这里的粉碎 我们把石头的总和求出 /2 则就是两两相撞 两个的差值就是最后的结果
        //也就是求得 目标值为 sum/2 的 也就是装满这个sum/2 这个容量的背包的最大重量
        int len =stones.length;
        //dp数组的含义 dp[j]为容量为 j的背包 可以容纳的最大价值
        int sum =0;
        for (int i = 0; i <len ;i++) {
            sum+=stones[i];
        }
        int target = sum/2;
        int[] dp = new int[target+1];
        //初始化dp数组
        dp[0] = 0;
        //状态转移方程  注意01背包的便利顺序 j需要从后向前便利 以保证每个位置只取一遍
        for (int i = 0; i <len; i++) {
            //j >=stones[i] 保证物品的容量 小于背包的容量
            for (int j = target; j >=stones[i]; j--) {
                dp[j] = Math.max(dp[j],dp[j-stones[i]]+stones[i]);
            }
        }
        //因为我们求得到的是最大的容量的一半  所以 sum-dp[target] 为其中的一半  sum-dp[target] 一定是>=dp[target]的
        //另一半也为 dp[target] 所以求得的最小值就为  sum-dp[target]-dp[target] = sum- 2*dp[target]
        return sum- 2*dp[target];
    }

    public static void main(String[] args) {
        int[] s = {2,7,4,1,8,1};
        System.out.println(lastStoneWeightII(s));
    }
}