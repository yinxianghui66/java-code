import java.util.HashMap;
import java.util.Map;
//链接:https://leetcode.cn/problems/subarray-sum-equals-k/
//使用前缀和+哈希表 统计
//主要是推导公式 求值为k  则区间值sum - k  符合条件 即为次数+1
class Solution {
    public int subarraySum(int[] nums, int k) {
        //使用哈希表存储 0 - i-1 区间的和  存储合法的次数
        Map<Integer,Integer> hash=new HashMap<>();
        hash.put(0,1); //处理边界值 也就是当k等于前缀和时
        int sum=0,ret=0;
        for (int i = 0; i <nums.length; i++) {
            sum+=nums[i];  //前缀和  //没有必要真正创建前缀和数组
            ret+=hash.getOrDefault(sum-k, 0);  //这里用前缀和 sum-k  即为k  如果当前位置没有值 则默认为0
            hash.put(sum,hash.getOrDefault(sum,0)+1);      //存储前缀和到哈希表  出现次数+1
        }
        return ret;
    }
}