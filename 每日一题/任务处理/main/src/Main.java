import java.util.Arrays;
import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int testCases = sc.nextInt();
        String s = "213";
        while (testCases-->0){
            int n=sc.nextInt();
            int k=sc.nextInt();
            int[] tasks = new int[n];
            for (int i = 0; i <n; i++) {
                tasks[i] = sc.nextInt();
            }
            //使用贪心解决 尽量让两个人的任务时间接近
            Arrays.sort(tasks);
            int mingTime = 0;
            int baiTime = 0;
            for (int i = 0; i <k ; i++) {
                if(i<=k/2){
                    mingTime+=tasks[i];
                }else {
                    baiTime +=tasks[i];
                }
            }
            System.out.println(Math.max(mingTime, baiTime));
        }
    }
}