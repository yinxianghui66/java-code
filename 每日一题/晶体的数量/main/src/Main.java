import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        int n =scanner.nextInt();
        //初始化总个数
        int total = 0;
        for (int i = 0; i <n; i++) {
            //处理每一个晶体块
            int A= scanner.nextInt();
            int B = scanner.nextInt();
            //计算晶体个数
            long units = contCrystals(A,B,n);
            total+=units;
        }
        System.out.println(total);
    }

    private static long contCrystals(int a, int b,int n) {
        //计算正A边形的晶体个数
        int count =0;
        for (int i = 0; i <n; i++) {
            int sideLen = b+1;
            count+=i*sideLen*a;
        }
        return count;
    }
}