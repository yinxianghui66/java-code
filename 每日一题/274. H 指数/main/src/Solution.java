import java.util.Arrays;
//给你一个整数数组 citations ，其中 citations[i] 表示研究者的第 i 篇论文被引用的次数。计算并返回该研究者的 h 指数。
//根据维基百科上 h 指数的定义：h 代表“高引用次数” ，一名科研人员的 h 指数 是指他（她）至少发表了 h 篇论文，并且 至少
// 有 h 篇论文被引用次数大于等于 h 。如果 h 有多种可能的值，h 指数 是其中最大的那个。
//输入：citations = [3,0,6,1,5]
//输出：3
//解释：给定数组表示研究者总共有 5 篇论文，每篇论文相应的被引用了 3, 0, 6, 1, 5 次。
// 由于研究者有 3 篇论文每篇 至少 被引用了 3 次，其余两篇论文每篇被引用 不多于 3 次，所以她的 h 指数是 3
class Solution {
    public int hIndex(int[] citations) {
        //遍历数组 找最大的h指数 也就是依次找满足条件的
        //至少有1篇论文引用超过1次 至少有2篇论文引用超过2次 等等
        int h=0,i=citations.length-1;
        Arrays.sort(citations);
        //从后向前遍历数组 后面的数一定比前面的大
        //所以如果找到值<h 则说明前面的都小于h
        //并且i从已经遍历过的位置来 说明后面的数都大于h 并且一定是出现了h次了
        //妙 太妙了。。。。 我怎么想不出来
        while (i>=0&&citations[i]>h){
            h++;
            i--;
        }
        return h;
    }
    //使用二分 对于指数h来说 左边都是满足的 右边都是不满足的
    public int hIndex2(int[] citations) {
        int n =citations.length;
        int l=0,r=n;
        while(l<r){
            int mid = (l+r+1)/2;
            //是否存在出现mid次的数字 如果存在则h指数+ 如果不存在则h指数-1;
            if(check(citations,mid)) l=mid;
            else r=mid-1;
        }
        return r;
    }
    public boolean check(int[] cs,int mid){
        int count=0;
        for(int i:cs){
            if(i>=mid) count++;
        }
        return count>=mid;
    }
}