import java.util.ArrayList;
import java.util.List;
//链接:https://leetcode.cn/problems/find-all-anagrams-in-a-string/
//本题使用滑动窗口 + 哈希表 +自定义变量统计有效字符个数
//使用哈希表 统计每个字符出现的次数
//维护一个定长的滑动窗口(也就是p字符串的长度)
//入窗口后 额外判断入窗口的是不是一个有效的字符(所谓有效就是出现的个数小于等于p的哈希表中出现的个数)
//入窗口 这个 位置的数量++
//当我窗口的长度大于p的长度 需要出窗口 出窗口前 额外判断出窗口的这个字符是不是有效的字符
//更新结果时 只需要判断有效字符个数和p的哈希表的长度相同 即可保存结果
//返回所有结果即可
class Solution {
    public  List<Integer> findAnagrams(String ss, String pp) {
        List<Integer> ret=new ArrayList<>();
        //模拟两个哈希表 转换成字符数组
        char[] s=ss.toCharArray();
        char[] p=pp.toCharArray();
        int [] hash1=new int[26];  //统计字符串p每个字符出现的个数
        for(char ch:p) hash1[ch-'a']++;
        int [] hash2=new int[26];  //统计窗口中 每个字符出现的个数
        int m=p.length;
        for(int left=0,right=0,count=0;right<s.length;right++){
            char in=s[right];
            hash2[in-'a']++;  //进窗口
            if(hash2[in-'a']<=hash1[in-'a']) count++;  //用变量统计有效字符的个数+进窗口
            if(right-left+1>m){   //判断  当前窗口大小大于 has1的长度大小时 需要出窗口
                char out=s[left];
                //维护变量+出窗口
                if(hash2[out-'a']<=hash1[out-'a'])  count--;  //有效字符-1
                hash2[out-'a']--;  //出窗口
                left++;  //别忘了出窗口后left++
            }
            //更新结果
            if(count==m){
                ret.add(left);
            }
        }
        return  ret;
    }

}