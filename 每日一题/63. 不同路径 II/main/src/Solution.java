//给定一个 m x n 的整数数组 grid。一个机器人初始位于 左上角（即 grid[0][0]）。
// 机器人尝试移动到 右下角（即 grid[m - 1][n - 1]）。机器人每次只能向下或者向右移动一步。
//网格中的障碍物和空位置分别用 1 和 0 来表示。机器人的移动路径中不能包含 任何 有障碍物的方格。
//返回机器人能够到达右下角的不同路径数量。
//测试用例保证答案小于等于 2 * 109
//在上一题的基础上 多了个障碍物的选项 也就是该位置的状态不存在 则我直接跳过这个状态
//这里使用的是 直接把dp数组的对应位置 变为-1 表示该位置不存在 则对应判断
class Solution {
    public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        //1.定义dp数组的每一位的含义 dp[i][j] 达到这个位置 可以走的路径个数
        int rows = obstacleGrid.length;
        int columns = obstacleGrid[0].length;
        int[][] dp = new int[rows+1][columns+1];
        //2.初始化dp数组 并写状态方程
        //特殊判断 第一个位置就为石头 则直接返回
        if(obstacleGrid[0][0] == 0){
            dp[0][0] = 1; // [0][0]->[row-1][columns-1]
        }
        //3.确定遍历顺序 dp[i][j]依赖于前置数据 所以是从勾后向前c
        for (int i = 0; i <rows; i++) {
            for (int j = 0; j <columns; j++) {
                if(i==0&&j==0){
                    continue;
                }
                if(obstacleGrid[i][j] == 1) {
                    dp[i][j] = -1;
                    continue;
                }
                if(i==0){
                    dp[i][j] = dp[i][j-1];
                    continue;
                }
                if(j==0){
                    dp[i][j] = dp[i-1][j];
                    continue;
                }
                if(dp[i - 1][j] == -1){
                    dp[i][j] = dp[i][j-1];
                    continue;
                }
                if(dp[i][j - 1] == -1){
                    dp[i][j] = dp[i-1][j];
                    continue;
                }
                dp[i][j] = dp[i-1][j]+dp[i][j-1];
            }
        }
        //4.打印dp数组 调试 看是否符合预期
        if(dp[rows - 1][columns - 1] == -1) {
           return 0;
        }
        return dp[rows-1][columns-1];
    }

    public static void main(String[] args) {
        int[][] num = {{0}};
        System.out.println(uniquePathsWithObstacles(num));
    }
}