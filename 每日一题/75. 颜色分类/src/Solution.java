//链接:https://leetcode.cn/problems/sort-colors/description/
//使用三指针解决问题  思考问题 使用left和right 将数组分为三部分
//[0,left]  区间全为0  [left+1,right-1] 区间值全为1 [right,n-1] 区间值全为2
//使用i 遍历整个数组 在遍历过程中 将数组分为四部分
//[0,left] 区间全为0 [left+1,i-1] 值全为1  [i,right-1] 位置为带扫描的数据 [right,n-1] 值全为2
//根据数组i位置值 分三种情况 注意细节处理 当i位置==2 时 此时i不能++ !
//终止条件为i扫描到后面right区间即可
class Solution {
    public  void sortColors(int[] nums) {
        int n=nums.length;
        int left=-1,right=n,i=0;
        while(i<right){  //当扫描区间为空时停止
            if(nums[i]==0){
                swap(nums,++left,i++);
            }else if(nums[i]==2){
                swap(nums,--right,i);  //此时i不要++  因为此时的i区间内还是待扫描数据
            }else{
                i++;
            }
        }
    }
    public  void swap(int[] num,int i,int j){
        int ret=num[i];
        num[i]=num[j];
        num[j]=ret;
    }


}