//如果在将所有大写字符转换为小写字符、并移除所有非字母数字字符之后，短语正着读和反着读都一样。则可以认为该短语是一个 回文串 。
//字母和数字都属于字母数字字符。
//给你一个字符串 s，如果它是 回文串 ，返回 true ；否则，返回 false 。
//https://leetcode.cn/problems/valid-palindrome/?envType=study-plan-v2&envId=top-interview-150
class Solution {
    public boolean isPalindrome(String s) {
        s =s.toLowerCase().trim();
        StringBuffer stringBuffer = new StringBuffer();
        for(int i = 0;i<s.length();i++){
            char ch = s.charAt(i);
            //为小写字母||数字时则加入
            if(ch>='a'&&ch<='z'||ch>='0'&&ch<='9'){
                stringBuffer.append(ch);
            }
        }
        String str = stringBuffer.toString();
        int left = 0 ,right =str.length()-1;
        //挨个比较字符 如果有一个不同则不符合条件
        while(left<right){
            if(str.charAt(left)!=str.charAt(right)) return false;
            left++;
            right--;
        }
        return true;
    }
}