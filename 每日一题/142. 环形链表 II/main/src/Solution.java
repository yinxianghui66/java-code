//https://leetcode.cn/problems/linked-list-cycle-ii/
//给定一个链表的头节点  head ，返回链表开始入环的第一个节点。 如果链表无环，则返回 null。
//如果链表中有某个节点，可以通过连续跟踪 next 指针再次到达，则链表中存在环。
// 为了表示给定链表中的环，评测系统内部使用整数 pos 来表示链表尾连接到链表中的位置
// （索引从 0 开始）。如果 pos 是 -1，则在该链表中没有环。
// 注意：pos 不作为参数进行传递，仅仅是为了标识链表的实际情况。
//不允许修改 链表。
class ListNode {
      int val;
      ListNode next;
      ListNode(int x) {
          val = x;
          next = null;
      }
  }
//首先判断链表是否有环
//再找到环的入口 //通过数学推导 可以得到 x = z 说明从快慢指针的相遇点 开始走
//和此时从head开始的点 开始走 两个指针 相遇 此时的点为环的入口
public class Solution {
    public ListNode detectCycle(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;
        while (fast!=null&&fast.next!=null){
            //快慢指针
            slow = slow.next;
            fast = fast.next.next;
            if(fast == slow){
                //相遇 定义新起点
                ListNode index1 = fast;
                ListNode index2 = head;
                while (index2!=index1){
                    index2 =index2.next;
                    index1 = index1.next;
                }
                //走完之后 返回他俩相遇的位置
                return index2;
            }
        }
        return null;
    }
}