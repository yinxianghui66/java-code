//链接:https://leetcode.cn/problems/duplicate-zeros/description/
// 双指针的思想
//从后往前复写 找到0就复写两位(cur-- dest-=2) 非零复写一位 (cur-- dest--)
//这题要先思考 找到最后一位需要复写的数字 使用双指针模拟一遍 找到位置
class Solution {
    public void duplicateZeros(int[] arr) {
        int cur=0;
        int dest=-1;
        int n=arr.length;
        //1.找到最后一个复写的数字
        //判断cur位置 如果为0 dest往后两位 非零 dest往后一位
        // 判断dest是否到最后位置
        //cur++
        while(cur<n){
            if(arr[cur]==0){
                dest+=2;
            }else{
                dest++;
            }
            //当dest遍历到最后 就停止
            if(dest>=n-1){
                break;
            }
            cur++;
        }
        //2.处理边界值判断 (当倒数第二位为0 的情况时)
        if(dest==n){
            arr[n-1]=0;
            dest-=2;
            cur--;
        }
        //3.从后向前完成复写操作
        while(cur>=0){
            if(arr[cur]!=0){
                arr[dest--]=arr[cur--];
            }else{
                //找到0就覆盖两位0
                arr[dest--]=0;
                arr[dest--]=0;
                cur--;
            }
        }
    }
}