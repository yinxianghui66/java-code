//给定一个字符串 s 和一个整数 k，从字符串开头算起，每计数至 2k 个字符，就反转这 2k 字符中的前 k 个字符。
//如果剩余字符少于 k 个，则将剩余字符全部反转。
//如果剩余字符小于 2k 但大于或等于 k 个，则反转前 k 个字符，其余字符保持原样
//输入：s = "abcdefg", k = 2
//输出："bacdfeg"
class Solution {
    public String reverseStr(String s, int k) {
        //一次处理2k个位置的元素
        char[] ss = s.toCharArray();
        for(int i=0;i<s.length();i+=2*k){
            //只有当前位置不超出k才对前半段进行反转 
            if(i+k<=s.length()-1){
                //对字符串前半段进行反转
                //注意这里需要-1 对应数组下标
                ss=revese(ss,i,i+k-1);
                continue;
            }
            //对剩下的进行反转
            ss= revese(ss,i,s.length()-1);
        }
        return new String(ss);
    }
    public static char[] revese(char[] s,int left,int rigth){
        while(left<rigth){
            char tmp = s[left];
            s[left] = s[rigth];
            s[rigth] = tmp;
            left++;
            rigth--;
        }
        return s;
    }
}