import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = Integer.parseInt(scanner.nextLine());
        String[] originalTexts = new String[N];
        String[] correctedTexts = new String[N];
        for (int i = 0; i <N; i++) {
            originalTexts[i] = scanner.nextLine();
        }
        for (int i = 0; i <N; i++) {
            correctedTexts[i] = scanner.nextLine();
        }
        int total = 0;
        for (int i = 0; i <N; i++) {
            total+=calulate(originalTexts[i],correctedTexts[i]);
        }
        System.out.println(total);
    }

    private static int calulate(String original, String corrected) {
        //动态规划
        int m = original.length();
        int n = corrected.length();
        int[][] dp = new int[m+1][n+1];
        //初始化dp数组
        for (int i = 0; i <=m; i++) {
            dp[i][0] = i;
        }
        for (int j = 0; j <=n ; j++) {
            dp[0][j] = j;
        }
        for (int i = 1; i <=m ; i++) {
            for(int j=1;j<=n;j++){
                if(original.charAt(i-1)==corrected.charAt(j-1)){
                    dp[i][j]= dp[i-1][j-1];
                }else {
                    dp[i][j] = Math.min(dp[i-1][j-1],Math.min(dp[i-1][j],dp[i][j-1]))+1;
                }
            }
        }
        return dp[m][n];
    }
}