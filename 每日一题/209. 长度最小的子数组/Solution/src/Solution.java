//给定一个含有 n 个正整数的数组和一个正整数 target
//找出该数组中满足其总和大于等于 target 的长度最小的 连续子数组
// [numsl, numsl+1, ..., numsr-1, numsr] ，并返回其长度。如果不存在符合条件的子数组，返回 0
//滑动窗口(其实就是双指针)
//https://leetcode.cn/problems/minimum-size-subarray-sum/description/
class Solution {
    public int minSubArrayLen(int target, int[] nums) {
        int start = 0, end = 0;
        //定义值为最大值
        int ans = Integer.MAX_VALUE;
        int sum = 0;
        int n = nums.length;
        while (end<n){
            //进窗口
            sum += nums[end];
            //判断
            while (sum>=target){
                //此时不符合条件了 则更 新最小值 并且去掉第一位 继续遍历
                ans = Math.min(ans,end-start+1);
                sum -= nums[start];
                start++;
            }
            //继续向后滑动
            end++;
        }
        //如果值没有变过 说明不存在 返回0 反之返回最小长度!
        return ans == Integer.MAX_VALUE ? 0:ans;
    }
    //先找到一个符合条件的位置 然后后移 便利所有的位置 找到最小值
    public int minSubArrayLen2(int target, int[] nums) {
       //求满足条件的最小数组长度 那么可以定义两个指针 一个指向头 另一个指向符合条件的位置
        //定义结果应该为最大的值
        int res = Integer.MAX_VALUE;
        int start =0;
        int sum =0;
        //先找到符合条件的位置 遍历数组  这里的end 就是指向符合条件的位置
        for(int end =0;end<nums.length;end++){
            //统计和的值 用于比较条件
            sum += nums[end];
            //直到符合条件的位置 停下来
            //注意这里是while  不是if  if只能移动一位 这样如果移动一位后还是不满足条件 就不动了
            //所以需要找到符合条件的位置才停下来
            while (sum>=target){
                //此时的end位置 即为符合条件的位置 统计长度 然后开头的位置前移 直到遍历完数组
                //记得取最小值
                res = Math.min(res,end-start+1);
                //然后前移位置 并对应减去前面的值
                sum -= nums[start];
                //前移后 位置 ++
                start++;
            }
        }
        //如果找不到 返回 0 所以如果res没变 说明没有符合条件的 则返回0
        return res==Integer.MAX_VALUE?0:res;
    }
}