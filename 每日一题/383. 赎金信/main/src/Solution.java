//给你两个字符串：ransomNote 和 magazine ，判断 ransomNote 能不能由 magazine 里面的字符构成。
//如果可以，返回 true ；否则返回 false
//magazine 中的每个字符只能在 ransomNote 中使用一次
//https://leetcode.cn/problems/ransom-note/description/?envType=study-plan-v2&envId=top-interview-150
//使用哈希表的思想 但是不要直接用哈希表 使用字符数组模拟哈希表
// c - 'a' 映射26个位置 统计字符出现的次数  magazine 统计完后 ransomNote进行--操作
//因为是 magazine 包含 ransomNote 所以遍历完 ransomNote 每个位置的值一定不能小于0
//则说明包含 反之 则直接返回false
class Solution {
    public boolean canConstruct(String ransomNote, String magazine) {
        if (ransomNote.length()>magazine.length()) return false;
        int cnt[] = new int[26];
        for(char c :magazine.toCharArray()){
            //统计该字符出现的次数
            cnt[c - 'a']++;
        }
        for(char c :ransomNote.toCharArray()){
            //减去ransomNote所有的出现的字符 如果这个位置的出现次数为负数 则一定不符合条件
            cnt[c - 'a']--;
            if (cnt[c-'a']<0) return false;
        }
        //如果遍历完 ransomNote 都没出现小于0的部分 则符合条件
        return true;
    }
}