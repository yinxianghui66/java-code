import java.util.HashMap;
import java.util.Map;
//链接:https://leetcode.cn/problems/contiguous-array/
//使用前缀和+哈希表解决
//将问题转换一下 数组中都是 0 或者 1 我们要找相同数量的最长子数组
//我们可以将数组里的 0 全部看作-1 则相同数量时 所有的1+0(看作-1) 和为0 即满足相同数量的条件
//处理细节问题
//1.哈希表中存什么？  存的前缀和 和 对应位置的下标
//2.什么时候存入哈希表 在使用完后 丢进哈希表
//3.如果有重复的<sum,i> 怎么存？  只保留最前面的那一对<sum,i>
//4.默认的前缀和为0的情况 怎么存 存 0 位置的默认值为-1
//5.数组长度怎么算？  i-j(当前sum位置的哈希表中的下标)
class Solution {
    public int findMaxLength(int[] nums) {
        Map<Integer,Integer> hash=new HashMap<>();
        //这里的默认值为-1 我们map中存储的是 前缀和,数组下标
        hash.put(0,-1);
        int sum=0,ret=0;
        for (int i = 0; i <nums.length;i++) {
            sum+=(nums[i]== 0 ? -1 : 1);  //没必要真正修改数组的内容 如果位置为 0 则对应看作为-1 反之为1
            if(hash.containsKey(sum)){
                ret=Math.max(ret,i-hash.get(sum));  //如果哈希表中存在这个前缀和  也就是满足条件 此时更新数组的长度
            }else {
                hash.put(sum,i);   //如果找不到 则添加到哈希表
            }
        }
        return ret;
    }
}