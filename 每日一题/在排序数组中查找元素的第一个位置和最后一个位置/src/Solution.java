//链接:https://leetcode.cn/problems/find-first-and-last-position-of-element-in-sorted-array/description/?envType=study-plan-v2&envId=top-100-liked
//使用二分查找 两次 找到第一个大于target的下标-1 (递增的 -1即为target)和第一个大于等于target的下标
//最后校验一下是否符合 返回下标
class Solution {
    public int[] searchRange(int[] nums, int target) {
        //找到第一位等于target的数字下标
        int leftIdx=binarySearch(nums,target,true);
        //找到一位大于target的数字下标 -1 既为target的位置
        int rightIdx=binarySearch(nums,target,false)-1;
        //校验
        if(leftIdx<=rightIdx&&rightIdx<nums.length&&
                nums[leftIdx]==target&&nums[rightIdx]==target){
            return new int[]{leftIdx,rightIdx};
        }
        return new int[]{-1,-1};
    }
    public int binarySearch(int[] nums, int target,boolean lower) {
        int left=0;
        int right=nums.length-1;
        int ans=nums.length;
        while(left<=right){
            int mid=(left+right)/2;
            if(nums[mid]>target||(lower&&nums[mid]>=target)){
                right=mid-1;
                ans=mid;
            }else{
                left=mid+1;
            }
        }
        return ans;
    }
}