import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t= scanner.nextInt();
        for (int i = 0; i <t; i++) {
            int n = scanner.nextInt();
            int k = scanner.nextInt();
            int p = scanner.nextInt();
            int[] toys = new int[k];
            for (int j = 0; j <k; j++) {
                toys[j] = scanner.nextInt();
            }
            int sum =0;
            boolean res= false;
            for (int mask = 0; mask <k; mask++) {
                if(sum==n){
                    res=true;
                    break;
                }
                sum+=toys[mask];
                if(sum>n){
                    //减去上一位的值 再试试
                    sum-=toys[mask];
                }
            }
            //使用补充缝隙
            if(res==false){
                for(i=0;i<p;i++){
                    sum+=1;
                    if(sum==n){
                        System.out.println("YES");
                    }
                }
            }

            //剩余的空间
            if(res){
                System.out.println("YES");
            }else {
                System.out.println("NO");
            }
        }
    }
}


