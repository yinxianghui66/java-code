//给定字符串 s 和 t ，判断 s 是否为 t 的子序列。
//字符串的一个子序列是原始字符串删除一些（也可以不删除）字符而不改变剩余字符相对位置形成的新字符串。（例如，"ace"是"abcde"的一个子序列，而"aec"不是）。
//进阶：
//如果有大量输入的 S，称作 S1, S2, ... , Sk 其中 k >= 10亿，你需要依次检查它们是否为 T 的子序列。在这种情况下，你会怎样改变代码？
//输入：s = "abc", t = "ahbgdc"
//输出：true
//使用双指针 指向两个字符串 如果字符相同就移动字串的指针 和母串的指针
//如果不相同 则只移动母串的 (相当于消除了这个母串的字符)如果字串 或母串的指针超出长度 既结束循环
//判断left移动了几次 如果和字串的长度相等则返回true
class Solution {
    public boolean isSubsequence(String s, String t) {
        int left = 0,right = 0;
        while(right<t.length()&&left<s.length()){
            //如果字符相同则指针移动
            if(s.charAt(left)==t.charAt(right)){
                left++;
            }
            //如果不同直接++ 相当于消去了这个字符
            right++;
        }
        //最后判断left移动了几次 如果是刚到等于长度 说明全部都可以抵消
        return left==s.length();
    }
}