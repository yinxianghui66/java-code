import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
//链接:https://leetcode.cn/problems/fruit-into-baskets/
//滑动窗口
//可以理解为找最长子数组 但是有个篮子的限制
//模拟一个哈希表 额外定义一个变量 用来统计水果的种类
//水果种类大于篮子数量2时 出窗口
//其他的和找最长子数组是一样的
class Solution {
    public int totalFruit(int[] fruits) {
        //可以使用数组模拟哈希表
        int n = fruits.length;
        int hash[] = new int[n + 1];
        int ret = 0;
        int kinds = 0;
        for (int left = 0, right = 0; right < n; right++) {
            //进窗口
            int in = fruits[right];
            if (hash[in] == 0) {  //说明是新水果 种类 +1  当这个位置的水果为0次时 说明是新水果
                kinds++;
            }
            hash[in]++;
            while (kinds > 2) {  //判断
                int out = fruits[left];
                hash[out]--;  //出窗口
                if (hash[out] == 0) {  //当对应位置水果为0时 水果种类-1
                    kinds--;
                }
                left++;
            }
            //更新结果
            ret = Math.max(ret, right - left + 1);
        }
        return ret;
    }
}

//        Map<Integer,Integer> hash=new HashMap<Integer,Integer>();  //统计窗口中水果的种类
//        int ret=0;
//        for(int left=0,right=0;right<fruits.length;right++){
//            int in=fruits[right];
//            hash.put(in,hash.getOrDefault(in,0)+1); //进窗口
//            while(hash.size()>2){  //判断
//                int out=fruits[left];
//                hash.put(out,hash.get(out)-1);  //出窗口
//                if(hash.get(out)==0){
//                    hash.remove(out);
//                }
//                left++;
//            }
//            //更新结果
//            ret=Math.max(ret,right-left+1);
//        }
//        return ret;

