//给定一个二叉树，找出其最小深度。
//最小深度是从根节点到最近叶子节点的最短路径上的节点数量。
//说明：叶子节点是指没有子节点的节点。
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class Solution {
    public int minDepth(TreeNode root) {
        return getHeight(root);
    }
    public int getHeight(TreeNode node){
        if(node==null) return 0;
        int lefthiget = getHeight(node.left);
        int righthight = getHeight(node.right);
        //特殊逻辑处理 这里定义的最小深度 是没有字节点的叶子节点 所以如果出现 像 1 null 3 这种情况 应该去除这种情况 并求另一边的深度
        if(node.left==null&&node.right!=null) return righthight+1;
        if(node.left!=null&&node.right==null) return lefthiget+1;
        //当左右子树都不为空时 取两者的最小值 即可
        int hight = Math.min(lefthiget,righthight)+1;
        return hight;
    }
}