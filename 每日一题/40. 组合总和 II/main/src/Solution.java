import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

//给定一个候选人编号的集合 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合
//candidates 中的每个数字在每个组合中只能使用 一次
//注意：解集不能包含重复的组合。
class Solution {
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        //进行排序 保证重复的元素都挨在一起
        Arrays.sort(candidates);
        boolean[] isUse = new boolean[candidates.length];
        backTracking(candidates,target,0,0,isUse);
        return res;
    }
    List<List<Integer>> res = new ArrayList<>();
    LinkedList<Integer> path = new LinkedList<>();
    public void backTracking(int[] candidates,int target,int sum,int statrIndex,boolean[] isUse){
        if(sum>target) return;
        if(sum==target){
            //符合目标 存储结果
            res.add(new ArrayList<>(path));
            return;
        }

        //处理单层逻辑
        for (int i = statrIndex; i < candidates.length; i++) {
            //数层去重 必需满足 前一个位置的值未被使用过 才可以去重(保证这个位置的值 前面已经包含过了 这个位置是重复的)
            //如果前一位使用过 则为树枝的操作 不能去掉这种情况(因为他包含了后面的情况)
            if(i>0&&candidates[i]==candidates[i-1]&& !isUse[i - 1]){
                continue;
            }
            path.add(candidates[i]);
            sum+=candidates[i];
            isUse[i] = true;
            backTracking(candidates,target,sum,i+1,isUse);
            path.removeLast();
            isUse[i] = false;
            sum-=candidates[i];
        }
    }
}