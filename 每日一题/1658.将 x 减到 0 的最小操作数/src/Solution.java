//链接:https://leetcode.cn/problems/minimum-operations-to-reduce-x-to-zero/
//正向思考很难考虑出来 这题我们反向思考
//让我们找从左 或者右边减去数 使得他们的和为x
//那么就可以看作 左边一个区间a 右边一个区间b 中间的区间为和为sum
//转换思想 a+b=x 整个数组和为num   那么中间区间的值就应该为num-x
//又要求最短的步数 那么就是中间的sum的区间越长 步数越少
//于是问题就转换成了求 目标值为num-x 的最长子数组
//于是就转变成滑动窗口问题了 当sum小于目标值的时候 入窗口
//当大于目标值的时候 出窗口
//当等于目标值的时候 保存结果 继续入窗口 找到最大的长度ret
//最后返回时注意 应该返回的是数组长度n-ret 最大区间的长度即可  ret默认为-1  如果一个区间都找不到就返回-1
class Solution {
    public int minOperations(int[] nums, int x) {
        int num=0;
        int n=nums.length;
        for(int i=0;i<n;i++){
            num+=nums[i];
        }
        int target=num-x;
        if(target<0) return -1;
        int left=0,ret=-1,sum=0;
        for(int right=0;right<n;right++){
            //入窗口
            sum+=nums[right];
            while(sum>target){ //判断
                sum-=nums[left];  //出窗口
                left++;
            }
            if(sum==target){
                //保存结果
                ret=Math.max(right-left+1,ret);
            }
        }
        //注意处理细节
        return ret==-1?-1:n-ret;
    }
}