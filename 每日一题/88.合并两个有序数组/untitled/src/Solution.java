//给你两个按 非递减顺序 排列的整数数组 nums1 和 nums2，另有两个整数 m 和 n ，分别表示 nums1 和 nums2 中的元素数目。
//https://leetcode.cn/problems/merge-sorted-array/description/
//请你 合并 nums2 到 nums1 中，使合并后的数组同样按 非递减顺序 排列。
//输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
//输出：[1,2,2,3,5,6]
//解释：需要合并 [1,2,3] 和 [2,5,6] 。
//合并结果是 [1,2,2,3,5,6] ，其中斜体加粗标注的为 nums1 中的元素。
class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        //从后往前赋值 找到最大的数
        int i= nums1.length-1;
        //一共要合并N次 因为nums2长度为n
        while(n>0){
            //因为俩数组都是排好序的 所以只需要找到两个数据的最后以为比较 就可以得到最大的数
            //将最大的数放到数组最好
            if(m>0&&(nums1[m-1]>nums2[n-1])){
                nums1[i]=nums1[m-1];
                i--;
                m--;
            }else{
                nums1[i]=nums2[n-1];
                i--;
                n--;
            }
        }
    }
}