//https://leetcode.cn/problems/zigzag-conversion/
//模拟+优化(找规律)
//模拟算法可以解决 但是时间复杂度太高并且需要创建一个矩阵 空间复杂度也不低
//我们可以优化一下模拟算法 没必要真正创建一个矩阵 只需要得到对应位置的下标 拼接在一起
//我们假设一个矩阵 每个位置放的都是字符串s的下标 可以得出规律  公差d = 2*numRows-2
//第一行的规则 即 i+d i+2d i+3d  等待
//中间行的规则 分为两位 一位规则为 i+d 一位规则为 d-i 后面一次为 i+2d 2d-i i+3d 3d-i
//最后一行规则如上 即 i+d i+2d i+3d
class Solution {
    public String convert(String s, int numRows) {
        if(numRows==1) return s;
        StringBuffer ret=new StringBuffer();
        int d=2*numRows-2;
        int n=s.length();
        for(int i=0;i<n;i+=d) ret.append(s.charAt(i));
        //处理中间行
        for(int k=1;k<numRows-1;k++){
            for(int i=k,j=d-i;i<n||j<n;j+=d,i+=d){
                if(i<n) ret.append(s.charAt(i));
                if(j<n) ret.append(s.charAt(j));
            }
        }
        //枚举最后一行
        for(int i=numRows-1;i<n;i+=d) ret.append(s.charAt(i));
        return ret.toString();
    }
}