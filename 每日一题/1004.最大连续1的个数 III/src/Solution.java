//链接:https://leetcode.cn/problems/max-consecutive-ones-iii/
//使用滑动窗口 同向双指针
//此题有可以翻转的0  我们只需要判断这个零 最大是几个即可
//定义 left  right  right表示入窗口 如果当前位置不为0 就直接入窗口
//找到为0的数 可以反转的数量++ 当可以反转的数量和给定的相同时 就需要出窗口 一直到反转数量<给定数量
//此时更新一次结果 并且继续循环

//[1,1,1,0,0,0,1,1,1,1,0] 举个例子
//left和right都为0 此时
//left指向为1 当遇到0时 zero++ 当不满足zero<=k时 (left在0下标 right在5下标) 此时就为这个区间的最大长度
//再去枚举中间的数据也不会长于这个区间 没有意义 直接优化掉
//left++ 找到为0的数 让zero-- 直到满足条件 zero<=k 继续寻找下一个区间
//此时left 在下标4的位置 right在下标5的位置 继续遍历
//每次更新最大值即可
class Solution {
    public int longestOnes(int[] nums, int k) {
        int left=0,zero=0;
        int n=nums.length;
        int res=0;
        //入窗口
        for(int right=0;right<n;right++){
            if(nums[right]==0){
                zero++;
            }
            //判断
            while(zero>k){
                //出窗口
                if(nums[left]==0){
                    zero--;
                }
                left++;
            }
            //更新结果
            res=Math.max(res,right-left+1);
        }
        return res;
    }
}