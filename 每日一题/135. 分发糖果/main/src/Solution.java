//n 个孩子站成一排。给你一个整数数组 ratings 表示每个孩子的评分。
//你需要按照以下要求，给这些孩子分发糖果：
//每个孩子至少分配到 1 个糖果。
//相邻两个孩子评分更高的孩子会获得更多的糖果。
//请你给每个孩子分发糖果，计算并返回需要准备的 最少糖果数目
//输入：ratings = [1,0,2]
//输出：5
//解释：你可以分别给第一个、第二个、第三个孩子分发 2、1、2 颗糖果。 ·
class Solution {
    public static int candy(int[] ratings) {
        //分情况去考虑 先考虑 右边比左边大的情况 得出一个糖果的结论
        int[] candy1 = new int[ratings.length];
        int[] candy2 = new int[ratings.length];
        candy1[0] = 1;
        candy2[ratings.length-1] = 1;
        //右边比左边大
        for (int i = 1; i <ratings.length; i++) {
            if(ratings[i]>ratings[i-1]){
                //那边右边的位置 应该比左边多1个
                candy1[i] = candy1[i-1]+1;
            }else {
                //不比左边大 则直接给1个
                candy1[i] =1;
            }
        }
        //再考虑左边比右边大的情况 注意这里应该是从后往前遍历
        for (int i = ratings.length-2; i >=0 ; i--) {
            if(ratings[i]>ratings[i+1]){
                //那么左边的位置的值 应该比后一位的值+1
                candy2[i] = candy2[i+1]+1;
            }else {
                //不比右边大 则为1
                candy2[i] = 1;
            }
        }
        //最后需要兼顾两种情况 所以 取对应位置的较大值即可
        int res =0;
        for (int i = 0; i <candy1.length; i++) {
            res+=Math.max(candy1[i],candy2[i]);
        }
        return  res;
    }

}