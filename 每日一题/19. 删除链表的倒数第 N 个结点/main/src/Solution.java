import java.util.List;
//给你一个链表，删除链表的倒数第 n 个结点，并且返回链表的头结点。
//输入：head = [1,2,3,4,5], n = 2
//输出：[1,2,3,5]
class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next;}
  }
//使用快慢指针 关键在于如何找到倒数第n个节点
//这里使用快慢指针 让快指针先走n步 然后再一起走
//快指针走到头 为空的时候 慢指针指向的就是倒数第n个节点
//要删除一个节点 要先找到他的前一个节点 所以这里是 n+1 步数 i <= n
//指向要删除节点的下一个节点 达到删除的目的
class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        //创建虚拟头节点
        ListNode cur = new ListNode();
        cur.next = head;
        ListNode slow = cur;
        ListNode fast = cur;
        //让快指针率先移动 n+1 步 快慢指针再一起移动
        //只要快指针指向 null 了 此时慢指针指向的就是要删除的节点
        for (int i = 0; i <=n ; i++) {
            fast = fast.next;
        }
        while (fast != null){
            fast = fast.next;
            slow = slow.next;
        }
        //删除节点 指向节点的后面的后面 达到删除节点的目的
        if(slow.next!=null){
            slow.next = slow.next.next;
        }
        return cur.next;
    }
    //问题在于怎么找到倒数第n个节点 使用快慢指针！
    public ListNode removeNthFromEnd2(ListNode head, int n) {
        ListNode dummyNode = new ListNode();
        dummyNode.next = head;
        //使用快慢指针 快指针先走n步 慢指针开始走 当快指针走到null 的时候 满指针的位置就是到倒数第n位
        //但是要操作删除的话 需要找到删除节点的前一位 所以快指针走 n+1 步 此时刚好
        //注意这里从虚拟头节点开始遍历
        ListNode fast = dummyNode;
        ListNode slow = dummyNode;
        //先走
        for(int i = 0;i<n+1;i++){
            fast = fast.next;
        }
        //再一起走  这里不是fast 不然fast就没法走到null了
        while(fast!=null){
            fast = fast.next;
            slow = slow.next;
        }
        //此时slow的位置为要删除节点的前一位 操作删除
        ListNode temp =slow.next.next;
        slow.next = temp;
        return dummyNode.next;
    }
}