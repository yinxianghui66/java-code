//链接:https://leetcode.cn/problems/longest-substring-without-repeating-characters/description/
//使用滑动窗口  两个指针都无需回调 单向双指针
//使用哈希表 来判断当前区间是否存在重复元素(这里是模拟了一个哈希表 让字符的对应ASCII值 对应到hash的位置上 比如第一个为A 那么映射到hash数组中
// s[right] 的值为65  映射到hash数组65的位置 将其++ 设置为1 此后再出现该位置 就再加加 表示改元素出现的次数)
//定义左右指针 让当前位置哈希表+1 表示存在一次了
//判断 当前位置值是否出现超过一次 如果满足条件 就要出窗口
//将左边的元素划出 一直到划到重复元素的位置 将其划出 此时left指向的就是重复元素的下一位(画图举例 如果找到一个重复元素 那么重复元素之前的都无语枚举了
// 因为此时的区间就是最大的了 就算后面不重复 也不会有比此时还大的区间 就直接舍去)
//此时的right也无需回到left+1的位置 继续向后走 找到下一个重复元素 重复上述步骤
//并且在每一次划出窗口后 保存结果 取最大区间长
//注意left++和right++
class Solution {
    public int lengthOfLongestSubstring(String ss) {
        //将字符串转换成字符数组
        char[] s=ss.toCharArray();
        //创建一个字符数组 模拟哈希表
        int[] hash=new int[128];
        int left=0,right=0,ret=0;
        int n=ss.length();
        while (right<n){
            //入窗口  哈希表的对应位置+1  表示当前字符出现次数
            hash[s[right]]++;
            while (hash[s[right]]>1){//判断是否出窗口 这里是找到重复字符 也就是>1的
                //出窗口 让left划出  一直到划出到right位置的下一位 (也就是本来是hash[s[right]] 这个位置出现两次了 一直到前面的left将这个位置的值
                //划出 满足判断条件停下 此时left指向的就是hash[s[right]]位置的值的下一位 )
                hash[s[left]]--;
                left++;
            }
            //更新结果
            ret=Math.max(ret,right-left+1);
            right++;
        }
        return ret;
    }
}