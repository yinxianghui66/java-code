//相亲数：A的真因数之和为B，B的真因数之和为C，且有C=A，则A和B是一对相亲数。
public class Test {
    public static void main(String[] args) {
     int count=0;
       for(int sum=2;sum<=1000;sum++){
           int sum1=getNum(sum);//求sum的因数和sum1
           if(sum1>sum){//缩小范围
               int sum2=getNum(sum1);//求sum1的因数和sum2
              if(sum2==sum){//如果sum==sum2则为相亲数
                   System.out.println(sum1+"和"+sum2+"是一对相亲数。");
                   count++;
              }
           }
      }
       System.out.println("共有"+count+"对相亲数");
    }
    public static int  getNum(int num){
        int sum=1;
        for(int i=2;i<=num/2;i++){
            if(num%i==0){
                sum+=i;
            }
        }
        return sum;
    }
}
