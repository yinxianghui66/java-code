//给定一个len长度的数组，用递归的方法求数组和
public class Test {
    public static void main(String[] args) {
        int[]arr1={1,3,5,7,6};
        System.out.println(arraySum(arr1, arr1.length));
    }
    public static int arraySum(int[]array,int len){
        if(len==0){
            return 0;
        }else {
            int n=len-1;
            if (n == 0) {
                return array[n];
            } else
                return array[n]+arraySum(array,len-1);
        }
    }
}
