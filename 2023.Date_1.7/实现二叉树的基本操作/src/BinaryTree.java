import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree {

    static class TreeNode {
        public char val;
        public TreeNode left;//左孩子的引用
        public TreeNode right;//右孩子的引用

        public TreeNode(char val) {
            this.val = val;
        }
    }


    /**
     * 创建一棵二叉树 返回这棵树的根节点
     *
     * @return
     */
    public TreeNode createTree() {
        TreeNode A=new TreeNode('A');
        TreeNode B=new TreeNode('B');
        TreeNode C=new TreeNode('C');
        TreeNode D=new TreeNode('D');
        TreeNode E=new TreeNode('E');
        TreeNode F=new TreeNode('F');
        TreeNode G=new TreeNode('G');
        A.left=B;
        A.right=E;
        B.left=C;
        B.right=D;
        E.left=F;
        E.right=G;
        return  A;
    }

    // 前序遍历
    public void preOrder(TreeNode root) {
        if(root==null){
            return;
        }
        System.out.println(root.val+" ");
        preOrder(root.left);
        preOrder(root.right);
    }

    // 中序遍历
    void inOrder(TreeNode root) {
        if(root==null){
            return;
        }
        preOrder(root.left);
        System.out.println(root.val+" ");
        preOrder(root.right);
    }

    // 后序遍历
    void postOrder(TreeNode root) {
        if(root==null){
            return;
        }
        preOrder(root.left);
        preOrder(root.right);
        System.out.println(root.val+" ");
    }

    public static int nodeSize;

    /**
     * 获取树中节点的个数：遍历思路
     */
    void size(TreeNode root) {
        if(root==null){
            return;
        }
        nodeSize++;   //遍历了多少次就记多少次
        size(root.right);
        size(root.left);
    }

    /**
     * 获取节点的个数：子问题的思路
     *
     * @param root
     * @return
     */
    int size2(TreeNode root) {  //结点的个数有左节点+右节点+根节点（+1）
        if(root==null){
            return 0;
        }
        int left=size2(root.left);
        int right=size2(root.right);
        return left+right+1;
    }


    /*
     获取叶子节点的个数：遍历思路
     */
    public static int leafSize = 0;

    void getLeafNodeCount1(TreeNode root) {  //只有当左右结点都为空的时候才是叶子节点
        if(root==null){
            return;
        }
        if(root.right==null&&root.left==null){  //有一个叶子结点记一次数
            leafSize++;
        }
        getLeafNodeCount1(root.left);
        getLeafNodeCount1(root.right);
    }

    /*
     获取叶子节点的个数：子问题
     */
    int getLeafNodeCount2(TreeNode root) {
        if(root==null){
            return 0;
        }
        if(root.left==null&&root.right==null){  //有多少个执行多少次，左节点left个+右节点right个就为一共多少个
            return 1;
        }
        int left=getLeafNodeCount2(root.left);
        int right=getLeafNodeCount2(root.right);
        return left+right;
    }

    /*
    获取第K层节点的个数
     */
    int getKLevelNodeCount(TreeNode root, int k) {    //k层节点就为k-1层所有的节点的左节点加上右节点
        if(root==null){
            return 0;
        }
        if(k==1){  //当k为第一层的时候就只有一个结点
            return 1;
        }
        int left=getKLevelNodeCount(root.left,k-1);
        int right=getKLevelNodeCount(root.right,k-1);
        return  left+right;
    }

    /*
     获取二叉树的高度
     时间复杂度：O(N)
     */
    int getHeight(TreeNode root) {  //那一边高就取那边加上第一层的高度
        if(root==null){
            return 0;
        }
        int leftSize=getHeight(root.left);
        int rightSize=getHeight(root.right);
        return (leftSize>rightSize)?(leftSize+1):(leftSize+1);
    }


    // 检测值为value的元素是否存在
    TreeNode find(TreeNode root, char val) {
        if(root==null){
            return null;
        }
        if(root.val==val){
            return root;
        }
        TreeNode rootleft=find(root.left,val);
        if(rootleft!=null){
            return rootleft;
        }
        TreeNode rootright=find(root.right,val);
        if(rootright!=null){
            return rootright;
        }
        return null;
    }

    //层序遍历
    void levelOrder(TreeNode root) {
        if(root==null){  //没有返回值的写法，利用队列
                 return ;
             }
             Queue<TreeNode> queue=new LinkedList<>();
             queue.offer(root);
             while(!queue.isEmpty()){
                 TreeNode cur=queue.poll();
                 System.out.print(cur.val+" ");
                 if(cur.left!=null){
                     queue.offer(cur.left);
                 }
                 if(cur.right!=null){
                     queue.offer(cur.right);
                 }
             }
    }


    // 判断一棵树是不是完全二叉树
    Queue<TreeNode> queue=new LinkedList<>();
    boolean isCompleteTree(TreeNode root) {  //利用队列判断 弹出一个元素判断他是不是空，如果不是空就把这个不是空的结点的左右带进来，如果为空就停止
        if(root==null){                      //如果剩下的元素都为null则为完全二叉树，如果有不是空的元素就不是完全二叉树
            return true;
        };
        queue.offer(root);
        while(!queue.isEmpty()) {
            TreeNode cur=queue.poll();
            if(cur!=null){
                queue.offer(cur.left);
                queue.offer(cur.right);
            }else {
                break;
            }
        }
        while (!queue.isEmpty()){
            TreeNode tmp=queue.poll();
            if(tmp!=null){
                return  false;
            }
        }
        return true;
    }


}
