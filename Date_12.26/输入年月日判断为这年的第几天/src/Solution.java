import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int year=scanner.nextInt();
        int month=scanner.nextInt();
        int date=scanner.nextInt();
        int sum=0;
        if(year%4==0&&year%100!=0||year%400==0){  //判断这年是否为闰年
            int arr[]={31,29,31,30,31,30,31,31,30,31,30,31};  //如果是二月为29天
            for (int i = 0; i <month-1; i++) {
               sum+=arr[i];
            }
            sum+=date;
            System.out.println(year+"年"+month+"月"+date+"日"+"是该年的第"+sum+"天");
        }else{
            int arr[]={31,28,31,30,31,30,31,31,30,31,30,31};   //不是闰年二月为28天
            for (int i = 0; i <month-1; i++) {     //这里要-1对应数组的下标
                sum+=arr[i];   //前面所有月的天数加起来
            }
            sum+=date;  //最后加上当月的多少天
            System.out.println(year+"年"+month+"月"+date+"日"+"是该年的第"+sum+"天");
        }
        scanner.close();  //释放输入流
    }
}
