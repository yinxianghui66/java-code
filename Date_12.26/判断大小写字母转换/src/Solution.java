import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        char letter=scanner.next().charAt(0);   //输入一个字符
        if(letter>=97&&letter<=122){   //使用ASCII码判断是否为小写字母
            System.out.println((char)(letter-32));   //如果是转换为大写字母
        }else{
            System.out.println("不是小写字母");
        }

    }
}
