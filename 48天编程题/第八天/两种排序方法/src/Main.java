import java.util.Scanner;
//考拉有n个字符串字符串，任意两个字符串长度都是不同的。考拉最近学习到有两种字符串的排序方法： 1.根据字符串的字典序排序。例如：
//        "car" < "carriage" < "cats" < "doggies < "koala"
//        2.根据字符串的长度排序。例如：
//        "car" < "cats" < "koala" < "doggies" < "carriage"
//        考拉想知道自己的这些字符串排列顺序是否满足这两种排序方法，考拉要忙着吃树叶，所以需要你来帮忙验证。
//如果这些字符串是根据字典序排列而不是根据长度排列输出"lexicographically",
//        如果根据长度排列而不是字典序排列输出"lengths",
//        如果两种方式都符合输出"both"，否则输出"none"
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        String s= scanner.nextLine();  //取走输入的回车
        String arr[]=new String[n];
        for (int i = 0; i <n; i++) {
            arr[i]=scanner.nextLine();
        }
        int dic=1;   //1表示是按照顺序的  -1为不遵守规则
        int len=1;
        for (int i = 0; i <arr.length-1; i++) {
            if (arr[i].compareTo(arr[i+1])>0) {   //这里要用compareTo 不能用>  <!!!    如果首字符小于大于后面的字符就不合规矩
                dic = -1;
            }
        }
        if(dic==-1){
            for (int i = 0; i < arr.length-1; i++) {
            if(arr[i].length()>=arr[i+1].length()){    //前一个字符串比后一个长就不合规矩
                len=-1;
                }
            }
            if(len==1){
                System.out.println("lengths");
            }else {
                System.out.println("none");
            }
        }
        if(dic==1){
            for (int i = 0; i < arr.length-1; i++) {
                if(arr[i].length()>arr[i+1].length()){
                    len=-1;
                }
            }
            if(len==1){
                System.out.println("both");
            }else {
                System.out.println("lexicographically");
            }
        }
    }
}
