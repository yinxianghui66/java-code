import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner  scanner=new Scanner(System.in);
        int A= scanner.nextInt();
        int B= scanner.nextInt();
        if(A<B){
            int c=A;
            A=B;
            B=c;
        }
        System.out.println(A*B/least(A,B));

//         if(A%B==0||B%A==0){
//             System.out.println(A>B?A:B);
//         }else {
//             System.out.println(A*B);
//         }

//        for (int i = 1; i <100000 ; i++) {
//            if((i%A==0)&&(i%B==0)){
//                System.out.println(i);
//                break;
//            }
//        }
    }

    public static int least(int a, int b) {  //求得最大公约数
        if (a % b == 0) {  //如果可以整除 此时的除数b就为最大公约数
            return b;
        }
        return least(b,(a % b));  //不能整除传入除数和余数继续

    }
}
