import java.util.Scanner;
//输入一行，每行空格分割，分别为年月日 ，输出为这一年的第几天
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arr[] = new int[3];
        for (int i = 0; i < arr.length; i++) {   //输入到数组里面 下标为零是年，下标1为月，下标2为这个月多少天
            arr[i] = scanner.nextInt();
        }
        int count =0; //初始化加上这个月的多少天
        int arr2[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};   //平年每个月的多少天
        if ((arr[0]%4==0&&arr[0]%100!=0)||arr[0]%400==0){  //如果是闰年二月就多一天
            arr2[1]+=1;
        }
        for (int i = 0; i <arr[1]-1 ; i++) {  //输入为几月就从一月加到几月 注意-1，当前月不能加
            count+=arr2[i];
        }
        count+=arr[2];  //加上这个月的多少天
        System.out.println(count);
    }
}
