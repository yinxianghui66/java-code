import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

public class Main1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            BigInteger bigInteger[]=new BigInteger[3];
            for (int i = 0; i <bigInteger.length ; i++) {
                bigInteger[i]=scanner.nextBigInteger();   //使用BigInteger数组来防止数据过大
            }
            Arrays.sort(bigInteger);  //将数据从小到大排序
            if(bigInteger[0].add(bigInteger[1]).compareTo(bigInteger[2])>0){ //如果最小的两个数的和大于第三边就可以构成三角形
                System.out.println("Yes");
            }else {
                System.out.println("No");
            }
        }
    }
}
