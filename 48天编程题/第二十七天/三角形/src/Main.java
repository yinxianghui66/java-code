import java.util.Scanner;
//输入三个数 判断是否可以成为三角形
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            double a = scanner.nextDouble();  //防止溢出使用Double类型
            double b = scanner.nextDouble();
            double c = scanner.nextDouble();
            //两边之和大于第三边并且两边之差小于第三步
            if ((a + b > c && a - b < c) && ((a + c > b && a - c < b)) && (b + c > a && b - c < a)) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }
}
