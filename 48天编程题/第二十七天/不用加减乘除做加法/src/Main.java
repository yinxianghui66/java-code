import java.util.Scanner;
//给你两个数 不用加减乘除算出两数之和
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int num1= scanner.nextInt();
        int num2= scanner.nextInt();
        System.out.println(Add(num1, num2));
    }
    public static int Add(int num1,int num2) {
        int c=num1^num2;  //如果没有进位的情况就是num1^num2
        int d=(num1&num2)<<1;  //如果有进位 就num1&num2<<1 就为进位后的数
        if(d!=0){  //只要这个数不为零就需要进位
            return  d+c;
        }else {  //为零就直接返回c
            return c;
        }
    }
}
