import java.util.Scanner;
//链接：https://www.nowcoder.com/questionTerminal/d83721575bd4418eae76c916483493de
//        来源：牛客网
//        广场上站着一支队伍，她们是来自全国各地的扭秧歌代表队，现在有她们的身高数据，请你帮忙找出身高依次递增的子序列。
//        例如队伍的身高数据是（1、7、3、5、9、4、8），
//        其中依次递增的子序列有（1、7），（1、3、5、9），（1、3、4、8）等，其中最长的长度为4。
//动态规划
//问题：求N个元素数组的最大上升自序列
//定义状态：F(i)：以arr[i]结尾的最长上升自序列
//状态转移方程： 按照上面的案例分析
//  F(0): 1  {1}    以下标0结尾的元素 (1)
//  F(1): 2  {1,7}  以下标1结尾的元素 (7)      拿结尾元素7和1比较 如果7比1小就可以组成序列
//  F(2): 2  {1,3}  以下标2结尾的元素 (3)
//  F(3): 3  {1,3,5}  以下标3结尾的元素 (5)    拿结尾元素和arr元素挨个比较 如果结尾元素比arr元素大就可以组成 dp[j]+1
//  F(4): 4  {1,3,5,9}  以下标4结尾的元素 (9)    如果小就啥也不干
//  状态转移方程为 F(i)=max(F(i),F(j)+1)    如果之前的长度比后面长就不变 取两者大值
//  结果存储每一步在dp数组
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            int n=scanner.nextInt();
            int[]arr=new int[n];
            for (int i = 0; i <n; i++) {
                arr[i]= scanner.nextInt();
            }
            System.out.println(LIS(arr,n));
        }
    }

    private static int LIS(int[] arr, int n) {
        //保存以arr[i]结尾的最长上升子序列的个数
        int []dp =new int[n];
        dp[0]=1;//从1开始子序列
        int max=1;
        for (int i = 1; i <n; i++) {
            //求以arr[i] 结尾的最长上升子序列的个数
            //将arr[i]与arr[0]~arr[i-1] 比较
            //如果大于说明可以组成上升子序列
            //更新dp[i]=max=(dp[i],dp[j]+1)
            dp[i]=1;
            for (int j = 0; j <i; j++) {
                if(arr[i]>arr[j]){
                    dp[i]=Math.max(dp[i],dp[j]+1);  //如果前一个的序列比后的长 取两者的最大值
                }
            }
            max=Math.max(max,dp[i]);  //找到最大的序列长度
        }
        return max;
    }
}
