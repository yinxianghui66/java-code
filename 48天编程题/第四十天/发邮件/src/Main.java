import java.util.Scanner;
//链接：https://www.nowcoder.com/questionTerminal/95e35e7f6ad34821bc2958e37c08918b
//        来源：牛客网
//        NowCoder每天要给很多人发邮件。有一天他发现发错了邮件，把发给A的邮件发给了B，
//        把发给B的邮件发给了A。于是他就思考，要给n个人发邮件，在每个人仅收到1封邮件的情况下，
//        有多少种情况是所有人都收到了错误的邮件？
//        即没有人收到属于自己的邮件。
public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        long c[]=new long[22];
        c[0]=0;  //特殊的 如果为0个人为1
        c[1]=0; //为1个人肯定发不错 为0种
        c[2]=1;  //如果为两个人就只有相互发错一种
        for (int i = 3; i <21 ; i++) {
            c[i]=(i-1)*(c[i-1]+c[i-2]);  //对于n个人 如果发送错 此时有两种情况
                                        //举例 A B C D 四个 此时我把A的发给B了
        }                               // 此时 如果 B 的邮件发到A 两个相互错了 此时就为f(n-2) 这两个一定是互相错的了
                                        //如果此时B 的邮件发给  C或者D了    就为f(n-1)
        while (sc.hasNext()){
            System.out.println(c[sc.nextInt()]);
        }
    }
}
