import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        int count=0;
        int max=0;
        for (int i = 0; i <32; i++) {   //32位处理
            if((n&1)==1){   //如果最后一位为1 那么就count++  如果不为1则不是连续的了
                count++;
                if(max<count){  //如果后面有队列是大于之前的1了，就更新一下
                    max=count;
                }
            }else {   //不是1就不是连续的了，count置为0
                count=0;
            }
            n>>=1;  //n一次右移一位，一位一位比较
        }
        System.out.println(max);
    }
}
