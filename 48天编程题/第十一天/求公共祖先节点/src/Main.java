import java.util.Scanner;
//将一棵无穷大满二叉树的结点按根结点一层一层地从左往右编号，根结点编号为1。
//        现给定a，b为两个结点。设计一个算法，返回a、b最近的公共祖先的编号。注意其祖先也可能是结点本身。
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int a= scanner.nextInt();
        int b= scanner.nextInt();
        while (a!=b){   //如果a b不相等  就还没找到公共结点
            if(a>b){   //找出ab较大值 /2
                a/=2;、
            }else {
                b/=2;
            }
        }
        System.out.println(a);
    }
}
