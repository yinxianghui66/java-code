import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        int count=0;
        for (int i = 1; i < n; i++) {  //除去0 不是完全数
            if(perfect(i)){  //判断当前数是不是完全数
                count++;
            }
        }
        System.out.println(count);
    }

    private static boolean perfect(int i) {
        int sum=0;
        for (int j = 1; j <i; j++) {  //除去其本身 如果可以整除就是约数
            if(i%j==0){
                sum+=j;  //约数和相加
            }
        }
        if(sum==i){  //如果相同就为完全数
            return true;
        }
        return false;
    }
}
