import java.util.Scanner;

public class Main{
    public static void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNext()){
            String A=scanner.nextLine();
            System.out.println(judge2(A));
        }
    }
    //得到牌的张数
    public static int judge(String a){
        String[] A=a.split(" ");
        if(A.length==1)
            return 1;
        else if(A.length==3)
            return 3;
        else if(A.length==4)
            return 4;
        else if(A.length==5)
            return 5;
        return 2;
    }
    //得到输出结果的方法
    public static String judge2(String A){
        String[] op=A.split("-");
        int a=judge(op[0]);  //第一手牌的长度
        int b=judge(op[1]);   //第二手牌的长度
        if(op[0].equals("joker JOKER")||op[1].equals("joker JOKER")) //如果有王炸的情况
            return "joker JOKER";
        if(a!=b){
            if(a==4)  //如果为炸弹的情况
                return op[0];
            else if(b==4)
                return op[1];    //如果为炸弹的情况
            else return "ERROR";
        }
        else {
            String a1=op[0].split(" ")[0];    //一定牌的大小相同
            String a2=op[1].split(" ")[0];
            return lk(a1,a2)?op[0]:op[1];    //返回两组牌大的一组
        }
    }
    //判断字符串A是否大于字符串b
    public static boolean lk(String a, String b){
        String[] s=new String[]{"3","4","5","6","7","8","9","10","J","Q","K","A","2","joker","JOKER"};
        int next1=0,next2=0;
        for(int i=0;i<s.length;i++){
            if(s[i].equals(a))
                next1=i;
            if(s[i].equals(b))
                next2=i;
        }
        return next1>=next2?true:false;  //通过比较数组下标来确定大小
    }
}