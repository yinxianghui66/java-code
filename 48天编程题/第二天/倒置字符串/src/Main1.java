import java.util.Scanner;

public class Main1 {

    public static void reverse(char[] c,int i,int j) {
        while(i < j) {
            char tmp = c[i];
            c[i] = c[j];
            c[j] = tmp;
            i++;
            j--;
        }
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        String s = cin.nextLine();
        s = s.trim();//去掉字符串两端的多余的空格
        char[] c = s.toCharArray();//将字符串转化为数组
        reverse(c,0,c.length-1);//所有的字符都反转
        int start = 0;
        for(int i = 0; i < c.length; i++) {
            if(c[i] == ' ') {
                reverse(c,start,i-1);//反转每一个单词
                start = i+1;
            }
        }
        reverse(c,start,c.length-1);//反转最后一个单词
        System.out.println(c);//输出
    }

}
