import sun.security.krb5.SCDynamicStoreConfig;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int arr[]=new int[n];
        for (int i = 0; i <arr.length; i++) {
            arr[i]=scanner.nextInt();
        }
        int count=1;
        int flg=0;    //用来表示状态  0不增不减，-1为递减  1为递增
        for (int i = 0; i <arr.length-1; i++) {
            if (arr[i]<arr[i+1]){
                if(flg==0){
                    flg=1;
                }
                if(flg==-1){
                    flg=0;
                    count++;
                }
            }else if(arr[i]>arr[i+1]){
                if(flg==0){
                    flg=-1;
                }
                if(flg==1){
                    flg=0;
                    count++;
                }
            }
        }
        System.out.println(count);
    }
}
