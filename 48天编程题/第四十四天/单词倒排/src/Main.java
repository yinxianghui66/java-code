//链接：https://www.nowcoder.com/questionTerminal/81544a4989df4109b33c2d65037c5836
//        来源：牛客网
//        对字符串中的所有单词进行倒排。
//        说明：
//        1、构成单词的字符只有26个大写或小写英文字母；
//        2、非构成单词的字符均视为单词间隔符；
//        3、要求倒排后的单词间隔符以一个空格表示；如果原字符串中相邻单词间有多个间隔符时，倒排转换后也只允许出现一个空格间隔符；
//        4、每个单词最长20个字母；
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            String s = scanner.nextLine();
            StringBuffer stringBuffer=new StringBuffer();
            for (int i = 0; i < s.length(); i++) {
                //如果为合法的字母
                if ((s.charAt(i) >= 'a' && s.charAt(i) <= 'z') || (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z')||s.charAt(i)==' ') {
                    //合法就加入
                    stringBuffer.append(s.charAt(i));
                    //如果是连续的间隔符
//                    if(s.charAt(i)==' '&&s.charAt(i+1)==' '&&i!=s.length()-1) {
//                        i++;
//                        //跳过这个间隔符
//                    }
                } else {
                    //如果是非法字符或者分隔符 一律按照一个分隔符加入set
                    stringBuffer.append(' ');
//                    //如果非法字符连续 就跳过
//                    if(!((s.charAt(i+1) >= 'a' && s.charAt(i+1) <= 'z') || (s.charAt(i+1) >= 'A' && s.charAt(i+1) <= 'Z')||s.charAt(i+1)==' '&&i!=s.length()-1)){
//                        i++;
//                    }
                }
            }
            //反转后输出
            stringBuffer.reverse();
            StringBuffer stringBuffer3=new StringBuffer();
            for (int i = 0; i <stringBuffer.length(); i++) {
                //遍历查找分隔符 找到一个就翻转输出一次
                if(stringBuffer.charAt(i)!=' '||i==stringBuffer.length()-1){
                    //只要不是分隔符就加入
                    stringBuffer3.append(stringBuffer.charAt(i));

                    if(i==stringBuffer.length()-1){
                        //如果结尾处有里面有空格需要去除空格
                        if(stringBuffer3.charAt(stringBuffer3.length()-1)==' '){
                            //如果结尾是空格需要删除
                            stringBuffer3.deleteCharAt(stringBuffer3.length()-1);
                        }
                        //最后翻转不需要分隔符
                        System.out.print(stringBuffer3.reverse());
                    }
                }else if(stringBuffer.charAt(i)==' '){
                    //找到一个分隔符 翻转输出 加上本来的' '分隔符 清空stringBuffer3
                    System.out.print(stringBuffer3.reverse()+" ");
                    stringBuffer3.delete(0,stringBuffer3.length());
                }
            }

        }
    }
}
