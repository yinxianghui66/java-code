import java.util.Scanner;
//        从C++转到Java的程序员，一开始最不习惯的就是变量命名方式的改变。
//        C语言风格使用下划线分隔多个单词，例如“hello_world
//        而Java则采用一种叫骆驼命名法的规则:除首个单词以外
//        所有单词的首字母大写，例helloWorld
//        设计代码帮助自动转换变量名
//         思路如下
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            String s= scanner.nextLine();
            StringBuffer stringBuffer=new StringBuffer(s);
            for (int i = 0; i <stringBuffer.length(); i++) {
                if(stringBuffer.charAt(i)=='_'){
                    //找到下划线 删除这位的下划线  给这位添加大写字母 删除后一位原来的小写字母
                    stringBuffer.deleteCharAt(i);
                    char c=stringBuffer.charAt(i);
                    stringBuffer.insert(i,Character.toUpperCase(c));
                    stringBuffer.deleteCharAt(i+1);
                }
            }
            System.out.println(stringBuffer);
        }
    }
}
