import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        while (sc.hasNext()) {
            String arr[]=new String[20];
            for (int i = 0; i < 20; i++) {
                arr[i]= sc.next();
            }
            //从各个方向判断五子棋
            System.out.println(sover(arr)?"Yes":"No");
        }
    }

    private static boolean sover(String[] arr) {
        //对棋谱的每个位置进行检测 如果该位置是棋子 就对棋子周围是否存在五子连珠
        for (int i = 0; i <20; i++) {
            for (int j = 0; j <20; j++) {
                if(arr[i].charAt(j)=='*'||(arr[i].charAt(j)=='+')){
                    //找到棋子 对周围进行统计
                    if(Count(arr,i,j,arr[i].charAt(j))>=5){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    private static int  Count(String[] arr ,int x,int y,char c){
        //向i j 位置的四个方向检测 统计相同颜色的最大个数
        int[][][] direct={
                {{0,-1},{0,1}},  //横向    从左到右
                {{-1,0},{1,0}}, //数向    从上到下
                {{-1,-1},{1,1}}, //左上到右下  从左上到右下
                {{-1,1},{1,-1}}, //右上到左下   //从右上到左下
        };
        int result=0;
        for (int i = 0; i <4; i++) {
            //统计单个方向相同棋子的个数
            int num=0;
            for (int j = 0; j <2; j++) {
                //将一个方向分成两个方向遍历
                int nx=x;
                int ny=y;
                //朝一个方向统计个数
                while (nx>=0&&nx<20&&ny>=0&&ny<20&&arr[nx].charAt(ny)==c){
                    num++;
                    nx=nx+direct[i][j][0];
                    ny=ny+direct[i][j][1];
                }
            }
            result=Math.max(num,result);  //找到最大值
        }
        return result-1;  //两个方向中间的会统计两次 最后减去1
    }
}
