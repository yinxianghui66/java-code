import java.util.Scanner;
import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            Stack<String> sk = new Stack<String>();
            for (int i = 0; i < n; i++) {
                String s = scanner.next();  //一次取一个元素
                switch (s) {
                    case "+"://如果是运算符 从栈取出两个元素运算结果放回栈
                        sk.push(String.valueOf(Integer.valueOf(sk.pop()) + Integer.valueOf(sk.pop())));
                        break;
                    case "-":  //如果是减法 就用大的减去小的
                        int x= Integer.parseInt(sk.pop());
                        int y= Integer.parseInt(sk.pop());
                        sk.push(String.valueOf(y-x));
                        break;
                    case "*":
                        sk.push(String.valueOf(Integer.valueOf(sk.pop()) * Integer.valueOf(sk.pop())));
                        break;
                    case "/":
                        int x1= Integer.parseInt(sk.pop());
                        int y1= Integer.parseInt(sk.pop());
                        sk.push(String.valueOf(y1/x1));
                        break;
                    default:
                        sk.push(s);  //如果是数字就入栈
                        break;
                }
            }
            System.out.println(sk.pop());  //栈顶元素就为最终结果
        }
    }
}
