import java.util.Scanner;
//链接：https://www.nowcoder.com/questionTerminal/b388bdee5e3e4b1c86a79ad1877a3aa4?f=discussion
//        来源：牛客网
//        For two rational numbers, your task is to implement the basic arithmetics, that is,
//        to calculate their sum, difference,
//        product and quotient.

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s1= scanner.next();
        String s2= scanner.next();
        //将分子分母都分配出来
        long num1=Integer.parseInt(s1.substring(0,s1.indexOf('/')));
        long demo1=Integer.parseInt(s1.substring(s1.indexOf('/')+1));
        long num2=Integer.parseInt(s2.substring(0,s2.indexOf('/')));
        long demo2=Integer.parseInt(s2.substring(s2.indexOf('/')+1));
        //将第一个分数变为最简
        String n1=resultSort(num1,demo1);
        String n2=resultSort(num2,demo2);
        String numResult=num(num1,demo1,num2,demo2);
        String differenceResult=difference(num1,demo1,num2,demo2);
        String accumulateResult=accumulate(num1,demo1,num2,demo2);
        String businessResult=business(num1,demo1,num2,demo2);
        System.out.println(n1+" "+"+"+" "+n2+" "+"="+" "+numResult);
        System.out.println(n1+" "+"-"+" "+n2+" "+"="+" "+differenceResult);
        System.out.println(n1+" "+"*"+" "+n2+" "+"="+" "+accumulateResult);
        System.out.println(n1+" "+"/"+" "+n2+" "+"="+" "+businessResult);
    }

    private static String business(long num1, long demo1, long num2, long demo2) {
        //除法需要特殊判断 如果分母为零直接返回inf
        if(demo2==0){
            return "Inf";
        }
        long num=num1*demo2;
        long demo=demo1*num2;
        //如果商的分母为负数 那么就取相反数
        if(demo<0){
            num=-num;
            demo=-demo;
        }
        return resultSort(num,demo);
    }

    private static String accumulate(long num1, long demo1, long num2, long demo2) {
        long num=num1*num2;
        long demo=demo1*demo2;
        return resultSort(num,demo);
    }

    private static String difference(long num1, long demo1, long num2, long demo2) {
        long num=num1*demo2-demo1*num2;  //两个分数相减 分子交叉相乘再相减
        long demo=demo1*demo2;   //分母为两个分母相乘
        return resultSort(num,demo);
    }

    private static String num(long num1, long demo1, long num2, long demo2) {
        long num=num1*demo2+demo1*num2;  //两个分数相加 分子交叉相乘再相加
        long demo=demo1*demo2;   //分母为两个分母相乘
        return resultSort(num,demo);
    }

    private static String resultSort(long num, long demo) {  //负责将分数处理为最简的假分数 或者分数
        String result="";
        result+="(";
        if(demo==0){ //如果分母为返回Inf
            return "Inf";
        }
        if (num % demo == 0) {  //如果分数为整数的时候
            result+=num/demo;
        }else {
            long divisor=measure(num,demo);
            //现在给分数化简
            num/=divisor;
            demo/=divisor;
            //处理假分数的情况
            if(num/demo!=0){
                result+=(num/demo)+" "+(Math.abs(num%demo));
            }else {
                //如果不是假分数就直接返回
                result+=num;
            }
            result+="/"+demo; //最后加上除号和分母
        }
        result+=")";
        if(result.charAt(1)!='-'){ //当前的分数不为负数
            //如果不为-就不需要()
            return result.substring(1,result.length()-1);
        }
        return result;
    }

    private static long measure(long x,long y){
        long z=y;
        while (x%y!=0){
            z=x%y;
            x=y;
            y=z;
        }
        return Math.abs(z);  //这里一定要返回正数 因为x*y可能超出了long的长度导致溢出返回负数
    }

}
