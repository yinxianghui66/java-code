import java.util.*;
public class Main2{
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        String str1 = scan.next();
        String str2 =scan.next();
        // 因为输入为 字符串，所以要 先从字符串里提取出 分子 和 分母
        long numerator1 = Integer.parseInt(str1.substring(0,str1.indexOf('/')));
        long denominator1 =Integer.parseInt(str1.substring(str1.indexOf('/')+1));
        long numerator2 = Integer.parseInt(str2.substring(0,str2.indexOf('/')));
        long denominator2 =Integer.parseInt(str2.substring(str2.indexOf('/')+1));
        // 把第一个数 处理为合规的数，然后直接输出
        String num1 = resultSort(numerator1,denominator1);
        // 把第二个数 处理为合规的数，然后直接输出
        String num2 = resultSort(numerator2,denominator2);
        String sumResult = sum(numerator1,denominator1,numerator2,denominator2);
        String differenceResult = difference(numerator1,denominator1,numerator2,denominator2);
        String productResult = product(numerator1,denominator1,numerator2,denominator2);
        String quotientResult =quotient(numerator1,denominator1,numerator2,denominator2);
        System.out.println(num1+" "+ "+"+" "+ num2+" "+"=" +" "+sumResult);
        System.out.println(num1+" "+ "-"+" "+ num2+" "+"=" +" "+differenceResult);
        System.out.println(num1+" "+ "*"+" "+ num2+" "+"=" +" "+productResult);
        System.out.println(num1+" "+ "/"+" "+ num2+" "+"=" +" "+quotientResult);
    }
    // 这个就是处理 结果的函数
    public static String resultSort(long numerator1,long denominator1){
        String result ="";
        result += "(";
        // 这里 处理整数
        if(numerator1 % denominator1 == 0){
            result += numerator1 / denominator1;
            // 这里处理分数
        }else{
            // 算最大公约数
            long divisor = divisor(numerator1,denominator1);
            numerator1 /= divisor;  //约分  化成最简
            denominator1 /= divisor;
            // 处理假分数
            if(numerator1 / denominator1 != 0){  //如果不能整除说明可以化为假分数
                result += (numerator1 / denominator1)+" "+(Math.abs((numerator1 %
                        denominator1)));
            }else{
                result += numerator1 ;  //如果没有假分数就直接加上分子
            }
            result += "/"+denominator1;  //最后加上/和分母

        }
        result += ")";
        // 针对结果是负数还是正数，进行分别处理
        if(result.charAt(1)!='-'){  //如果是负数则需要()不为负数就去除()
            result = result.substring(1,result.length()-1);
        }
        return result;
    }
    // 求和函数
    public static String sum(long numerator1,long denominator1,long numerator2,long denominator2){
        numerator1 = numerator1 * denominator2 + denominator1*numerator2;
        denominator1 = denominator2* denominator1;
        return resultSort(numerator1,denominator1);
    }
    // 求差函数
    public static String difference(long numerator1,long denominator1,long numerator2,long denominator2){
        numerator1 = numerator1 * denominator2 - denominator1 * numerator2;
        denominator1 = denominator2* denominator1;
        return resultSort(numerator1,denominator1);
    }
    // 求 积函数
    public static String product(long numerator1,long denominator1,long numerator2,long denominator2){
        numerator1 = numerator1 * numerator2;
        denominator1 = denominator1 * denominator2;
        return resultSort(numerator1,denominator1);
    }
    // 求商函数
    public static String quotient(long numerator1,long denominator1,long numerator2,long denominator2){
        if(numerator2 == 0)
            return "Inf";
        numerator1 = numerator1 * denominator2;
        denominator1 = denominator1 * numerator2;
        // 如果商的分母是负数，那就给分子、分母同时取相反数
        if(denominator1 < 0){
            denominator1 = -denominator1;
            numerator1 = -numerator1;
        }
        return resultSort(numerator1,denominator1);
    }
    // 辗转相除法 求最大公约数
    public static long divisor(long numerator1,long denominator1){
        if(denominator1 == 0)
            return Math.abs(numerator1);
        return divisor(denominator1,numerator1 % denominator1);
    }
}