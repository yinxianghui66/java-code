import java.util.Scanner;
//        一个正整数可以分解成一个或多个数组的积。例如36=2*2*3*3，
//                即包含2和3两个因子。NowCoder最近在研究因子个数的分布规律，现在给出一系列正整数，
//                他希望你开发一个程序输出每个正整数的因子个数。
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            int n=scanner.nextInt();
            int count=0;
            for (int i = 2; i <Math.sqrt(n); i++) {
                if(n%i==0){ //从2开始 如果能被整除 说明是一个因子
                    while (n%i==0){  //一直整除到不能整除 寻找下一个因子
                        n/=i;
                    }
                    count++;//因子++
                }
            }
                if(n==1){ //如果为1 的话就说明只有一个一个因子
                    System.out.println(count);
                }else { //如果不为1说明n为一个素数 还有一个本身也是因子
                    System.out.println(++count);
                }

        }
    }
}
