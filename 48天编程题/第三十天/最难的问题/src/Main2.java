import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            String s= scanner.nextLine();
            char[] arr=s.toCharArray(); //把密码转成数组存储
            for (int i = 0; i <arr.length; i++) {
                if(arr[i]>=65&&arr[i]<=90){  //当前密码为大写字母才破译 不为大写字母直接跳过
                    if(arr[i]<'F'){  //如果是小于F的就往后+21到对应的原文
                        arr[i]=(char)(arr[i]+21);
                    }else {
                        arr[i]=(char) (arr[i]-5);  //如果不小于F 那就直接往前-5 对应原文
                    }
                }
            }
            String str= new String(arr);
            System.out.println(str);
        }
    }
}
