import java.util.Scanner;
//        NowCoder生活在充满危险和阴谋的年代。为了生存，他首次发明了密码，用于军队的消息传递。假设你是军团中的一名军官，
//                需要把发送来的消息破译出来、并提 供给你的将军。
//        消息加密的办法是：对消息原文中的每个字母，分别用该字母之后的第5个字母替换
//                （例如：消息原文中的每个字母A 都分别替换成字母F），其他字符不 变，并且消息原文的所有字母都是大写的。密码中的字母与原文中的字母对应关系如下。
//        密码字母：A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
//        原文字母：V W X Y Z A B C D E F G H I J K L M N O P Q R S T U
//这是最笨的办法 一个一个写出来  真正一般不能这么写
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            String s= scanner.nextLine();
            StringBuffer stringBuffer=new StringBuffer();
            char arr[]={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X',
                    'Y','Z'};
            char arr1[]={'V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
            'P','Q','R','S','T','U'};
            for (int i = 0; i <s.length(); i++) {
                char c=s.charAt(i);  //挨个取出
                if(c>=65&&c<=90){  //只有字母才可以破译
                for (int j = 0; j <arr.length; j++) {
                    if(arr[j]==c){  //在密码表中查找
                        stringBuffer.append(arr1[j]);  //找到了就取出对应下标的密码
                        break;
                        }
                    }
                }else {
                    stringBuffer.append(c); //如果不是密码表里对应的就原样打印
                }
            }
            String str=stringBuffer.toString();
//            stringBuffer.delete(0,stringBuffer.length());  //每次输出完都清空一下
            System.out.println(str);
        }
    }
}

