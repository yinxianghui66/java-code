import java.util.Scanner;
//链接：https://www.nowcoder.com/questionTerminal/05f97d9b29944c018578d98d7f0ce56e
//        来源：牛客网
//
//        Web系统通常会频繁地访问数据库，如果每次访问都创建新连接，性能会很差。为了提高性能，
//        架构师决定复用已经创建的连接。当收到请求，并且连接池中没有剩余可用的连接时，系统会创建一个新连接，
//        当请求处理完成时该连接会被放入连接池中，供后续请求使用。
//
//        现在提供你处理请求的日志，请你分析一下连接池最多需要创建多少个连接。
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            int size=0;
            int max=0;
            for (int i = 0; i <n; i++) {
                String name= scanner.next();
                String ret= scanner.next();
                if(ret.equals("connect")){ //只要有一个连接 就相当于创建一个连接  //如果输入连续多个connect 在还没有
                    size++;                //disconnect 的时候 就需要创建多个连接
                    max=Math.max(max,size);  //取两者的大值 说明在前一个connect还没断开的时候 最多创建了多少个连接
                }else if(ret.equals("disconnect")){  //当为断开时 这个连接就断开了  size--
                    size--;
                }
            }
            System.out.println(max);
        }
    }

}
