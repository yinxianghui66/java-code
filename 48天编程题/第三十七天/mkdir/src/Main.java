import java.util.Arrays;
import java.util.Scanner;
//链接：https://www.nowcoder.com/questionTerminal/433c0c6a1e604a4795291d9cd7a60c7a
//        来源：牛客网
//
//        工作中，每当要部署一台新机器的时候，就意味着有一堆目录需要创建。例如要创建目录“/usr/local/bin”，
//        就需要此次创建“/usr”、“/usr/local”以及“/usr/local/bin”。好在，Linux下mkdir提供了强大的“-p”选项，
//        只要一条命令“mkdir -p /usr/local/bin”就能自动创建需要的上级目录。
//        现在给你一些需要创建的文件夹目录，请你帮忙生成相应的“mkdir -p”命令。

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            String s[] = new String[n];
            scanner.nextLine();
            for (int i = 0; i < n; i++) {
                s[i] = scanner.nextLine();
            }
            Arrays.sort(s); //给字符串数组排序  这样就可以保证最接近的字符串相邻
            for (int i = 1; i < n; i++) {  //用后一个和前一个比较 看是否是延续关系 注意判断/a/ab和/a/abc  如果是延续关系前一句就可以不用打印
                if (s[i].contains(s[i - 1]) && s[i].charAt(s[i - 1].length()) == '/') {  //如果是后者包含前者的关系 并且后者的字符串中前者的长度位置的字符应该为/
                    s[i - 1] = "";  //将其置为空                                 //也就是用来区分 /a/ab和/a/abc (这种并不是延续关系)  /a 和/a/b才是延续关系
                }
            }
            for (int i = 0; i < n; i++) { //遍历 如果是层级关系前一层就为"" 就不打印
                if (!"".equals(s[i])) {
                    System.out.println("mkdir -p " + s[i]);
                }

            }
            System.out.println();
//        3
//                /a
//                /a/b
//                /a/b/c
//        3
//                /usr/local/bin
//                /usr/bin
//                /usr/local/share/bin
//
//        mkdir -p /a/b/c
//
//        mkdir -p /usr/bin
//        mkdir -p /usr/local/bin
//        mkdir -p /usr/local/share/bin
        }
    }
}

