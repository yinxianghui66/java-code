public class Main {
    public static void main(String[] args) {
        int arr[][]=new int[6][6];
        int t=100;
        for (int i = 0; i <6 ; i++) {
            for (int j = 0; j <6 ; j++) {
                arr[i][j]=t++;
            }
        }
        System.out.println(getMost(arr));
    }
    //状态转移方程：F(i,j)=max(board[i-i][j],board[i][j-1])+board[i][j]
    public static int getMost(int[][] board) {
        // write code here
        int n=board.length;
        int m=board[0].length;
        for (int i = 1; i <n ; i++) {  //处理第一行
            board[0][i]+=board[0][i-1];
        }
        for (int i = 1; i <m; i++) {  //处理第一列
            board[i][0]+=board[i-1][0];
        }
        //处理非第一行和第一列
        for (int i = 1; i <n ; i++) {  //从1开始因为第一行和第一列已经处理完了
            for (int j = 1; j <m; j++) {
                board[i][j]+=Math.max(board[i-1][j],board[i][j-1]);   //每一次的结果就为左边和右边的最大值
            }
        }
        return board[n-1][m-1];  //返回最后结果
    }
}