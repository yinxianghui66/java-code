import java.util.Scanner;
//nowcoder有两盒（A、B）乒乓球，有红双喜的、有亚力亚的……现在他需要判别A盒是否包含了B盒中所有的种类，
//        并且每种球的数量不少于B盒中的数量，该怎么办呢？
//链接：https://www.nowcoder.com/questionTerminal/bb4f1a23dbb84fd7b77be1fbe9eaaf32
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
//            String s= scanner.nextLine();
//            String[] box=s.split(" ");
            String A=scanner.next();
            String B=scanner.next();
            if(func(A,B)){
                System.out.println("Yes");
            }else {
                System.out.println("No");
            }
        }
    }

    private static boolean func(String A, String B) {
       if(A.length()==0){
           return false;
       }
       if(B.length()==0){
           return true;
       }
        int count=0;
        StringBuffer stringBufferA=new StringBuffer(A);
        for (int i = 0; i <B.length(); i++) {
            //遍历B 挨个字符找A 找到就删除A中对应的乒乓球
            for (int j = 0; j <stringBufferA.length(); j++) {
                if(stringBufferA.charAt(j)==B.charAt(i)){
                    //将对应下标修改为0 表示已经不在了  统计删除了多少个
                    //注意插入一个0 删除后一个本来的元素
                    stringBufferA.insert(j,'0');
                    stringBufferA.deleteCharAt(j+1);
                    count++;
                    //找到一个就结束找下一个
                    break;
                }
            }
        }
        //如果A中删除的个数和B的长度的个数相当
        if(B.length()<=count){
            return  true;
        }
        return false;
    }
}
