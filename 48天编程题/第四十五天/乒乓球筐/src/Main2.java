import java.util.Scanner;

public class Main2{
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        while(in.hasNext()) {
            String boxA = in.next();
            String boxB = in.next();
            StringBuffer input=new StringBuffer(boxA);  //把A转成StringBuffer
            boolean contain=true;
            char[] find=boxB.toCharArray();  //把B转换成字符数组存储
            for (char c: find) {  //遍历字符数组
                int index=input.indexOf(String.valueOf(c));   //只要在input中找到B的一个元素 就直接给这个元素删除
                if(index!=-1){
                    input.deleteCharAt(index);
                }else {  //只要有一个是B里有A没有的  就直接返回No
                    System.out.println("No");
                    contain=false;
                    break;
                }
            }
            if(contain){
                System.out.println("Yes");
            }
        }
    }
}