import java.util.*;
//链接：https://www.nowcoder.com/questionTerminal/03ba8aeeef73400ca7a37a5f3370fe68
//        来源：牛客网
//
//        定义一个单词的“兄弟单词”为：交换该单词字母顺序（注：可以交换任意次），而不添加、删除、修改原有的字母就能生成的单词。
//        兄弟单词要求和原来的单词不同。例如： ab 和 ba 是兄弟单词。 ab 和 ab 则不是兄弟单词。
//        现在给定你 n 个单词，另外再给你一个单词 x ，让你寻找 x 的兄弟单词里，按字典序排列后的第 k 个单词是什么？
//        注意：字典中可能有重复单词。
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String[] array = in.nextLine().split(" ");
        List<String> bro = new ArrayList<>();
        String pattern = array[array.length - 2];
        char[] temp = pattern.toCharArray();
        int count = 0;
        Arrays.sort(temp);
        String sortedPattern = String.valueOf(temp);
        for(int i = 1; i < array.length - 2; i++){
            //相同的单词
            if(array[i].equals(pattern)) continue;
            //不同的单词
            //先排序，再比对
            char[] cur = array[i].toCharArray();
            Arrays.sort(cur);
            String sortedCur = String.valueOf(cur);
            if(sortedCur.equals(sortedPattern)){
                bro.add(array[i]);
                count++;
            }
        }
        Collections.sort(bro);
        int index = Integer.valueOf(array[array.length - 1]);
        System.out.println(count);
        if( index <= count){
            System.out.println(bro.get(index - 1));
        }
    }
}