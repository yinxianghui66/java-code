import java.util.Scanner;
        /*
链接：https://www.nowcoder.com/questionTerminal/d95d98a2f96e49078cd7df84ba0c9d79
        来源：牛客网
        和中国的节日不同，美国的节假日通常是选择某个月的第几个星期几这种形式，因此每一年的放假日期都不相同。具体规则如下：
        * 1月1日：元旦
        * 1月的第三个星期一：马丁·路德·金纪念日
        * 2月的第三个星期一：总统节
        * 5月的最后一个星期一：阵亡将士纪念日
        * 7月4日：美国国庆
        * 9月的第一个星期一：劳动节
        * 11月的第四个星期四：感恩节
        * 12月25日：圣诞节
        现在给出一个年份，请你帮忙生成当年节日的日期。
 */
//给你年月日 求这一天是星期几
//给你一个月的1号是星期几  求第n个星期的第e天   =1+(n-1)*7+(7-w+e)%7
//已知6月1 是星期几 怎么找5月的最后一个星期一  =32-(w==1?7:w-1);
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            int year= scanner.nextInt();
            System.out.printf("%d-01-01\n",year);  //1月一日元旦
            int w=0;
            w=week(year,1,1);  //一月一日是星期几
            System.out.printf("%d-01-%02d\n",year,fun(w,3,1));  //一月的第三个星期一 马丁纪念日
            w=week(year,2,1);  //二月月一日是星期几
            System.out.printf("%d-02-%02d\n",year,fun(w,3,1));  //二月的第三个星期一 总统节
            w=week(year,6,1);  //求6月1日星期几
            System.out.printf("%d-05-%02d\n",year,func(w));  //求五月的最后一个星期一 只需要知道六月一日是星期几
                                                            //把六月一日看做五月32日 用32-w==1?7:w-1  阵亡将士纪念日

            System.out.printf("%d-07-04\n",year); //七月四日国庆
            w=week(year,9,1);  //九月一日是星期几
            System.out.printf("%d-09-%02d\n",year,fun(w,1,1));  //九月的第一个星期一 劳动节
            w=week(year,11,1);  //十一月一日是星期几
            System.out.printf("%d-11-%02d\n",year,fun(w,4,4)); //十一月的第四个星期四 感恩节

            System.out.printf("%d-12-25\n",year); //十二月二十五日 圣诞节
            System.out.println(); //每次输出一个要记得换还  最后一个也要换行
        }
    }
    private static final int[] DAYS={31,28,31,30,31,30,31,31,30,31,30,31};

    private static int nDays(int y,int m,int d){  //给定年月日 求这年过了多少天
        int n=d; //最后加上这个月过了多少天
        for (int i = 0; i <m-1; i++) { //求经过了多少天
            n+=DAYS[i];
        }
        if(m>2&&isleapYear(y)){  //如果是闰年并且使用月份大于二月 就要+1
            n++;
        }
        return n;
    }
    //传入年月日  找到从公元前1年12月31日开始到现在y,m,n过了多久天
    private static int diff(int y,int m,int d){
        // 原理是y-1年 +中间有多少闰年+ 这一年过了多少天  需要公式推导
        return (y-1)+(y-1)/4-(y-1)/100+(y-1)/400+nDays(y,m,d);
    }
    //得到了过了多少天 就可以求出这天是星期几了
    private static int week(int y,int m,int d){
        int w=diff(y,m,d)%7;
        if(w==0){
            w=7;
        }
        return w;
    }
    //根据一月一日星期几 求第n个星期 e天是星期几
    private static int fun(int w,int n,int e){
        return 1+(n-1)*7+(7-w+e)%7;  //1 代表第一天也要加上  公式都是一步步推导出来的
    }
    //根据六月一日是星期几 求 五月最后一个星期一是那天
    private  static int func(int w){
        int d=0;
        if(w==1){  //如果六月一为星期一 那么只需要32-7 就可以得到是那天  把六月一当作这个月的第32天
            d=7;
        }else {
            d=w-1;
        }
        return 32-d;
    }
    private static boolean isleapYear(int year) { //判断闰年
        if((year%4==0&&year%100!=0)||(year%400)==0){
            return true;
        }
        return false;
    }
}
