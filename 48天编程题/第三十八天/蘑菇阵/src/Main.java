import java.util.Scanner;
//链接：https://www.nowcoder.com/questionTerminal/ed9bc679ea1248f9a3d86d0a55c0be10
//        来源：牛客网
//        现在有两个好友A和B，住在一片长有蘑菇的由n＊m个方格组成的草地，A在(1,1),B在(n,m)。
//        现在A想要拜访B，由于她只想去B的家，所以每次她只会走(i,j+1)或(i+1,j)这样的路线，
//        在草地上有k个蘑菇种在格子里(多个蘑菇可能在同一方格),问：A如果每一步随机选择的话(若她在边界上，则只有一种选择)，
//        那么她不碰到蘑菇走到B的家的概率是多少？
//使用动态规划的思想
// 将蘑菇位置置为1
//每个位置的概率等于这个位置的上面概率+左边两条路的概率和 如果此时为边界就只有一条路*1.0  如果不是边界有两条路概率为0.5
//在遍历数组的时候 使用数组的ij下标看蘑菇阵数组相同的位置是否为蘑菇 如果为蘑菇 此处的概率为0.0
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            int N = scanner.nextInt();
            int M = scanner.nextInt();
            int K = scanner.nextInt();
            int[][] xy = new int[N+1][M+1];
            for (int i = 0; i < K; i++) {
                int x= scanner.nextInt();
                int y= scanner.nextInt();
                xy[x][y]=1;  //将蘑菇位置置为1
            }
            double result=func(N,M,xy);
            System.out.printf("%.2f\n",result);
        }
    }

    private static double func(int n, int m, int[][]xy) {
        double[][] res=new double[n+1][m+1];
        res[1][1]=1.0;  //如果A和B在一起就是100%概率
        for (int i = 1; i <=n ; i++) {  //从1 1开始
            for (int j = 1; j <=m; j++) {
                if(!(i==1&&j==1)) {  //只要不是开始坐标
                    //此时的坐标概率相当于从上过来的概率+从左过来的概率  //如果是边界则只有一种办法走 概率就是1.0 如果不是边界概率就是两条路0.5
                    res[i][j] = res[i - 1][j] * (j == m ? 1.0 : 0.5) + res[i][j - 1] * (i == n ? 1.0 : 0.5);
                }
                if(xy[i][j]==1){  //xy和res一样大 如果此时的xy[i][j]为1就说明res这个位置也是蘑菇 就不可以走 概率为0
                    res[i][j]=0.0;
                }
            }
        }
        return res[n][m];   //此时的n m 相当于res[i - 1][j] + res[i][j - 1] 两条路的概率
    }
}
