////动态规划的思想
//import java.util.*;
//public class Main2{
//    public static double dp(int[][] arr,int n,int m){
//        double[][] res = new double[n+1][m+1];
//        res[1][1] = 1.0;
//        for(int i = 1;i <= n;i++){
//            for(int j = 1;j <= m;j++){
////如果A不在（1,1）位置，此时他有两个方向可以走（向左或者向右），概率均为0.5
////在最后一列和最后一行的时候他只有一个方向可以走，所以概率为1.0
//                if(!(i == 1 && j == 1)){
//                    res[i][j] = res[i - 1][j] * (j == m ? 1.0 : 0.5) + res[i][j - 1] * (i == n ? 1.0 : 0.5);
//                }
//                if(arr[i][j] == 1){
//                    res[i][j] = 0.0;
//                }
////如果arr[i][j]位置是蘑菇，则不可能走到该位置，所以该位置的概率为0.0
//
//        }
//        return res[n][m];
//    }
//    public static void main(String[] args){
//        Scanner sc = new Scanner(System.in);
//        while(sc.hasNext()){
//            int n = sc.nextInt();
//            int m = sc.nextInt();
//            int[][] arr = new int[n+1][m+1];
//            int k = sc.nextInt();
//            while(k != 0){
//                int x = sc.nextInt();
//                int y = sc.nextInt();
//                arr[x][y] = 1;
//                k--;
//            }
//            double result =  dp(arr,n,m);
//            System.out.printf("%.2f\n",result);
//        }
//
//    }
//}
//
