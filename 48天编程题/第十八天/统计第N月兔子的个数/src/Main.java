import java.util.Scanner;
//有一只兔子，从第3个月开始每月生1只兔子，小兔子长到第3个月开始每个月也会生1只兔子，假如兔子都不死，问第n个月的兔子总数是多少？
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        System.out.println(rebbit(n));
    }
    public static int rebbit(int n){
        if(n<3){
            return 1;  //前两个月都是一只
        }else {
            return  rebbit(n-1)+rebbit(n-2);  //递归 第n月的兔子数为前两个月之和
        }
    }
}
