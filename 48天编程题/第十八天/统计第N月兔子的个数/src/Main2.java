import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        System.out.println(Fibonacci(n));
    }

    public static int Fibonacci(int n) {
        int arr[]=new int[n+1];
        arr[1]=1;
        arr[2]=1;
        for (int i = 3; i <=n ; i++) {
            arr[i]=arr[i-1]+arr[i-2];
        }
        return arr[n];
    }
}
