import java.util.Locale;
import java.util.Scanner;
/*        问题描述：在计算机中，通配符一种特殊语法，广泛应用于文件搜索、数据库、正则表达式等领域。现要求各位实现字符串通配符的算法。
        要求：
        实现如下2个通配符：
        *：匹配0个或以上的字符（注：能被*和?匹配的字符仅由英文字母和数字0到9组成，下同）
        ？：匹配1个字符
        注意：匹配时不区分大小写。*/
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s1= scanner.nextLine();
        String s2=scanner.nextLine();
        s1=s1.toLowerCase();
        s2=s2.toLowerCase();
        System.out.println(matching(s1, s2, 0,0 ));
    }
    public static boolean matching(String s1,String s2,int c1,int c2){
        if(s1.length()==c1&&s2.length()==c2){
            return true;
        }
        if(s1.length()==c1||s2.length()==c2){
            return false;
        }
        if(s1.length()<c1||s2.length()<c2){
            return false;
        }
            if(s1.charAt(c1) == '?'){
                if(!func(s2.charAt(c2))){
                   return false;
                }  //为？就两边都+1
                return matching(s1,s2, c1+1, c2+1);
            }else if(s1.charAt(c1) == '*'){
                int tmp=c1;
                while (tmp < s1.length()&&s1.charAt(tmp) == '*'){
                    tmp++;
                }
                tmp--;
                return matching(s1,s2,tmp+1,c2)|| matching(s1,s2,tmp+1,c2+1)||matching(s1,s2,tmp,c2+1);
                //三种情况 一种匹配一个 两边都加以 ，一种匹配零个 跳过*  一种匹配多个字符
            }else if(s1.charAt(c1)==s2.charAt(c2)){  //相同两边都为+1
                return matching(s1,s2,c1+1,c2+1);
            }
            return  false;
        }
    public static boolean func(char c){
        return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'z');
    }
}

