import java.util.Scanner;
//输入链表长度 内容 和要翻转的范围  输出翻转后的链表的头结点
class ListNode{
    int val;
    ListNode next;
    ListNode(){};
    ListNode(int val){
        this.val=val;
    }
}
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int len=scanner.nextInt();
        int val[]=new int[len];  //使用数组模拟实现链表
        for (int i = 0; i <len; i++) {
            val[i]= scanner.nextInt();  //输入
        }
        int left= scanner.nextInt();
        int right= scanner.nextInt();
        ListNode head=new ListNode(val[0]);  //头结点为0下标
        ListNode cur=head;
        for (int i = 1; i <len; i++) {  //初始链表
            cur.next=new ListNode(val[i]);
            cur=cur.next;
        }
        head=reverseList(head,left,right);//翻转left-right的数据
        while (head!=null){
            System.out.print(head.val+" ");
            head=head.next;
        }
    }
        private static ListNode reverseList(ListNode head, int left, int right) {
            ListNode dummy=new ListNode(-1);
            dummy.next=head;
            ListNode prev=dummy;
            for (int i = 1; i <left; i++){
                prev=prev.next;
            }
            ListNode prev2=prev.next;
            ListNode prev3=prev.next;
            ListNode cur=prev2.next;
            for (int i = left; i <right; i++) {
                ListNode curNext=cur.next;
                cur.next=prev2;
                prev2=cur;
                cur=curNext;
                prev3.next=curNext;
            }
            prev.next=prev2;
            return dummy.next;
    }
}
