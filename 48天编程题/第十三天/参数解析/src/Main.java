import java.util.Scanner;

//        在命令行输入如下命令：
//        xcopy /s c:\\ d:\\e，
//        各个参数如下：
//        参数1：命令字xcopy
//        参数2：字符串/s
//        参数3：字符串c:\\
//        参数4: 字符串d:\\e
//        请编写一个参数解析程序，实现将命令行各个参数解析出来。
//              1.参数分隔符为空格
//              2.对于用""包含起来的参数，如果中间有空格，不能解析为多个参数。比如在命令行输入
//              xcopy /"C:\\program files" "d:\"时，参数仍然是4个，第3个参数应该是字符串C:\\program files，而不是C:\\program，" +
//              "注意输出参数时，需要将""去掉，引号不存在嵌套情况。
//              3.参数不定长
//              4.输入由用例保证，不会出现不符合要求的输入


public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s= scanner.nextLine();
        int count=0;
        String arr[]=new String[10];
        int j=0;
        StringBuffer stringBuffer=new StringBuffer();
        for (int i = 0; i <s.length(); i++) {
            if (s.charAt(i) == ' ') {   //遍历判断 找到第一个空格 前面的就为一组参数
                count++;   //参数计数
                arr[j] = stringBuffer.toString();  //把参数输出到字符数组
                i++;   //跳过当前的’ ‘ 空格进入下一位
                j++;  //字符串数组传入一个参数++
                stringBuffer.delete(0, stringBuffer.length());   //清空 stringBuffer
            }

            if (s.charAt(i) == '"') {  //如果碰到“
                for (int k = i + 1; k < s.length(); k++) {  //跳过“ 直接从后面开始找
                    if (s.charAt(k) == '"') {   //找到另一个” 与之对应
                        i++;  //直接跳过当前的”
                        i += stringBuffer.length() + 2;  //因为i+1 和i++ 相当于字符串少了2的长度 这里给他加上 相当于整个字符串都加入进去了
                        count++;  //参数计数                    //下标自然也要加上出去的长度
                        arr[j] = stringBuffer.toString();    //传入""之间的数据
                        j++;  //字符串数组++
                        stringBuffer.delete(0, stringBuffer.length());   //清空 stringBuffer
                        break;
                    } //如果当前不是另一个则添加 直到找到“
                    stringBuffer.append(s.charAt(k));
                }
            }
            if(i<=s.length()-1){  //防止越界的 当i<=最后一位 因为上面的i += stringBuffer.length() + 2 可能会超出i的范围
                stringBuffer.append(s.charAt(i));
            }
            if(i==s.length()-1){  //用来输出最后一组  因为结尾的位置并没有空格 就需要手动给他输进去
                count++;
                arr[j] = stringBuffer.toString();
                j++;
                stringBuffer.delete(0, stringBuffer.length());
            }
        }
        System.out.println(count);  //输出参数个数
        for (int i = 0; i < count; i++) {
            System.out.println(arr[i]);  //输出字符串数组内容
            }
        }
    }

