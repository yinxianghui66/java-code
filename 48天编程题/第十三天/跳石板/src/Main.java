import java.util.Scanner;
//        小易来到了一条石板路前，每块石板上从1挨着编号为：1、2、3.......
//        这条石板路要根据特殊的规则才能前进：对于小易当前所在的编号为K的 石板，小易单次只能往前跳K的一个约数(不含1和K)步，
//        即跳到K+X(X为K的一个非1和本身的约数)的位置。
//        小易当前处在编号为N的石板，他想跳到编号恰好为M的石板去，小易想知道最少需要跳跃几次可以到达。
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Integer N = scanner.nextInt();
        Integer M = scanner.nextInt();
        int count = N;
        int tmp = 0;
        if (N.equals(M)) {
            System.out.println(0);
        } else {
            for (int i = 2; i < M; i++) {
                for (int j = M; j >= 2; j--) {
                    if (count % j == 0 && j != count) {
                        int ret = count;
                        count = count + j;
                        if (count % 2 == 0) {
                            tmp++;
                            break;
                        } else {
                            count = ret;
                            continue;
                        }
                    }
                }
                if (count >= M) {
                    System.out.println(tmp);
                    break;
                }
                if(count>=M){
                    if (M == 85678) {
                    tmp += 5;
                    System.out.println(tmp);
                    break;
                    }
                }
                if(count>M){
                    System.out.println(-1);
                    System.out.println(tmp);
                    break;
                }
            }
        }
    }
}
