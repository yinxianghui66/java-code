import java.util.Scanner;

//求一个数最少几步可以变Fibonacci数  一步只能+1 或者-1
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int  N= scanner.nextInt();
        long arr[]=new long[50];
        arr[0]=0;
        arr[1]=1;
        for (int i = 2; i <arr.length ; i++) {   //初始化Fibonacci数组
            arr[i]=arr[i-1]+arr[i-2];
        }
            for (int i = 0; i < arr.length; i++) {   //遍历数组
                if (arr[i] == N) {
                    System.out.println(0);
                    break;
                }
                if (arr[i] < N && arr[i + 1] > N) {  //找到Fibonacci数<N<Fibonacci数的区间，相减就可以得到
                    if (N - arr[i] < arr[i + 1] - N) {
                        System.out.println(N - arr[i]);
                    } else {
                        System.out.println(arr[i + 1] - N);
                    }
                }
            }
    }
}
