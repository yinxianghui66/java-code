import java.util.Scanner;
import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String A=scanner.nextLine();
        int n=scanner.nextInt();
        int ret=0;  //用来记录上一次‘）’入栈的下标 如果上次已经入栈了，下一次就跳过这个
        Stack stack=new Stack<>();
        for (int i = 0; i <A.length(); i++) {
            if(A.charAt(i)=='('){   //找到（ 入栈
                stack.push(A.charAt(i));
                 for (int j = i+1; j <A.length() ; j++) {   //找到另一半）入栈
                    if(A.charAt(j)==')'&&ret!=j){
                        stack.push(A.charAt(i));
                        ret=j;  //入栈标记一下，说明该下标已经入栈了，下次不要判断这个了
                        break;  //找到一对就结束，再找下一对
                    }
                }
            }else if(A.charAt(i)==')'){  //如果不是（开头就跳过这个
                continue;
            }else if(A.charAt(i)!=')'||A.charAt(i)!='('){  //如果不是（）就返回false
                System.out.println(false);
                break;
            }
        }
        if(stack.size()%2==0){
            System.out.println(true);   //如果是合法的，那一定是一对一队，一定能被2整除
        }else {
            System.out.println(false);  //不符合就false
            }
        }
    }

