import java.util.Scanner;
//链接：https://www.nowcoder.com/questionTerminal/3549ff22ae2c4da4890e9ad0ccb6150d
//        来源：牛客网
//
//        NowCoder开了一家早餐店，这家店的客人都有个奇怪的癖好：他们只要来这家店吃过一次早餐，就会每天都过来；并且，所有人在这家店吃了两天早餐后，接下来每天都会带一位新朋友一起来品尝。
//        于是，这家店的客人从最初一个人发展成浩浩荡荡成百上千人：1、1、2、3、5……
//        现在，NowCoder想请你帮忙统计一下，某一段时间范围那他总共卖出多少份早餐（假设每位客人只吃一份早餐）。
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        long arr[]=new long[81];  //注意使用long数组存储斐波那契数 否则太大就会出错
        arr[0]=0;
        arr[1]=1;
        while (scanner.hasNext()) {
            int from = scanner.nextInt();
            int to= scanner.nextInt();
            long count=0;
            for (int i =2; i <81; i++) {  //初始化斐波那契数列
                arr[i]=arr[i-1]+arr[i-2];
            }
            for (int i = from; i <=to ; i++) {  //计算从from-to天的营收
                count+=arr[i];
            }
            System.out.print(count+" ");
            System.out.println(); //输出一个换行输入
        }

    }
}
