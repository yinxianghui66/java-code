import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            String strips=scanner.next();
            String trim= scanner.next();
            StringBuffer stringBuffer=new StringBuffer(strips);
            int count=0;
            count=func(strips,trim);  //查找两个字符串中子串的个数
            System.out.println(count);
        }
    }

    private static int func(String strips, String trim) {
        int i=strips.indexOf(trim);  //在字符串里面找子串 如果存在 返回子串的开始下标 如果不存在返回-1  如果存在 说明返回的i下标前面一定不是子串了
        if(i==-1){  //如果为-1 就说明不存在返回0
            return 0;
        }
        return 1+func(strips.substring(i+trim.length()),trim);  //如果存在 就相当于每次+1（这一次找到了） 并且下次的字符串减去现在已经
    }                                                          //找出的子串 使用substring 返回从i开始（i是在字符串中找到子串的开始下标）
}                                                              //加上子串的长度 就相当于“减去了这块布” 最后字符串中找不到子串就返回0

