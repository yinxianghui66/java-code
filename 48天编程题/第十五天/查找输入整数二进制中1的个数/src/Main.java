import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            int count=0;
            String s = Integer.toBinaryString(n);  //输入的数转换为字符串的二进制
            for (int i = 0; i <s.length(); i++) {
                if(s.charAt(i)=='1'){
                    count++;    //如果有1就加一
                }
            }
            System.out.println(count);
        }
    }
}
