import java.util.Scanner;
//        在地下室里放着n种颜色的手套，手套分左右手，但是每种颜色的左右手手套个数不一定相同。A先生现在要出门，所以他要去地下室选手套。
//        但是昏暗的灯光让他无法分辨手套的颜色，只能分辨出左右手。所以他会多拿一些手套，然后选出一双颜色相同的左右手手套
//        。现在的问题是，他至少要拿多少只手套(左手加右手)，才能保证一定能选出一双颜色相同的手套。
public class Main {

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        int arr[]={0,7,1,6};
        int arr2[]={1,5,0,6};
        System.out.println(findMinimum(n, arr, arr2));
    }

        public static int findMinimum(int n, int[] left, int[] right) {
            int leftnum=0;  //左边不为0的个数
            int rightnum=0;  //右边不为零的个数
            int leftmin=Integer.MAX_VALUE;   //左边的最小值
            int rightmin=Integer.MAX_VALUE;  //右边的最小值
            int count=0;
            for (int i = 0; i <n; i++) {
                if(left[i]*right[i]==0) {   //只要我的一边为0 那就说明不可能配套
                    count += left[i] + right[i];  //拿上不为零的一边的手套    保证所有颜色手套都要拿到
                }else {
                leftnum+=left[i]; //左边不为零的个数
                rightnum+=right[i];  //右边不为零的个数
                leftmin=Math.min(leftmin,left[i]);  //左边不为零的最小值
                rightmin=Math.min(rightmin,right[i]);  //右边不为零的最小值
                }
            }
            return count+Math.min(leftnum-leftmin+1,rightnum-rightmin+1)+1;   //左边的和 减去最小值，加上一（保证可以拿到一个其他颜色的）
        }    //一定要保证 左边或者右边所有手套都拿到       //右边的和减去最小值 也加上一 （最后加一是从左/右边拿一个手套 就可以保证一定有配套的）

}
