import java.util.Scanner;
//小易有一个W*H的网格盒子，网格的行编号为0 ~ H-1，网格的列编号为0 ~ W-1。每个格子至多可以放一块蛋糕，任意两块蛋糕的欧几里得距离不能等于2。
//最多能放多少块蛋糕
//解题思路：通过欧几里得距离，和网格的编号总结规律可以得到如果是满足欧几里得距离不能等于2，那么x1=x2，y1=y2+2或者 y1=y2 x1=x2+2
//也就是说，如果i j位置放上蛋糕了，那么[i][j+2]位置和[i+2][j]位置就不能再放了
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int W=scanner.nextInt();
        int H=scanner.nextInt();
        int count=0;
        int arr[][]=new int[W][H];
        for (int i = 0; i <W; i++) {  //先给所有位置都放上蛋糕
            for (int j = 0; j <H; j++) {
                arr[i][j]=1;
            }
        }
        for (int i = 0; i <W; i++) {
            for (int j = 0; j <H; j++) {  //i  j位置已经放上蛋糕了
                if(arr[i][j]==1){
                    count++;
                    if((i+2)<W){  //排除掉[i+2][j] 和[i][j+2] 两个不能放蛋糕的位置  （必须在网格中)
                        arr[i+2][j]=0;
                    }
                    if((j+2)<H){
                        arr[i][j+2]=0;
                    }
                }
            }
        }
        System.out.println(count);  //输出可以放的个数

    }
}
