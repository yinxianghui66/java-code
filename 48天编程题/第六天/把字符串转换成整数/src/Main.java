import java.util.Scanner;
//将一个字符串转换成一个整数，要求不能使用字符串转换整数的库函数。 数值为 0 或者字符串不是一个合法的数值则返回 0
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String str=scanner.nextLine();
        int count=0;
        int flag=1;
        char[] arr =str.toCharArray();   //转换为字符数组
        if(str.isEmpty()){
            System.out.println(0);
        }
        if(arr[0]=='+'){  //正负号处理
            flag=1;
            arr[0]='0';
        }
        if(arr[0]=='-'){
            flag=-1;
            arr[0]='0';
        }
        for (int i = 0; i <arr.length; i++) {
            if(arr[i]<'0'||arr[i]>'9'){  //只要不数字直接返回0
                count=0;
                break;
            }
            count=count*10+arr[i]-'0';  //将char类型的arr[i]转换为整数 '0'的ascll码为48 字符减去对应的ascll就为对应的大小
        }
        if(((flag==1)&&count>Integer.MAX_VALUE)||((flag==-1)&&count<Integer.MIN_VALUE)){  //溢出处理
            System.out.println(0);
        }
        System.out.println(count*flag);
    }
}
