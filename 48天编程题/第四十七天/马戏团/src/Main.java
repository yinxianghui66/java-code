import java.util.Arrays;
import java.util.Scanner;

//链接：https://www.nowcoder.com/questionTerminal/c2afcd7353f84690bb73aa6123548770
//        来源：牛客网
//        搜狐员工小王最近利用假期在外地旅游，在某个小镇碰到一个马戏团表演，
//        精彩的表演结束后发现团长正和大伙在帐篷前激烈讨论，小王打听了下了解到，
//        马戏团正打算出一个新节目“最高罗汉塔”，即马戏团员叠罗汉表演。考虑到安全因素，
//        要求叠罗汉过程中，站在某个人肩上的人应该既比自己矮又比自己瘦，或相等。
//        团长想要本次节目中的罗汉塔叠的最高，由于人数众多，正在头疼如何安排人员的问题。小王觉得这个问题很简单，
//        于是统计了参与最高罗汉塔表演的所有团员的身高体重，并且很快找到叠最高罗汉塔的人员序列。 现
//        在你手上也拿到了这样一份身高体重表，请找出可以叠出的最高罗汉塔的高度，这份表中马戏团员依次编号为1到N。
//首先对体重进行排序 然后求身高的最长上升子序列
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            //读入信息
            int n= scanner.nextInt();
            node[] arr=new node[n];
            for (int i = 0; i <n; i++) {
                scanner.nextInt();
                arr[i]=new node(scanner.nextInt(),scanner.nextInt());
            }
            System.out.println(getMaxLength(arr,n));
        }
    }
    public static int getMaxLength(node[]arr,int n){
        //排序
        Arrays.sort(arr);
        //计算最长子序列的长度
        int ret=0;
        int[] maxLength=new int[n];
        //F(i):以第i个元素结尾的最大子序列长度
        //初识状态 F(i)=1
        for (int i = 0; i <n; i++) {
            maxLength[i]=1;
        }
        for (int i = 1; i <n ; i++) {
            for (int j = 0; j <i; j++) {
                if(arr[j].h<=arr[i].h){
                    //转移方程
                    maxLength[i]=Math.max(maxLength[i],maxLength[j]+1);
                }
            }
            //更新最值 max(F(i))
            ret=Math.max(ret,maxLength[i]);
        }
        return ret;
    }


}
class node implements Comparable<node>{
    //定义对象 体重排序
    int w;
    int h;
    public node(int w,int h){
        this.w=w;
        this.h=h;
    }
    @Override
    public int compareTo(node obj) {
       int ret=w-obj.w;
        if(ret==0){
            return obj.h-h;
        }
        return ret;
    }
}
