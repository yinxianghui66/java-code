import java.util.Scanner;
//动态规划
//链接：https://www.nowcoder.com/questionTerminal/661c49118ca241909add3a11c96408c8
//来源：牛客网
//有 n 个学生站成一排，每个学生有一个能力值，牛牛想从这 n 个学生中按照顺序选取 k 名学生，
// 要求相邻两个学生的位置编号的差不超过 d，使得这 k 个学生的能力值的乘积最大，你能返回最大的乘积吗？
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int arr[]=new int[n];
        int k;
        int d;
        for (int i = 0; i <n; i++) {
            arr[i]= scanner.nextInt();
        }
        k= scanner.nextInt();
        d=scanner.nextInt();
        System.out.println(getMax(arr,n,k,d));
    }
    public static long getMax(int[]arr,int n,int k,int d){
        long[][] maxVaue=new long[n+1][k+1];
        long[][] minVaue=new long[n+1][k+1];
        long max=0;
        //F(i,j) 以第i个学生结尾 共选了j个同学的最大值
        //初始状态：F(i,1): arr[i]
        for (int i = 1; i <=n; i++) {
            maxVaue[i][1]=minVaue[i][1]=arr[i-1];
        }
        for (int i = 1; i <=n; i++) {
            for (int j = 1; j <=k; j++) {
                //约束条件 相邻同学的间隔不超过d
                for (int m = i-1; m >=Math.max(i-d,1);m--) {

                    maxVaue[i][j]=Math.max(maxVaue[i][j],Math.max(maxVaue[m][j-1]*arr[i-1],
                    minVaue[m][j-1]*arr[i-1]));

                    minVaue[i][j]=Math.min(minVaue[i][j],Math.min(maxVaue[m][j-1]*arr[i-1],
                            minVaue[m][j-1]*arr[i-1]));
                }
            }
            //更新最大值max(F(i,j))
            max=Math.max(max,maxVaue[i][k]);
        }
        return max;
    }
}
