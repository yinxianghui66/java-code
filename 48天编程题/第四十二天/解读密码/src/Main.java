import java.util.Scanner;
/*链接：https://www.nowcoder.com/questionTerminal/16fa68271ee5448cafd504bb4a64b482
        来源：牛客网
        nowcoder要和朋友交流一些敏感的信息，例如他的电话号码等。因此他要对这些敏感信息进行混淆，
        比如在数字中间掺入一些额外的符号，让它看起来像一堆乱码。
        现在请你帮忙开发一款程序，解析从nowcoder那儿接收到的信息，读取出中间有用的信息。*/
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            StringBuffer stringBuffer=new StringBuffer();  //存储密码
            String s = scanner.nextLine();
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) >= '0' && s.charAt(i) <= '9') {  //遍历查找如果是数字就加入
                    stringBuffer.append(s.charAt(i));
                }
            }
            System.out.println(stringBuffer.toString());
        }
    }
}
