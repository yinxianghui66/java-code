import java.util.Scanner;
//链接：https://www.nowcoder.com/questionTerminal/610e6c0387a0401fb96675f58cda8559
//来源：牛客网
//
//今年公司年会的奖品特别给力，但获奖的规矩却很奇葩：
//1. 首先，所有人员都将一张写有自己名字的字条放入抽奖箱中；
//2. 待所有字条加入完毕，每人从箱中取一个字条；
//3. 如果抽到的字条上写的就是自己的名字，那么“恭喜你，中奖了！”
//现在告诉你参加晚会的人数，请你计算有多少概率会出现无人获奖？
//使用错排算法
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            float result = (func(n)/count(n)) * 100; //概率应该是拿不到的方法/总共多少种拿法 *100%
            System.out.printf("%.2f", result);
            System.out.println("%");
//            float result = (func(n)/count(n)) * 100;
//            System.out.println(String.format("%.2f",result)+"%");
        }
    }
    private static float func(int n){ //求有多少种拿不到自己号的方法 也就是分子
        if(n==1){  //如果只有一个人概率就为0
            return 0;
        }
        if(n==2){  //如果有两个人 拿不到自己的方法就是一种
            return 1;
        }
        return (n-1)*(func(n-1)+func(n-2));  //那么n个人 拿到方法的个数就是 n-1*func(n-1)+func(n-2)
    }                                               //当第一个人那名字的时候就有两种拿法  一种是拿到前一个已经拿过的数这样下一个拿的次数就是func(n-1)
    private static float count(int n){  //求n的阶乘    //另一种是把后一位的名字拿走 这样就相当于下一次的也一起拿走了 那么下一次的个数就是fun(n-2)
        if(n==0){
            return 1;
        }
        return n*count(n-1);
    }
}
