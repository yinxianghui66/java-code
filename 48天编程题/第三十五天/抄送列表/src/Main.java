import java.util.Scanner;
//链接：https://www.nowcoder.com/questionTerminal/286af664b17243deb745f69138f8a800
//        来源：牛客网
//        NowCoder每天要处理许多邮件，但他并不是在收件人列表中，有时候只是被抄送。
//        他认为这些抄送的邮件重要性比自己在收件人列表里的邮件低，因此他要过滤掉这些次要的邮件，优先处理重要的邮件。
//        现在给你一串抄送列表，请你判断目标用户是否在抄送列表中。
//输入
// Joe,Kewell,Leon
//Joe
//"Letendre, Bruce",Joe,"Quan, William"
//William
//输出：
//Ignore
//Important
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            String list= scanner.nextLine();
            String name= scanner.nextLine();
            StringBuffer stringBuffer=new StringBuffer();
            int flag=0;
            int k=0;
            list+=',';   //给末尾+上,用来结束
            for (int i = 0; i <list.length(); i++) {
                if(list.charAt(i)=='"'){  //如果有双引号  则只要不是"就插入 一直找到另一个" 这一对的内容和name进行比较
                    for (int j = i+1; j <list.length() ; j++) {  //i+1是因为i当前为引号+1 从下一个字符开始录入
                        if(list.charAt(j)!='"'){   //则只要不是"就插入
                            stringBuffer.append(list.charAt(j));
                        }
                        if(list.charAt(j)=='"'){  //如果找到另一个"号了 就比较stringBuffer和name的内容 必须完全一致 就输出Ignore
                            k=j+1;  //如果找到另一个"号了 就说明这个字符串已经遍历完了  下次从这个" 的下一位开始找
                            if(stringBuffer.toString().equals(name)){
                                flag=1; //如果找到完全相同的就标记一下
                                System.out.println("Ignore");
                            }
                            stringBuffer.delete(0,stringBuffer.length()); //比较完一次就清空一次stringBuffer
                            break; //找到一对"" 就直接结束
                        }
                    }
                }
                if(k!=0&&(k-1)!=list.length()-1){  //判断之前是否有带""的字符串 如果不为就说明之前遍历了""字符串 那么下次就从k的位置往下找
                    i=k;                            //赋值之后变为0 便于下一次的""查找  并且判断当前的k不是我的字符串的末尾
                    k=0;
                }
                if(list.charAt(i)==','){ //使用,分割 如果找到,就说明之前的字符串为一个 对比stringBuffer和name 如果相同输出Ignore
                    if(stringBuffer.toString().equals(name)){
                        flag=1;
                        System.out.println("Ignore");
                    }
                    stringBuffer.delete(0,stringBuffer.length());  //比较完清空stringBuffer
                    continue;  //结束本次循环 跳过当前找到的,
                }
//                if(i==list.length()-1){  //因为末尾是没有,结束的 所以判断当前的i是不是末尾 如果是就直接比较
////                    stringBuffer.append(list.charAt(i));
////                    if(stringBuffer.toString().equals(name)){  //比较文件末尾的stringBuffer和name 相同输出Ignore
////                        flag=1;
////                        System.out.println("Ignore");
////                    }
////                }
                stringBuffer.append(list.charAt(i));  //加入stringBuffer
            }
            if(flag==0){  //如果前面没输出Ignore  则代表没有找到收件人姓名 输出Important!
                System.out.println("Important!");
            }
        }
    }
}