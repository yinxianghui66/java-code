import java.util.Scanner;

public class Main {
//    给定两个32位整数n和m，同时给定i和j，将m的二进制数位插入到n的二进制的第j到第i位,
//    保证n的第j到第i位均为零，且m的二进制位数小于等于i-j+1，其中二进制的位数从0开始由低到高

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        int m=scanner.nextInt();
        int j=scanner.nextInt();
        int i= scanner.nextInt();
        int count=binInsert(n,m,j,i);
        System.out.println(count);
        
    }
        public static int binInsert(int n, int m, int j, int i) {
            // write code here
            m<<=j;
            return n|m;
        }
}
