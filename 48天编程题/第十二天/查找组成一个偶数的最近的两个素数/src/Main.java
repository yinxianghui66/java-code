import java.util.Arrays;
import java.util.Scanner;
//任意一个偶数（大于2）都可以由2个素数组成，组成偶数的2个素数有很多种情况，本题目要求输出组成指定偶数的两个素数差值最小的素数对。

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        for (int i = n/2; i >=2; i--) {
            if(isPrime(i)&&isPrime(n-i)){    //从中间开始找保证找到的是最小差值  找出 2-n的素数
                System.out.println(i);
                System.out.println(n-i);
                break;
            }
        }
    }
    public static boolean isPrime(int n){
        for (int i = 2; i <n; i++) {
            if(n%i==0){
                return false;
            }
        }
        return true;
    }

}
