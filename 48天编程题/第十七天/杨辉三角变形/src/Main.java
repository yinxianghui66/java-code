import java.util.Scanner;
//第一行只有一个数1， 以下每行的每个数，是恰好是它上面的数，左上的数和右上数等3个数之和（如果不存在某个数，认为该数就是0）。
//        求第n行第一个偶数出现的位置。如果没有偶数，则输出-1。例如输入3,则输出2，输入4则输出3。
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        if(n<=2){   //寻找规律  如果n<=2 的时候一定没有偶数 返回 -1
            System.out.println(-1);
        }else if(n%2==0&&n%4==0){  //如果n为偶数 并且n可以被4整除 返回3
            System.out.println(3);
        }else if(n%2==0&&n%4!=0){   //如果n为偶数但是不能被4整除 返回4
            System.out.println(4);
        }else {
            System.out.println(2);   //如果n为基数 位置为2
        }
    }
}
