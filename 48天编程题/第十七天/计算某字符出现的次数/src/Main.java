import java.util.Locale;
import java.util.Scanner;
//写出一个程序，接受一个由字母、数字和空格组成的字符串，
//        和一个字符，然后输出输入字符串中该字符的出现次数。（不区分大小写字母）
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String str= scanner.nextLine(); //输入字符串
        int count=0;
        char s=scanner.next().charAt(0);  //输入单个字符
        if(s>=65&&s<=90){   //如果需要查找的字符为大写
            str=str.toUpperCase();   //将小写全部转为大写
            for (int i = 0; i <str.length(); i++) {
                Character s1= str.charAt(i);  //挨个取出字符
                if(s1.compareTo(s)==0){  //和要查找的字符对比
                    count++;
                }
            }
        }else if(s>=97&&s<=122){   //如果是不为大写字母 为小写字母
            str=str.toLowerCase();  //将大写全部转为小写
            for (int i = 0; i <str.length(); i++) {   //同理
                Character s1= str.charAt(i);
                if(s1.compareTo(s)==0){
                    count++;
                }
            }
        }else {  //查找的数既不是大写字母也不是小写字母
            for (int i = 0; i <str.length(); i++) {
                Character s1= str.charAt(i);
                if(s1.compareTo(s)==0){
                    count++;
                }
            }
        }
        System.out.println(count);
    }
}
