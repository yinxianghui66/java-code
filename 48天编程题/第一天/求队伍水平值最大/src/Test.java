import java.util.Arrays;
import java.util.Scanner;
//牛牛举办了一次编程比赛,参加比赛的有3*n个选手,每个选手都有一个水平值a_i.现在要将这些选手进行组队,
// 一共组成n个队伍,即每个队伍3人.牛牛发现队伍的水平值等于该队伍队员中第二高水平值。
//
public class Test {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        long sum=0;
        long []arr=new long[3*n];
        for (int i = 0; i <3*n; i++) {
            arr[i] = scanner.nextLong();
        }
        Arrays.sort(arr);  //从小到大排序
//        for (int i = n; i >0; i--) {
//                int lenth=arr.length-n-i;
//                sum+=arr[lenth];
//        }
//            for (int j = 0; j <=2*n; j+=2) {
//                if(arr[j]<arr[j+1]){
//                    sum+=arr[j+1];
//                }else{
//                    sum+=arr[j];
//                }
//            }
        for (int i = 0; i <n; i++) {
            sum+=arr[arr.length-2*(i+1)];   //对应的第二大的下标就找到了
        }
        System.out.println(sum);
    }
}
