import java.util.Scanner;
//        居然有假币！ 现在猪肉涨了，但是农民的工资却不见涨啊，没钱怎么买猪肉啊。
//                nowcoder这就去买猪肉，结果找来的零钱中有假币！！！可惜nowcoder 一不小心把它混进了一堆真币里面去了
//                。只知道假币的重量比真币的质量要轻，给你一个天平（天平两端能容纳无限个硬币），
//                请用最快的时间把那个可恶的假币找出来。
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            int n= scanner.nextInt();
            if(n==0){
                break;
            }
            System.out.println(func(n));
//            System.out.println(func2(n));
        }
    }

    private static int func(int n) {
        if(n==1){  //只有一个硬币无须比较
            return 0;
        }
        if(n<=3){  //当n<3 的时候只需要比较一次
            return 1;
        }
        int met=(n+2)/3;     //把n分为三组 这里+2是为了让met比rest大
        int rest=n-2*met;   //分为三组 最小的那一组就是n-2+2组 为剩下的一组
        int time=1+func(Math.max(met,rest));//把n分为3组  每次先比较前两组 在加上剩下的一组的n的比较次数
        return time;
    }

//    private static int func2(int n) {
//        int count = 0;
//        if(n==1){
//            return 0;
//        }
//        if(n<=3){
//            return 1;
//        }
////        while (n != 0) {
////            if(n==1){
////                return count;
////            }
////            if(n==2){
////                count++;
////                return count;
////            }
////            n/=2;
////            count++;
////        }
////
////        return count;
//        while (n>=2){
//            n=n/3;
//            count++;
//        }
//        if(n<=3){
//            return count+1;
//        }
//        return count;
//    }
}
