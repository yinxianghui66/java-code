
//一只成熟的兔子每天能产下一胎兔子。每只小兔子的成熟期是一天。 某人领养了一只小兔子，请问第N天以后，他将会得到多少只兔子。
import java.util.Scanner;
//还是斐波那契数列问题
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            int N = scanner.nextInt();
            long arr[] = new long[91];   //一定要注意长度！！！！！！！！！！！！很多次了
            if(N==0){
                System.out.println(0);
            }
            arr[0]=1;
            arr[1] = 1;
            for (int i = 2; i < arr.length; i++) {
                arr[i] = arr[i - 1] + arr[i -2];
            }
            System.out.println(arr[N]);
        }
    }
}
