import java.util.Scanner;
//链接：https://www.nowcoder.com/questionTerminal/5973a31d58234b068fa1fe34f7290855
//        来源：牛客网
//
//        NowCoder每天要给许多客户写电子邮件。正如你所知，如果一封邮件中包含多个收件人，
//        收件人姓名之间会用一个逗号和空格隔开；如果收件人姓名也包含空格或逗号，则姓名需要用双引号包含。
//        现在给你一组收件人姓名，请你帮他生成相应的收件人列表。
public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        while (sc.hasNextInt()) {
            int n = sc.nextInt();
            sc.nextLine();  //输入完后还有个回车记得处理
            String name[] = new String[n];
            for (int i = 0; i < n; i++) {
                name[i] = sc.nextLine();
            }
            for (int i = 0; i < n; i++) {   //遍历字符串数组
                for (int j = 0; j < name[i].length(); j++) {  //遍历字符串
//                    if (name[i].charAt(j) == ',' && name[i].charAt(j + 1) == ' ' && j+1 != name[i].length() - 1) {
//                        StringBuffer stringBuffer = new StringBuffer();  //如果找到,并且下一个是" "就说明这一封信是两个收件人之间
//                        stringBuffer.append('"');
//                        stringBuffer.append(name[i]);
//                        stringBuffer.append('"');
//                        name[i] = stringBuffer.toString();
//                        break;
//                    } else
                        if (name[i].charAt(j) == ',' || name[i].charAt(j) == ' ') { //如果找到，或者" "就说明收件人名字有 或,
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append('"');
                        stringBuffer.append(name[i]);
                        stringBuffer.append('"');
                        name[i] = stringBuffer.toString();
                        break;
                    }
                }
            }
            for (int i = 0; i < n; i++) {
                if(n==1||i==n-1){  //如果只有一个后面就不需要， 分割  如果是最后一个也不需要， 分割
                    System.out.print(name[i]);
                }else {
                    System.out.print(name[i] + "," + " ");
                }
            }
            System.out.println();
        }
    }
}
