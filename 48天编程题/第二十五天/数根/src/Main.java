import java.util.Scanner;
//        数根可以通过把一个数的各个位上的数字加起来得到。如果得到的数是一位数，那么这个数就是数根；如果结果是两位数或者包括更多位的数字，
//                那么再把这些数字加起来。如此进行下去，直到得到是一位数为止。
//        比如，对于24 来说，把2 和4 相加得到6，由于6 是一位数，因此6 是24 的数根。
//        再比如39，把3 和9 加起来得到12，由于12 不是一位数，因此还得把1 和2 加起来，最后得到3，这是一个一位数，因此3 是39 的数根。
//        现在给你一个正整数，输出它的数根。
  //恶心到家了 说给你一个正整数 结果给的长度long long都装不下 只能用String接收  如果他输入的数的大小超过int
//的范围 下面这代码就出错了  我真是服了
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            int count = number(n);
            int Roots = seekroots(count, n);
            System.out.println(Roots);
        }

    }

    private  static int seekroots(int count,int n) {
        int ret=0;
        for (int i = 0; i <count; i++) {
            ret+=n%10;
            if(number(n)==1){
                break;
            }
            if(number(n)>1) {
                n /= 10;
            }
        }
        if(number(ret)!=1){
            int count1=number(ret);
            ret=seekroots(count1,ret);
        }
        return ret;
    }

    private static int number(int n) {
        if(n>=0&&n<=9){
            return 1;
        }
        int count=0;
        while (true) {
            count++;
            if (n/10<=9) {
                count++;
                break;
            } else {
                n /= 10;
            }
        }
        return count;
    }
}
