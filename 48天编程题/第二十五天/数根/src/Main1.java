import java.util.Scanner;

public class Main1 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            String n= scanner.next();
            while (n.length()>1){  //如果当前的字符串长度大于一说明不是一位数
                int result=0;
                for (int i = 0; i<n.length(); i++) {
                    result+=n.charAt(i)-'0';        //把字符串的每一位变成数字加起来

                }
                n=String.valueOf(result);   //把每一位加起来的结果变回字符串
            }
            System.out.println(n);  //输出结果
        }
    }
}
