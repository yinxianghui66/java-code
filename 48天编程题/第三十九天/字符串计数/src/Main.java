import java.util.Scanner;
//求字典序在 s1 和 s2 之间的，长度在 len1 到 len2 的字符串的个数，结果 mod 1000007。
//思路 举例 如果字符串是数字0-9 十进制  那么如果想求len位有多少种就可以使用s2-s1
//把字母转换一下  看做二十六进制 算出每一位的差值 存储出来 然后最后根据二十六进制还原出来个数
// 比如说现在是两位ab-ce 我就可以每一位相减得出差值 存储到数组 c-a为2 e-b为3
//通过两位的26进制还原为  2*26^1+3*26^0=55 再加上一位的(此时len2为2 len1为1) 2*26^0=2
//总个数就是55+2=57  最后要减去-1  因为s2会多算一次  结果就为56
//如果要求的len的长度大于我字符串的长度 那么就需要进行补位在进行计算
//比如说此时字符串是 ab ce  len1=1 len2=3  此时len2的长度大于字符串长度
//就要给字符串进行补位 给ab补位 aba 给ce补位到 ab(z+1)  因为如果补z z-a为25 会少一个结果所以+1
//补位完在进行每一位的个数加起来为最终结果
public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        while (sc.hasNext()){
            //输入数据  因为后面如果要补位需要对字符串进行修改 这个使用这个方便修改
            StringBuffer s1= new StringBuffer(sc.next());
            StringBuffer s2= new StringBuffer(sc.next());
            int len1= sc.nextInt();
            int len2= sc.nextInt();
            //判断字符串s1,s2是否长度>len2  如果大于就不需要补位
            for (int i = s1.length(); i <len2; i++) {
                s1.append("a");  //如果长度不足len2 那么就给s1补个a
            }
            for (int i = s2.length(); i < len2; i++) {
                s2.append('z'+1);  //如果长度不足len2 那么就给s2补个z+1  因为如果补z z-a为25会少一个
            }
            //将s1 s2 对应位置上的字符相减 保存每位的结果 最后进行二十六进制计算
            int arr[]=new int[len2];
            for (int i = 0; i <len2; i++) {
                    arr[i]=s2.charAt(i)-s1.charAt(i);  //计算两个字符的距离
            }
            //计算从len1-len2 长度 的字符串个数 数组中存储数据挨个进行26进制计算
            long result=0;
            for (int i = len1; i <=len2; i++) {
                for (int j = 0; j <i; j++) {  //挨个求len1的个数+到len2的个数
                    result+=(arr[j]*Math.pow(26,i-j-1));
                }
            }
            System.out.println(result-1%1000007);  //最后减去-1 是因为s2多算了一次
        }
    }
}
