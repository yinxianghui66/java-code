//注意类名必须是Main，才能进行调试
//使用res矩阵，存储子问题的结果。
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while(in.hasNext()){
            String[] str = in.nextLine().split(" ");
            int resLen = longestCommonSubsequence1(str[0], str[1]);
            System.out.println( resLen );
        }
    }

    private static int longestCommonSubsequence1(String s1, String s2) {
        int m = s1.length();
        int n = s2.length();
        if(m==0||n==0) return 0;
        //存储子问题的结果。
        //行和列的索引表示：取字符串从左到中间的子串的长度
        //索引范围：[0-m]和[0-n]
        int[][] res = new int[m+1][n+1];
        //逐行添加子问题的结果。 第一行和第一列值未初始化值，系统默认为0
        //i或j表示取字符串从左到中间的子串的长度
        for(int i=0; i<=m; i++){
            for(int j=0; j<=n; j++){
                if(i==0||j==0)
                    res[i][j] = 0;
                else if(s1.charAt(i-1)==s2.charAt(j-1))    //i而j表示的是子串长度，所以这里要减1
                    res[i][j] = res[i-1][j-1] + 1;
                else{
                    res[i][j] = Math.max(res[i-1][j], res[i][j-1]);
                }
            }
        }
        return res[m][n];
    }
}
