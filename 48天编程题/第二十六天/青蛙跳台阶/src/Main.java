import java.util.Scanner;
//一只青蛙一次可以跳上1级台阶，也可以跳上2级……它也可以跳上n级。求该青蛙跳上一个n级的台阶(n为正整数)总共有多少种跳法。
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
//        for (int i = 0; i <=n ; i++) {
//            for (int j = n; j >=0 ; j--) {
//                if(i+j==n){
//                    count++;
//                }
//            }
//            if(n==1){
//                count=1;
//            }
//        }
//        System.out.println(count);
        System.out.println(func(n));
    }

    private static int func(int n) {
        if(n==0){
            return 0;
        }else if(n==1){
            return 1;
        }
        return 2*func(n-1);//根据公式递推得到  f(n) = f(n-1)+f(n-2)+...+f(n-(n-1)) + f(n-n)
                            //  f(n)=f(0)+f(1)+f(2)+f(3)+..+f(n-2)+f(n-1)
    }                        //f(n-1)=f(0)+f(1)+f(2)+f(3)+...+f(n-2)
}                            //两式相减 即可得到f(n)=2*f(n-1)
