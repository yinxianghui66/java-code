import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int arr[]=new int[n];
        for (int i = 0; i <n; i++) {
            arr[i]=scanner.nextInt();
        }
        int max=arr[0];
        int curmax=arr[0];
        for (int i = 1; i <n; i++) {
            curmax=Math.max(curmax+arr[i],arr[i]);
            if(curmax>max){
                max=curmax;
            }
        }
        System.out.println(max);
    }
}
