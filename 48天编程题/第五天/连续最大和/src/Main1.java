import java.util.Scanner;

public class Main1 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int arr[]=new int[n];
        for (int i = 0; i <n; i++) {
            arr[i]=scanner.nextInt();
        }
        int curmax=arr[0];
        for (int i = 1; i <n; i++) {
            curmax=Math.max(curmax+arr[i],arr[i]);
        }
        System.out.println(curmax);
    }
}
