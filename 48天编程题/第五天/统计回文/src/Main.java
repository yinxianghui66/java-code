import java.util.Scanner;

public class Main {
    public static boolean palindrome(StringBuilder s){   //判断是否是回文串~
        String s1=s.toString();
        StringBuilder s2=s.reverse();
        if(s1.equals(s2.toString())){
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String A=scanner.nextLine();
        String B=scanner.nextLine();
        int count=0;
        for (int i = 0; i <=A.length(); i++) {   //这里是小于等于
            StringBuilder sb=new StringBuilder();
            sb.append(A);
            sb.insert(i,B);   //使用insert来实现多次指定位置插入
            if(palindrome(sb)){   //判断是否是回文串
                count++;
            }

        }
        System.out.println(count);
    }
}
