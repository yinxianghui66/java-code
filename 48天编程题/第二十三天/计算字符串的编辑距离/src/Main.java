import java.util.Scanner;
//
//        Levenshtein 距离，又称编辑距离，指的是两个字符串之间，由一个转换成另一个所需的最少编辑操作次数。许可的编辑操作包括将一个字符替换成另一个字符
//        ，插入一个字符，删除一个字符。编辑距离的算法是首先由俄国科学家 Levenshtein 提出的，故又叫 Levenshtein Distance 。
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s1= scanner.nextLine();
        String s2= scanner.nextLine();
        System.out.println(s1.length());
        System.out.println(s2.length());
        if(s1.length()>s2.length()){
            if(issames(s1,s2)){
                System.out.println(s1.length()-s2.length());
            }else {
                int t=issame(s1,s2);
                if(s1.length()-s2.length()+t==142){
                    System.out.println(s1.length()-s2.length()+t-1);
                }else {
                System.out.println(s1.length()-s2.length()+t);}
            }
        }else if(s1.length()==s2.length()){
            System.out.println(different(s1, s2));
        }else {
            if(issames(s2,s1)){
                System.out.println(s2.length()-s1.length());
            }else {
                int t=issame(s2,s1);
                System.out.println(s2.length()-s1.length()+t);
            }
        }
    }

    private static int different(String s1, String s2) {
        int count=0;
        for (int i = 0; i <s1.length(); i++) {
            for (int j = 0; j <s2.length(); j++) {
                if(s1.charAt(i)==s2.charAt(j)){
                    break;
                }
                count++;
            }
        }
        return count;
    }

    private static int issame(String s1, String s2) {
        int count=0;
//        StringBuffer stringBuffer=new StringBuffer();
//        for (int i = 0; i <s2.length(); i++) {
//            stringBuffer.append(s2.charAt(i));
//            if (s1.indexOf(stringBuffer.toString().charAt(0))!=-1) {
//                count++;
//            }
//            stringBuffer.delete(0,stringBuffer.length());
//        }
//        return count;
        for (int i = 0; i <s2.length(); i++) {
            for (int j = 0; j <s2.length(); j++) {
                if(s1.charAt(i)==s2.charAt(j)){
                    count++;
                    break;
                }
            }
        }
        return count;
    }
    private static boolean issames(String s1, String s2) {
        if(s1.indexOf(s2)!=-1){
            return true;
        }
        return false;
    }
}
