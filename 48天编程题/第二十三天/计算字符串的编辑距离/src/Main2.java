import java.util.*;
//初始状态:f(i,0) = i; f(0,j) = j;
//状态转移: f(i,j) = str[i]==str2[j]? f(i-1,j-1): min(f(i-1,j),f(i,j-1),f(i-1,j-1))+1
//状态定义: f(i,j)  str1 前 i 个子串和 str2 前j 个子串的最小编辑距离
//返回结果:f(row,col);
public class Main2{
    public static int func(String str1,String str2){
        int row = str1.length();
        int col = str2.length();
        int[][] dp = new int[row+1][col+1];

        for(int i = 0;i<= row;i++){
            dp[i][0] = i;
        }
        for(int i = 0;i<=col;i++){
            dp[0][i] = i;
        }
        for(int i = 1;i<=row;i++){
            for(int j = 1;j<=col;j++){
                if(str1.charAt(i-1)==str2.charAt(j-1)){
                    dp[i][j] = dp[i-1][j-1];
                }else{
                    int min = Math.min(dp[i-1][j],dp[i][j-1]);
                    dp[i][j] = Math.min(dp[i-1][j-1],min) + 1;
                }
            }
        }
        return dp[row][col];
    }
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        String str1 = scanner.nextLine();
        String str2 = scanner.nextLine();
        int ret = func(str1,str2);
        System.out.println(ret);
    }
}