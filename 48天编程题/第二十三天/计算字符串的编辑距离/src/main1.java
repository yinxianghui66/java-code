import java.util.Scanner;

public class main1 {
    public static int getDistance(char str1[],char str2[],int k1,int k2,int len1,int len2){
        if (len1-k1==0) //当其中某个字符串剩余考察长度为0的时候,编辑距离就是另一个字符串的长度
            return len2-k2;
        else if (len2-k2==0)
            return len1-k1;
        else {
            if (str1[k1]==str2[k2]) //当前字符比较相等的时候,继续从两字符串下一位置考察
                return getDistance(str1, str2, k1+1, k2+1, len1, len2);
            else { //当前字符比较不等的时候
                //分别考虑替换(均从两字符串下一位置考察,当前位置视为替换)
                int a=getDistance(str1, str2, k1+1, k2+1, len1, len2)+1;
                //插入或删除(一方停留当前位置,另一方考察下一位置)
                int b=getDistance(str1, str2, k1, k2+1, len1, len2)+1;
                int c=getDistance(str1, str2, k1+1, k2, len1, len2)+1;
                if(a<b&&a<b){
                    return a;
                }else if(b<a&&b<c){
                        return b;
                }
                else if(c<a&&c<b){
                        return c;
                }
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        char str1[]=new char[100];
        char str2[]=new char[100];
        while (scanner.hasNext()) {
            String s1= scanner.nextLine();
            String s2= scanner.nextLine();
            System.out.println(getDistance(str1, str2, 0, 0, s1.length(), s2.length()));
        }

    }

}
