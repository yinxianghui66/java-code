import java.util.Arrays;
import java.util.Scanner;
//春节期间小明使用微信收到很多个红包，非常开心。在查看领取红包记录时发现
//某个红包金额出现的次数超过了红包总数的一半。请帮小明找到该红包金额。
// 写出具给定一个红包的金额数组gifts及它的大小n，请返回所求红包的金额。体算法思路和代码实现，要求算法尽可能高效。
//若没有金额超过总数的一半，返回0。
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        int arr[]=new int[n];
        for (int i = 0; i <n; i++) {
            arr[i]= scanner.nextInt();
        }
        System.out.println(getValue(arr, n));
    }
        public static int getValue(int[] gifts, int n) {
            // write code here
//            int count = 0;  //计数
//            for (int i = 0; i < n; i++) {  //一前一后比较
//                for (int j = i + 1; j < n; j++) {
//                    if (gifts[i] == gifts[j]) {  //如果金额相同就计数器+1
//                        count++;
//                    }
//                    if (count > n / 2) {  //超过红包总数就输出
//                        return gifts[i];
//                    }
//                }
//            }
//            return 0;  //如果遍历完了还没有超过一半的就返回0
            Arrays.sort(gifts);
            if(gifts[n/2]!=gifts[n/2-1]){
                return 0;
            }else {
                return gifts[n/2];
            }
        }
}
