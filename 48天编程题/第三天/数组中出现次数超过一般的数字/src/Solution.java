import java.util.*;

//给一个长度为n的数组，数组中有一个数字出现的次数超过数组长度的一半，请找出这个数字。
//例如输入一个长度为9的数组[1,2,3,2,2,2,5,4.,2]。由于数字2在数组中出现了5次，超过数组长度的一半，因此输出2。
public class Solution {
    public int MoreThanHalfNum_Solution (int[] numbers) {
        // write code here
        int count=0;
        if(numbers.length==1){
            return numbers[0];
        }
        for (int i = 1; i <numbers.length; i++) {    //定义两个下标，挨个遍历
            for (int j = 0; j <numbers.length; j++) {   //如果相同就++，如果超过数组一半就输出
                if(numbers[i]==numbers[j]){
                    count++;
                }
                if(count>numbers.length/2){
                    return numbers[i];
                }
            }
            count=0;   //没超过一半重置
        }
        return 0;
    }
}