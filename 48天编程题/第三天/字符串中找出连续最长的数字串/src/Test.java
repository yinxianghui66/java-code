import java.util.Scanner;
//输入abcd12345ef125ss123456789   找出最长的数字串  打印123456789

//思路是遍历字符串，如果是数字就计数器++，记录长度，使用max来记录最长的数字串，保证max最长，使用end来记录最长数字串的末尾
//如果走着走着突然不是字母了，就把count置为0重新记录，再碰到数字开始计数，最后使用substring方法，来直接输出最长字符串的位置
public class Test {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String str=scanner.nextLine();
        int count=0;    //表示当前数字串长度
        int max=0;    //表示最长的数字串长度
        int end=0;      //表示最长的数字串的末尾位置
        for (int i = 0; i <str.length(); i++) {
            if(str.charAt(i)>='0'&&str.charAt(i)<='9'){
                count++;
                if(max<count){   //一直保持max是最长的 如果后面的数字串比max长要更新
                    max=count;
                    end=i;   //更新队尾位置
                }
            }else {
                count=0;
                }
            }
        System.out.println(str.substring(end-max+1,end+1));    //找到最长的数字串长度，末尾位置，相减就是最长数字串的头位置
        }
    }
