import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
//        while (scanner.hasNext()) {
//            int n = scanner.nextInt();
//            long a=0,b=1;
//            long sum=0;
//            for (int i = 0; i <n; i++) {
//                sum=(a+b)%100000; //如果不足100000 位就原样输出 超过6位就取后六位
//                a=b;
//                b=sum;
//            }
//            System.out.println(sum);
//        }

        int fib[]=new int[100001]; //创建数组存储斐波那契数列
        fib[0]=1;
        fib[1]=1;
        for (int i = 2; i<=100000 ; i++) {
            fib[i]=(fib[i-1]+fib[i-2])%1000000;  //如果当前长度超过1000000  只存储斐波那契数的后六位
        }
        while (scanner.hasNext()){
            int n= scanner.nextInt();
            System.out.printf(n>29?"%06d\n":"%d\n",fib[n]);  //如果n>29 斐波那契数就大于100000 输出后六位
        }
    }
}
//123