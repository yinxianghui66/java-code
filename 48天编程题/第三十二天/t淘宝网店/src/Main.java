import java.util.Scanner;
//NowCoder在淘宝上开了一家网店。他发现在月份为素数的时候，当月每天能赚1元；否则每天能赚2元。
//        现在给你一段时间区间，请你帮他计算总收益有多少。
//思路结就是把不用的年转换为同一年计算+前面年的盈利 我们把开始和结束时间看作一年 那么这一年的盈利
//=开始月天数-开始日期盈利+开始月+1和结束月-1 之间月份盈利+结束月盈利 +相差n-1年的盈利=总盈利
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            int year1 = scanner.nextInt();
            int month1 = scanner.nextInt();
            int day1 = scanner.nextInt();
            int year2 = scanner.nextInt();
            int month2 = scanner.nextInt();
            int day2 = scanner.nextInt();
            int years = 0;
            long count = 0;
            for (int i = 1; i <=12; i++) {  //计算正常盈利一年挣多少 （不算闰年的时候）
                if (isprime(i)) { //如果是素数
                    for (int j = 0; j < DAYS[i-1]; j++) {
                        years++;
                    }
                } else {
                    for (int j = 0; j <DAYS[i-1]; j++) {
                        years += 2;
                    }
                }
            }
            while (true) {
                if (month1 == month2) {  //如果月份相同 就直接后面日期减去前面日期 判断月份素数 加上结果
                    for (int i = 0; i <= day2 - day1; i++) {  //=，记得加上本身那一天
                        if (isprime(month1)) {
                            count++;
                        } else {
                            count += 2;
                        }
                    }
                    break;
                }
                //把结果分成三份 一份是开始时间的月日 一份是开始月份和结束月份之间的盈利 一份最后结束月份时的 盈利
                if (isprime(month1)) {  //如果当前月是素月  那么这个月的天数-已经从几号开始 就代表这个月营业了多少天
                    for (int j = 0; j <= DAYS[month1 - 1] - day1; j++) {
                        count++;
                    }
                } else { //如果不是素月就一天+1  这里需要等于 不要忘记第一天也算盈利
                    for (int j = 0; j<=DAYS[month1 - 1] - day1; j++) {
                        count += 2;
                    }
                }
                int m1 = month1;  //这里算两个月之间的月份盈利
                int m2 = month2;
                for (int j = m1 + 1; j <= m2-1; j++) { //假如是1 月-12月 那么前面算了一月的 后面算12月的 这里计算
                    if (isprime(j)) {                   //2-11 整月的盈利就可以了
                        for (int k = 0; k < DAYS[j-1]; k++) {  //如果当前月是素月 那么一次+1  记得数组下标-1
                            count++;
                        }
                    } else {
                        for (int k = 0; k < DAYS[j-1]; k++) {
                            count += 2;
                        }
                    }
                }
                if (isprime(month2)) {  //这里计算结束月的盈利  判断最后一个月是不是素月
                    for (int j = 0; j < day2; j++) {  //如果是素月就一次+1  一共加结束日次 就可以了
                        count++;
                    }
                } else {
                    for (int j = 0; j < day2; j++) {
                        count += 2;
                    }
                }
                if (isleapYear(year2)) {  //如果当前年是闰年的话 需要+1 代表2月多出的一天的盈利
                    if (month2 > 2) {
                        count = count + 1;
                    }
                }
                break;   //三部分的和加起来就是这年的盈利
            }

            for (int i = year1; i < year2; i++) { //如果不在同一年 就从开始年遍历到结束年 每年的盈利就为正常盈利
                                                //如果为闰年就结果+1 相当于从2000年-2998年 +2999年的盈利
                if (isleapYear(i)) {
                    count += years+1;
                } else {
                    count += years;
                }
            }
            System.out.println(count);
        }
    }

    private static boolean isprime(int n){  //判断素数
        if(n<=3){
            return n>1;
        }
        for (int i = 2; i <n; i++) {
            if(n%i==0){
                return false;
            }
        }
        return true;
    }
    private static final int[] DAYS={31,28,31,30,31,30,31,31,30,31,30,31};  //平年的天数

    private static boolean isleapYear(int year) { //判断闰年
        if((year%4==0&&year%100!=0)||(year%400)==0){
            return true;
        }
        return false;
    }
}
