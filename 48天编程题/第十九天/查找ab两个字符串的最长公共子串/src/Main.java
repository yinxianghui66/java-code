

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s1= scanner.nextLine();
        String s2= scanner.nextLine();
//        StringBuffer stringBuffer=new StringBuffer();
//        String[]arr=new String[1000];
//        int max=0;
//        int t=0;
//        int count=0;
//        for (int i = 0; i <=s1.length(); i++) {
//            for (int j = 0; j <s2.length(); j++) {
//                if(i==s1.length()){
//                    arr[t]=stringBuffer.toString();
//                    break;
//                }
//                if(s1.charAt(i)==s2.charAt(j)){
//                    stringBuffer.append(s2.charAt(j));
//                    i++;
//                    count++;
//                }else {
//                    if(count!=0){
//                        arr[t]=stringBuffer.toString();
//                        t++;
//                        j=0;
////                        i=0;
//                        stringBuffer.delete(0,stringBuffer.length());
//                        count=0;
//                    }
//                }
//
//            }
//        }
        if(s1.length()<s2.length()){  //找长的字符串
            System.out.println(func(s1, s2));
        }else {
            System.out.println(func(s2,s1));
        }
    }

    public static String func(String s1, String s2) {
        ArrayList<String> list=new ArrayList<>();
        for (int i = 0; i <s1.length()+1; i++) {  //从长的字符串中找短的字符串内容
            for (int j = i+1; j <s1.length()+1; j++) {
                String sub=s1.substring(i,j);     //取i-j区域的字符串
                if(s2.contains(sub)&&sub.length()>1){//如果i-j区域包含在s2中 并且长度大于一就存入链表
                    list.add(sub);
                }
            }
        }
        int max=0;
        int index=0;
        for (int i = 0; i < list.size();i++) {
            int len=list.get(i).length();
            if(max<len){   //找出长度最大的子串索引
                max=len;
                index=i;   //找到大的记录一下下标
            }
        }
        return list.get(index);
    }
}
