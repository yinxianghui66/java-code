import java.util.Scanner;
//        某商店规定：三个空汽水瓶可以换一瓶汽水，允许向老板借空汽水瓶（但是必须要归还）。
//        小张手上有n个空汽水瓶，她想知道自己最多可以喝到多少瓶汽水。
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int count=0;
        while (scanner.hasNextInt()){
            int n= scanner.nextInt();
            if(n==0){
                break;
            }
            System.out.println(n/2);    //有两个空瓶的时候可以借一瓶喝 喝完再还回去 ，就相当于两个瓶子就可以换一片瓶喝
        }

    }
}
