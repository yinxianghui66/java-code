import java.util.Scanner;
//不用+求a+b
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int A= scanner.nextInt();
        int B= scanner.nextInt();
        int c,d;
        c=A^B;     //不带进位的加法
        d=(A&B)<<1;  //如果此时为0 就说明没有进位，如果不是则d就表示进位的位数
        if(d!=0){
            System.out.println(d+c);  //不带进位的加上进位就为结果
        }else{
            System.out.println(c);
        }
    }
}
