import java.util.Scanner;
//请计算n*m的棋盘格子（n为横向的格子数，m为竖向的格子数）沿着各自边缘线从左上角走到右下角，总共有多少种走法，
//        要求不能走回头路，即：只能往右和往下走，不能往左和往上走。
  //这个题重要的是理解思想 一层一层化为一行或者一列
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        int m=scanner.nextInt();
        System.out.println(med(m,n));
    }
    public static int med(int n,int m){
        if((m==1&&n>=1)||(n==1)&&m>=1){   //如果只是一行或者一列，当前走法就是m+n
            return m+n;
        }
        return med(n-1,m)+med(n,m-1);
    }
}
