import java.util.*;
import java.io.*;
//链接：https://www.nowcoder.com/questionTerminal/ceb89f19187b4de3997d9cdef2d551e8
//        来源：牛客网
//        上图是一个电话的九宫格，如你所见一个数字对应一些字母，因此在国外企业喜欢把电话号码设计成与自己公司名字相对应。
//        例如公司的Help Desk号码是4357，因为4对应H、3对应E、5对应L、7对应P，因此4357就是HELP。
//        同理，TUT-GLOP就代表888-4567、310-GINO代表310-4466。
//        NowCoder刚进入外企，并不习惯这样的命名方式，现在给你一串电话号码列表，请你帮他转换成数字形式的号码，并去除重复的部分。
public class Main{
    //是否是数字
    public static boolean isDigit(char s){
        return (s >= '0' && s <= '9');
    }
    //是否是大写
    public static boolean isUpper(char s){
        return (s >= 'A' && s <= 'Z');
    }

    public static void main(String[] args) throws Exception{
        Map<Character,Character> map = new HashMap<>();
        String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String num =   "22233344455566677778889999";
        char[] alphach = alpha.toCharArray();
        char[] numch = num.toCharArray();
        for(int i = 0;i < alphach.length;i++){
            map.put(alphach[i],numch[i]);
        }
        Set<String> set = new TreeSet<>();
        String line;
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        while((line = bf.readLine()) != null){
            set.clear();
            int n = Integer.parseInt(line);
            for(int i = 0;i < n;i++){
                line  = bf.readLine();
                char[] linech = line.toCharArray();
                StringBuilder sb = new StringBuilder();
                for(char ch : linech){
                    if(isDigit(ch)){
                        sb.append(ch);
                    }else if(isUpper(ch)){
                        sb.append(map.get(ch));
                    }
                }
                line = sb.substring(0,3) + "-" + sb.substring(3);
                set.add(line);
            }
            for(String s : set){
                System.out.println(s);
            }
            //处理下一组数据和上一组数据之间有一个空格
            System.out.println();
        }

    }
}
