import java.util.Scanner;
//输入一行字符串 输出该字符串的反转后的字符串
//例如 abcd 输出 dcba
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s= scanner.nextLine();
        StringBuffer stringBuffer=new StringBuffer();  //存储反转后的字符串
        for (int i = s.length()-1; i>=0; i--) {   //从后往前遍历字符串
            stringBuffer.append(s.charAt(i));
        }
         s=stringBuffer.toString();  //输入到源字符串
        System.out.println(s);
    }
}
