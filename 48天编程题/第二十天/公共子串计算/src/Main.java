
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s1= scanner.nextLine();
        String s2= scanner.nextLine();
        int count=0;
        if(s1.length()>s2.length()){   //找出 s1 和s2 中长度小的哪一个
            System.out.println(func(s2, s1));
        }else {
            System.out.println(func(s1, s2));
        }
    }

    private static int func(String s1, String s2) {
        String s="";   //用来看最长的字符串长度
        int l=0;   //记录目前s字符串的长度
        for (int i = 0; i <s1.length(); i++) {
            for (int j = i+1; j <=s1.length() ; j++) {
                String t=s1.substring(i,j);   //把s1从0-s1.length内容 第一次一个 第二次两个字符 第三次...
                if(s2.contains(t)&&j-i>l){  //如果这个字符串存在s2中就说明是子串 并且后面加入的必须比之前子串长
                    l=j-i;  //更新子串长度
                    s=s1.substring(i,j);  //最长的子串放到s中
                }
            }
        }
        return s.length();  //返回s的长度
    }
}
