import java.util.Scanner;
import java.util.Stack;
//链接：https://www.nowcoder.com/questionTerminal/2cc32b88fff94d7e8fd458b8c7b25ec1
//        来源：牛客网
//        给定一个十进制数 M ，以及需要转换的进制数 N 。将十进制数 M 转化为 N 进制数。
//        当 N 大于 10 以后， 应在结果中使用大写字母表示大于 10 的一位，如 'A' 表示此位为 10 ， 'B' 表示此位为 11 。
//        若 M 为负数，应在结果中保留负号。
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int M = scanner.nextInt();//输入要转换的数字
            int N = scanner.nextInt();    //输入要转换为几进制
            System.out.println(solve(M,N));
        }
    }
        public static String solve (int M,int N){
            // write code here
            if (M == 0) {
                return "0";
            }
            boolean flag = false;
            Stack stack = new Stack<Character>();   //我是用栈来实现逆序输出
            if (M < 0) {   //如果M是负数转换成正数处理
                M = -M;
                flag = true;
            }
            if (N > 9) {  //如果是大于9进制就需要ABCD...
                while (M > 0) {
                    switch (M % N) {
                        case 10:
                            stack.push('A');
                            break;
                        case 11:
                            stack.push('B');
                            break;
                        case 12:
                            stack.push('C');
                            break;
                        case 13:
                            stack.push('D');
                            break;
                        case 14:
                            stack.push('E');
                            break;
                        case 15:
                            stack.push('F');
                            break;
                        default:
                            stack.push(M % N);
                    }
                    M = M / N;
                }
                if (flag) {
                    stack.push('-');
                }
                StringBuffer stringBuffer = new StringBuffer();
                while (!stack.isEmpty()) {
                    stringBuffer.append(stack.pop());  //使用stringBuffer拼接字符串
                }
                return stringBuffer.toString();
            } else {
                while (M > 0) {  //如果是小于等于9进制
                    stack.push(M % N); //直接取模 往后除
                    M = M / N;
                }
                if (flag) {  //如果输入的为负数就拼接-
                    stack.push('-');
                }
                StringBuffer stringBuffer = new StringBuffer();
                while (!stack.isEmpty()) {
                    stringBuffer.append(stack.pop());
                }
                return stringBuffer.toString();
            }
        }
}

