
import java.util.*;
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            int n1=scanner.nextInt();
            int n2=scanner.nextInt();
            int n3=scanner.nextInt();
            int n4=scanner.nextInt();
            int A=(n1+n3)/2;
            int B1=(n2+n4)/2;
            int B2=(n3-n1)/2;   //B是有两种方式可以求得
            int C=(n4-n2)/2;

            if(B1!=B2){  //只要这两种方式得到的结果不相同就是不存在的
                System.out.println("NO");
            }else {
                System.out.println(A+" "+B1+" "+C);
            }
        }
    }
}
