//A,B,C三个人是好朋友,每个人手里都有一些糖果,我们不知道他们每个人手上具体有多少个糖果,但是我们知道以下的信息：
//
//A - B, B - C, A + B, B + C. 这四个数值.每个字母代表每个人所拥有的糖果数.
//
//现在需要通过这四个数值计算出每个人手里有多少个糖果,即A,B,C。这里保证最多只有一组整数A,B,C满足所有题设条件。
import java.util.*;
public class Main1{
    public static void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNext()){
            int n1=scanner.nextInt();
            int n2=scanner.nextInt();
            int n3=scanner.nextInt();
            int n4=scanner.nextInt();
            int A=(n1+n3)/2;
            int B1=(n2+n4)/2;
            int B2=(n3-n1)/2;
            int C=(n4-n2)/2;

            if(B1!=B2){
                System.out.println("No");
            }else{
                System.out.println(A+" "+B1+" "+C);
            }
        }
    }
}
