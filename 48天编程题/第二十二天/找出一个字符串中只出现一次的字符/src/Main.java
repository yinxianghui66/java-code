import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s= scanner.nextLine();
        int count=0;
        char ss=' ';
        StringBuffer stringBuffer=new StringBuffer(s);   //使用stringBuffer存储字符串
        for (int i = 0; i <stringBuffer.length(); i++) {
            for (int j = i+1; j <stringBuffer.length() ; j++) {  //一前一后挨个遍历
                if(stringBuffer.charAt(i)==stringBuffer.charAt(j)){  //如果有两个字符相同
                    ss=stringBuffer.charAt(i);   //记录上次相同的字符 防止之后还会有这个字符来
                    stringBuffer.setCharAt(i,' ');   //给两个位置的字符变为' '
                    stringBuffer.setCharAt(j,' ');
                   break;
                }else {
                    if(ss==stringBuffer.charAt(j)){   //如果这次的字符和上次两个相同置为空的相同 就说明这个字符已经出现三次了 就直接置为' '
                        stringBuffer.setCharAt(j,' ');
                    }
                }
            }
        }
        for (int i = 0; i <stringBuffer.length(); i++) {  //遍历stringBuffer
            if(stringBuffer.charAt(i)!=' ') {   //  如果不为空 就说明只有一次出现了 字符串中没有相同的字符
                System.out.println(stringBuffer.charAt(i));
                count++;   //用来记录是否有符合的字符
                break;  //如果有多个出现一次的 就先输出在前面的只出现一次的字符
            }
        }
        if(count==0){
            System.out.println(-1);  //如果一次合法字符都没有就返回-1
        }
    }
}
