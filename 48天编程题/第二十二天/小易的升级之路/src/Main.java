import java.util.Scanner;
//        小易经常沉迷于网络游戏.有一次,他在玩一个打怪升级的游戏
//                ,他的角色的初始能力值为 a.在接下来的一段时间内,他将会依次遇见n个怪物,每个怪物的防御力为b1,b2,b3...bn.
//                如果遇到的怪物防御力bi小于等于小易的当前能力值c,那么他就能轻松打败怪物,并 且使得自己的能力值增加bi;如果bi大于c,
//                那他也能打败怪物,但他的能力值只能增加bi 与c的最大公约数.那么问题来了,在一系列的锻炼后,小易的最终能力值为多少?
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            int n= scanner.nextInt();  //输入
            int a= scanner.nextInt();
            int arr[]=new int[n];
            for (int i = 0; i <n; i++) {
                arr[i]=scanner.nextInt();
            }
            for (int i = 0; i <n ; i++) {
                    if(a>=arr[i]){   //如果当前a大于等于怪物的防御
                        a+=arr[i];   //a+怪物防御值
                    }else if(a<arr[i]){  //如果a小于怪物的防御值
                        a+=func(a,arr[i]);  //只能增加 a和怪物防御值的最大公约数
                    }
                }
            System.out.println(a);
        }
    }

    private static int func(int a, int i) { //求最大公约数
        int rem=0;
        while (i>0){
            rem=a%i;
            a=i;
            i=rem;
        }
        return a;
    }
}
