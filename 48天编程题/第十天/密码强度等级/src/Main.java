import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s= scanner.nextLine();
        int length=len(s);  //字符串长度
        int letter=let(s);   //字母
        int digit=Statistics(s); //数字
        int symbol=sym(s);   //符号
        int reward=rew(s);   //奖励
        int summax=length+letter+digit+symbol+reward;
        if(summax>=90){
            System.out.println("VERY_SECURE");
        }else if(summax>=80){
            System.out.println("SECURE");
        }else if(summax>=70){
            System.out.println("VERY_STRONG");
        }else if(summax>=60){
            System.out.println("STRONG");
        }else if(summax>=50){
            System.out.println("AVERAGE");
        }else if(summax>=25){
            System.out.println("WEAK");
        }else if(summax>=0){
            System.out.println("VERY_WEAK");
        }
    }
    public static int num=0; // 设置静态变量 表示这个字符串里面是否存在对应的 数字 大小写字母 和符号
    public static int letterslet=0;
    public static int lettersmax=0;
    public static int rewards=0;
    private static int rew(String s) {

            if(lettersmax==1&&letterslet==1&&rewards==1&&num==1){  //都有
                return 5;
             }
            if((num==1&&rewards==1&&letterslet==1)||(num==1&&rewards==1&&lettersmax==1)){  //有大写/小写字母 和 数字和符号
                return 3;
            }
            if((lettersmax==1&&num==1)||(letterslet==1&&num==1)){  //有大写/小写字母和数字
                return 2;
            }
            return 0;
    }


    public static int len(String s){
        if(s.length()<=4){
            return 5;
        }
        if(s.length()>=5&&s.length()<=7){
            return 10;
        }
        if(s.length()>=8){
            return 25;
        }
        return 0;
    }
    private static int let(String s) {
        int countlet=0;
        int countmax=0;
        for (int i = 0; i <s.length(); i++) {
            if((s.charAt(i)>='a'&&s.charAt(i)<='z')){   //判断大写还是小写
                letterslet=1;
                countlet++;
            }
            if((s.charAt(i)>='A'&&s.charAt(i)<='Z')){
                lettersmax=1;
                countmax++;
            }
        }
        if(countlet==s.length()){  //只有小写字母
            return 10;
        }
        if(countmax==s.length()){  //只有大写字母
            return 10;
        }
        if((countlet!=0&&countmax==0)||(countlet==0&&countmax!=0)){   //如果有大写/小写字母但是没有 小写/大写字母的情况
            return 10;
        }
        if(countlet!=0&&countmax!=0){  //大小写都有
            return 20;
        }
       return 0;
    }
    private static int Statistics(String s) {
        int count=0;
        for (int i = 0; i <s.length(); i++) {
            if((s.charAt(i)>='0'&&s.charAt(i)<='9')){   //是否为数字
                count++;
            }
        }
        if(count==0){
            return 0;
        }
        if(count==1){
            num=1;
            return 10;
        }
        if(count>1){
            num=1;
            return 20;
        }
        return 0;
    }
    private static int sym(String s) {
        int count=0;
        for (int i = 0; i <s.length(); i++) {   //是否有字符
            if((s.charAt(i)>=32&&s.charAt(i)<=47)||(s.charAt(i)>=58&&s.charAt(i)<=64)
            ||(s.charAt(i)>=91&&s.charAt(i)<=96)||(s.charAt(i)>=123&&s.charAt(i)<=126)
            ){
                count++;
            }
        }
        if(count==0){
            return 0;
        }
        if(count==1){
            rewards=1;
            return 10;
        }
        if(count>1){
            rewards=1;
            return 25;
        }
        return 0;
    }

}
