import java.util.Scanner;
//给定一个二维数组board，代表棋盘，其中元素为1的代表是当前玩家的棋子，0表示没有棋子，-1代表是对方玩家的棋子。
//        当一方棋子在横竖斜方向上有连成排的及获胜（及井字棋规则），返回当前玩家是否胜出。
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int board[][]=new int[3][3];
        int j=0;
        for (int i = 0; i < board.length; i++) {
            if((board[i][j]==board[i][j+1])&&board[i][j+2]==board[i][j+1]){    //横排三个相同的情况
                if(board[i][j]!=0&&board[i][j]!=-1){   //如果不为0或者对面赢就是我赢了
                    System.out.println(true);
                }
            }
        }
        for (int i = 0; i < board.length;i++) {
            if((board[j][i]==board[j+1][i])&&board[j+2][i]==board[j+1][i]){ //竖着一列的三个相同
                if(board[i][i]!=0&&board[j][i]!=-1){   //如果不为0或者对面赢就是我赢了
                    System.out.println(true);
                }
            }
        }
        if((board[0][0]==board[1][1])&&board[0][0]==board[2][2]){  //对角线情况
            if(board[0][0]!=0&&board[0][0]!=-1){
                System.out.println(true);
            }
        }
        if((board[0][2]==board[1][1])&&board[0][2]==board[2][0]){ //对角线情况
            if(board[0][2]!=0&&board[0][2]!=-1){
                System.out.println(true);
            }
        }
        System.out.println(false);
    }
}
