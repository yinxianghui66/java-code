import java.util.*;
//
//        洗牌在生活中十分常见，现在需要写一个程序模拟洗牌的过程。 现在需要洗2n张牌，从上到下依次是第1张，第2张，第3张一直到第2n张。首先，
//        我们把这2n张牌分成两堆，左手拿着第1张到第n张（上半堆），右手拿着第n+1张到第2n张（下半堆）。接着就开始洗牌的过程，先放下右手的最后一张牌，
//        再放下左手的最后一张牌，接着放下右手的倒数第二张牌，再放下左手的倒数第二张牌，直到最后放下左手的第一张牌。接着把牌合并起来就可以了。
//        例如有6张牌，最开始牌的序列是1,2,3,4,5,6。首先分成两组，左手拿着1,2,3；右手拿着4,5,6。在洗牌过程中按顺序放下了6,3,5,2,4,1。
//        把这六张牌再次合成一组牌之后，我们按照从上往下的顺序看这组牌，就变成了序列1,4,2,5,3,6。 现在给出一个原始牌组，
//        请输出这副牌洗牌k次之后从上往下的序列。
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t=scanner.nextInt();
        while (t-->0){  //一共有t组牌 洗完一组就-1
            int n= scanner.nextInt();
            int k= scanner.nextInt();
            int arr[]=new int[n*2];  //有2*n张牌
            for (int i = 0; i < n*2; i++) {   //输入牌数
                arr[i]= scanner.nextInt();
            }
            while (k-->0){  //洗一次就-一次k
                shuffle(arr,n);  //洗牌方法
            }
            for (int i = 0; i <arr.length; i++) {
                if(t==0&&i==arr.length-1){  //最后一次输出 不在行末打印空格
                    System.out.print(arr[i]);
                }else {
                    System.out.print(arr[i]+" ");
                }
            }
            System.out.println();  //一次换行
        }
    }

    private static void shuffle(int[] cards, int n) {
        int left[]=new int[n];  //把牌分为左手
        int right[]=new int[n];  //把牌另一半分为右手
        for (int i = 0; i <n; i++) {  //把牌分到左手
            left[i]=cards[i];
        }
        int l=0;
        for (int i = n; i <2*n; i++) {//把牌分到右手
            right[l]=cards[i];
            l++;
        }
        for (int i = 0,j=0,k=0; i <2*n ; i++) {
            if(i%2==0){
                cards[i]=left[j];   //因为最后要反过来看 所以第一次就是先放左手的牌
                j++;  //放完牌往后移一位
            }else {
                cards[i]=right[k];  //第二次放右手的牌
                k++;
            }
        }
    }
}