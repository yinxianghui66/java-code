import com.sun.org.apache.bcel.internal.generic.NEW;

class Test {
    int a;
    int b;
    int c;
    public  void hello() {
        System.out.println("hello");
        this.a=0;
        this.he();


    }

    public Test(int e) {
        this.a = e;
    }
    public Test(int a,int b,int c){
        this(c);
        this.a=a;
        this.b=b;
    }
    public void he(){
        System.out.println("777");
    }
}
class People{
    String name;
    int age;

    public People(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int getAge() {
        return age;
    }
}
class Boy extends People{
    String type;

    public Boy(String type,String name,int age) {
        super(name,age);
        this.name="阿斯顿";
        this.type = type;
    }
}
public class MyApplication {
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Test test= new Test(1,2,3);
        test.hello();
        System.out.println(test.a);
        People people=new People("大佬",321);
        System.out.println(people.name);
        Boy boy=new Boy("牛逼","马丁",23);
        System.out.println(boy.name);
    }
}