public class Count {

        public static int countSegments(String s) {
            if(s.length() == 0) {
                return 0;
            }
            String[] words = s.split("\\s+");
            int len = words.length;
            if(len == 0) return 0;
            if(words[0].equals("")) len--;
            if(words[words.length-1].equals("")) len--;
            return len;
        }

    public static void main(String[] args) {  //统计字符串中的单词个数，这里的单词指的是连续的不是空格的字符。
        String s="Hello, my name is John";
        System.out.println(countSegments(s));
    }
}
