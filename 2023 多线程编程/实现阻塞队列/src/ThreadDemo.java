import java.util.concurrent.BlockingDeque;
import java.util.function.BinaryOperator;

class MYBlockingDeque{
        private  int[] items=new int[1000];
        //约定[head,tail)队列的有效元素   head指向头，tail指向队尾
        volatile private int head;
        volatile private int tail;
        volatile private int size=0;  //记录元素个数

        synchronized public void  put(int elem) throws InterruptedException {
//            if(size==items.length){
                while (size==items.length){  //防止被interrupt方法提前唤醒 ,如果不符合条件就继续wait
                //队列满了直接返回
//                return;
                    this.wait();       //加入阻塞功能

            }
            //新元素插入队列
            items[tail]=elem;
            tail++;
            //循环队列 tail达到末尾，tail从头开始
            if(tail==items.length){
                tail=0;
            }
            size++;
            this.notify();    //唤醒队列空的阻塞
        }
        synchronized public Integer take() throws InterruptedException {
            if(size==0){
                //队列为空直接返回
//                return null;
                this.wait();   //队列空的就需要阻塞
            }
            //从对列出元素
            int val=items[head];
            head++;
            if(head==items.length){
                head=0;
            }
            size--;
            this.notify();  //唤醒队列满的阻塞
            return val;
        }
}
public class ThreadDemo {
    public static void main(String[] args) throws InterruptedException {
        MYBlockingDeque myBlockingDeque=new MYBlockingDeque();
        myBlockingDeque.put(3);
        myBlockingDeque.put(4);
        myBlockingDeque.put(5);
        int val=0;
        val=myBlockingDeque.take();
        System.out.println(val);
        val=myBlockingDeque.take();
        System.out.println(val);
        val=myBlockingDeque.take();
        System.out.println(val);
        val=myBlockingDeque.take();
    System.out.println(val);   //出四次队列，第四次阻塞等待~
    }
}
