package threading;

public class ThreadDome6 {
    public static void main(String[] args) {
        Thread t=new Thread(()->{
           while (!Thread.currentThread().isInterrupted()){
               System.out.println("111");
               try {
                   Thread.sleep(1000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }
        });
        t.start();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t.interrupt();
        System.out.println("666");
    }
}
