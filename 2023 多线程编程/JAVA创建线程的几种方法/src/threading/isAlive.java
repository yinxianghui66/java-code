package threading;

public class isAlive {
    public static void main(String[] args) {
        Thread t=new Thread(()->{
            System.out.println("66");
        });
        t.start();
        //新建的t线程里面没有任何循环和slepp，意味着这个线程会快速执行完
        //main线程中执行完毕后，此时t线程“基本”就执行完了，此时t对象还在  ->“基本”  可能会出现true的情况
        //但是在系统中队友的t的线程已经结束了，所以false                 操作系统如果调度不过来可能会发生
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(t.isAlive());
    }
}
