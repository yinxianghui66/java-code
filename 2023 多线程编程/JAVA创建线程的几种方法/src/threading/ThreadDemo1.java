package threading;
class MyThread extends Thread{
    @Override
    public void run(){  //入口方法
        while (true){
            System.out.println("我来了");   //每次先打印我来了 还是我走了  是不一定的，多线程在cpu上调度的顺序是不确定的（随机)
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {  //如果还没睡眠1000毫秒被打断就报异常
                e.printStackTrace();
            }
        }
    }

}
                            //idea 本身一个进程，创建一个JAVA进程
public class ThreadDemo1 {  //运行程序所有的线程都是运行在JAVA进程中的
    public static void main(String[] args) {  //main方法本身也是一个线程
        Thread t=new MyThread();  //创建MyThread实例2 (通过代码新创建的线程）
        t.start();  //启动线程   创建了新线程
        while (true){  //如果是单个线程 那么就会一直执行一个循环的（线程是一个单独的执行流）
            System.out.println("我走了");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
