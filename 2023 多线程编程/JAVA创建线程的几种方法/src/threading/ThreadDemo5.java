package threading;
public class ThreadDemo5 {
    public static void main(String[] args) {
        Thread t=new Thread(()->{
            while (true){
                System.out.println("我飞来啦");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        while (true){
            System.out.println("我又走了");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
