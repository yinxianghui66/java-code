import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

class MyThreadpool1{
    private BlockingDeque<Runnable> queue=new LinkedBlockingDeque();
    public void submit(Runnable runnable) throws InterruptedException {
        queue.put(runnable);  //把任务放到阻塞队列
    }
    public MyThreadpool1(int n){
            for (int i = 0; i <n ; i++) {
                Thread t = new Thread(() -> {
                    while (true) {
                        Runnable runnable = queue.take();
                        runnable.run();
                    }
                });
                    t.start();
                }
        }
    }
public class Main {
    public static void main(String[] args) {

    }
}
