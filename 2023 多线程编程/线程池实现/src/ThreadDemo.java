import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

class MyThreadpool{
            private BlockingDeque<Runnable> queue=new LinkedBlockingDeque<>();  //阻塞队列
            public void submit(Runnable runnable) throws InterruptedException {
                queue.put(runnable);  //把任务放到阻塞队列
            }
            public MyThreadpool(int n){  //模拟固定线程数量的线程池
                for (int i = 0; i <n; i++) {
                    Thread t=new Thread(()->{
                        try {
                            while (true) {   //要让线程不停的取任务
                                Runnable runnable = queue.take();
                                runnable.run();
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
                    t.start();
                }
            }
        }
public class ThreadDemo {
    public static void main(String[] args) throws InterruptedException {
        MyThreadpool pool=new MyThreadpool(10);
        for (int i = 0; i <1000; i++) {
            int number=i;
            pool.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println("hello"+number);
                }
            });
        }
    }
}
