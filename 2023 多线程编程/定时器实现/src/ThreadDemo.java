import java.util.PriorityQueue;
import java.util.Timer;
import java.util.concurrent.PriorityBlockingQueue;

class MyTask implements Comparable<MyTask>{//表示一个任务  任务有什么内容
    public Runnable runnable;   //线程的任务
    public long time;  //绝对时间戳

    public MyTask(Runnable runnable, long delay) {
        this.runnable = runnable;
        //取当前时间的时间戳，加上需要等待的时间，就是真正执行的时间
        this.time = System.currentTimeMillis()+delay;
    }
    @Override
    public int compareTo(MyTask o){   //每次取出的时间最小的元素
        return (int) (this.time-o.time);  //在堆中存储的比较规则
    }
}

class MyTimer{
    private Object object=new Object();
    //带有优先级的阻塞队列  核心数据结构
    private PriorityBlockingQueue<MyTask> queue=new PriorityBlockingQueue<>();
    public void  schedule(Runnable runnable,long delay){
        //根据参数构造，插入队列
        MyTask myTask=new MyTask(runnable,delay);   //delay是输入的多少毫秒后执行任务
        queue.put(myTask);
        synchronized (object) {
            object.notify();   //唤醒  这里是为了防止插入一个新任务，如果新任务的执行时间要早于现在正在等待的，那就需要唤醒等待的任务
        }                           //让新的距离执行时间短的任务先执行  下面where再去取元素就会取出新插入的最短时间的任务了
    }
    public MyTimer(){
        Thread t=new Thread(()->{
            while (true){
                try {
                    //阻塞队列只有入队列和出队列是阻塞的，只能出队列来看是否到没到时间

                        MyTask myTask=queue.take();
                        //获取队列的元素，如果为空就阻塞了
                        long curtime=System.currentTimeMillis();
                        if(myTask.time<=curtime){  //比较一下时间，到了就执行，没到就接着等
                        myTask.runnable.run();
                        }else {  //时间还没到，把取出的元素再放回去
                            queue.put(myTask);
                            synchronized (object){
                            //防止忙等，使用wait方法，使cup休眠到对应的要执行时间的时候再唤醒
                            object.wait(myTask.time-curtime);   //程序真正执行的时间-现在的时间
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

}
public class ThreadDemo {
    public static void main(String[] args) {
        MyTimer myTimer=new MyTimer();
         myTimer.schedule(new Runnable() {
             @Override
             public void run() {
                 System.out.println("hello4");
             }
         },3000);
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello3");
            }
        },3000);
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("heool2");
            }
        },3000);
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("heool1");
            }
        },3000);
        System.out.println("hello");
    }
}
