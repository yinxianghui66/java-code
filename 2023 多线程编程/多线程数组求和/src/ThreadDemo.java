import java.util.Random;
//         给定一个很长的数组 (长度 1000w), 通过随机数的方式生成 1-100 之间的整数.
//        实现代码, 能够创建两个线程, 对这个数组的所有元素求和.
//        其中线程1 计算偶数下标元素的和, 线程2 计算奇数下标元素的和.
//        最终再汇总两个和, 进行相加
//        记录程序的执行时间.

public class ThreadDemo {
    volatile public static int ret=0;
    volatile public static int ret2=0;
    public static void main(String[] args) throws InterruptedException {
        long startTime=System.currentTimeMillis();  //记录开始时间
        int []array=new int[10000000];
        Random random=new Random();
        for (int i = 0; i <array.length; i++) {
            array[i]=(1+random.nextInt(100));  //随机生成1-100 的随机数

        }
        Thread t1=new Thread(()->{
            for (int i = 0; i <array.length; i+=2) {    //下标为偶数的相加
                ret+=array[i];
            }
            System.out.println("t1: "+ret);
        });
        t1.start();
        Thread t2=new Thread(()->{
            for (int i = 1; i <array.length; i+=2) {  //下标基数相加
                ret2+=array[i];
            }
            System.out.println("t2: "+ret2);
        });
        t2.start();
        t1.join();//等待线程结束
        t2.join();
        int sum=ret+ret2;
        long endTime=System.currentTimeMillis();  //记录结束时间
        System.out.println("结果： "+sum);
        System.out.println("执行时间： "+(endTime-startTime));
    }
}
