import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
public class Test {
    public static void main(String[] args) {
        BlockingDeque<Integer> blockingDeque=new LinkedBlockingDeque<>();
        Thread t1=new Thread(()->{
            while (true){
                try {
                    int val=blockingDeque.take();
                    System.out.println("消费元素:"+val);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();
        Thread t2=new Thread(()->{
            int val=0;
            while (true){

                try {
                    System.out.println("生产元素"+val);
                    blockingDeque.put(val);
                   Thread.sleep(1000);   //生产者一秒生产一个 ，消费者不受限制
                    val++;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t2.start();
    }
}
