import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class ThreadDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable<Integer> callable=new Callable<Integer>() {   //就是带返回值的Runnable~  <> 指定包装类
            @Override
            public Integer call() throws Exception {
                int sum=0;
                for (int i = 1; i <=1000; i++) {  //别忘了<=1000
                    sum+=i;
                }
                return sum;
            }
        };
        FutureTask<Integer> futureTask=new FutureTask<>(callable);

        Thread t=new Thread(futureTask);  //不能直接callable ，需要 FutureTask来包装一下
        t.start();
        System.out.println(futureTask.get());  //后面使用FutureTask就可以了
    }
}
