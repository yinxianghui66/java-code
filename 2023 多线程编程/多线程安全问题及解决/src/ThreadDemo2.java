import java.util.Scanner;

public class ThreadDemo2 {
     volatile public  static  int flag=0;
    public static void main(String[] args) throws InterruptedException {
        Thread t1=new Thread(()->{
            while (flag==0){

            }
            System.out.println("循环结束");
        });
        Thread t2=new Thread(()->{
            Scanner scanner=new Scanner(System.in);
            flag=scanner.nextInt();
        });
        t1.start();
        t2.start();
    }
}
