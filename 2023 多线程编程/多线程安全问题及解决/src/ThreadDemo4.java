public class ThreadDemo4 {
    public static void main(String[] args) {
        final Object object=new Object();
        Thread t1=new Thread(()->{
            synchronized (object){
                while (true){
                    try {
//                        Thread.sleep(1000);   //让线程进入睡眠等待
                        object.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        },"t1");
        t1.start();
        Thread t2=new Thread(()->{
            synchronized (object){     //线程等待锁
                System.out.println("haha");
            }
        },"t2");
        t2.start();
    }
}
