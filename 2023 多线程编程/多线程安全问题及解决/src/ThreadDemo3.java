public class ThreadDemo3 {
    public static void main(String[] args) {
        Thread t = new Thread(() -> {
                for (int i = 0; i < 10; i++) {
                }
            }, "张三");
            System.out.println(t.getName() + ": " + t.getState());  //在线程还没创建出来，Thread已经创建出来打印状态
                t.start();
            while (t.isAlive()) {
                System.out.println(t.getName() + ": " + t.getState());  //在线程还存活的时候打印状态
            }
                System.out.println(t.getName() + ": " + t.getState());  //线程已经结束后，获取状态
            }
    }
