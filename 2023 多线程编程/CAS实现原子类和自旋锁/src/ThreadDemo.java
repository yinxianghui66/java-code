import java.util.concurrent.atomic.AtomicInteger;

public class ThreadDemo {
    public static void main(String[] args) throws InterruptedException {
        AtomicInteger num=new AtomicInteger(0);   //自带的原子类（原子类的操作都是线程安全的）

        Thread t1=new Thread(()->{
            for (int i = 0; i <50000; i++) {
                num.getAndIncrement();  //原子类内置的++方法，都是安全的  想当于num++
////                num.incrementAndGet();  //相当于++num
//                num.decrementAndGet();   //相当于--num
//                num.getAndDecrement();  //相当num--
            }
        });
        Thread t2=new Thread(()->{
            for (int i = 0; i <50000; i++) {
                num.getAndIncrement();
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(num.get());  //get方法获取数值
    }
}
