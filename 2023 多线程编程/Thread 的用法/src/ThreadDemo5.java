public class ThreadDemo5 {
    public static void main(String[] args) throws InterruptedException {
        Object object=new Object();
        Thread t1=new Thread(()->{
                System.out.println("wait前");
                synchronized (object) {
                    try {
                        object.wait();   //锁对象和wait对象必须相同
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("wait之后");
        });
        t1.start();
        Thread t2=new Thread(()->{
            synchronized (object){
                System.out.println("notify开始前");
                object.notify();
                System.out.println("notify结束");
            }
        });
        Thread.sleep(1000);  //让线程先wait
        t2.start();
    }
}
