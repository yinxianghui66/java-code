public class ThreadDome {//中断一个进程
    public static void main(String[] args) {
        Thread t=new Thread(()->{
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println("吃坤");
                try {
                    Thread.sleep(1000);           //sleep 如果出现异常就会将标志位重置（设为false）
                } catch (InterruptedException e) {     //如果此时标志位变为true 就会结束并抛出异常
                    e.printStackTrace();
                    break;                              //所以抛异常之后如果没有break，标志位会一直为false 就会一直循环下去！！
                }
            }
        });
        t.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t.interrupt();   //这里的interrupt()有两个作用  一是将标志位改为true
                        //如何此时线程正在阻塞（例如sleep) 就会立刻结束，并抛出对应的异常
    }
}
