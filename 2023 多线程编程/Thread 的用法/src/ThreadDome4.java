public class ThreadDome4 {
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 20; i++) {
            int j=i;
            Thread t = new Thread(() -> {
                System.out.println(j);
            });
            t.start();
            t.join();  //主线程等待最后打印
        }
        System.out.println("ok");
    }
}
