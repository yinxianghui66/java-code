public class ThreadDome2 {//等待一个线程
    public static void main(String[] args) throws InterruptedException {
        Thread t=new Thread(()->{
            System.out.println("2");
        });
        t.start();
        t.join();  //主线程等待t线程结束    在main线程中等待t线程
        System.out.println("1");  //等待的时候 主线程是阻塞的
    }
}
