import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadDemo1 {
    public static void main(String[] args) {
       ExecutorService pool= Executors.newCachedThreadPool();   //创建线程数动态增长的线程
        pool.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello1");
            }
        });
    }
}
