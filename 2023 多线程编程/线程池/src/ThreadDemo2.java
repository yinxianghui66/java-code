import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadDemo2 {
    public static void main(String[] args) {
       ExecutorService pool= Executors.newSingleThreadExecutor();  //使用单个线程的线程池
       pool.submit(new Runnable() {
           @Override
           public void run() {
               System.out.println("666");
           }
       });
    }
}
