import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadDemo3 {
    public static void main(String[] args) {
        ExecutorService pool= Executors.newScheduledThreadPool(3);   //使用周期性线程池
        pool.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("999");
            }
        });

    }
}
