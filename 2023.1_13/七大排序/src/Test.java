import com.sun.prism.impl.shape.BasicRoundRectRep;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

public class Test {
    public static void main(String[] args) {
        int[] array = {12,56,32,67,10,19,4};
//      Test.insertSort(array);
//        Test.shellSort(array);
        Test.selectSort(array);
        System.out.println(Arrays.toString(array));
    }
    public static void insertSort(int[] array) {  //插入排序  一个i一个j（i的后一位）来判断，如果找一个大的把大的数据存储到tmp中
        for(int i=1;i<array.length;i++){
            int tmp=array[i];
            int j=i-1;
            for(;j>=0;j--){
                if(array[j]>tmp){
                    array[j+1]=array[j];
                }else{
                    array[j+1]=tmp;
                    break;
                }
            }
            array[j+1]=tmp;
        }
    }
    public static void shellSort(int[] array) {  //希尔排序  对数据进行分组，然后对分组的数据进行插入排序，分完一次组下一次再分，一直分到只有一组
        int gap = array.length;                     //最后整组进行插入排序，因为前面分组，每次进行插入排序已经使得很多数据有序了，最后进行插入排序
            while (gap > 1) {                       //效率会非常快
                shell(array,gap);
                gap /= 2;
        }
        //整体进行插入排序
        shell(array,1);
    }

    public static void shell(int[] array,int gap) {
        for(int i=gap;i<array.length;i++){
            int tmp=array[i];
            int j=i-gap;
            for(;j>=0;j-=gap){
                if(array[j]>tmp){
                    array[j+gap]=array[j];
                }else{
                    break;
                }
            }
            array[j+gap]=tmp;
        }
    }

    public static void selectSort(int[] array) {  //选择排序  找到一个最小值放到前面，然后接着往后找一个最小值放到第二个，以此类推完成排序
        for (int i = 0; i <array.length; i++) {
            int mindex = i;  //最小值的下标
            int j = i + 1;  //从下标的下一个开始找
            for (; j < array.length; j++) {
                if (array[mindex] > array[j]) {  //如果找到的数据比目前最小的数据小
                    mindex = j;    //记录下来最小数据的下标
                }
            }
            swap(array,i,mindex);  //在这里把最小数换到前面
        }
    }
    private static void swap(int[] array,int i,int j){
        int tmp=array[i];
        array[i]=array[j];
        array[j]=tmp;
    }
    private static int partition(int[] array,int left,int right){  //快排  Hoare 法
        int l=left;
        int tmp=array[left];
        while (left<right&&left>right){
            while (array[right]>=tmp){
                right--;
            }
            while (left<right&&array[left]<=tmp){
                left++;
            }
            swap(array,right,left);
        }
        swap(array,left,l);
        return left;
    }
   public static void quickSort(int[] array) {
        Deque<Integer> stack = new LinkedList<>();
        int left = 0;
        int right = array.length-1;
        stack.push(left);
        stack.push(right);
        while (!stack.isEmpty()) {
            right=stack.pop();
            left=stack.pop();
            int pivot = partition(array, left, right);
            if (pivot > left + 1) {
                stack.push(left);
                stack.push(pivot - 1);
            }
            if (pivot < right - 1) {
                stack.push(pivot + 1);
                stack.push(right);
            }

        }
    }
}

