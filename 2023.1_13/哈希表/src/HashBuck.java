import java.util.*;

public class HashBuck {
    static class Node {
        public int kay;
        public int val;
        public Node next;

        public Node(int kay, int val) {
            this.kay = kay;
            this.val = val;
        }
    }

    public Node[] array;
    public int usedSize;
    public double loadFactor = 0.75;

    public HashBuck() {
        array = new Node[8];
    }

    public void put(int kay, int val) {
        int index = kay % array.length;
        Node cur = array[index];
        while (cur != null) {
            if (cur.kay == kay) {
                cur.val = val;
                return;
            }
            cur = cur.next;
        }
        Node node = new Node(kay, val);
        node.next = array[index];
        array[index] = node;
        usedSize++;
        if (calculateLoadFactor() >= loadFactor) {
            resize();
        }
    }

    private void resize() {   //扩容

    }

    public double calculateLoadFactor() {
        return usedSize * 1.0 / array.length;
    }

    //    public void func(int array[]){  //有十个数据，有重复的，去重
//        Set<Integer> hashSet=new HashSet<>();
//        for (int i = 0; i <array.length ; i++) {
//            hashSet.add(array[i]);
//        }
//        return;
//    }
    public void func(int array[]) {  //有十个数据，有重复的，去重
        Map<Integer, Integer> hashMap = new HashMap<>();
//        for (int x : array) {
//                hashMap.put(x, hashMap.getOrDefault(x, 0) + 1);
//        }
        for (int x:array) {
            if()
        }
    }
//    public void func2(int array[]) {  //找到只出现一次的数据
//        Set<Integer> hashSet=new HashSet<>();
//        for (int i = 0; i <array.length; i++) {
//            if(!hashSet.contains(array[i])){
//                hashSet.add(array[i]);
//            }else{
//                hashSet.remove(array[i]);
//            }
//        }
//        for (int i = 0; i <array.length; i++) {
//            if(hashSet.contains(array[i])) {
//                return array[i];
//            }
//        }
//    }
}
