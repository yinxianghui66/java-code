public class BinarySearchTree {
    static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val) {
            this.val = val;
        }
    }

    public TreeNode root = null;
    /**
     * 查找二叉搜索树中指定的val值
     * @param val
     * @return
     */
    public TreeNode find(int val) {
        TreeNode cur=root;
        while (cur!=null){
            if(cur.val>val){
                cur=cur.left;
            }else if (cur.val<val)
            {
                cur=cur.right;
            }else{
                return cur;
            }
        }
        return null;
    }
    /**
     * 二叉树中插入一个元素
    * */
    public void insert(int val){  //\\
        if(root==null){
            root=new TreeNode(val);
            return;
        }
        TreeNode cur=root;
        TreeNode parent=null;
        while(cur!=null){
            parent=cur;
            if(cur.val>val){
                cur=cur.left;
            }else if(cur.val<val){
                cur=cur.left;
            }else {
                return;
            }
        }
        TreeNode node=new TreeNode(val);
        if(parent.val>val){
            parent.left=node;
        }else if(parent.val<val){
            parent.right=node;
        }
    }
    public void rmove(int val){   //删除val结点


    }
}