import java.util.Arrays;

class Solution  {
    public int removeElement(int[] nums, int val) {
        int j=0;
        for (int i = 0; i <nums.length; i++) {
            if(nums[i]!=val)
            {
                nums[j]=nums[i];
                j++;
            }
        }
        return j;
    }
}
public class Test {
    public static void main(String[] args) {
        int []arr={3,2,2,3};
        Solution solution=new Solution();
        System.out.println(solution.removeElement(arr, 3));
        System.out.println(Arrays.toString(arr));
    }
}
