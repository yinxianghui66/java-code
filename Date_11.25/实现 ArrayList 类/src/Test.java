class MyArraylist {

    public int[] elem;
    public int usedSize;//0
    //默认容量
    private static final int DEFAULT_SIZE = 10;

    public MyArraylist() {
        this.elem = new int[DEFAULT_SIZE];
    }

    /**
     * 打印顺序表:
     *   根据usedSize判断即可
     */
    public void display() {
        for (int i = 0; i <this.usedSize; i++) {
            System.out.print(this.elem[i]+" ");
        }
    }

    // 新增元素,默认在数组最后新增
    public void add(int data) {
        this.elem[usedSize]=data;
        this.usedSize++;
    }

    /**
     * 判断当前的顺序表是不是满的！
     * @return true:满   false代表空
     */
    public boolean isFull() {  //顺序表是否已满
       return this.usedSize==this.elem.length;
    }


    private boolean checkPosInAdd(int pos) {  //检查pos是否合法
        if(pos<0||pos>=this.usedSize){
            return false;
        }
        return true;//合法
    }

    // 在 pos 位置新增元素
    public void add(int pos, int data) {
        checkPosInAdd(pos);
        if(isFull()){
            System.out.println("已经满了");
        }
        for (int i = 0; i <this.usedSize-1; i++) {
            this.elem[i+1]=this.elem[i];
        }
        this.elem[pos]=data;
        this.usedSize++;
    }

    // 判定是否包含某个元素
    public boolean contains(int toFind) {
        for (int i = 0; i <this.usedSize; i++) {
            if(this.elem[i]==toFind){
                return true;
            }
        }
        return false;
    }
    // 查找某个元素对应的位置
    public int indexOf(int toFind) {
        for (int i = 0; i <this.usedSize; i++) {
            if(this.elem[i]==toFind){
                return i;
            }
        }
        return -1;
    }

    // 获取 pos 位置的元素
    public int get(int pos) {
        checkPosInAdd(pos);
        return this.elem[pos];
    }

    private boolean isEmpty() {  //判断顺序表是否为空

        return this.elem[0]==0;

    }
    // 给 pos 位置的元素设为【更新为】 value
    public void set(int pos, int value) {
            checkPosInAdd(pos);
            this.elem[pos]=value;
    }

    /**
     * 删除第一次出现的关键字key
     * @param key
     */
    public void remove(int key) {
        int index=indexOf(key);
        if(index==-1){
            System.out.println("没有这个数");
        }
        for (int i = key; i <this.usedSize-1; i++) {
                this.elem[i]=this.elem[i+1];
        }
        this.usedSize--;
        this.elem[usedSize]=0;
    }

    // 获取顺序表长度
    public int size() {
            return this.usedSize;
    }

    // 清空顺序表
    public void clear() {
        for (int i = 0; i <usedSize; i++) {
            this.elem[i]=0;
        }
        this.usedSize=0;
    }
public static class Test {
    public static void main(String[] args) {
        MyArraylist myArraylist=new MyArraylist();
        myArraylist.add(0,66);   //将0位置设置为66
        myArraylist.add(55);  //新增55
        myArraylist.add(56);  //新增56
        myArraylist.add(57);  //新增57
        System.out.println("顺序表是否为空"+myArraylist.isFull());
        myArraylist.display();  //打印顺序表
        System.out.println("是否包含66"+myArraylist.contains(66));
        System.out.println("55的位置在"+myArraylist.indexOf(55));
        System.out.println("获取下标为1位置的元素"+myArraylist.get(1));
        myArraylist.set(1,88);    //将1下标的元素设置为88
        System.out.println("将1下标的元素设置为88");
        myArraylist.display();
        System.out.println();
        myArraylist.remove(56);  //删除出现第一次的56
        myArraylist.display();
        System.out.println("顺序表长度为"+myArraylist.size());
        myArraylist.clear();
        System.out.println("清空顺序表");
        myArraylist.display();
    }
}
}
