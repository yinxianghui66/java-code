import java.util.Arrays;

public class Test {
    public static int[] revolve(int M, int N ,int []arr){
        int len=M;
        for (int i = 0; i <N; i++) {   //前移几个数字
            int tmp=arr[len-1];
            for (int j = len-1; j>0; j--) {
                arr[j]=arr[j-1];   //因为最后一位一开始已经备份，所以直接覆盖，使用前一位赋值给后一位
            }
            arr[0]=tmp;  //最后将最后一位赋值给第一位
        }
        return arr;
    }
    public static void main(String[] args) {
        int []arr={1,2,3,4,5,6};
        System.out.println(Arrays.toString(revolve(6,2,arr)));
    }
}
