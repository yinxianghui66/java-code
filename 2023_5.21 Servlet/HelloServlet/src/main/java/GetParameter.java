import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PseudoColumnUsage;

@WebServlet("/getParameter")
public class GetParameter extends HelloServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //前端通过URL 的query String 传递 username和password 两个属性
        //在请求中查找username属性对应的值 如果找到返回字符串 没有返回null
        String username=req.getParameter("username");
        if(username==null){
            System.out.println("username这个key在 query String中不存在");
        }
        String password=req.getParameter("password");
        if(password==null){
            System.out.println("password这个key在 query String中不存在");
        }
        System.out.println("username=" + username +",password=" + password);
        resp.getWriter().write("ok");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //前端通过body 以form表单的形式 传递 username和password
        //给请求设置编码方式 告诉后端前端使用的是什么编码方式
        req.setCharacterEncoding("utf8");
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        if(username==null){
            System.out.println("username这个属性在body中不存在");
        }
        if(password==null){
            System.out.println("password这个属性在body中不存在");
        }
        System.out.println("username=" + username+",password=" + password);
        resp.getWriter().write("okk");
    }

}
