import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet("/showRequest")
public class ShowRequest extends HelloServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       StringBuilder result=new StringBuilder();
       //请求的名称和版本
       result.append(req.getProtocol());
       //加入HTML的换行
       result.append("<br>");
       //请求的方法
        result.append(req.getMethod());
        result.append("<br>");
        //请求的URI
        result.append(req.getRequestURI());
        result.append("<br>");
        //请求的Query String
        result.append(req.getQueryString());
        result.append("<br>");
        //请求的 URI 的ContextPath
        result.append(req.getContextPath());
        result.append("<br>");
        result.append("========================");
        //获取所有的键值对键的名称
        result.append("<br>");
        Enumeration<String> headerNames= req.getHeaderNames();
        //遍历 Query String 键值对的键和对应的value
        while (headerNames.hasMoreElements()){
            String headerName=headerNames.nextElement();
            //获取此时键值对名 对应的value值
            String headerValue=req.getHeader(headerName);
            result.append(headerName+": "+headerValue+"<br>");
        }
        //在响应中设置body的类型方便浏览器进行解析
        resp.setContentType("text/html;charset=utf8");
        resp.getWriter().write(result.toString());
    }
}
