import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/refresh")
public class RefreshServlet extends HelloServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //给header设置一个Refresh属性 实现间隔多少时间自动刷新
        resp.setHeader("Refresh","1");
        //获取当前时间戳 写回
        resp.getWriter().write("time="+System.currentTimeMillis());
        resp.getWriter().write("<br>");
        //创建一个时间类
        Date date=new Date();
        //使用SimpleDateFormat 指定时间格式
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        resp.setContentType("text/html; charset=utf8");
        resp.getWriter().write(sdf.format(date)); //转换格式写回
    }
}
