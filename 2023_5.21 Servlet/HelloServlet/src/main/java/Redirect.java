import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/redirect")
public class Redirect extends HelloServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //当用户访问这个路径时 自动重定向到百度首页
        //需要设置状态码为302重定向
//        resp.setStatus(302);
//        resp.setHeader("Location","http://www.baidu.com");
        resp.sendRedirect("http://www.baidu.com");
    }
}
