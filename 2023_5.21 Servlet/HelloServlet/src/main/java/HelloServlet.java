import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {
//    @Override
//    public void init() throws ServletException {
//        //只有实例化（调用对应请求）只会执行一次（在doGet之前）
//        System.out.println("执行init");
//    }
//      重写方法
//    @Override
//    public void destroy() {
//        //在HttpServlet 销毁之前执行一次  用来做一些收尾工作
//        System.out.println("执行destroy");
//    }


    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //百度的Servlet跨域解决方法
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Credentials", "true");
        resp.setHeader("Access-Control-Allow-Methods", "*");
        resp.setHeader("Access-Control-Max-Age", "3600");
        resp.setHeader("Access-Control-Allow-Headers", "Authorization,Origin,X-Requested-With,Content-Type,Accept,"
                + "content-Type,origin,x-requested-with,content-type,accept,authorization,token,id,X-Custom-Header,X-Cookie,Connection,User-Agent,Cookie,*");
        resp.setHeader("Access-Control-Request-Headers", "Authorization,Origin, X-Requested-With,content-Type,Accept");
        resp.setHeader("Access-Control-Expose-Headers", "*");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        System.out.println("hello Servlet");
        resp.setContentType("text/html; charset=utf8");  //告诉浏览器这里返回的是utf8编码方式 让浏览器用utf8解析
        resp.getWriter().write("这是个 doGet方法");
        System.out.println("这是个 doGet方法");
        resp.setHeader("Access-Control-Allow-Origin", "*");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf8");
        resp.getWriter().write("这是个 doPost");
        System.out.println("这是个 doPost");
        resp.setHeader("Access-Control-Allow-Origin", "*");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf8");
        resp.getWriter().write("这是个 doPut");
        System.out.println("这是个 doPut");
        resp.setHeader("Access-Control-Allow-Origin", "*");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf8");
        resp.getWriter().write("这是个 doDelete");
        System.out.println("这是个 doDelete");
        resp.setHeader("Access-Control-Allow-Origin", "*");
    }
}
