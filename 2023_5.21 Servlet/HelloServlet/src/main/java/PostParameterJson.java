import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

class User{
    public String username;
    public String password;
}
@WebServlet("/postParameterJson")
public class PostParameterJson extends HelloServlet{
    //使用jackson 核心对象就是ObjectMapper
    //通过使用这个对象 可以把json字符串解析成java对象 也可以将java对象 转换成json格式字符串
    private ObjectMapper objectMapper=new ObjectMapper();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //通过post请求的body 传递一个json格式的字符串
        //通过jackson 传入流对象和对应的对象
        User user=objectMapper.readValue(req.getInputStream(),User.class);
        System.out.println("username=" + user.username+",password=" + user.password);
        resp.getWriter().write("okk");
    }
}
