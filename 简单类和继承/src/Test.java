
class Personnel{
    public String name;
    public int number;

    public Personnel(String name, int number) {
        this.name = name;
        this.number = number;
    }
    public void job(){
        System.out.println("工作");
    }
}
class Employee extends Personnel{

    public Employee(String name, int number) {
        super(name, number);
    }
    public void overtime(){
        System.out.println("加班");
    }
}
class Executives extends Employee{
    public String executives;

    public Executives(String name, int number, String executives) {
        super(name, number);
        this.executives = executives;
    }

    public void manage(){
        System.out.println("管理");
    }
}
class Manger extends Executives{
    public String department;

    public Manger(String name, int number, String executives, String department) {
        super(name, number, executives);
        this.department = department;
    }

    public void diplomacy(){
        System.out.println("外交");
    }

}
public class Test {
    public static void main(String[] args) {
        Employee employee=new Employee("小凯",123123);
        System.out.print("雇员姓名："+employee.name+" ");
        System.out.println("雇员工号为："+employee.number+" ");
        employee.job();
        employee.overtime();
        Executives executives=new Executives("小刘",1231,"清洁工");
        System.out.print("行政人员姓名："+executives.name+" ");
        System.out.print("行政人员工号："+executives.number+" ");
        System.out.println("行政人员职务："+executives.executives+" ");
        executives.job();
        executives.manage();
        Manger manger=new Manger("小张",25213,"财务经理","财务部");
        System.out.print("经理姓名："+manger.name+" ");
        System.out.print("经理工号："+manger.number+" ");
        System.out.print("经理职务："+manger.executives+" ");
        System.out.println("经理的部门："+manger.department+" ");
        manger.job();
        manger.diplomacy();
    }
}
