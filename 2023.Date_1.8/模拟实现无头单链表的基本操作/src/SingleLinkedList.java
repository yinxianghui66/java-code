
public class SingleLinkedList {
    static class ListNode {
        public int val;//存储的数据
        public ListNode next;//存储下一个节点的地址

        public ListNode(int val) {
            this.val = val;
        }
    }
    public ListNode head;
    //头插法
    public void addFirst(int data){
        ListNode listNode=new ListNode(data);
        listNode.next=head;
        head=listNode;
    }
    //尾插法
    public void addLast(int data){
        ListNode listNode=new ListNode(data);
        if(head==null){
            head=listNode;
            return;
        }
        ListNode cur=head;
        while (cur!=null){
            cur=cur.next;
        }
        cur.next=listNode;
    }
    //任意位置插入,第一个数据节点为0号下标
    public boolean addIndex(int index,int data){
        if(index==0){
            addFirst(data);
            return true;
        }
        if(index==size()){
            addLast(data);
            return true;
        }
        ListNode cur=find(index);  //找到index位置的地址
        ListNode listNode=new ListNode(data);
        listNode.next=cur.next;
        cur.next=listNode;

    }
    private ListNode find(int index){
        ListNode cur=head;
        int count=0;
        while (count!=index-1){
            cur=cur.next;
            count++;
        }
        return cur;
    }
    //查找是否包含关键字key是否在单链表当中
    public boolean contains(int key){}

    //删除第一次出现关键字为key的节点
    public void remove(int key){

    }
    //删除所有值为key的节点
    public void removeAllKey(int key){

    }
    //得到单链表的长度
    public int size(){

    }
    public void display(){

    }

    public void clear(){

    }
}
