public class Test {
    public static void main(String[] args) {
        int count=0;
        for (int i = 1; i <=10000; i++) {   //循环一万次
            for (int j = 1; j <i; j++) {  //从1-i一个一个试除，注意除去这个数本身（j<=i）不可取
                if(i%j==0){   //如果j可以整除i那么他就是其中一个因子
                    count+=j;    //设置计数器存储因子的和
                }
            }
            if(count==i){   //试除完后判断计数器和本身是否相同
                System.out.println(i+"是完全数");
            }
            count=0;   //计数器清零
        }
    }
}
