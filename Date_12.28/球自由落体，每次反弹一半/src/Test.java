//一球从100米高度自由落下，每次落地后反跳回原高度的一半；再落下，求它在 第10次落地时，共经过多少米？第10次反弹多高？
public class Test {
    public static void main(String[] args) {
        double n = 100;
        double sum = 100;//初始下落高度
        for (int i = 1; i < 10; i++) {//将i写为1，第一次下落特殊考虑
            sum = sum + n;
            n = n / 2;
        }
        System.out.println("第十次落地后的路程是" + sum + "米");
        System.out.println("第十次落地后反弹" + n / 2 + "米");
        //第十次落地后反弹的高度也就是下一次开始下落的高度
    }

}
