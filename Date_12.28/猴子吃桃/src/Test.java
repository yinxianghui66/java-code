
//一个猴子，第一天摘了若干桃子，当天就吃了一半+1个，第二天还是吃一半+1个，到第十条发现只剩1个桃子，求第一天拿了多少
public class Test {
    public static void main(String[] args) {
        int sum=1;  //第十天剩一个桃子
        for (int i = 10; i >1 ; i--) {  //一共十天
            sum=(sum+1)*2;   //每天吃了多少+回去
        }
        System.out.println("第一天拿了"+sum+"个桃子");
    }
}
