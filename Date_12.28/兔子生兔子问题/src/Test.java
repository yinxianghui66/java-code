import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        System.out.println("第"+n+"月有"+fun(n)+"对兔子");

    }
    public static int fun(int n){
        if(n!=1&&n!=2){
            if(n!=3){
                return fun(n-1)+fun(n-2);   //当从第四个月开始，第n月的对数为前两个月的对数和
            }
            return 2;   //当月份为3时返回两对
        }
        else return 1;  //当月份为1或2时直接返回一对
    }
}
