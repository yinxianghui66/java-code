public class TestHeap {
        public int[] elem;
        public int usedSize;

        public TestHeap() {
                this.elem=new int[10];
        }
//        public void initElem(int[] array){   //初始化
//                for (int i = 0; i <array.length; i++) {
//                        elem[i]=array[i];
//                        usedSize++;
//                }
//        }

        /**
         * 建堆的时间复杂度：O(N)
         *
         * @param array
         */

        public void createHeap(int[] array) {
                for (int i = 0; i <array.length; i++) {
                        elem[i]=array[i];
                        usedSize++;
                }
                for (int parent = (usedSize-1-1)/2; parent>=0; parent--) {
                        shiftDown(parent,usedSize);
                }
        }

        /**
         *
         * @param root 是每棵子树的根节点的下标
         * @param len  是每棵子树调整结束的结束条件
         * 向下调整的时间复杂度：O(logn)
         */
        private void shiftDown(int root,int len) {
                int child=root*2+1;
                while (child<len){
                    if(child+1<len&&elem[child]<elem[child+1]){
                            child++;
                    }
                    if(elem[root]<elem[child]){
                            int ret=elem[child];
                            elem[child]=elem[root];
                            elem[root]=ret;
                            root=child;
                            child=2*root+1;
                    }else{
                            break;
                    }
                }
        }


        /**
         * 入队：仍然要保持是大根堆
         * @param val
         */
        public void push(int val) {

        }

        private void shiftUp(int child) {

        }

        public boolean isFull() {

        }

        /**
         * 出队【删除】：每次删除的都是优先级高的元素
         * 仍然要保持是大根堆
         */
        public void pollHeap() {
        }

        public boolean isEmpty() {
        }

        /**
         * 获取堆顶元素
         * @return
         */
        public int peekHeap() {
        }
}
