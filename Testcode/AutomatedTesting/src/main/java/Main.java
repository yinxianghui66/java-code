import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class Main {
    public static void main(String[] args) throws InterruptedException {
//        test();  //查找搜索结果 是不是测试或者软件 是就输出测试通过
//        test2();     //打开百度搜索java107  等待三秒情况输入框
//        test3();   //测试百度按钮是不是对应文本
//        test4();  //智能等待
            test5();   //浏览器的前进 后退 滚动条调整 窗口调整
//        test6();  //键盘案例
//        test7();  //鼠标操作
//        test8();
    }

    private static void test8() throws InterruptedException {
        WebDriver webDriver=new ChromeDriver();
        //打开百度首页
        webDriver.get("https://www.baidu.com");
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);  //显示等待
        //找到输入框输入selenium
        webDriver.findElement(By.cssSelector("#kw")).sendKeys("selenium");
        //点击百度一下
        webDriver.findElement(By.cssSelector("#su")).click();
        sleep(3000);  //强制等待
        //获取搜索结果并打印
        List<WebElement> em = webDriver.findElements(By.cssSelector("a em"));
        for (int i = 0; i <em.size(); i++) {
            System.out.println(em.get(i).getText());
        }

    }

    private static void test7() {
        WebDriver webDriver=new ChromeDriver();
        //打开百度
        webDriver.get("http://www.baidu.com");
        //单机百度一下
        webDriver.findElement(By.cssSelector("#su")).click();
        //右击百度一下
        WebElement webElement=webDriver.findElement(By.cssSelector("#su"));
        Actions action=new Actions(webDriver);
        action.moveToElement(webElement).contextClick().perform();

    }

    private static void test6() throws InterruptedException {
        //打开百度首页
        WebDriver webDriver=new ChromeDriver();
        webDriver.get("https://www.baidu.com");
        //找到百度搜索框 输入java107
        webDriver.findElement(By.cssSelector("#kw")).sendKeys("521");
        //ctrl+A
        webDriver.findElement(By.cssSelector("#kw")).sendKeys(Keys.CONTROL,"A");
        sleep(3000);
        //ctrl+X
        webDriver.findElement(By.cssSelector("#kw")).sendKeys(Keys.CONTROL,"C");
        sleep(3000);
        webDriver.findElement(By.cssSelector("#kw")).clear();
        //ctrl+V
        sleep(3000);
        webDriver.findElement(By.cssSelector("#kw")).sendKeys(Keys.CONTROL,"V");
    }

    private static void test5() throws InterruptedException {
        //打开百度首页
        WebDriver webDriver=new ChromeDriver();
        webDriver.get("https://www.baidu.com");
        //找到百度搜索框 输入java107
        webDriver.findElement(By.cssSelector("#kw")).sendKeys("521");
        //点击搜索
        webDriver.findElement(By.cssSelector("#su")).click();

        //进行强制等待三秒
        sleep(3000);
        //浏览器后退前一步
        webDriver.navigate().back();
        //强制等待三秒
        sleep(3000);
        //刷新页面
        webDriver.navigate().refresh();
        sleep(3000);
        //浏览器前进一步
        webDriver.navigate().forward();
        //滚动条到最下
        ((JavascriptExecutor)webDriver).executeScript("document.documentElement.scrollTop=10000");
        //浏览器最大化
        webDriver.manage().window().maximize();
        sleep(3000);
        //进入全屏模式
        webDriver.manage().window().fullscreen();
        //设置固定宽高
        webDriver.manage().window().setSize(new Dimension(1000,1000));
        webDriver.close();
    }

    private static void test4() {
        //创建一个驱动
        WebDriver webDriver=new ChromeDriver();
        //打开百度首页
        webDriver.get("https://www.baidu.com/");
        //判断元素是否可以被点击
        WebDriverWait wait=new WebDriverWait(webDriver,1);
        //如果可以 正常执行 如果不是 则报错
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#bottom_layer > div > p:nth-child(4) > a")));

    }

    private static void test3() {
        ChromeOptions options = new ChromeOptions();
        //允许所有请求
        options.addArguments("--remote-allow-origins=*");
        WebDriver webDriver = new ChromeDriver(options);
        //打开百度网页
        webDriver.get("https://www.baidu.com");

        String button_value=webDriver.findElement(By.cssSelector("#su")).getAttribute("value");//查找到元素的属性 这里是value
        if(button_value.equals("百度一下")){
            System.out.println("测试通过");
        }else {
            System.out.println(button_value);
            System.out.println("测试不通过");
        }
    }

    private static void test2() throws InterruptedException {
//        ChromeOptions options = new ChromeOptions();
//        //允许所有请求
//        options.addArguments("--remote-allow-origins=*");
//        WebDriver webDriver = new ChromeDriver(options);
        //创建驱动
        WebDriver webDriver=new ChromeDriver();
        //打开百度网页
        webDriver.get("https://www.baidu.com");
        //找到百度搜索框 输入java107
        webDriver.findElement(By.cssSelector("#kw")).sendKeys("java107");
        //点击搜索
//        webDriver.findElement(By.cssSelector("#su")).click();
        webDriver.findElement(By.cssSelector("#kw")).submit();  //效果一样 但是submit 具有弊端 对标签要求苛刻
        sleep(3000);                                //如果元素放在from 是没有问题的
        //清空输入框的数据                                   //如果元素放在非from标签中 此时就会报错
        webDriver.findElement(By.cssSelector("#kw")).clear();
    }

    private static void test() throws InterruptedException {
        int flag=0;
        ChromeOptions options = new ChromeOptions();
        //允许所有请求
        options.addArguments("--remote-allow-origins=*");
        WebDriver webDriver = new ChromeDriver(options);
        //打开百度网页
        webDriver.get("https://www.baidu.com");
        //找到百度搜索的输入框  使用css定位元素
        WebElement element= webDriver.findElement(By.cssSelector(".s_ipt"));
        //使用xpath 定位元素
        WebElement element1=webDriver.findElement(By.xpath("//*[@id=\"kw\"]"));
        //输入测试信息
        element1.sendKeys("软件测试");
        //找到百度一下按钮 点击搜索
        webDriver.findElement(By.cssSelector("#su")).click();
        sleep(3000);
        //校验内容
        //获取到得到搜索结果
        List<WebElement> elements=webDriver.findElements(By.cssSelector("a em"));  //使用List存储搜索结果
        for (int i = 0; i<elements.size() ; i++) {
            //如果搜索的结果有软件或者测试 就说明测试通过
            if(elements.get(i).getText().contains("测试")||elements.get(i).getText().contains("软件")){
                System.out.println(elements.get(i).getText());
                System.out.println("测试通过");
            }else{
                System.out.println("测试不通过");
            }
        }
    }

}