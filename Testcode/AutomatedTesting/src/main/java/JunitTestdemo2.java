import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.stream.Stream;

public class JunitTestdemo2 {
    @ParameterizedTest
    //单参数
    @ValueSource(ints={1,2,3})
    void Test01(int num){
        System.out.println(num);
        //判断后面的是否和前面数据相等 相同就测试通过
//        Assertions.assertEquals(2,num);
        //判断后面的是否和前面数据相等 不相同就测试通过
//        Assertions.assertNotEquals(2,num);
        String str=null;
        //判断str为空 如果为空测试通过
        Assertions.assertNull(str);
        //判断str为空 如果不为空测试通过
//        Assertions.assertNotNull(str);
    }
    @ParameterizedTest
    //多参数(多种类型参数)
    @CsvSource({"1,2,3,''"})
    void Test02(String x,String y,int z,String q){
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        System.out.println(q);
        System.out.println("==========");
    }
    //CSV文件 将测试数据放到文件 按照一定格式读取
    @ParameterizedTest
    @CsvFileSource(resources = "test02.csv")
    void Test03(int num,String name){
        System.out.println("学号"+num+" "+"姓名"+":"+name);
    }
    public static Stream<Arguments> Generator() {
        return Stream.of(Arguments.arguments(
                "1,张三",
                "2,李四"
        ));
    }
    @ParameterizedTest
    //通过一个方法传入测试参数
    @MethodSource("Generator")
    void Test04(String num,String name){
        System.out.println(num+name);
    }

}
