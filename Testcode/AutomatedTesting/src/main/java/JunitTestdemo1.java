import org.junit.jupiter.api.*;
//设置注解 表示当前测试用例不再按照默认的顺序执行 而是按照我们设定的顺序执行
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class JunitTestdemo1 {
    //表示当前测试用例第一个执行
    @Order(1)
    //这个注解代表这是个测试用例
    @Test
    void Test01(){
        System.out.println("这是Test01方法");
    }
    @Order(2)
    @Test
    void Test04(){
        System.out.println("这是Test04方法");
    }
    @Order(3)
    @Test
    void Test03(){
        System.out.println("这是Test03方法");
    }
    @Order(4)
    @Test
    void Test02(){
        System.out.println("这是Test02方法");
    }

//    代表 只有在测试用例前执行一次 一般用于初始化一些数据
    @BeforeAll
    static void SetUp(){
        System.out.println("这是SetUp方法");
    }
    //代表 只有在测试用例执行完后执行一次 一般用于关闭一些数据和连接
    @AfterAll
    static void TearDown(){
        System.out.println("这是TearDown方法");
    }
    //在每个测试用例执行前执行一次
    @BeforeEach
    void  BeforeEachTest(){
        System.out.println("这是BeforeEachTest方法");
    }
    //在每个测试用例执行完后执行一次
    @AfterEach
    void AfterEachTest(){
        System.out.println("这是AfterEachTest方法");
    }

}
