import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;

@Suite
//通过class测试用例运行  测试套件 按照这里书写的顺序进行全部执行
//@SelectClasses({JunitTestdemo1.class,JunitTestdemo2.class})
//通过包 运行包中所有的测试用例
@SelectPackages(value = {"Package01","Package02"})
public class RunSuite {

}
