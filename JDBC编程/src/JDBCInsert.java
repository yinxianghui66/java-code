import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JDBCInsert {
    public static void main(String[] args) throws SQLException {
        //1.创建并初始化一个数据源、
        DataSource dataSource=new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java107_2?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("20020401yxh");
        //2.和数据库构建连接
        Connection  connection=dataSource.getConnection();
        //3.构造SQL语句
        String sql="insert into t1 values(6,'六号!!')";
        PreparedStatement statement=connection.prepareStatement(sql);
        //4.执行SQL语句
        int ret=statement.executeUpdate();
        System.out.println("ret="+ret);
        //5.释放必要的资源
        statement.close();
        connection.close();
    }
}
