import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

class add{
    public static void main(String[] args) throws SQLException {
        //创建并初始化一个数据源
        DataSource dataSource=new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java107_2?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("20020401yxh");
        //和数据库建立链接
        Connection connection=dataSource.getConnection();
        //构造SQL语句
         Scanner scanner=new Scanner(System.in);
         System.out.println("输入姓名");  //要先输入姓名，不然String类型会把回车当做姓名执行了！！！
        String name=scanner.nextLine();
        System.out.println("输入ID");
         int id=scanner.nextInt();
         String sql="insert into student values(?,?)";
        PreparedStatement statement= connection.prepareStatement(sql);
         statement.setInt(1,id);
         statement.setString(2,name);
         //真正输入到SQL里的语句
        System.out.println(statement);
        //执行SQL语句
        int ret=statement.executeUpdate();
        System.out.println("ret=:"+ret);
        //释放资源
        statement.close();  //后运行的什么就先释放什么
        connection.close();
    }
}
class Delete{
    public static void main(String[] args) throws SQLException {
        //创建并初始化一个数据源
        DataSource dataSource=new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java107_2?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("20020401yxh");
        //和数据库建立连接
        Connection connection=dataSource.getConnection();
        //构造SQL语句
        Scanner scanner=new Scanner(System.in);
        System.out.println("根据姓名请输入，要删除的数据");
        String name=scanner.nextLine();
        String sql="delete from student where name=?";
        PreparedStatement statement=connection.prepareStatement(sql);
        statement.setString(1,name);
        System.out.println(statement);
        //执行SQL语句
        int ret=statement.executeUpdate();
        System.out.println("ret="+ret);
        //释放资源
        statement.close();
        connection.close();
    }
}
class Select{
    public static void main(String[] args) throws SQLException {
        //创建并初始化数据源
        DataSource dataSource=new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java107_2?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("20020401yxh");
        //和数据库建立连接
        Connection connection=dataSource.getConnection();
        //构造SQL语句
        String sql="select * from student";
        PreparedStatement statement=connection.prepareStatement(sql);
        //执行SQL语句
        ResultSet resultSet=statement.executeQuery();  //只有查询用这个
        //遍历集合结果
        while (resultSet.next()){   //resultSet.next() 是读取这一行，如果没有数据就为false
            int id=resultSet.getInt("id");  //读取这一行的int类型的数据
            String name=resultSet.getString("name");   //读取这一行的String类型的数据
            System.out.println("id="+id+",name="+name);
        }
        //释放资源
        resultSet.close();
        statement.close();
        connection.close();
    }
}
class update{
    public static void main(String[] args) throws SQLException {
        //创建并初始化数据源
        DataSource dataSource=new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java107_2?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("20020401yxh");
        //链接数据库
        Connection connection=dataSource.getConnection();
        //构造SQL语句
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入姓名");
        String  name=scanner.nextLine();
        System.out.println("将他的id改为");
        int id=scanner.nextInt();
        String sql="update student set id=? where name=?";
        //预编译语句
        PreparedStatement statement=connection.prepareStatement(sql);
        statement.setInt(1,id);
        statement.setString(2,name);
        //执行SQL语句
        int ret=statement.executeUpdate();
        System.out.println("ret="+ret);
        //释放资源
        statement.close();
        connection.close();
    }
}
