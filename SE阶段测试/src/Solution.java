import java.util.*;
public class Solution {
    public static /*int[]*/void merge(int A[], int m, int B[], int n) {  //如果是void类型，需要考虑怎么返回新建的数组引用

//        int []C=Arrays.copyOf(A,m+n);
//        for (int i = 0; i <n; i++) {   //为什么这里i=2的时候为什么会先判断呢？？导致我数组越界了
//            C[m]=B[i];
//            m++;
//            if(i==2){
//                break;
//            }
//        }
//        Arrays.sort(C);
//        A=C;

//        A=Arrays.copyOf(A,m+n);
//        for (int i = 0; i <n; ) {
//           A[m]=B[i];
//           m++;
//           i++;
////           if(i==2){
////               break;
////          }
//       }
//       Arrays.sort(A);
//        return A;
        int []C=Arrays.copyOf(A,m+n);
       for (int i = 0; i <n; i++) {
          C[m]=B[i];
          m++;
          i++;
        }
       Arrays.sort(C);
       A=C;
    }


    public static void main(String[] args) {
        int []arr={4,5,6};
        int []arr1={1,2,3};
//        int []arr3=merge(arr,arr.length,arr1,arr1.length);
        merge(arr,arr.length,arr1,arr1.length);
        int []arr4={0};
        arr=Arrays.copyOf(arr,6);
        arr4=arr;    //在main方法中就可以将引用赋值给引用，为什么方法中就不可以呢？？？
        System.out.println(Arrays.toString(arr));  //为什么呢？？
        System.out.println(Arrays.toString(arr4));
//        System.out.println(Arrays.toString(arr3));

    }
}