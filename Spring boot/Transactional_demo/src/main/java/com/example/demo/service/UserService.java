package com.example.demo.service;

import com.example.demo.mapper.UserMapper;
import com.example.demo.model.Userinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

@Service
//声明的接口
public class UserService {
    @Autowired
    private UserMapper userMapper;
    //UserController 开启事务 调用这个方法
    //这个方法也开启事务 调用insert事务方法(有异常发生) 默认的传播级别
    //如果已经开启了事务 则添加当前事务 如果没有事务 则创建一个新事务
    //要么一起成功 要么一起回滚 insert发生异常了 就全部回滚 一条也插入不进去
    //上面是三个事务都为默认传播机制
    @Transactional(propagation = Propagation.NESTED)
    public int add(Userinfo userinfo){
        int res=userMapper.add(userinfo);
        System.out.println("add res->"+res);
        return res;
    }

    @Transactional(propagation = Propagation.NESTED)
    public int insert(Userinfo userinfo){
        int num=10/0;
        int res=userMapper.add(userinfo);
        System.out.println("insert res->"+res);
        return res;
    }
}
