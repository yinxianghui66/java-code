package com.example.demo.controller;

import com.example.demo.model.Userinfo;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/user")
@RestController
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("/add")
    // Transactional 注解 如果发生错误或者异常 就会回滚
    @Transactional
    public int add(){
        Userinfo userinfo=new Userinfo();
        userinfo.setUsername("老六");
        userinfo.setPassword("123");
        //非空判断
        //调用service 添加执行
        int res= userService.add(userinfo);
        System.out.println("result "+res);
        userService.insert(userinfo);
        //添加一个异常 运行失败 自动回滚数据库
        //如果异常被捕获 就不会触发回滚
//        try {
//            int num = 10 / 0;
//        }catch (Exception e){
//            //重新抛出异常 让代理对象感知
////            throw  e;
//            //使用代码 手动回滚事务
//            System.out.println(e.getMessage());
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//        }
        //将结果返回给前端
        return  res;
    }

}
