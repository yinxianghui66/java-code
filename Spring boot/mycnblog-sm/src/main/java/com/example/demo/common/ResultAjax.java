package com.example.demo.common;

import lombok.Data;

@Data
public class ResultAjax {
    public int code;
    public String msg;
    public Object data;
    //成功后执行的方法
    public static ResultAjax suss(Object data){
        ResultAjax resultAjax=new ResultAjax();
        resultAjax.setCode(200);
        resultAjax.setMsg("");
        resultAjax.setData(data);
        return resultAjax;
    }
    public static ResultAjax suss(String data,String msg,int code){
        ResultAjax resultAjax=new ResultAjax();
        resultAjax.setCode(code);
        resultAjax.setMsg(msg);
        resultAjax.setData(data);
        return resultAjax;
    }
    //失败执行的方法
    public static ResultAjax fail(int code,String msg){
        ResultAjax resultAjax=new ResultAjax();
        resultAjax.setCode(-1);
        resultAjax.setMsg(msg);
        resultAjax.setData("");
        return resultAjax;
    }
    public static ResultAjax fail(int code,String msg,Object data){
        ResultAjax resultAjax=new ResultAjax();
        resultAjax.setCode(-1);
        resultAjax.setMsg(msg);
        resultAjax.setData((String) data);
        return resultAjax;
    }
}
