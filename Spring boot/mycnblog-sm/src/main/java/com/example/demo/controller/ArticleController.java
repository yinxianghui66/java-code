package com.example.demo.controller;

import com.example.demo.common.ResultAjax;
import com.example.demo.common.SessionUtils;
import com.example.demo.model.Articleinfo;
import com.example.demo.model.Userinfo;
import com.example.demo.model.vo.UserinfoVo;
import com.example.demo.service.ArticleService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

@RestController
@RequestMapping("/article")
public class ArticleController {
    private static final int _DESC_LENGTH=120;
    @Autowired
    private ArticleService articleService;
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    @Autowired
    private UserService userService;

    //得到当前登陆用户的文章
    @RequestMapping("/mylist")
    public ResultAjax myList(HttpServletRequest request){
        //得到当前登陆用户
        Userinfo userinfo=SessionUtils.getSessionUser(request);
        if(userinfo==null){
            return ResultAjax.fail(-1,"请先登录");
        }
        //根据id 查询用户有哪些文章
        List<Articleinfo> list=articleService.getListById(userinfo.getId());
        //使用多线程  将文章正文变为简介
        if(list!=null&&list.size()>0){
            //并行处理list集合
            list.stream().parallel().forEach((art)->{
                //截取简介长度<=120
                if(art.getContent().length()>_DESC_LENGTH){
                    art.setContent(art.getContent().substring(0,120));
                }
            });

        }
        //返回数据给前端d
        return ResultAjax.suss(list);
    }
    //根据文章id 删除文章
    @RequestMapping("/del")
    public ResultAjax del(Integer aid,HttpServletRequest request){
        //参数校验
        if(aid<=0||aid==null){
            return ResultAjax.fail(-1,"错误");
        }
        //获取当前登录用户
        Userinfo userinfo=SessionUtils.getSessionUser(request);
        if(userinfo==null){
            return ResultAjax.fail(-2,"请先登录");
        }
        //判断文章归属人   判断和删除可以合并一条SQL判断 delete from articleinfo where id=#{aid} and uid=#{uid}
//        if(aid!=userinfo.getId()){
//            return ResultAjax.fail(-2,"当前文章不属于你");
//        }
        //删除操作
        int res=articleService.deleteByAid(aid,userinfo.getId());
        //返回数据
        return ResultAjax.suss(res);
    }
    //写文章
    @RequestMapping("/add")
    public ResultAjax add(Articleinfo articleinfo,HttpServletRequest request){
        //校验参数
        if(articleinfo==null||!StringUtils.hasLength(articleinfo.getTitle())||
        !StringUtils.hasLength(articleinfo.getContent())){
            return ResultAjax.fail(-1,"参数错误");
        }
        //获取用户登陆信息
        Userinfo userinfo=SessionUtils.getSessionUser(request);
        if(userinfo==null){
            return ResultAjax.fail(-2,"请先登录");
        }
        //将文章添加到数据库 设置文章的归属为当前用户id
        articleinfo.setUid(userinfo.getId());
        int res=articleService.add(articleinfo);
        //返回结果
        return ResultAjax.suss(res);
    }
    //根据aid 查询该文章 返回文章
    @RequestMapping("/update_init")
    public ResultAjax update_init(Integer aid,HttpServletRequest request){
        if(aid==null||aid<=0){
            return ResultAjax.fail(-1,"参数错误");
        }
        //得到登录用户 校验是否是本人查询(SQL语句中校验)
        Userinfo userinfo=SessionUtils.getSessionUser(request);
        //拿到文章内容
        Articleinfo articleinfo=articleService.getarticleById(aid,userinfo.getId());
        return ResultAjax.suss(articleinfo);
    }
    //修改文章
    @RequestMapping("/update")
    public ResultAjax revise(Articleinfo articleinfo,HttpServletRequest request){

        if(articleinfo==null||!StringUtils.hasLength(articleinfo.getTitle())||
                !StringUtils.hasLength(articleinfo.getContent())||articleinfo.getId()==0){
            return ResultAjax.fail(-1,"参数错误");
        }

        //获取用户登陆信息
        Userinfo userinfo=SessionUtils.getSessionUser(request);
        if(userinfo==null){
            return ResultAjax.fail(-2,"请先登录");
        }
        articleinfo.setUid(userinfo.getId());
        //执行修改操作 并且校验当前文章是否为该用户的
        int res=articleService.update(articleinfo);
        //返回结果
        return ResultAjax.suss(res);
    }
    //查询文章详情页面
    @RequestMapping("/detail")
    public ResultAjax detail(Integer aid) throws ExecutionException, InterruptedException {
        //校验参数
        if(aid==null||aid<=0){
            return ResultAjax.fail(-1,"非法参数");
        }
        //根据aid 查询文章信息  得到用户uid
        Articleinfo articleinfo=articleService.getDetailById(aid);

        if(articleinfo==null||articleinfo.getId()<=0){
            return ResultAjax.fail(-1,"非法参数");
        }
        //根据uid查询用户详情  使用多线程
        FutureTask<UserinfoVo> userTask=new FutureTask(()-> userService.getUserById(articleinfo.getUid()));
        taskExecutor.submit(userTask);
        //根据uid查询用户发表的总文章数
        FutureTask<Integer> artCountTask=new FutureTask(()-> articleService.getArtCountById(articleinfo.getUid()));
        taskExecutor.submit(artCountTask);
        //组装数据
        UserinfoVo userinfoVo=userTask.get(); //等待线程池执行完成
        int artCount=artCountTask.get();
        //将查询出的文章总数 赋值到拓展的用户表中
        userinfoVo.setArtCount(artCount);
        HashMap<String,Object> result=new HashMap<>();
        //返回两个对象 文章对象和用户对象
        result.put("user",userinfoVo);
        result.put("art",articleinfo);
        //返回结果
        return ResultAjax.suss(result);
    }
    //访问阅读量增加
    @RequestMapping("/increment_rcount")
    public ResultAjax incrementRrtCount(Integer aid) {
        if(aid==null||aid<=0){
            return ResultAjax.fail(-1,"参数错误");
        }
        int res=articleService.incrementRCount(aid);
        return ResultAjax.suss(res);
    }

    //查询分页功能
    @RequestMapping("/getlistbypage")
    public ResultAjax getListByPage(Integer pindex,Integer psize) throws ExecutionException, InterruptedException {
        //1.参数矫正  即使不传参数 也有一个默认值
        if(pindex==null||pindex<1){
            pindex=0;
        }
        if(psize==null||psize<1){
            psize=2;
        }
        //2.并发执行文章列表和总页数的查询
        //查询文章
        int finaloffset= pindex*(psize -1);  //分页公式
        int finalpsize=psize;
        FutureTask<List<Articleinfo>> listTask=new FutureTask(()->{
            return articleService.getListByPage(finalpsize,finaloffset);
        });
        //计算总页数
        FutureTask<Integer> sizeTask=new FutureTask(()->{
            int totalCount=articleService.getCount();
            //记得向上取整
            double sizeTemp=totalCount*1.0/finalpsize*1.0;
            return (int) Math.ceil(sizeTemp);
        });
        taskExecutor.submit(listTask);
        taskExecutor.submit(sizeTask);
        //3.组装数据
        List<Articleinfo> list=listTask.get();
        int size=sizeTask.get();
        HashMap<String,Object> map=new HashMap<>();
        map.put("list",list);
        map.put("size",size);
        //4.返回数据
        return ResultAjax.suss(map);
    }

}
