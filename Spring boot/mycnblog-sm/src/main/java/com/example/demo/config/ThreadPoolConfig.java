package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class           ThreadPoolConfig {
    //手动注入线程池
    @Bean
    public ThreadPoolTaskExecutor taskExecutor(){
            ThreadPoolTaskExecutor executor=new ThreadPoolTaskExecutor();
            executor.setCorePoolSize(5);
            executor.setQueueCapacity(10000);
            executor.setCorePoolSize(10);
            executor.setThreadNamePrefix("MyThread-");
            executor.initialize();
            return executor;
    }
}
