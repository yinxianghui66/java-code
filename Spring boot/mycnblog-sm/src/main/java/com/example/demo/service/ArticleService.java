package com.example.demo.service;

import com.example.demo.mapper.ArticleMapper;
import com.example.demo.model.Articleinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleService {
    @Autowired
    private ArticleMapper articleMapper;
    //根据uid 查询所有文章
    public List<Articleinfo> getListById(int uid) {
        List<Articleinfo> list=articleMapper.getaListById(uid);
        return list;
    }
    //根据id判断归属人 删除当前文章
    public int deleteByAid(Integer aid,int uid){
        return articleMapper.deleteByAid(aid,uid);
    }

    //添加操作
    public int add(Articleinfo articleinfo){
        return articleMapper.add(articleinfo);
    }
    //根据aid  查询文章
    public Articleinfo getarticleById(Integer aid,int uid){
        return articleMapper.getarticleById(aid,uid);
    }
    //修改文章
    public int update(Articleinfo articleinfo){
        return articleMapper.update(articleinfo);
    }
    //根据aid查询文章 没有校验用户
    public Articleinfo getDetailById(int aid){
        return articleMapper.getDetailById(aid);
    }
    //根据uid  查询该用户有多少文章
    public int getArtCountById(int uid){
        return articleMapper.getArtCountById(uid);
    }
    //阅读量添加
    public int incrementRCount(int aid){
        return articleMapper.incrementRCount(aid);
    }
    //分页功能 查询当前页面文章
    public List<Articleinfo> getListByPage(Integer psize, Integer pindex){
        return articleMapper.getListByPage(psize,pindex);
    }
    //计算总页数 需要查询的总文章数
    public int getCount(){
        return articleMapper.getCount();
    }




}
