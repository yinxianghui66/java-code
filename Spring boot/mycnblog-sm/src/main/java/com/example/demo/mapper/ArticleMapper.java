package com.example.demo.mapper;

import com.example.demo.model.Articleinfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ArticleMapper {

    @Select("select * from articleinfo where uid=#{uid} order by id desc")
    List<Articleinfo> getaListById(@Param("uid") int uid);

    @Delete("delete from articleinfo where id=#{aid} and uid=#{uid}")
    int deleteByAid(@Param("aid") Integer aid,int uid);

    @Insert("insert into articleinfo(title,content,uid) values(#{title},#{content},#{uid})")
    int add(Articleinfo articleinfo);

    @Select("select * from articleinfo where id=#{aid} and uid=#{uid}")
    Articleinfo getarticleById(@Param("aid") Integer aid,@Param("uid") int uid);

    @Update("update articleinfo set title=#{title},content=#{content} where id=#{id} and uid=#{uid}")
    int update(Articleinfo articleinfo);

    @Select("select * from articleinfo where id=#{aid}")
    Articleinfo getDetailById(@Param("aid") int aid);

    @Select("select count(*) from articleinfo where uid=#{uid}")
    int getArtCountById(@Param("uid") int uid);

    @Update("update articleinfo set rcount=rcount+1 where id=#{aid}")
    int incrementRCount(@Param("aid") int aid);

    @Select("select * from articleinfo order by id desc limit #{psize} offset #{offset}")
    List<Articleinfo> getListByPage(@Param("psize") Integer psize,@Param("offset") Integer offset);

    @Select("select count(*) from articleinfo")
    int getCount();
}
