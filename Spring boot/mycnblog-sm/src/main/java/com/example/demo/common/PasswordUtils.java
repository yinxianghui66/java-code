package com.example.demo.common;


//密码的工具类

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class PasswordUtils {

    //加盐加密！
    public static String encrypt(String password){
        //1.生成盐值！
        String salt= UUID.randomUUID().toString().replace("-","");
        //2.将盐值+密码 进行MD5 得到最终密码
        String finalPassword= DigestUtils.md5DigestAsHex((salt+password).getBytes(StandardCharsets.UTF_8));
        //3.将盐值和最终密码返回
        return salt+"$"+finalPassword;
    }

    /**
     *
     * @param password 待验证密码
     * @param dbPassword  数据库中 盐值和最终密码
     * @return
     */
    public static boolean decrypt(String password,String dbPassword){
        //1.校验参数
        if(!StringUtils.hasLength(password)||!StringUtils.hasLength(dbPassword)||dbPassword.length()!=65){
            return false;
        }
        //2.得到盐值  注意对$转义
        String[] dpPasswordArray=dbPassword.split("\\$");
        if(dpPasswordArray.length!=2){
            return false;
        }
        //3.对待验证密码和拿到的盐值加密  得到最终待验证密码
        String salt=dpPasswordArray[0];
        String dbFinalPassword=dpPasswordArray[1];

        String FinalPassword=DigestUtils.md5DigestAsHex((salt+password).getBytes(StandardCharsets.UTF_8));
        //4.验证最终密码和最终待验证密码 是否一致
        if(FinalPassword.equals(dbFinalPassword)){
            return true;
        }
        return false;
    }
}
