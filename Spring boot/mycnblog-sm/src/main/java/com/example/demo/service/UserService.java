package com.example.demo.service;

import com.example.demo.mapper.UserMapper;
import com.example.demo.model.Userinfo;
import com.example.demo.model.vo.UserinfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    public UserMapper userMapper;

    public int reg(Userinfo userinfo){
        return userMapper.reg(userinfo);
    }
    public Userinfo login(String username){
        return userMapper.login(username);
    }

    public UserinfoVo getUserById(int uid){
        return userMapper.getUserById(uid);
    }
}
