package com.example.demo.mapper;

import com.example.demo.model.Userinfo;
import com.example.demo.model.vo.UserinfoVo;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;
import java.util.List;

public interface UserMapper {

    @Insert("insert into userinfo(username,password) values(#{username},#{password})")
    int reg(Userinfo userinfo);

    @Select("select * from userinfo where username=#{username}")
    Userinfo login(String username);

    @Select("select * from userinfo where id=#{uid}")
    UserinfoVo getUserById(@Param("uid") int uid);
}
