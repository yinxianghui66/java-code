package com.example.demo.controller;

import com.example.demo.common.AppVariable;
import com.example.demo.common.PasswordUtils;
import com.example.demo.common.ResultAjax;
import com.example.demo.model.Userinfo;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.crypto.Data;
import java.util.Date;


@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public ResultAjax login(Userinfo userinfo , HttpServletRequest request){
        //拿到数据 校验参数是否正确！！
        if(userinfo==null||!StringUtils.hasLength(userinfo.getUsername())||
        !StringUtils.hasLength(userinfo.getPassword())){
            return ResultAjax.fail(-1,"参数有误！");
        }
        //从数据库查询数据  这种效率比较低 也有一些缺陷
//        List<Userinfo> list=userService.login();
//        //校验账号密码是否正确
//        for (int i = 0; i <list.size() ; i++) {
//            if(list.get(i).getUsername().equals(userinfo.getUsername())){
//                if(list.get(i).getPassword().equals(userinfo.getPassword())){
//                    return ResultAjax.suss("登陆成功");
//                }
//            }
//        }
        //根据用户名查询出一个对象  查询到的对象密码和输入的密码对比
        Userinfo res=userService.login(userinfo.getUsername());
        if(res==null||res.getId()==0){
            return ResultAjax.fail(-2,"用户名或者密码错误");
        }
        //校验密码
       if(!PasswordUtils.decrypt(userinfo.getPassword(),res.getPassword())) {
           return ResultAjax.fail(-2,"用户名或者密码错误");
       }
        //验证成功 登陆
        //对比成功后 存储Session
        HttpSession session = request.getSession(true);
        session.setAttribute(AppVariable.SESSION_USERINFO_KEY, res);
        //返回结果
        return ResultAjax.suss(66);
    }
    @RequestMapping("/reg")
    //对应前端的data中发送的名称 框架会直接从请求中读取
    public ResultAjax reg(Userinfo userinfo){
        //非空校验
        if(userinfo==null||!StringUtils.hasLength(userinfo.getUsername())||
        !StringUtils.hasLength(userinfo.getPassword())){
            return ResultAjax.fail(-1,"参数异常");
        }
        //之后的注册 全部使用加盐加密
        userinfo.setPassword(PasswordUtils.encrypt(userinfo.getPassword()));
        //插入数据数据库
        int result=userService.reg(userinfo);
        //返回结果
        return ResultAjax.suss(result);
    }
    //注销方法
    @RequestMapping("/logout")
    public ResultAjax logout(HttpServletRequest request){
        HttpSession session=request.getSession(false);
        if(session!=null&&session.getAttribute(AppVariable.SESSION_USERINFO_KEY)!=null){
            session.removeAttribute(AppVariable.SESSION_USERINFO_KEY);
        }
        return ResultAjax.suss(1);
    }
}
