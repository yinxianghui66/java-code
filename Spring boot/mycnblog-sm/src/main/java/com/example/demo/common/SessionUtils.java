package com.example.demo.common;

import com.example.demo.model.Userinfo;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

//session 工具类
public class SessionUtils {

    public static Userinfo getSessionUser(HttpServletRequest request){
        //拿到当前登陆用户
        HttpSession session=request.getSession(false);
        if(session!=null&&session.getAttribute(AppVariable.SESSION_USERINFO_KEY)!=null){
            //此时说明已经登陆
            return (Userinfo) session.getAttribute(AppVariable.SESSION_USERINFO_KEY);
        }
        return null;
    }
}
