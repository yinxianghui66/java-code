//根据Key 获取url中的value
function getParamValue(key){
    //得到当前url参数buf 
    var params=location.search;
    //去掉？
    if(params.indexOf("?")>=0){
        params=params.substring(1);
    }
    //根据&将多个参数分割 存入数组
    var paramarr=params.split("&");
    //循环对比key 并返回value
    if(paramarr.length>=1){
        for(var i=0;i<paramarr.length;i++){
            var item=paramarr[i].split("=");
            if(item[0]==key){
                return item[1];
            }
        }
    }
    return null;
}
