package com.example.demo.dao;

import com.example.demo.model.Articleinfo;
import com.example.demo.model.Userinfo;

import org.apache.tomcat.util.threads.ThreadPoolExecutor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

//告诉当前测试程序 目前项目运行在Spring boot中
@SpringBootTest
class UserMapperTest {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ArticleMapper articleMapper;
    @Test
    void getAll() {
        List<Userinfo> list=userMapper.getAll();
        System.out.println(list);
    }

    @Test
    void getUserById() {
        Userinfo userinfo=userMapper.getUserById(1);
        System.out.println(userinfo.toString());
    }

    @Test
    void login() {
        Userinfo userinfo=userMapper.login("admin","admin");
        System.out.println(userinfo.toString());
    }

    @Test
    void getAllByOrder() {
        List<Userinfo> list=userMapper.getAllByOrder("desc");
        System.out.println(list);
    }
    //不影响数据库 会自动回滚 这个注解
//    @Transactional
    @Test
    void delById() {
        int res=userMapper.delById(2);
        System.out.println("受影响的行数"+res);
    }

    @Test
    void update() {
        Userinfo userinfo=new Userinfo();
        //将id为1的用户名改为超级管理员
        userinfo.setId(1);
        userinfo.setUsername("超级管理员");
        int res=userMapper.update(userinfo);
        System.out.println("受影响的行数"+res);
    }

    @Test
    void add() {
        Userinfo userinfo=new Userinfo();
        //添加张三用户
        userinfo.setUsername("张三");
        userinfo.setPassword("123");
        userinfo.setPhoto("image/cat.jpg");
        int res=userMapper.add(userinfo);
        System.out.println("受影响的行数"+res);
    }
    @Test
    @Transactional
    void inset() {
        Userinfo userinfo=new Userinfo();
        //添加用户 并返回其id
        userinfo.setUsername("李四");
        userinfo.setPassword("123");
        userinfo.setPhoto("");
        int res=userMapper.insert(userinfo);
        System.out.println("受影响的行数 "+res+" ID: "+userinfo.getId());
    }

    @Test
    void getLikeList() {
        String username="三";
        List<Userinfo> list=userMapper.getLikeList(username);
        System.out.println(list);
    }
    @Test
    void getUserList(){
        int uid=1;
        final  Object[] resultArray=new Object[2];
        //开启线程
        ThreadPoolExecutor threadPool=new ThreadPoolExecutor(5,10,100, TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(100));
        threadPool.submit(new Runnable() {
            @Override
            public void run() {
                //1.根据uid查询userinfo
                 resultArray[0]=userMapper.getUserById2(uid);
            }
        });
        threadPool.submit(new Runnable() {
            @Override
            public void run() {
                //2.根据uid查询文章列表
                resultArray[1]=articleMapper.getListById(uid);
            }
        });
        //组装数据  等待线程池执行结束再组装数据
        while (threadPool.getTaskCount()
                !=threadPool.getCompletedTaskCount()){
        }
        Userinfo userinfo= (Userinfo) resultArray[0];
        List<Articleinfo> list= (List<Articleinfo>) resultArray[1];
        userinfo.setAlist(list);
        System.out.println(userinfo);
    }

    @Test
    void add2() {
        Userinfo userinfo=new Userinfo();
        userinfo.setUsername("王五");
        userinfo.setPassword("123");
        int res=userMapper.add2(userinfo);
        System.out.println("受影响的行数 "+res);
    }

    @Test
    @Transactional
    void add3() {
        Userinfo userinfo=new Userinfo();
        userinfo.setUsername("老九");
        userinfo.setPassword("123");
//        userinfo.setPhoto("小狗");
        int res=userMapper.add3(userinfo);
        System.out.println("受影响的行数 "+res);
    }

    @Test
    void getListByWhere() {
        Userinfo userinfo=new Userinfo();
//        userinfo.setId(1);
//        userinfo.setUsername("老八");
//        userinfo.setPassword("123");
        List<Userinfo> list=userMapper.getListByWhere(userinfo);
        System.out.println(list);
    }

    @Test
    void updateById() {
        Userinfo userinfo=new Userinfo();
        userinfo.setId(6);
        userinfo.setUsername("老6");
//        userinfo.setPassword("123456");
        int res=userMapper.updateById(userinfo);
        System.out.println("受影响的行数为-> "+res);
    }

    @Test
    @Transactional
    void delByIds() {
        //创建链表的时候 初始化数据
        List<Integer> list=new ArrayList<Integer>(){{
            add(2);add(3);add(6);add(7);add(8);add(9);add(10);
        }};
        int res=userMapper.delByIds(list);
        System.out.println("受影响的行数为-> "+res);
    }
}