package com.example.demo.common;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
//定义切面
@Aspect
@Component
public class UserAspect {
    //定义切点  拦截UserController下的所有方法
    @Pointcut("execution(* com.example.demo.controller.UserController.*(..))")
    public void pointcut(){}
    //通知
    @Before("pointcut()")
    public void doBefore(){
        System.out.println("执行了前置通知");
    }
    //后置通知
    @After("pointcut()")
    public void doAfter(){
        System.out.println("执行了后置通知");
    }
    //环绕通知
    @Around("pointcut()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("环绕通知执行前");
        //我们其他通知之前
        Object result=joinPoint.proceed();
        //执行完其他通知 最后执行
        System.out.println("环绕通知执行后");
        return result;
    }

}
