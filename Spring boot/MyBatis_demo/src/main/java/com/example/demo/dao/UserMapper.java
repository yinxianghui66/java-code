package com.example.demo.dao;

import com.example.demo.model.Userinfo;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

//接口 方法的声明
@Mapper  //数据持久层的标志
public interface UserMapper {
    //查询全部信息
    List<Userinfo> getAll();
    //根据用户id 查询对应数据
    Userinfo getUserById(@Param("uid") Integer uid);
    //登陆查询 SQL注入演示
    Userinfo login(@Param("username") String username,@Param("password") String password);
    //排序
    List<Userinfo> getAllByOrder(@Param("myorder") String order);
    //添加操作
    int add(Userinfo userinfo);
    //删除操作
    int delById(@Param("id") Integer id);
    //修改操作
    int update(Userinfo userinfo);
    //添加操作 拿到自增id
    int insert(Userinfo userinfo);
    //模糊查询
    List<Userinfo> getLikeList(@Param("username") String username);
    //多表查询  根据id查询
    @Select("select * from userinfo where id=#{id}")
    Userinfo getUserById2(@Param("id") Integer id);
    //使用动态SQL 实现非必传字段
    int add2(Userinfo userinfo);
    //搭配trim标签
    int add3(Userinfo userinfo);
    //where标签
    List<Userinfo> getListByWhere(Userinfo userinfo);
    //set标签  根据id修改用户名或密码(可传可不传)
    int updateById(Userinfo userinfo);
    //<foreach>标签 根据多个文章id删除文章
    int delByIds(List<Integer> ids);
}
