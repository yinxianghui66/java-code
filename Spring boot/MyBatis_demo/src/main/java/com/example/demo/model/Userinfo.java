package com.example.demo.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class Userinfo {
    private int id;
    //使用resultMap 映射数据表字段和类的字段不一致时
//    private String name;
    private String username;
    private String password;
    private String photo;
    private LocalDateTime createtime;
    private LocalDateTime updatetime;
    private int state;
    //联表字段
    private List<Articleinfo> alist;
}
