package com.example.demo.dao;

import com.example.demo.model.Articleinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ArticleMapper {
    //多表联查username
    @Select("select a.*,u.username from articleinfo a left join userinfo u on a.uid=u.id")
    List<Articleinfo> getAll();
    //多表查询 根据uid查询数据
    @Select("select * from articleinfo where uid=#{uid}")
    List<Articleinfo> getListById(@Param("uid") Integer uid);
}
