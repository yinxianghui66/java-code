package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    //链接点
    @RequestMapping("/getuser")
    public String getUser(){
        System.out.println("do getuser");
        return "get User";
    }
    @RequestMapping("sayHi")
    public String sayHi(){
        System.out.println("do sayHi");
        return "hello user";
    }
}
