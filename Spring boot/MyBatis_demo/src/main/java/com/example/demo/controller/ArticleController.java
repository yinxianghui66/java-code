package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/article")
public class ArticleController {
    @RequestMapping("/getarticle")
    public String getArticle(){
        System.out.println("do getArticle");
        return "getArticle";
    }
}
