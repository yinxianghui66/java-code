package com.example.demo.config;


import com.example.demo.common.ResultAjax;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
//统一异常处理
@RestControllerAdvice
public class ExceptionAdvice {
    //如果捕捉到空指针异常 就会出触发这里的代码
    @ExceptionHandler(NullPointerException.class)
    public ResultAjax doNullPointerException(NullPointerException e){
        ResultAjax resultAjax=new ResultAjax();
        resultAjax.setCode(-1);
        resultAjax.setMsg("空指针异常->"+e.getMessage());
        resultAjax.setData(null);
        return resultAjax;
    }
    //所有异常的父类都为Exception 可以直接对其进行处理
    @ExceptionHandler(Exception.class)
    public ResultAjax doException(NullPointerException e){
        ResultAjax resultAjax=new ResultAjax();
        resultAjax.setCode(-1);
        resultAjax.setMsg("异常->"+e.getMessage());
        resultAjax.setData(null);
        return resultAjax;
    }
}
