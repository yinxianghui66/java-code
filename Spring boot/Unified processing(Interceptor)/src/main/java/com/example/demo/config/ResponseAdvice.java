package com.example.demo.config;

import com.example.demo.common.ResultAjax;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;


//统一返回值的保底策略
@ControllerAdvice
public class ResponseAdvice implements ResponseBodyAdvice {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        //只有他为true 才会调用我们的beforeBodyWrite方法
        //返之不会调用
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {

        //对String 进行单独处理 因为这里是依赖String引擎去转换json格式
        //但是在这里 String类型的的引擎还没初始化好 导致如果是返回的字符串类型就会报错
        //这里我们对字符串类型单独处理
        //比较两个对象的类型是否相同  如果相同 直接返回
        if(body instanceof ResultAjax){
            return body;
        }
        if(body instanceof String){
            ResultAjax resultAjax=ResultAjax.succ(body);
            //将字符串转换成json格式
            try {
                return objectMapper.writeValueAsString(resultAjax);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        //如果不相同 直接返回成功方法 返回body 转换为统一格式
        return ResultAjax.succ(body);
    }
}
