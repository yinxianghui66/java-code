package com.example.demo.common;

import lombok.Data;

import javax.annotation.security.DenyAll;

@Data
public class ResultAjax {
    private int code; //状态码
    private String msg;  //状态码描述信息
    private Object data; //返回的数据
    //成功后执行的方法
    public static ResultAjax succ(Object data){
        ResultAjax resultAjax=new ResultAjax();
        resultAjax.setCode(200);
        resultAjax.setData(data);
        resultAjax.setMsg("");
        return  resultAjax;
    }
    //成功调用的方法 有描述信息的版本
    public static ResultAjax succ(Object data,String mag){
        ResultAjax resultAjax=new ResultAjax();
        resultAjax.setCode(200);
        resultAjax.setData(data);
        resultAjax.setMsg(mag);
        return  resultAjax;
    }
    //失败调用的方法 传入失败的状态码和描述信息
    public static ResultAjax fail(int code,String msg){
        ResultAjax resultAjax=new ResultAjax();
        resultAjax.setCode(code);
        resultAjax.setData("");
        resultAjax.setMsg(msg);
        return  resultAjax;
    }

}
