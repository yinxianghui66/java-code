package com.example.demo.config;

import com.example.demo.common.AppVar;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//随着项目启动 注入 在配置项中注入
@Component
//实现接口
public class UserInterceptor implements HandlerInterceptor {
    //重写preHandle  返回true时 表示拦截器验证成功 继续执行 返回false 验证失败 不会继续执行后续方法了
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //获取Session
        System.out.println("do UserInterceptor");
        HttpSession session=request.getSession(false);
        if(session!=null&&session.getAttribute(AppVar.SESSION_KEY)!=null){
            //用户已经登录
            return true;
        }
        response.sendRedirect("http://www.baidu.com");
        return false;
    }
}
