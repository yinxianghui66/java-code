package com.example.demo.controller;

import com.example.demo.common.AppVar;
import com.example.demo.common.ResultAjax;
import com.example.demo.model.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import static com.example.demo.common.AppVar.SESSION_KEY;

@RestController
@RequestMapping("/user")
public class UserController {
    @RequestMapping("login")
    public ResultAjax login(String username, String password, HttpServletRequest request){
        if(username.equals("zhangsan")&&password.equals("123")){
            HttpSession session=request.getSession(true);
            session.setAttribute("SESSION_KEY",SESSION_KEY);
            return ResultAjax.succ("登录成功");
        }
        return ResultAjax.fail(-1,"登录失败");
    }
    @RequestMapping("getuser")
    public String getGetUser() {
        System.out.println("do getUser");
        return "张三";
    }
    @RequestMapping("/reg")
    public ResultAjax reg(){
//        Object obj=null;
//        System.out.println(obj.hashCode());
        return ResultAjax.succ("reg");
    }
    //如果不手动返回 会由保底策略进行转换
    @RequestMapping("/user")
    public User user(){
        User user=new User();
        user.setId(1);
        user.setName("李四");
        user.setPassword("1223");
        return user;
    }

}
