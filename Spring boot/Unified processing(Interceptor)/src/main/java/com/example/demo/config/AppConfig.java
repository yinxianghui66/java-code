package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//@Configuration
public class AppConfig implements WebMvcConfigurer {
    @Autowired
    private UserInterceptor userInterceptor;
    //添加拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(userInterceptor).
                //拦截所有请求 再逐个放行
                addPathPatterns("/**").
                excludePathPatterns("/user/reg").
                excludePathPatterns("/user/login")
                .excludePathPatterns("/img/**")
                .excludePathPatterns("/css/**")
                .excludePathPatterns("/editor.md/**")
                .excludePathPatterns("/js/**")
                .excludePathPatterns("/WEB-INF/**")
                .excludePathPatterns("/blog_login.html");
    }
}
