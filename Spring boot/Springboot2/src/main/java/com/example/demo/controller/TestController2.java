package com.example.demo.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/test2")
public class TestController2 {

    @RequestMapping("/getName")
    public String getName(String name,Integer age){
        return "name->: "+name+"age->"+age;
    }

}
