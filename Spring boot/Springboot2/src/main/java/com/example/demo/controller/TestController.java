package com.example.demo.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/test",method = RequestMethod.GET)
public class TestController {
    @RequestMapping("/sayHi/mvc")
    public String sayHi(){
        return "你好 Spring MVC";
    }
    @GetMapping("/sayHi/mvc2")
    public String sayHi2(){
        return "你好 Spring MVC2";
    }
    @PostMapping("/sayHi/mvc3")
    public String sayHi23(){
        return "你好 Spring MVC3";
    }
}
