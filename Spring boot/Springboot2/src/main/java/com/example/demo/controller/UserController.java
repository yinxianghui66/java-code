package com.example.demo.controller;

import com.example.demo.model.User;
import org.apache.tomcat.util.http.parser.HttpParser;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/user")
public class UserController {
    private static String _SESSION_KEY="_SESSION_KEY";
    @RequestMapping("/add")
    public User add(User user){
        return  user;
    }
    //属性重命名 设置参数为非必要传递
    @RequestMapping("/name")
    public String name(@RequestParam(value = "n",required = false) String name){
        return name;
    }
    //获取前端的json对象
    @RequestMapping("/add_json")
    public User addByJson(@RequestBody User user){
        return user;
    }
    //获取路径中的属性
    @RequestMapping("/detail/{aid}")
    public Integer detail(@PathVariable("aid") Integer aid){
        return aid;
    }
    @RequestMapping("/detail2/{aid}/{name}")
    public String detail2(@PathVariable("aid") Integer aid,@PathVariable("name") String name){
        return "aid-> "+aid+" name-> "+name;
    }
    @RequestMapping("/upload")
    public String upload(@RequestPart("myfile")MultipartFile file) throws IOException {
        //1.生成一个唯一的id 使得每次保存的名字都不同  使用uuid(全球唯一ID)
        String name= UUID.randomUUID().toString().replace("-","");
        //2.得到源文件的后缀名(得到文件名字 进行分割 得到后缀名)
        //从文件名的最后一个点的地方截取到末尾
        String paths=file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        name+=paths;
        //文件保存路径
        String path="E:\\image\\"+name;
        file.transferTo(new File(path));
        return path;
    }
    @RequestMapping("getcookie")
    public String getCookie(@CookieValue(value = "java",required = false) String ck){
        return ck;
    }
    //创建Session

    @RequestMapping("set_session")
    public String setSession(HttpServletRequest request){
        HttpSession session=request.getSession(true);
        if(session!=null){
            session.setAttribute(_SESSION_KEY,"张三");
            return "session set success";
        }else {
            return "session set fail";
        }
    }
    //获取Session
    @RequestMapping("/get_session")
    public String getSession(@SessionAttribute(required = false,value = "_SESSION_KEY") String name){
        return name;
    }
    //获取Header
    @RequestMapping("/header")
    public String header(@RequestHeader("User-Agent") String userAgent){
        return userAgent;
    }
}
