package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j  //加入该注解 可以直接使用log对象 直接使用
public class LogController2 {
    @RequestMapping("/log2")
    public void log2()
    {
        log.trace("trace log2");
        log.debug("debug log2");
        log.info("info log2");
        log.warn("warn log2");
        log.error("error lo2");
    }

}
