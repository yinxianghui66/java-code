package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogController {
    //得到日志对象  使用@Slf4j 更加简单输出
    private static Logger log=
            LoggerFactory.getLogger(LogController.class);

    @RequestMapping("/log")

    public void log(){
        String message="今天周三 我要自己上厕所";
        log.trace("trace->"+message);
        log.debug("debug->"+message);
        log.info("info->"+message);
        log.warn("warn->"+message);
        log.error("error->"+message);
    }
}
