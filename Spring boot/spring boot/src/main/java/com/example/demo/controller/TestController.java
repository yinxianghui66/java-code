package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;                                                                                                                                                                                                                                                                                                                                           import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//
//@Controller
//@ResponseBody
@RestController
public class TestController {
//    //从配置文件 读取配置项 赋值到 myimage
//    @Value("${myimage.path}")
//    public String myimage;
//
    @RequestMapping("/sayHi")
    public String sayHi(){
        return  "Hello Spring boot";
    }

}
