package com.example.demo.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties("myobj")  //输入自定义对象的名称
@Component  //必须有五大类注解
public class User {
    private int id;
    private String name;
    private int age;
}
