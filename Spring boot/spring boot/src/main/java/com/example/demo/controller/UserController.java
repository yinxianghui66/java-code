package com.example.demo.controller;

import com.example.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

//@Controller
//@ResponseBody
@RestController
@RequestMapping("/user")
public class UserController {

//    @Value("${myobj.name}")
//    public String name;
    @Autowired
    private User user;

    @PostConstruct
    public void doUserController(){
        System.out.println(user.toString());
    }
}
