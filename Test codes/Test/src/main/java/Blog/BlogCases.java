package Blog;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
//这里使用的都是junit3!!  注意区分和5
//继承 使得每一个测试用例都有对应的创建驱动和关闭浏览器
public class BlogCases extends InitAndEnd{
    /**
     *
     *  输入正确账号密码 登陆成功
     */
    @Order(1)
    @ParameterizedTest
    //使用CSV文件 校验用户名 密码 url是否正确
    @CsvFileSource(resources = "blog_longin.csv")
    void LoginSuccess(String username,String password,String bolg_list_url) {
        //打开一个博客登录页
        //输入账号 密码
        //点击提交
        //校验当前页面和博客列表页是否相同(登录成功)
        //列表页展示的用户名是否是 自己账号的用户名
        webDriver.get("file:///E:/java-code/2023_5.5%20HTML/%E5%8D%9A%E5%AE%A2%E7%B3%BB%E7%BB%9F/blog_login.html?");//输入博客登录页的地址
        //智能等待 设置一个等待上限时间 如果提前获取到了就直接结束
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //输入用户名
        webDriver.findElement(By.cssSelector("#username")).sendKeys(username);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //输入密码
        webDriver.findElement(By.cssSelector("#possword")).sendKeys(password);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //点击登录
        webDriver.findElement(By.cssSelector("#submit")).click();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //校验当前页面是否是博客列表页
        String url=webDriver.getCurrentUrl();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //当前页面的url和博客列表页对比 如果相同则测试通过
        Assertions.assertEquals(bolg_list_url,url);
        //校验当前列表页的用户名是不是自己输入的用户名
        String name=webDriver.findElement(By.cssSelector("body > div.container > div.container-left > div > h3")).getText();
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        Assertions.assertEquals(username,name);
    }
    /**
     * 博客列表页 发布博客数量不为0 就通过
     */
    @Order(2)
    @Test
    void BlogList(){
        //打开博客列表页
        //获取所有文章的标题元素
        //判定元素数量不为0 测试通过
        webDriver.get("file:///E:/java-code/2023_5.5%20HTML/%E5%8D%9A%E5%AE%A2%E7%B3%BB%E7%BB%9F/bolg_list.html?");
        //获取标题元素个数
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        int titleNum= webDriver.findElements(By.cssSelector(".title")).size();
        Assertions.assertNotEquals(0,titleNum);
    }

    /**
     *
     *  查看全文 点击后 是否跳转到了博客详情页（url校验和博客标题）
     */

    @Order(3)
    @ParameterizedTest
    @ValueSource(strings = {"file:///E:/java-code/2023_5.5%20HTML/%E5%8D%9A%E5%AE%A2%E7%B3%BB%E7%BB%9F/blog_details.html?blogId=1"})
    void BlogDetail(String blog_url){
        //获取第一篇文章的标题
        String list_title=webDriver.findElement(By.cssSelector("body > div.container > div.container-right > div > div:nth-child(1)")).getText();
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        //点击查看全文按钮
        webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/div/a[1]")).click();
        //点击后校验当前页面的url和博客详情页是否一样
        String url=webDriver.getCurrentUrl();
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        Assertions.assertEquals(url,blog_url);
        //校验当前页面的标题和本来的第一批博客的标题是否一样
        //跳转后的文章标题
        String title=webDriver.findElement(By.cssSelector("body > div.container > div.container-right > h3")).getText();
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        Assertions.assertEquals(title,list_title);
    }

    /**
     * 写博客
     */
    @Order(4)
    @Test
    void EditBolg() throws InterruptedException {
        //找到写博客按钮，点击
        //找到输入框 输入博客标题
        //点击发布
        webDriver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)")).click();
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        //通过js代码 输入标题
        ((JavascriptExecutor)webDriver).executeScript("document.getElementById(\"textInput\").value=\"自动化测试\"");
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        //点击发布
        webDriver.findElement(By.cssSelector("#submitinput")).click();
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        //校验当前跳转的页面是不是博客列表页
        String url=webDriver.getCurrentUrl();
        Assertions.assertEquals("file:///E:/java-code/2023_5.5%20HTML/%E5%8D%9A%E5%AE%A2%E7%B3%BB%E7%BB%9F/bolg_list.html?editor-markdown-doc=%23%E5%9C%A8%E8%BF%99%E9%87%8C%E5%86%99%E4%B8%8B%E4%B8%80%E7%AF%87%E5%8D%9A%E5%AE%A2",url);
    }
    /**
     * 注销按钮  点击后 校验是否跳转到博客登陆页
     * 跳转之后是否有登录按钮
     */
    @Order(5)
    @Test
    void Logout(){
        //智能等待
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        //点击注销按钮
        webDriver.findElement(By.cssSelector("body > div.nav > a:nth-child(6)")).click();
        webDriver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
        //校验URL
        String cur_url=webDriver.getCurrentUrl();
        Assertions.assertEquals("file:///E:/java-code/2023_5.5%20HTML/%E5%8D%9A%E5%AE%A2%E7%B3%BB%E7%BB%9F/blog_login.html",cur_url);
        //校验登录按钮
        WebElement webElement=webDriver.findElement(By.cssSelector("#submit"));
        Assertions.assertNotNull(webElement);
    }
}
