package Blog;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class InitAndEnd {
    static WebDriver webDriver;
    @BeforeAll
    static void SetUp(){
        //创建浏览器驱动
        webDriver=new ChromeDriver();
    }
    @AfterAll
    static void TearDown(){
        //关闭浏览器
        webDriver.quit();
    }

}
