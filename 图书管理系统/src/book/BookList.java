package book;

public class BookList {
    private static final int DEFAULT_SIZE=10;
    private Book[] books=new Book[DEFAULT_SIZE];
    private int usedSize;

    public BookList() {
        books[0]=new Book("水浒传","施耐庵",66,"小说");
        books[1]=new Book("斗破苍穹","天蚕土豆",88,"玄幻小说");
        books[2]=new Book("龙族","江南",77,"小说");
        books[3]=new Book("斗罗大陆","唐家三少",55,"玄幻小说");
        this.usedSize=4;
    }

    public int getUsedSize() {
        return usedSize;
    }

    public void setUsedSize(int usedSize) {
        this.usedSize = usedSize;
    }

    public Book getBooks(int pos) {
        return this.books[pos];
    }

    public void setBook(Book book){
        this.books[usedSize]=book;
    }
    public void setBook(int j,Book book){
        this.books[j]=book;
    }
}
