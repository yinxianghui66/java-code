package opera;

import book.Book;
import book.BookList;

import java.util.Scanner;

public class Add implements IOPeration {
    @Override
    public void work(BookList bookList) {
        System.out.println("增加图书");
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入书名");
        String name= scanner.nextLine();
        System.out.println("请输入作者");
        String aothor= scanner.nextLine();
        System.out.println("请输入价格");
        int price= scanner.nextInt();
        String type1= scanner.nextLine();   //这里nextLine会把上一次价格回车读取，导致输入类型失败，应道手动读取空格一次再读取
        System.out.println("请输入类型");     //或者调整一下顺序，把类型调整到价格前面输入就可以避免这个问题
        String type= scanner.nextLine();


        Book book=new Book(name,aothor,price,type);
        int size=bookList.getUsedSize();
        for (int i = 0; i <size; i++) {
            Book tmp=bookList.getBooks(i);
            if(tmp.getName().equals(name)){
                System.out.println("这本书已经存在了，不可再放入");
                return;
            }
        }
        bookList.setBook(book);//将这本书存入书架
        System.out.println("增加成功");

        bookList.setUsedSize(size+1);   //将存入多少本书+1 存入成功

    }
}
