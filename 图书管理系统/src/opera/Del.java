package opera;

import book.Book;
import book.BookList;

import java.util.Scanner;

public class Del implements IOPeration{
    @Override
    public void work(BookList bookList) {
        System.out.println("删除图书");
        System.out.println("输入你要删除的图书");
        int index=-1;
        Scanner scanner=new Scanner(System.in);
        String name= scanner.nextLine();
        int ret=bookList.getUsedSize();
        for (int i = 0; i <ret; i++) {
            Book book=bookList.getBooks(i);
            if(book.getName().equals(name)){
                index=i;
                break;
            }
        }
        if(index==-1){
            System.out.println("没有这本书");
            return;
        }
        for (int j =index; j <ret-1; j++) {
            Book book=bookList.getBooks(j+1);
            bookList.setBook(j,book);
        }
        bookList.setUsedSize(ret-1);
        bookList.setBook(ret-1,null);  //将删除的书留下的重置为null
        System.out.println("删除成功");
    }
}
