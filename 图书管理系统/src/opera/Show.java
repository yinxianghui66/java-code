package opera;

import book.Book;
import book.BookList;

public class Show implements IOPeration{
    @Override
    public void work(BookList bookList) {
        System.out.println("打印所有图书");
        int size=bookList.getUsedSize();
        for (int i = 0; i <size; i++) {
            Book book=bookList.getBooks(i);
            System.out.println(book);
        }
    }
}
