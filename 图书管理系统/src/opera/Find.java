package opera;

import book.Book;
import book.BookList;

import java.util.Scanner;

public class Find implements  IOPeration{
    @Override
    public void work(BookList bookList) {
        System.out.println("查找图书");
        System.out.println("输入你要查找的图书");
        Scanner scanner=new Scanner(System.in);
        String name= scanner.nextLine();
        int ret=bookList.getUsedSize();
        int i=0;
        for ( i = 0; i <ret; i++) {
            Book book=bookList.getBooks(i);
            if(book.getName().equals(name)){
                System.out.println(book);
                System.out.println("查找成功");
                return;
            }
        }
            System.out.println("没有这本书");
    }
}
