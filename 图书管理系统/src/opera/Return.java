package opera;

import book.Book;
import book.BookList;

import java.util.Scanner;

public class Return implements IOPeration{
    @Override    public void work(BookList bookList) {
        System.out.println("归还图书");
        System.out.println("输入你要归还的图书");
        Scanner scanner=new Scanner(System.in);
        String name= scanner.nextLine();
        int ret=bookList.getUsedSize();
        for (int i = 0; i <ret; i++) {
            Book book=bookList.getBooks(i);
            if(book.getName().equals(name)&&book.isBorrowed()){
                book.setBorrowed(false);
                System.out.println("归还成功");
                return;
            }
        }
        System.out.println("没有这本书");
    }
}
