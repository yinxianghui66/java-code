import book.BookList;
import user.Administrator;
import user.Ordinary;
import user.User;

import java.util.Scanner;

public class Main {
    public static User login(){
        System.out.println("请输入你的姓名:");
        Scanner scanner=new Scanner(System.in);
        String name=scanner.nextLine();
        System.out.println("请输入你的身份：1->管理员   0->普通用户");
        int ret=scanner.nextInt();
        if(ret==1){
            return new Administrator(name);
        }else
        {
            return new Ordinary(name);
        }
    }

    public static void main(String[] args) {
        BookList bookList=new BookList();
        User user=login();
        while (true) {
            int ret = user.menu();
            user.doWork(ret,bookList);
        }
    }
}
