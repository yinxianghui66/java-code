package user;

import com.sun.org.apache.bcel.internal.generic.NEW;
import opera.*;

import java.util.Scanner;

public class Ordinary extends User{
    public Ordinary(String name) {
        super(name);
        this.ioPerations=new IOPeration[]{
                new Exit(),
                new Find(),
                new Borrowing(),
                new Return(),
        };
    }
    public int menu(){
        System.out.println("***********************************");
        System.out.println("hello"+" "+name+"欢迎来到图书管理系统");
        System.out.println("1.查找图书");
        System.out.println("2.借阅图书");
        System.out.println("3.归还图书");
        System.out.println("0.退出系统");
        System.out.println("************************************");
        System.out.println("请输入你的操作");
        Scanner scanner=new Scanner(System.in);
        int ret=scanner.nextInt();
        return ret;
    }
}
