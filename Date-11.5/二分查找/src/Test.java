public class Test {
    public static int find(int[]arr,int temp){
        int left=0;
        int right=arr.length-1;
        while (left<=right){
            int mid=(left+right)/2;
            if(arr[mid]==temp){
                return mid;
            }else if(arr[mid]>temp){
                right=mid-1;
            }else {
                left = mid +1;
            }
        }
        return -1;
    }
    public static void main(String[] args) {
        int []arr={1,2,3,4,5,6,7,8,9,10};
        int ret=find(arr,5);
        if(ret==-1){
            System.out.println("没有找到该元素");
        }else{
            System.out.println("该元素的下标为"+ret);
        }

    }
}
