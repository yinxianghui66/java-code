public class Test {
    public static int find(int[]arr){
        int ret=arr[0];
        for (int i = 1; i <arr.length; i++) {
            ret^=arr[i];
        }
        return ret;
    }
    public static void main(String[] args) {
        int []arr={4,1,2,1,2};
        System.out.println(find(arr));
    }
}