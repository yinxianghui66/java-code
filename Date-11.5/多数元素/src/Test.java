public class Test {
    public static int findMoreNum2(int[]arr){
        int count=0;
        int tmp=arr[0];
        for (int i = 0; i <arr.length; i++) {
            if(arr[i]==tmp){
                count++;
            }else if(arr[i]!=tmp){
                count--;
            }else {
                tmp=arr[i];
                count++;
            }
        }
        return tmp;
    }
    public static void main(String[] args) {
        int []arr={2,2,1,1,1,2,2};
        System.out.println(findMoreNum2(arr));
    }
}
