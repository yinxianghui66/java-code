import java.util.Arrays;

public class Test {
    public static void fan(int[]arr){
        for (int i = 0; i <arr.length; i++) {
            arr[i]=arr[i]*2;
        }
    }
    public static void main(String[] args) {
        int []arr={1,2,3};
        fan(arr);
        System.out.println(Arrays.toString(arr));
    }
}
