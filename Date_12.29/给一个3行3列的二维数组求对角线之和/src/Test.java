import java.util.Random;
//给定一个3行3列的二维数组，里面的数据随机生成，求两条对角线的和
public class Test {
    public static void main(String[] args) {
        int sum=0;
        int count=0;
        int [][]arr=Generate(3,3);  //生成一个n行k列的二维数组
        for (int i = 0; i <3 ; i++) {
            for (int j = 0; j < 3; j++) {
                if(i==j||i+j==2){
                    sum+=arr[i][j];
                }
            }
        }
        System.out.println(sum);
    }
    public static int[][]Generate(int n,int k){
        Random input=new Random();
        int [][]arr=new int [n][k];
        for (int i = 0; i <n ; i++) {
            for (int j = 0; j <k; j++) {
                arr[i][j]=input.nextInt(100);
            }
        }
        return arr;
    }
}
