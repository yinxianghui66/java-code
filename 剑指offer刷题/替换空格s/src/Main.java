import java.util.Scanner;
//牛客链接:https://www.nowcoder.com/practice/0e26e5551f2b489b9f58bc83aa4b6c68?tpId=265&tqId=39209&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D13&difficulty=undefined&judgeStatus=undefined&tags=&title=
//请实现一个函数，将一个字符串s中的每个空格替换成“%20”。
//        例如，当字符串为We Are Happy.则经过替换之后的字符串为We%20Are%20Happy。
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()) {
            String s = scanner.nextLine();
            System.out.println(replaceSpace(s));
        }
    }
    public static String replaceSpace (String s) {
        // write code here
        StringBuffer stringBuffer=new StringBuffer();
        for (int i = 0; i <s.length(); i++) {
            if(s.charAt(i)==' '){
                stringBuffer.append("%20");
            }else {
                stringBuffer.append(s.charAt(i));
            }
        }
        return stringBuffer.toString();
    }
}
