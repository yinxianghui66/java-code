//力扣链接：https://leetcode.cn/problems/add-two-numbers/
//给你两个 非空 的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，并且每个节点只能存储 一位 数字。
//        请你将两个数相加，并以相同形式返回一个表示和的链表。
//        你可以假设除了数字 0 之外，这两个数都不会以 0 开头。
//思路：分为两步
//1.将链表每一位都按位相加 特殊情况如果l2长度大于l1就直接将l2加入到l1后
//2.遍历 判断相加后的每一位数据 如果大于等于10 就进位  特殊情况 如果是最后一位 就需要特殊判断 然后新建一位
//返回处理完的数据即可
class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }
public class Main {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode head1=l1;
        ListNode head2=l2;
        //遍历
        while (head1!=null){
            if(head2!=null){
                //找到每一位进行相加
                head1.val+=head2.val;
                head2=head2.next;
            }
            //如果l2大于l1长度将剩余的加入到l1
            if(head1.next==null&&head2!=null){
                head1.next=head2;
                break;
            }
            head1=head1.next;
        }
        //处理进位 找到大于10的数进位
        merge(l1);
        return l1;
    }

    private void merge(ListNode head) {
        while (head!=null){
            //遍历此时的数据 如果找到大于等于10的就需要进位
            if(head.val>=10){
                head.val= head.val%10;
                //如果此时已经是最后一位 但是需要进位 就新建一位
                if(head.next==null){
                    head.next=new ListNode(0);
                }
                head.next.val+=1;
            }
            head=head.next;
        }
    }
//    public ListNode addTwoNumbers2(ListNode l1, ListNode l2) {
//        return add(l1,l2,0);
//    }
    //private ListNode add(ListNode l1, ListNode l2, int bit) {
//        if(l1==null&&l2==null&&bit==0){
//            return null;
//        }
//        int val=bit;
//        //遍历链表 将数据相加
//        if(l1!=null){
//            val+=l1.val;
//            l1=l1.next;
//        }
//        if(l2!=null){
//            val+=l2.val;
//            l2=l2.next;
//        }
//        ListNode node=new ListNode(val%10);
//        node.next=add(l1, l2, val/10);
//        return node;
//    }
}
