
//输入一个长度为 n 整数数组，实现一个函数来调整该数组中数字的顺序，使得所有的奇数位于数组的前面部分，
//        所有的偶数位于数组的后面部分，并保证奇数和奇数，偶数和偶数之间的相对位置不变
//https://www.nowcoder.com/practice/ef1f53ef31ca408cada5093c8780f44b?tpId=265&tqId=39223&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D13&difficulty=undefined&judgeStatus=undefined&tags=&title=
//  遍历两次数组 一次找奇数 放在前面 一次找偶数 放在奇数后面即可
public class Main {
    public static void main(String[] args) {
        int arr[]=new int[5];
        arr[0]=2;
        arr[1]=4;
        arr[2]=6;
        arr[3]=5;
        arr[4]=7;
        System.out.println( reOrderArray(arr).toString());
    }
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param array int整型一维数组
     * @return int整型一维数组
     */
    public static int[] reOrderArray (int[] array) {
        // write code here
        //
        int j=0;
        int arr[]=new int[array.length];
        for (int i = 0; i <array.length;i++) {
            if(array[i]%2!=0){
                arr[j]=array[i];
                j++;
            }
            if(j==array.length){
                break;
            }
        }
        for (int i = 0; i <array.length; i++) {
            if(array[i]%2==0){
                arr[j]=array[i];
                j++;
            }
            if(j==array.length){
                break;
            }
        }
        return arr;
    }
}
