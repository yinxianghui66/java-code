import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

class TreeNode {
      int val;
      TreeNode left;
    TreeNode right;
      TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
         this.val = val;
        this.left = left;
        this.right = right;
     }
 }
class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        //创建存储结果
        List<List<Integer>> list=new ArrayList<>();
        //如果为空直接返回
        if(root==null){
            return list;
        }
        //设置队列存储结点
        Queue<TreeNode> queue=new ArrayDeque<>();
        queue.add(root);
        //只要队列不为空
        while(!queue.isEmpty()){
            List<Integer> level=new ArrayList<>();
            //size为队列长度
            int size=queue.size();
            //遍历size次 也就是出这一行的元素
            //[3,9,20,null,null,15,7]  举例
            //开始3 size为1 3出队列 将值存入list 左右9 10节点入队列
            //再9 左右为空不做处理 9的值放到list中存储
            //再10 左右 15 7入队列 10的值放到list中存储
            //接着往下直到队列为空
            for(int i=0;i<size;i++){
                //出队列并且返回该节点
                TreeNode node=queue.poll();
                //当前结点值入队列
                level.add(node.val);
                //如果左右节点不为空就入栈
                if(node.left!=null){
                    queue.add(node.left);
                }
                if(node.right!=null){
                    queue.add(node.right);
                }

            }
            list.add(level);
        }
        return list;
    }
}

public class Main {

}


