class ListNode {
    public ListNode head;
    public int val;
    public ListNode next;
    public ListNode(int i) {
    }
}
//给定单向链表的头指针和一个要删除的节点的值，定义一个函数删除该节点。返回删除后的链表的头节点。
//        1.此题对比原题有改动
//        2.题目保证链表中节点的值互不相同
//        3.该题只会输出返回的链表和结果做对比，所以若使用 C 或 C++ 语言，你不需要 free 或 delete 被删除的节点
//牛客链接： https://www.nowcoder.com/practice/f9f78ca89ad643c99701a7142bd59f5d?tpId=265&tqId=39280&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D13&difficulty=undefined&judgeStatus=undefined&tags=&title=
// 思路 建立两个节点 一个在前一个在后 遍历链表 找到val对应的位置 将前序节点的后一个改为当前节点的下一个(删除当前节点)即可
public class Main {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param head ListNode类
     * @param val int整型
     * @return ListNode类
     */
    public ListNode deleteNode (ListNode head, int val) {
        // write code here
        ListNode res=new ListNode(0);
        res.next=head;
        //前序节点
        ListNode pre=res;
        //当前节点
        ListNode cur=head;
        while(cur!=null){
            //遍历找到这个val 断开连接
            if(cur.val==val){
                //前一个节点的下一个 就是当前节点的下一个（断开连接）
                pre.next=cur.next;
                break;
            }
            pre=cur;
            cur=cur.next;
        }
        //如果没有找到这个值 就返回原本的头结点
        return res.next;
    }
}
