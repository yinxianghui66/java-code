import java.util.Stack;
//链接：https://www.nowcoder.com/practice/54275ddae22f475981afa2244dd448c6?tpId=265&tqId=39213&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D13&difficulty=undefined&judgeStatus=undefined&tags=&title=
//用两个栈来实现一个队列，使用n个元素来完成 n 次在队列尾部插入整数(push)和n次在队列头部删除整数(pop)的功能。
//        队列中的元素为int类型。保证操作合法，即保证pop操作时队列内已有元素。。。

public class Main {
    Stack<Integer> stack1 = new Stack<Integer>();
    Stack<Integer> stack2 = new Stack<Integer>();

    public void push(int node) {
        //入栈
        stack1.push(node);
    }

    public int pop() {
        //如果第二个栈为空就入栈 开始逆序
        if(stack2.isEmpty()) {
            //只要栈不为空就入到另一个栈中
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
        }
        return  stack2.pop();
    }
}

