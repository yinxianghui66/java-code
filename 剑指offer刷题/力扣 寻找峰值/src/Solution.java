//力扣寻找峰值https://leetcode.cn/problems/find-peak-element/
//难度 是优化算法

//峰值元素是指其值严格大于左右相邻值的元素。
//给你一个整数数组 nums，找到峰值元素并返回其索引。数组可能包含多个峰值，在这种情况下，返回 任何一个峰值 所在位置即可。
//你可以假设 nums[-1] = nums[n] = -∞ 。
//
public class Solution {
    public static void main(String[] args) {
        int arr[]=new int[3];
        arr[0]=3;
        arr[1]=2;
        arr[2]=1;
        System.out.println(findPeakElement(arr));
    }
    public static int findPeakElement(int[] nums) {
        if(nums.length==1){
            return 0;
        }
        int index=nums.length;
        for (int i = 1; i <nums.length; i++) {
            if(i==nums.length-1){
                break;
            }
            int left=nums[i-1];
            int right=nums[i+1];
            if(left<nums[i]&&right<nums[i]){
                return i;
            }
        }
        if(nums[0]>nums[1]){
            return 0;
        }
        return index-1;
    }
}
