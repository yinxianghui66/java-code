import java.util.Scanner;
//牛客链接https://www.nowcoder.com/practice/abc3fe2ce8e146608e868a70efebf62e?tpId=265&tqId=39208&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D13&difficulty=undefined&judgeStatus=undefined&tags=&title=
//在一个二维数组array中（每个一维数组的长度相同），每一行都按照从左到右递增的顺序排序，
//        每一列都按照从上到下递增的顺序排序。请完成一个函数
//，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
//思路是线性遍历 根据题目的性质 一个位置他的左边 上边一定比这个位置的数小 右边 下边一定比这个数大
//那么可以找到一个边界 左下角或者右上角 比如说是右上角 拿右上角的数据和要找的数据比较 如果要查找的这个数比这个位置的数大 就说明要找的
//的这个数一定在这个位置的下边 如果小于说明这个数一定在这个位置的左边 一直遍历完整个二维数组 如果没找到就返回false 找到就返回true
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        int arr[][]=new int[4][4];
        for (int i = 0; i < arr[0].length; i++) {
            for (int j = 0; j <arr.length ; j++) {
                arr[i][j]= scanner.nextInt();
            }
        }
        System.out.println(Find(n, arr));
    }
    public static boolean Find(int target, int [][] array) {
        //从右上角开始比较
        int row=0;  //从第一行的最后一个元素 也就是右上角开始对比
        int col=array[0].length-1;
        //只要row不越界  col>=0 保证这一行有元素 如果<0说明没有元素
        while (row<=array.length-1&&col>=0){
            if(target>array[row][col]){
                //如果当前位置数大于要找的数 那么这个数一定在当前位置的下面
                row++;
            }else if(target<array[row][col]){
                //如果当前位置数小于要找的数 那么这个数一定在当前位置的左边
                col--;
            }else {  //如果相等 说明找到了
                return true;
            }
        }
        return false;
    }
}
