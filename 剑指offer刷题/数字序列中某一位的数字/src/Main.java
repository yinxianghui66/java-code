import java.util.ArrayList;
//数字以 0123456789101112131415... 的格式作为一个字符序列，在这个序列中第 2 位
//（从下标 0 开始计算）是 2 ，第 10 位是 1 ，第 13 位是 1 ，以此类题，请你输出第 n 位对应的数字。
//说是简单 一点都不简单。。。。   还没看懂。。  一点不想做  是数学题
public class Main {
    public static void main(String[] args) {
        System.out.println(findNthDigit(14));
    }
    public static int findNthDigit (int n) {
        //记录n是几位数
        int digit = 1;
        //记录当前位数区间的起始数字：1,10,100...
        long start = 1;
        //记录当前区间之前总共有多少位数字
        long sum = 9;
        //将n定位在某个位数的区间中
        while(n > sum){
            n -= sum;
            start *= 10;
            digit++;
            //该区间的总共位数
            sum = 9 * start * digit;
        }
        //定位n在哪个数字上
        String num = "" + (start + (n - 1) / digit);
        //定位n在数字的哪一位上
        int index = (n - 1) % digit;
         return (int)(num.charAt(index)) - (int)('0');
    }
//    public static int findNthDigit (int n) {
//        // write code here
//        ArrayList<Character> arrayList=new ArrayList<>();
//        for (int i = 0; i <10000; i++) {
//            for (int j = 0; j <size(i); j++) {
//               String s=String.valueOf(i);
//               arrayList.add(s.charAt(j));
//            }
//        }
//        return arrayList.get(n);
//    }
//    private static int size(int i) {
//        if(i==0){
//            return 1;
//        }
//        int size=0;
//        while(i!=0){
//            i/=10;
//            size++;
//        }
//        return size;
//    }

}
