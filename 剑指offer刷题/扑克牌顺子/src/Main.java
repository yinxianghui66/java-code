//现在有2副扑克牌，从扑克牌中随机五张扑克牌，我们需要来判断一下是不是顺子。
//        有如下规则：
//        1. A为1，J为11，Q为12，K为13，A不能视为14
//        2. 大、小王为 0，0可以看作任意牌
//        3. 如果给出的五张牌能组成顺子（即这五张牌是连续的）就输出true，否则就输出false。
//        4.数据保证每组5个数字，每组最多含有4个零，数组的数取值为 [0, 13]

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int arr[]={0,3,2,6,4};
        System.out.println(IsContinuous(arr));
    }
    public static boolean IsContinuous (int[] numbers) {
        Arrays.sort(numbers);
        int joker=0;
        //这里没有排序后最后一张一定为最大的 并且不可能为大小王 所以不用判断
        for (int i = 0; i <4;i++) {
            //统计大小王数量
            if(numbers[i]==0){
                joker++;
                //如果有重复的提前结束
            }else if(numbers[i]==numbers[i+1]){
                return false;
            }
        }
        //已经排序后 最大牌-去除去大小王的最小牌 如果小于5即可构成顺子
        return numbers[4]-numbers[joker]<5;
    }
//    public static boolean IsContinuous (int[] numbers) {
//        // write code here
//        Arrays.sort(numbers);
//        int ret=0;
//        for (int i = 0; i <numbers.length; i++) {
//            if(numbers[i]==0){
//                ret++;
//                continue;
//            }
//            if(numbers.length-1==i){
//                return true;
//            }
//            if(numbers[i]!=numbers[i+1]-1){
//                if(numbers[i]+ret!=numbers[i+1]){
//                    return false;
//                }
//                if(ret==0){
//                    return false;
//                }
//                ret--;
//            }
//        }
//        return true;
//    }

}
