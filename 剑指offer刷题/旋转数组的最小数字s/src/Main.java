import java.util.Scanner;

//有一个长度为 n 的非降序数组，比如[1,2,3,4,5]，将它进行旋转，即把一个数组最开始的若干个元素搬到数组的末尾，
//        变成一个旋转数组，比如变成了[3,4,5,1,2]，或者[4,5,1,2,3]这样的。请问，给定这样一个旋转数组，
//        求数组中的最小值。
//牛客：https://www.nowcoder.com/practice/9f3231a991af4f55b95579b44b7a01ba?tpId=265&tqId=39215&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D13&difficulty=undefined&judgeStatus=undefined&tags=&title=
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int array[]=new int[5];
        for (int i = 0; i <array.length; i++) {
            array[i]= scanner.nextInt();
        }
        System.out.println(minNumberInRotateArray(array));
    }
    public static int minNumberInRotateArray(int [] array) {
        for(int i=0;i<array.length;i++){
            //遍历一次数组 因为是升序旋转 找到第一个前一个数比后一个大的 后一个数就为最小的有序
            //如果是最后一个就不找了 防止越界
            if(i==array.length-1){
                return array[0];
            }
            if(array[i]>array[i+1]){
                return array[i+1];
            }

        }
        //如果第一个为最小 遍历完都没找到就返回第一个
        return array[0];
    }
}
