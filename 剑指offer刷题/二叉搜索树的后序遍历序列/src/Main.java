//输入一个整数数组，判断该数组是不是某二叉搜索树的后序遍历的结果。如果是则返回
//        true ,否则返回 false 。假设输入的数组的任意两个数字都互不相同。
//https://www.nowcoder.com/practice/a861533d45854474ac791d90e447bafd?tpId=265&tqId=39235&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D13&difficulty=undefined&judgeStatus=undefined&tags=&title=
//下面是我第一次做的思路 这个思路只能解决一定是三个结点的树
//比如说：{4,6,7,5}
//不属于上图的后序遍历，从另外的二叉搜索树也不能后序遍历出该序列 ，
//        因为最后的5一定是根节点，前面一定是孩子节点，可能是左孩子，
//        右孩子，根节点，也可能是全左孩子，根节点，也可能是全右孩子，根节点，
// 这个才是正确的思路应该从最后一个结点一定为根节点 从左到右找第一个大于根节点的值 就是左右子树的分界点
// 还没改！！
public class Main {

    public static void main(String[] args) {
        int arr[]={4,6,7,5};
        System.out.println(VerifySquenceOfBST(arr));
    }
    public static boolean VerifySquenceOfBST(int [] sequence) {
        if(sequence.length==0){
            return true;
        }
        int ret=0;
        for(int i=0;i<sequence.length;i+=3){
            if(i+2<sequence.length){
                int left=sequence[i];
                int right=sequence[i+1];
                int gen=sequence[i+2];
                if(left<gen&&gen<right){
                    ret=1;
                }else{
                    ret=0;
                }
            }

        }
        if(ret==1){
            return true;
        }
        return false;
    }
}
