//输入一个长度为 n 的链表，设链表中的元素的值为 ai ，返回该链表中倒数第k个节点。
//        如果该链表长度小于k，请返回一个长度为 0 的链表。
//官方的解法是 栈或者快慢指针
 class ListNode {
   int val;
  ListNode next = null;
   public ListNode(int val) {
     this.val = val;
  }
}

public class Main {
    public static void main(String[] args) {


    }
    public ListNode FindKthToTail (ListNode pHead, int k) {
        //我的思路是 先遍历一遍链表 然后统计链表的长度 在根据倒数第k个  size-k得到正数第几个
        //再遍历到对应的第几个位置即可
        //如果链表为空 长度为0 直接返回
        if(pHead==null||k==0){
            return null;
        }
        // write code here
        //定义一个结点 代替头节点统计链表的长度
        ListNode head=pHead;
        //因为下面统计没有统计头节点本身的值 所以只要不为空 默认长度就为1
        int size=1;
        //只要没到结尾 接着遍历 size++
        while (head.next!=null){
            size++;
            head=head.next;
        }
        //统计完之后 如果size<k 就直接返回null
        if(size<k){
            return null;
        }
        //遍历链表size-k次 到正数的size-k 也就是倒数第k个位置
        for (int i = 0; i <size-k; i++) {
            pHead=pHead.next;
        }
        return pHead;
    }
}
