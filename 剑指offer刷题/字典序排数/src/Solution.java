import java.util.ArrayList;
import java.util.List;

public class Solution {


    public List<Integer> lexicalOrder(int n) {
        List<Integer> list=new ArrayList<>();
        int num=1;
        for (int i = 0; i <n; i++) {
            list.add(num);
            //第一位为1 后一位就应该为1*10
            //如果*10还是小于n 就继续往后
            //如果num下一位大于n了 就直接/10 ++进入下一个序列
            if(num*10<=n){
                num*=10;
            }else {
                //如果模除10等于9或者num+1>n  说明找到这位数的末尾了  重置到上一位
                while (num%10==9||num+1>n){
                    num/=10;
                }
                num++;
            }
        }
        return list;
    }
}
