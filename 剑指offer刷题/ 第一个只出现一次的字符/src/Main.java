import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        String s="google";
        System.out.println(FirstNotRepeatingChar(s));
    }
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param str string字符串
     * @return int整型
     */
    public static int FirstNotRepeatingChar (String str) {
        // write code here
        HashMap<Character,Integer> map=new HashMap<>();
        for (int i = 0; i <str.length(); i++) {
            //遍历一遍统计个数  再次遍历找第一个为1的个数 下标输出
            //加入map  key为对应字符 value为当前位置 如果有这个字符就为这个字符+1 如果不存在就为0+1
            map.put(str.charAt(i), map.getOrDefault(str.charAt(i),0)+1);
        }
        for (int i = 0; i <str.length(); i++) {
            if(map.get(str.charAt(i))==1){
                return i;
            }
        }
        return -1;
    }
}
