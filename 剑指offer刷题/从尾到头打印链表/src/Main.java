import java.util.ArrayList;
import java.util.Stack;
// 牛客链接 https://www.nowcoder.com/practice/d0267f7f55b3412ba93bd35cfa8e8035?tpId=265&tqId=39210&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D13&difficulty=undefined&judgeStatus=undefined&tags=&title=
//输入一个链表的头节点，按链表从尾到头的顺序返回每个节点的值（用数组返回）
public class Main {
    public static class ListNode {
        int val;
        ListNode next = null;
        ListNode(int val) {
           this.val = val;
       }
    }
    public static void main(String[] args) {

    }
    public static ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
        //存储结构
        ArrayList<Integer> res=new ArrayList<>();
        //使用栈实现逆序
        Stack <Integer> stack=new Stack<>();
        //只要不为空就入栈
        while (listNode!=null){
            stack.push(listNode.val);
            listNode=listNode.next;
        }
        //只要栈不为空就加入链表
        while (!stack.isEmpty()){
            res.add(stack.pop());
        }
        return  res;
    }
}
