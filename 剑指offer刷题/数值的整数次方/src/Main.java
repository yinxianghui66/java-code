import java.util.Scanner;
//实现函数 double Power(double base, int exponent)，求base的exponent次方。
//      1.保证base和exponent不同时为0。
//        2.不得使用库函数，同时不需要考虑大数问题
//        3.有特殊判题，不用考虑小数点后面0的位数。
//牛客：https://www.nowcoder.com/practice/1a834e5e3e1a4b7ba251417554e07c00?tpId=265&tqId=39220&rp=1&ru=/exam/oj/ta&qru=/exam/oj/ta&sourceUrl=%2Fexam%2Foj%2Fta%3FtpId%3D13&difficulty=undefined&judgeStatus=undefined&tags=&title=
//  思路 有n次方就乘以n次base  主要注意处理负次方
public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        //输入要求的base 的exponent次方
        int base=scanner.nextInt();
        int exponent=scanner.nextInt();
        System.out.println(Power(base,exponent));
    }
    public static double Power(double base, int exponent) {
        //如果是负次方 就转换为分数 负次方转换为次方
        if(exponent<0){
            base=1/base;
            exponent=-exponent;
        }
        //累乘
        double ret=1.0;
        for(int i=0;i<exponent;i++){
            //有exponent次方就乘以 exponent次
            ret*=base;
        }
        return ret;
    }
}
