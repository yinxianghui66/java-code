import java.util.ArrayList;
import java.util.List;

public class Solution {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param s string字符串
     * @return int整型
     */
    public static int lengthOfLongestSubstring (String s) {
        // write code here
        //需要用到滑动窗口 目前还没解决

        //处理长度为1的时候
        int count=0;
        StringBuffer stringBuffer=new StringBuffer();
        for (int j = 0; j <s.length(); j++) {

            for (int i = j; i <s.length(); i++) {

                //如果当前字符不包含在前面字符串中  加入
                if(stringBuffer.indexOf(String.valueOf(s.charAt(i)))==-1){
                    stringBuffer.append(s.charAt(i));
                    //处理如果都是不重复的字符
                    if(stringBuffer.length()>count){
                        count=stringBuffer.length();
                    }
                }else {
                    //如果重复了就判断当前字符串长度 使count为最大子串长度
                    if(stringBuffer.length()>count){
                        count=stringBuffer.length();
                    }
                    stringBuffer.delete(0,stringBuffer.length());
                }
            }
        }

        return count;
    }

    public static void main(String[] args) {
        String s="N$po-O66.n=h!!#oJM#MNh:kIwxSEjFP7F)(@ROpH5x|t*XC+[`jkWor@F!Cmu8{|rft,fx;QM1p4W+U|9`gk_}(0*=cc93P";
        System.out.println(lengthOfLongestSubstring(s));
    }



}