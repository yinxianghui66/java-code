import java.util.ArrayList;
import java.util.List;
//给定两个整数 n 和 k，返回范围 [1, n] 中所有可能的 k 个数的组合。
//你可以按 任何顺序 返回答案。
//因为刚刚做了子集  我根据上一个题的思路 将所有可能全部列出
//最后判断长度==k的输出  但是这样导致效率非常差 代码很垃圾
//之后还要看回溯 递归的算法
public class Main {
    public static void main(String[] args) {
        combine(4,2);
    }
    public static List<List<Integer>> combine(int n, int k) {
        //转换成1-n的数组
        int arr[]=new int[n];
        int j=0;
        for (int i = 1; i <=n ; i++) {
            arr[j] = i;
            j++;
        }
        List<List<Integer>> result=new ArrayList<>();
        List<Integer> intSub=new ArrayList<>();
        result.add(intSub);

        for (int i = 0; i <arr.length; i++) {
            int num=arr[i];
            int time=result.size();
            for (int l = 0; l <time; l++) {
                List<Integer> list=result.get(l);
                List<Integer> sub=new ArrayList<>(list);
                sub.add(num);
                result.add(sub);

            }
        }
        List<List<Integer>> res=new ArrayList<>();
        for (int i = 0; i <result.size(); i++) {
            if(result.get(i).size()==k){
                res.add(result.get(i));
            }
        }
        return res;
    }
}
