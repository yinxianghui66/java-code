import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
//给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。
// 回溯算法 头一次用还是比较难理解 搭配递归  很难理解啊！！！
// 看的B站的视频 还是挺好的 https://www.bilibili.com/video/BV19v4y1S79W/?spm_id_from=333.337.search-card.all.click&vd_source=6a5d592a6d366eb6c070be0d1e7a3630
//https://leetcode.cn/problems/permutations/description/ 力扣链接
public class Main {
    //用来存放最终结果
     List<List<Integer>> result=new ArrayList<>();
    //存放符合条件的排列
    LinkedList<Integer> path=new LinkedList<>();
    boolean[] used;
    public List<List<Integer>> permute(int[] nums) {
        if(nums.length==0){
            return result;
        }
        used=new boolean[nums.length];
        permuteHelper(nums);
        return result;
    }
    private void permuteHelper(int[] nums){
        //当排列的大小和我数组长度相同时就为一个排列
        //加入到结果集中 并结束这次递归
        if(path.size()==nums.length){
            result.add(new ArrayList<>(path));
            return;
        }
        //这里i从0开始 因为排列数据重复也算是两种 每次都从第一位开始排列
        for (int i = 0; i < nums.length; i++) {
            //使用user 来标记那个数据已经使用过了 如果为true 说明已经使用
            if(used[i]==true){
                continue;
            }
            //使用这个之后 设置为true标记
            used[i]=true;
            //添加这次的结果
            path.add(nums[i]);
            permuteHelper(nums);
            //回溯
            path.removeLast();
            used[i]=false;
        }
    }
}
