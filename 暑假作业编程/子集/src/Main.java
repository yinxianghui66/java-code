import java.util.ArrayList;
import java.util.List;
//给你一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。
//        解集 不能 包含重复的子集。你可以按 任意顺序 返回解集。
//https://leetcode.cn/problems/subsets/description/
//这个题 使用dfs  回溯 和拓展法 这里是拓展法
//开始赋值一个为空的子集(空为任何元素的子集)
//然后挨个遍历数组 在原来子集的基础上 将数组元素挨个加入之前的每一个子集
//例如开始是空集
//  []  加入1 变为[],[1]
//  [] [1]  加入2 变为 [],[1],[2],[1,2]
//  [],[1],[2],[1,2] 加入3 变为 [],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]
public class Main {
    public static void main(String[] args) {
        int num[]={1,2,3};
        subsets(num);
    }
    public static List<List<Integer>> subsets(int[] nums) {
        //创建intSub 存储每一个子集
        List<List<Integer>> result=new ArrayList<>();
        List<Integer> intSub=new ArrayList<>();
        //加入空集到result
        result.add(intSub);
        //遍历数组长度 将每个元素 依次加入每一个子集
        for (int i = 0; i <nums.length; i++) {
            int num=nums[i];
            int time=result.size();
            //当前有多少子集 每个子集都加入新元素
            for (int j = 0; j <time; j++) {
                //创建临时变量 存储每一个子集
                List<Integer> list=result.get(j);
                List<Integer> sub=new ArrayList<>(list);
                //给每个子集都添加新元素
                sub.add(num);
                result.add(sub);
            }
        }
        return result;
    }
}
