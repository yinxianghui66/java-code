import java.util.Scanner;

public class Test {
    public static void func(int n){
        if(n<=9){
            System.out.print(n%10+" ");
            return;
        }
        func(n/10);
        System.out.print(n%10+" ");
    }
    public static int fun(int n){
        if(n==1){
            return 1;
        }
        else{
            return n+fun(n-1);
        }
    }
    public static int  fan(int n){
        if(n<=9) {
            return n;
        }else{
            return n%10+fan(n/10);
        }
    }
    public static int fib(int n){
        if(n==1||n==2){
            return 1;
        }else{
            return fib(n-1)+fib(n-2);
        }
    }
    public static void main(String[] args) {
        System.out.println(fib(40));
    }
    public static void main4(String[] args) {
        System.out.println(fan(123));
    }
    public static void main3(String[] args) {
        System.out.println(fun(10));

    }
    public static void main2(String[] args) {
            func(1234);
    }
    public static void main1(String[] args) {
        Scanner scan=new Scanner(System.in);
        System.out.println("输入两个整数");
        int n=scan.nextInt();
        int k=scan.nextInt();
        scan.close();
        while((n%k)!=0){
            int c=n%k;
            n=k;
            k=c;
        }
        System.out.println("最大公约数为"+k);
    }
}
