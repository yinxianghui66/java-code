import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner n=new Scanner(System.in);
        int x=n.nextInt();
        for(int i=0;i<x;i++){
            for (int j=0;j<x;j++){
                if(i==j){
                    System.out.print("*");
                }else if(j==x-1-i){
                    System.out.print("*");
                }else{
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
