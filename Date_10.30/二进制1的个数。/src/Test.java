import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.println("请输入一个数");
        int ret=s.nextInt();
        s.close();
        int count=0;
        while (ret!=0){
            if((ret&1 )==1){
                count++;
            }
            ret=ret>>1;
        }
        System.out.println("该整数的二进制位一共有"+count+"个1");
    }
}
