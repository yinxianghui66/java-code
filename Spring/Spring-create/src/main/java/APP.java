
import com.demo.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.nio.file.attribute.UserPrincipalLookupService;

public class APP {
    public static void main(String[] args) {
        //获取Bean对象
        //1. 得到Spring上下文对象(使用static方法 时使用这种方法获取对象)
        ApplicationContext context=new ClassPathXmlApplicationContext("Spring-config.xml");
        //得到Bean对象  (依赖查找)
        User user=(User) context.getBean("user");
        //使用 打印 Bean对象
        System.out.println(user.sayHi("zhangsan"));
    }
}
