package com.java.demo.Controller;

import com.java.demo.model.User;
import com.java.demo.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

//用户控制器版本2
@Controller
public class UserController2 {
    //注入Users公共类
    @Autowired
    public User user;
    public void doMethod(){
        User user1=user;
        System.out.println("之前->"+user);
        user1.setName("777");
        System.out.println("User->"+user);
    }
}
