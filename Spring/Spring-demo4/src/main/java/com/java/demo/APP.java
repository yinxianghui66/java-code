package com.java.demo;

import com.java.demo.Controller.UserController;
import com.java.demo.Controller.UserController2;
import com.java.demo.Controller.UserController3;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
// 在Spring 项⽬中，通过 main ⽅法获取到 Controller 类，调⽤ Controller ⾥⾯通过注⼊的⽅式调⽤
//Service 类，Service 再通过注⼊的⽅式获取到 Repository 类，Repository 类⾥⾯有⼀个⽅法构建⼀
//个 User 对象，返回给 main ⽅法。Repository ⽆需连接数据库，使⽤伪代码即可。
public class APP {
    public static void main(String[] args) {
        ApplicationContext context=new ClassPathXmlApplicationContext("spring-config.xml");
        UserController controller=context.getBean("userController", UserController.class);
        //打印User
//        System.out.println(controller.sayHi());
        UserController2 controller2=context.getBean("userController2", UserController2.class);
        UserController3 controller3=context.getBean("userController3", UserController3.class);
        controller2.doMethod();
        controller3.doMethod();
    }
}
