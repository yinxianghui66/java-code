package com.java.demo.Controller;

import com.java.demo.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

//用户控制器版本3
@Controller
public class UserController3 {
    @Autowired
    public User user;
    public void doMethod(){
        System.out.println("user->"+user);
    }
}
