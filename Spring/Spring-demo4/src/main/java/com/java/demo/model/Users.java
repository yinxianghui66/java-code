package com.java.demo.model;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class Users {
    //公共对象  Bean默认是单例模式 只会创建一个
    //可以通过 @Scope 设置作用域
    @Bean("user")
    @Scope("prototype") //原型模式 多例模式
    public User getUser(){
        User user=new User();
        user.setName("王五");
        return user;
    }
}
