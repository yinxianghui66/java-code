package com.java.demo.Controller;

import com.java.demo.Service.UserService;
import com.java.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {
    @Autowired
    public UserService service;

    public User sayHi(){

        System.out.println("do UserController");
        return service.sayHi();
    }
}
