package com.java.demo.Service;

import com.java.demo.Repository.UserRepository;
import com.java.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User sayHi(){
        System.out.println("do  UserService");
        return  userRepository.user();
    }
}
