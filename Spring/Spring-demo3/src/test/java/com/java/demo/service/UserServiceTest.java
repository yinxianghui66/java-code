package com.java.demo.service;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

class UserServiceTest {

    @org.junit.jupiter.api.Test
    void add() {
        ApplicationContext context=new ClassPathXmlApplicationContext("spring-config.xml");
        UserService service=context.getBean("userService",UserService.class);
        service.add();
    }
}