import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanLifeTest {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("spring-config.xml");
        BeanLifeComponent component=context.getBean("mybean",BeanLifeComponent.class);
        component.sayHi();
        context.close();
    }
}
