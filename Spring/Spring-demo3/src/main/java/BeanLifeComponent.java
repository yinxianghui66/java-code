import org.springframework.beans.factory.BeanNameAware;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanLifeComponent implements BeanNameAware {
    //通知方法
    @Override
    public void setBeanName(String s) {
        System.out.println("执行了 BeanNameAware->"+s);
    }
    //初始化方法
    @PostConstruct
    public void  doPostConstruct(){
        System.out.println("执行了 doPostConstruct");
    }
    //xml写法
    public void myinit(){
        System.out.println("执行了 myinit");
    }
    //销毁方法
    @PreDestroy
    public void doPreDestroy(){
        System.out.println("执行了 doPreDestroy");
    }
    public void sayHi(){
        System.out.println("使用Bean");
    }


}
