package com.java.demo.service;

import com.java.demo.dao.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService4 {
    //构造方法注入
    private UserRepository userRepository;
    @Autowired
    public UserService4(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    public void sayHi(){
        System.out.println("do UserService4 sayHi");
        userRepository.add();
    }
}
