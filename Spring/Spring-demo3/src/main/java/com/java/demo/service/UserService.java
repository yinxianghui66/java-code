package com.java.demo.service;

import com.java.demo.dao.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    //更方便的注入Bean对象
    //1.属性注入(DI依赖注入) 使用注解@Autowired
    @Autowired
    private UserRepository userRepository;
    //1.使用
    public int add(){
        userRepository.add();
        System.out.println("do UserService add 方法");
        return 1;
    }
//    public int add(UserRepository userRepository){
//        userRepository.add();
//        return 1;
//    }



}
