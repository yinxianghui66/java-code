package com.java.demo.service;

import com.java.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserService2 {
    //属性注入   @Qualifier指定多个注入对象名称
    @Resource(name = "user1")
//    @Autowired
//    @Qualifier("user2")
    private User user;
   public void sayHi(){
       System.out.println(user.toString());
   }

}
