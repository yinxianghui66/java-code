import com.demo.UserService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;


public class APP2 {
    public static void main(String[] args) {
        //得到上下文对象
        BeanFactory context=new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
        //获取Bean
        UserService userService=context.getBean("user", UserService.class);
        //使用Bean
        userService.SayHi();
    }
}
