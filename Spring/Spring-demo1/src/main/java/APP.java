import com.demo.Student;
import com.demo.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class APP {
    public static void main(String[] args) {
        //得到Spring上下文
        ApplicationContext context=
                new ClassPathXmlApplicationContext("spring-config.xml");
        //获取bean对象(依赖查找->IoC的一种实现)
        UserService userService= context.getBean("user",UserService.class);
        //使用bean对象
        userService.SayHi();
        ApplicationContext context1=new ClassPathXmlApplicationContext("spring-config.xml");
        Student student=context.getBean("student",Student.class);
        student.put();
    }
}
