package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {
	//Spring启动类  启动项目 ！！！
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
