package com.demo.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 普通的文章实体类
 * */

public class ArticleInfo {
    private int aid;
    private String title;
    private String content;
    private LocalDateTime createtime;

    @Override
    public String toString() {
        return "ArticleInfo{" +
                "aid=" + aid +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", createtime=" + createtime +
                '}';
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreatetime() {
        return createtime;
    }

    public void setCreatetime(LocalDateTime createtime) {
        this.createtime = createtime;
    }
}
