package com.demo.service;

import com.demo.model.ArticleInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
@Order(2)
@Component
public class Articles {

    @Bean("articleInfo")//将当前方法返回的对象存储到IOC容器
    public ArticleInfo articleInfo(){
        //伪代码 使用new！
        ArticleInfo articleInfo=new ArticleInfo();
        articleInfo.setAid(1);
        articleInfo.setTitle("今天周几");
        articleInfo.setContent("今天星期六");
        articleInfo.setCreatetime(LocalDateTime.now());
        return articleInfo;
    }

//    @Bean("articleInfo")//将当前方法返回的对象存储到IOC容器
    public ArticleInfo articleInfo2(){
        //伪代码 使用new！
        ArticleInfo articleInfo=new ArticleInfo();
        articleInfo.setAid(1);
        articleInfo.setTitle("今天周几");
        articleInfo.setContent("今天星期日");
        articleInfo.setCreatetime(LocalDateTime.now());
        return articleInfo;
    }
    public void sayHi(){
        System.out.println("hello Articles");
    }

}
