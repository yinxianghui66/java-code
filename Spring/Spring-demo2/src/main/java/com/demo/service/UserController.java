package com.demo.service;
import com.demo.model.ArticleInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;

//使用类注解 加入Bean对象
@Order(1)
@Controller
public class UserController {
    public void SayHi(String name){
        System.out.println("Hi "+name);
    }
//    @Bean("articleInfo")//将当前方法返回的对象存储到IOC容器
    public ArticleInfo articleInfo3(){
        //伪代码 使用new！
        ArticleInfo articleInfo=new ArticleInfo();
        articleInfo.setAid(1);
        articleInfo.setTitle("666");
        articleInfo.setContent("今天星期日");
        articleInfo.setCreatetime(LocalDateTime.now());
        return articleInfo;
    }
}
