import java.beans.Introspector;

public class Test {
    public static void main(String[] args) {
        //调用Bean的命名规则 对比
       /*
        首先判断名字是否为空或者长度为0  直接返回原类名
        如果长度大于1并且 第二个字符为大写字母 并且 第一个字符为大写时  此时返回原类名
        如果都通过 将第一个字符转换为小写  返回类名
        只有当第一个字符为小写 并且第二个字符不为大写的情况下 才会变为默认的命名
                其他情况都是返回原类名
         */
        String name="Student";
        String name2="APPConfig";
        System.out.println(name+":"+ Introspector.decapitalize(name));
        System.out.println(name2+":"+ Introspector.decapitalize(name2));
    }
}
