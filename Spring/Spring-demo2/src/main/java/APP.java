import com.demo.model.ArticleInfo;
import com.demo.service.APPConfig;
import com.demo.service.Articles;
import com.demo.service.Student;
import com.demo.service.UserController;
import com.example.Teacher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class APP {
    public static void main(String[] args) {
        //获取上下文对象
        ApplicationContext context=new ClassPathXmlApplicationContext("spring-config.xml");
        //得到Bean对象 使用注解方式注册时 如果不指定value Bean的id默认为首字母小写其他相同的方法
        UserController userController=context.getBean("userController",UserController.class);
        //使用Bean
        userController.SayHi("张三");
        Student student=context.getBean("student",Student.class);
        student.sayHi();
        Teacher teacher=context.getBean("teacher",Teacher.class);
        teacher.sayHi();
        APPConfig appConfig=context.getBean("APPConfig",APPConfig.class);
        appConfig.sayHi();
        ArticleInfo articleInfo=context.getBean("articleInfo",ArticleInfo.class);
        System.out.println(articleInfo.toString());
        Articles articles=context.getBean("articles",Articles.class);
        articles.sayHi();
    }
}
