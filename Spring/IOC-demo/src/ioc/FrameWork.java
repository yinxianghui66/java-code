package ioc;
public class FrameWork {
    private Bottom bottom;
    public FrameWork(Bottom bottom){
        this.bottom=bottom;
    }
    public void init(){
        System.out.println("FrameWork init ");
        bottom.init();
    }
}
