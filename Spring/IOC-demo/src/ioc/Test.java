package ioc;
//模拟IOC容器
public class Test {
    private Tire tire;
    private Bottom bottom;
    private FrameWork frameWork;
    private Car car;
    public Test(){
        this.tire=new Tire(12);
        this.bottom=new Bottom(this.tire);
        this.frameWork=new FrameWork(this.bottom);
        this.car=new Car(this.frameWork);
    }
    public static void main(String[] args) {
        Test test=new Test();
        test.car.init();
    }
}
