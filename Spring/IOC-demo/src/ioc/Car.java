package ioc;
public class Car {
    //声明我需要这个对象
    private FrameWork frameWork;
    public Car(FrameWork frameWork){
        this.frameWork=frameWork;
    }
    public void init(){
        System.out.println("Car init");
        frameWork.init();
    }
}
