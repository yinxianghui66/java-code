public class Test {
    public static void printArray(int arr[]){
        for(int x:arr){
            System.out.println(x);
        }
    }
    public static void main(String[] args) {
        int []arr=new int[10];
        for(int i=0;i<arr.length;i++){
            arr[i]=i;
        }
        printArray(arr);
    }
}
