import java.util.Scanner;

public class Test {
    public static int max2(int x,int y){
        return x>y?x:y;
    }
    public static int max3(int x,int y,int z){
        int ret=max2(x,y);
        return ret>z?ret:z;
    }
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int a=in.nextInt();
        int b=in.nextInt();
        int c=in.nextInt();
        System.out.println(max3(a, b, c));
    }
}
