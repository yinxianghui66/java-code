import java.util.Scanner;

public class Test {
    public static int sum(int x,int y){
        return x+y;
    }
    public static float sum(float x,float y,float z){
        return x+y+z;
    }
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int a=in.nextInt();
        int b=in.nextInt();
        System.out.println(sum(a, b));
        float c=in.nextFloat();
        float d=in.nextFloat();
        float e=in.nextFloat();
        System.out.println(sum(c, d, e));
    }
}
