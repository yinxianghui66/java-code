import java.util.Scanner;

public class Test {
    public static void fan(int n){
        int count=0;
        int num=n;
        while(num>0){
            num/=10;
            count++;
        }
        for(int i=0;i<count;i++){
            System.out.print(n % 10+" ");
            n/=10;
        }
    }
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        fan(n);
    }
}
