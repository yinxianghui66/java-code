import java.util.Scanner;

public class Test {
    public static int sum(int n){
        if(n==1){
            return 1;
        }
        return sum(n-1)+n;
    }
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);

        int n=in.nextInt();
        System.out.println(sum(n));
    }
}
