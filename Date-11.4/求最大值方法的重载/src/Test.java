import java.util.Scanner;

public class Test {
    public static int max(int x,int y){
        return x>y?x:y;
    }
    public static float max(float x,float y,float z){
        float max1=x>y?x:y;
        return max1>z?max1:z;
    }
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int a=in.nextInt();
        int b=in.nextInt();
        System.out.println("最大值为"+max(a, b));
        float c=in.nextFloat();
        float d=in.nextFloat();
        float e=in.nextFloat();
        System.out.println("最大值为"+max(c, d, e));
    }
}
