import java.util.Scanner;

public class Test {
    public static int sum(int n){
        if(n<=9){
            return n;
        }
        return sum(n/10)+n%10;
    }
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        System.out.println(sum(n));
    }
}