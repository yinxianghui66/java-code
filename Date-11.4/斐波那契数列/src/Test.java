import java.util.Scanner;

public class Test {
    public static int fid(int n){
        int a1=1;  //a1代表第一项
        int a2=1;   //a2代表第二项
        if(n==1||n==2){   //如果是前两项直接返回1
            return 1;
        }
        int a3=0;
        for(int l=3;l<=n;l++){
            a3=a1+a2;    //第三项为第一项加第二项
            a1=a2;      //第一项就变为上一次的第二项
            a2=a3;      //第二项变为上一次的第三项
        }
        return a3;
    }、
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        System.out.println(fid(n));
    }
}
