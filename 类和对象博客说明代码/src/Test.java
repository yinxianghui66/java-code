

class Dog{
    public String name;  //狗的名字u
    public int age;   //狗的年龄
//    public float a;

    public Dog(String name, int age) {
        this.name = name;
        this.age = age;
    }

    //    public Dog(){   //如果不写构造方法，编译器会自动生成一个这样无参的构造方法
//
//    }
    public void eat(){   //狗的吃饭行为
        System.out.println("吃饭");
    }
    public void barks(){  //狗叫行为
        System.out.println("汪汪叫");
    }
}

class Date{
    public int year;
    public int month;
    public int day;

    public void setDay(int year,int month,int day){
        this.year=year;
        this.month=month;
        this.day=day;
    }
    public void print(){
        System.out.println(year+"/"+month+"/"+day);
    }
}

public class Test {


    public static void main(String[] args) {
        Dog dog=new Dog("小凯",16);  //如果这里只写一个小凯，则会执行一个参数的构造方法
//        Dog dog=new Dog("小凯");
        System.out.println(dog.name);
        System.out.println(dog.age);

    }
//    public static void main3(String[] args) {
//        System.out.println(dog.name);
//        System.out.println(dog.age);
//        System.out.println(dog.a);
//    }
    public static void main2(String[] args) {
        Date date=new Date();
        date.setDay(2022,11,13);
        date.print();
    }
//    public static void main1(String[] args) {
//        Dog dog=new Dog();  //实例化对象
//        Dog dog1=new Dog();  //再创建另外一只狗
//        dog.name="小凯";  //狗的名字初始化为小凯
//        dog.age=11;    //狗的年龄初始化为11
//        dog1.name="小张";
//        dog1.age=18;
//    }
}
