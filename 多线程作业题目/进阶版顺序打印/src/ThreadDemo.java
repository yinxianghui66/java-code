//有三个线程，分别只能打印A，B和C
//要求按顺序打印ABC，打印10次
public class ThreadDemo {
    private static volatile int count=0;
    public static void main(String[] args) {
         Object object=new Object();
        Thread t1=new Thread(()->{
            for (int i = 0; i <10; i++) {
                synchronized (object){
                    while (count%3!=0) {
                        try {
                            object.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.print("A");
                    count++;
                    object.notifyAll();
                }
            }
        });
        Thread t2=new Thread(()->{
            for (int i = 0; i <10; i++) {
                synchronized (object){
                    while (count%3!=1) {
                        try {
                            object.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.print("B");
                    count++;
                    object.notifyAll();
                }
            }
        });
        Thread t3=new Thread(()->{
            for (int i = 0; i <10; i++) {
                synchronized (object){
                    while (count%3!=2) {   //来判断当前是否该是他打印，如果不是就要阻塞等待正确的打印，打印完后释放所有
                        try {
                            object.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("C");
                    count++;
                    object.notifyAll();
                }
            }
        });
        t1.start();
        t2.start();
        t3.start();
    }
}
