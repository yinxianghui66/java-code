public class ThreadDemo {
    public static int count=0;
    public static void main(String[] args) {
        Object object=new Object();
        Thread a=new Thread(()->{
            synchronized (object) {
                while (count%3!=2) {  //必须是第三个打印才会打印，否则阻塞等待其他线程打印
                    try {
                        object.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread().getName());
                count++;
                object.notifyAll();
            }
        },"a");
        Thread b=new Thread(()->{
            synchronized (object) {
                while (count%3!=1) {
                    try {
                        object.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread().getName());
                count++;
                object.notifyAll();
            }
        },"b");
        Thread c=new Thread(()->{
            synchronized (object) {
                while (count%3!=0) {
                    try {
                        object.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread().getName());
                count++;
                object.notifyAll();
            }
        },"c");
        a.start();
        b.start();
        c.start();
    }
}
