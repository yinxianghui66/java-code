import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        double n=in.nextDouble();
        
    }
    public static void main4(String[] args) {   //在不使用第三个变量的情况下交换两个int类型变量的值
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
//        a=a+b;
//        b=a-b;   //a+b-b
//        a=a-b;   //a+b-a+b-b
//        System.out.print(a+" ");
//        System.out.print(b);
        int i=0;
        a=(a+b)-(b=a);
        System.out.print(a+" ");
        System.out.print(b);
    }
    public static void main3(String[] args) {
        Scanner in=new Scanner(System.in);
        double n=in.nextDouble();               //改进后可以直接给浮点数加上0.5，如果本身大于0.5的话就会进1，
        in.close();                            //强转后即可
        System.out.println((int)(n+0.5));
        //int i=(int)Math.round(n);   //利用java本身的函数，round用于四舍五入
        //System.out.println(i);
    }
    public static void main2(String[] args) {   //用户输入一个浮点数，将其四舍五入转换为整数
            Scanner in=new Scanner(System.in);
            double n=in.nextDouble();               //第一次自己的思路
            in.close();
            int a=(int)n;
            int x=(int)((n-a)*10);
            if(x>=5){
                System.out.println((int)n+1);
            }else{
                System.out.println((int)n);
            }
    }
    public static void main1(String[] args) {    //简单运算，输入两个整数计算加减乘除模除
        Scanner in = new Scanner(System.in);
        int a=in.nextInt();
        int b=in.nextInt();
        in.close();
        int ret=0;
        if(a<b){  //如果a<b时 需要交换两个数的值，确保得到正数和整数
            ret=b;
            b=a;
            a=ret;
        }
        System.out.print(a+b+" ");
        System.out.print(a-b+" ");
        System.out.print(a*b+" ");
        System.out.print(a/b+" ");
        System.out.print(a%b+" ");
    }
}
