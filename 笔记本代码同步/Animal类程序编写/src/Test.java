class Animal{
    void Walk(){
        System.out.println("调用动物的走路Walk方法");
    }
    void Eat(){
        System.out.println("调用动物的吃饭Eat方法");
    }
}
class Goat extends Animal{
    void Eat(){
        System.out.println("羊吃草");
    }
}
class  Wolf extends Animal{
    void Eat(){
        System.out.println("狼吃肉");
    }
}
public class Test {
    public static void main(String[] args) {
    Animal goat=new Goat();
    goat.Eat();
    goat.Walk();
    Animal wolf=new Wolf();
    wolf.Eat();
    wolf.Walk();
    }
}
