import java.util.Random;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {   //猜数字小游戏
        Scanner scan=new Scanner(System.in);
        Random random=new Random();    //根据系统时间生成
        int randNum=random.nextInt(50)+50;  //随机生成50-100数字 //（）里表示从
        System.out.println(randNum);  //随机生成的数字
        while(true){
            System.out.println("输入要猜的数字");
            int num= scan.nextInt();
            if(num>randNum){
                System.out.println("猜大了");
            }else if(num<randNum){
                System.out.println("猜小了");
            }else{
                System.out.println("猜对了");
                break;
            }

        }
    }
    public static void main9(String[] args) {  //循环输入 输出
        Scanner scan=new Scanner(System.in);
        while(scan.hasNextInt()){
            int n=scan.nextInt();
            System.out.println(n);
        }
    }
    public static void main8(String[] args) {
        Scanner scan=new Scanner(System.in);    //从键盘中输入内容 放在缓冲区
        System.out.println("请输入姓名");
        String name=scan.nextLine();   //next 遇到空格会结束  nextLine 则不会
        System.out.println(name);
        System.out.println("输入年龄");
        int n=scan.nextInt();
        System.out.println(n);
        System.out.println("输入工资");
        float f=scan.nextFloat();
        System.out.println(f);
    }
    public static void main7(String[] args) {
        int a=1;
        while(a<=100){                      //找出1-100中可以被3和5同时整除的数
            if(a%3 == 0&&a % 5== 0){
                System.out.println(a);
                a++;
                continue;
            }
            else{
                a++;
            }
        }
    }
    public static void main6(String[] args) {
        int i= 1;
        int sum= 0;
        while(i <= 5){   //1-5阶乘和
        int a= 1;
        int ret= 1;
        while(a<=i){
            ret *= a;
            a++;
        }
            i++;
            sum += ret;
        }
        System.out.println(sum);
    }
    public static void main5(String[] args) {
        int a=10;
        switch (a) {
            case 10:
                System.out.println("yes");
                break;
            case 2:
                System.out.println("no");
                break;
            default:
                System.out.println("输入有误");
                break;
        }
    }
    public static void main4(String[] args) {
        int year= 2020;
        if((year % 4 == 0&&year% 100!= 0) || year % 400 ==0){
            System.out.println(year+"是闰年");   //判断闰年
        }else{
            System.out.println(year+"不是闰年");
        }
    }
    public static void main3(String[] args) {
        int year =2020;
        if(year % 100 == 0){
            if(year % 400 == 0){
                System.out.println(year+"是闰年");
            }else{
                System.out.println(year+"不是闰年");
            }
        }else {
            if(year % 4 == 0){
                System.out.println(year+"是闰年");
            }else{
                System.out.println(year+"不是闰年");
            }
        }
    }
    public static void main2(String[] args) {
        int a= 10;
        if(a <0){
            System.out.println("负数 ");  //判断正负数
        }else if(a > 0){
            System.out.println("正数 ");
        }else{
            System.out.println("0 ");
        }
    }
    public static void main1(String[] args) {
        int a = 2;
        if(a % 2==0) {
            System.out.printf("偶数 ");   //判断奇偶
        }else {
            System.out.printf("奇数 ");
        }
    }
}
