import java.util.Arrays;

public class Test {
    public static void fanc(int []arr){
        int right=0;
        int left=arr.length-1;
        while (right<left) {
            if(right<left&&arr[right]%2!=0){
                right++;
            }
            if(right<left&&arr[left]%2==0){
                left--;
            }
            int tmp=arr[right];
            arr[right]=arr[left];
            arr[left]=tmp;
        }
    }
    public static void main1(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6};
        fanc(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void main(String[] args) {
        
    }
}