import java.util.concurrent.Semaphore;

public class ThreadDemo {
    public static void main(String[] args) {
        Semaphore semaphore=new Semaphore(4);
        for (int i = 0; i <20; i++) {
            Thread t = new Thread(() -> {
                System.out.println("申请资源");
                try {
                    semaphore.acquire();  //信号量--
                    System.out.println("获取到资源了");
                    Thread.sleep(1000);
                    System.out.println("我释放资源了");
                    semaphore.release();  //释放资源 计数器++
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            t.start();
        }
    }
}
