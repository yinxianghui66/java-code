import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class ThreadDemo {
    public static void main(String[] args) throws InterruptedException {
        BlockingDeque<String> queue=new LinkedBlockingDeque<>();
        //阻塞队列方法有俩
        //put 入队列   都带有阻塞功能
        queue.put("hello1");
        queue.put("hello2");
        queue.put("hello3");
        queue.put("hello4");
        queue.put("hello5");
        //take 出队列
        String result=null;
        result=queue.take();
        System.out.println(result);
        result=queue.take();
        System.out.println(result);
        result=queue.take();
        System.out.println(result);
        result=queue.take();
        System.out.println(result);
        result=queue.take();
        System.out.println(result);
        result=queue.take();
        System.out.println(result);
    }
}
