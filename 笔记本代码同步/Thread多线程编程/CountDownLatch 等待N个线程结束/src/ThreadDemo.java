import java.util.concurrent.CountDownLatch;

public class ThreadDemo {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(10);  //同时等待十个任务结束
        for (int i = 0; i < 10; i++) {
        Thread t = new Thread(() -> {
            try {
                Thread.sleep(1000);
                System.out.println("一个任务开始");
                latch.countDown();  //任务完成 计数器--
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t.start();
        }
        latch.await();
        System.out.println("结束");
    }
}
