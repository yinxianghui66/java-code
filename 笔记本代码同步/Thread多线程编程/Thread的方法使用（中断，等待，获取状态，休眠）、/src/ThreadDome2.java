public class ThreadDome2 {
    public static void main(String[] args) {
        Thread t=new Thread(()->{
            while (!Thread.currentThread().isInterrupted()){
                System.out.println("吃坤");
                try {
                    Thread.sleep(1000);         //当sleep被唤醒的时候，会自动的把标志位重置为false
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
        });
        t.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t.interrupt();  //interrupt()会做两件事，一件是把标志位设为true 另外是如果程序正在阻塞（正在执行sleep）就会立即停止阻塞
                        //然后抛出异常让sleep立即结束
    }
}
