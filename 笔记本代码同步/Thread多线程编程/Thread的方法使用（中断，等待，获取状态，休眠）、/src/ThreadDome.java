public class ThreadDome {//使用自定义标志位中断线程
    public static  boolean isQuit=false;   //自己创建一个公共变量
    public static void main(String[] args) {
        Thread t=new Thread(()->{
            while (!isQuit){
                System.out.println("出击");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        isQuit=true;   //在主线程睡了三秒后，把标志位改成true 终止进程
    }
}
