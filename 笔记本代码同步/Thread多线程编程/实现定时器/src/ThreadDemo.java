import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.PriorityBlockingQueue;

class MyTask implements Comparable<MyTask>{
    public Runnable runnable;
    public long time;
    public MyTask(Runnable runnable, long delay){
        this.runnable=runnable;
        this.time=System.currentTimeMillis()+delay;
    }
    //添加比较器，来比较任务在队列里的排序，保证最先执行的放在最前面
    @Override
    public int compareTo(MyTask o){
        return (int) (this.time-o.time);
    }
}

class  Mytimer{
    Object object=new Object();
    private PriorityBlockingQueue<MyTask> queue=new PriorityBlockingQueue<>();

    public void Myschedule(Runnable runnable,long time) throws InterruptedException {
        MyTask mytask=new MyTask(runnable,time);
        queue.put(mytask);
        //为了防止插入一个新任务 新任务在是最新的，你插入一个任务就应该唤醒上一次还在睡的任务，来保证最先执行
        synchronized (object){
            object.notify();
        }
    }
    public Mytimer() {
        Thread t=new Thread(()->{
                while (true) {
                    try {
                        synchronized (object) {
                            MyTask task = queue.take();
                            long curtime = System.currentTimeMillis();
                            if (task.time <= curtime) {
                                task.runnable.run();
                            } else {   //还没到时间就塞回去
                                queue.put(task);
                                //解决忙等问题
                                object.wait(task.time - curtime);
                            }
                        }
                        } catch(InterruptedException e){
                            e.printStackTrace();
                    }
            }
        });
        t.start();
    }
}
public class ThreadDemo {
    public static void main(String[] args) throws InterruptedException {
        Mytimer mytimer=new Mytimer();
        mytimer.Myschedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello2");
            }
        },3000);
        mytimer.Myschedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello1");
            }
        },2000);
        System.out.println("hello0");
    }
}
