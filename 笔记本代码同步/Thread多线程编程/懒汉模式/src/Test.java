class  Singleton{
    volatile private static Singleton singleton=null;
//     synchronized
     static Singleton getSingleton() {
         //如果对象已经有了就不用再加锁了~，如果还没对象再加锁创建~
         if (singleton == null) {
             synchronized (Singleton.class) {  //保证判断和new一定是原子的就可以~
                 if (singleton == null) {    //保证多线程下还是空的~
                     singleton = new Singleton();
                 }
             }
         }
         return singleton;
     }
    private Singleton(){};
}

public class Test {
    public static void main(String[] args) {
            Singleton s1=Singleton.getSingleton();
            Singleton s2=Singleton.getSingleton();
        System.out.println(s1 == s2);
    }
}
