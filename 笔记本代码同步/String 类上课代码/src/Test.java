import java.util.Locale;


public class Test {
    public static boolean effective(char s){              //验证回文串
        if((s>='0'&&s<='9')||(s>='a'&&s<='z')){
            return true;
        }
        return false;
    }
    public boolean isPalindrome(String s) {
        s=s.toLowerCase();
        int left=0;
        int right=s.length()-1;
        while(left<right){
            while (left<right&&!effective(s.charAt(left))){
                    left++;
            }
            while (left<right&&!effective(s.charAt(right))){
                right--;
            }
            if(s.charAt(left)!=s.charAt(right)){
                return false;
            }else{
                left++;
                right--;
            }

        }
        return true;
    }

    public static void main2(String[] args) {
        String str="name=zhangsan&age=18";
        String []str1=str.split("&");
        for (int i = 0; i <str1.length; i++) {
            String []str2=str1[i].split("=");
            System.out.println(str2[0]+"="+str2[1]);
        }
    }
    public static void main1(String[] args) {
        String str="192.168.1.1";
        String []str1=str.split("\\.");
        for (int i = 0; i <str1.length; i++) {
            System.out.println(str1[i]);
        }
    }
}
