class  Person{
    private String name;
    private int age;
    private static String sex;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("两个参数的构造方法");
    }

    public static String getSex() {
        return sex;
    }

    public static void setSex(String sex) {
        Person.sex = sex;
    }

    static {
        System.out.println("静态代码块");
    }
    {
        sex="man";
        System.out.println("构造代码块");
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    private void eat(){
        System.out.println("吃饭");
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
public class Test {
    public static void main(String[] args) {
        Person stu=new Person("张三",15);
        System.out.println(stu);
    }
}
