import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //输入根目录
        Scanner scanner=new Scanner(System.in);
        System.out.println("输入要扫描的根目录");
        File rootDir=new File(scanner.next());
        if(!rootDir.isDirectory()){
            System.out.println("输入错误");
            return;
        }
        //输入要查询的词
        System.out.println("请输入要查找的词");
        String word=scanner.next();
        //递归查找
        scanDir(rootDir,word);
    }

    private static void scanDir(File rootDir, String word) {
        //列出rootDir中的内容，如果为空，就返回
        File[] files=rootDir.listFiles();
        if(files==null){
            return;
        }
        for (File f:files) {
            System.out.println(f.getAbsolutePath());
            if(f.isFile()){
                //如果是文件   打开文件，读取内容
                String content=readFile(f);
                if(content.contains(word)){
                    System.out.println(f.getAbsolutePath()+"包含要查找的关键字");
                }
            }else if(f.isDirectory()){
                //如果是目录  递归执行 找到文件
                scanDir(f,word);
            }else {
                //两者都不是
                continue;
            }
        }
    }

    private static String readFile(File f) {
        StringBuffer stringBuffer=new StringBuffer();  //存储读取的结果
        try(Reader reader=new FileReader(f)){
            while (true){  //读取文件内容  使用字符流
                int ret=reader.read();
                if(ret==-1){
                    break;
                }
                stringBuffer.append((char)ret);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuffer.toString();
    }
}
