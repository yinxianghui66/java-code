package network;
import java.io.IOException;
import java.net.*;
import java.util.Scanner;
public class UdpEchoClient {
    private DatagramSocket socket=null;
    private String serverIP;
    private int serverPort;
    public UdpEchoClient(String serverIP,int serverPort) throws SocketException {  //需要制定服务器的ip地址和端口号
        socket=new DatagramSocket();   //对于客户端来说，不需要手动制定端口，系统会自动分配端口
        this.serverIP=serverIP;
        this.serverPort=serverPort;

    }
    public void start() throws IOException {
        //客户端也是随时待命的，只要你输入了请求，就立即发送，进行多次交换
        while (true){
            //1.先从控制台读取请求
            Scanner scanner=new Scanner(System.in);
            System.out.println("->输入请求");
            String request= scanner.nextLine();
            //2.把请求构造成UDP packet，进行发送
            DatagramPacket packet=new DatagramPacket(request.getBytes(),request.getBytes().length,
                    InetAddress.getByName(serverIP),serverPort);   //把控制台输入的字符串转换成字符数组，字符数组长度，输入输入的服务器
            socket.send(packet);  //真正发送出去了                                 //的ip地址和端口号  这里的 InetAddress获取的就是自己制定的ip地址和端口号
            //3.客户端读取服务器返回的响应
            //构造空的DatagramPacket进行接收数据
            DatagramPacket responsePacket=new DatagramPacket(new byte[4096],4096);
            socket.receive(responsePacket);
            //4.把服务器返回的响应转换成字符串显示出来
            String response=new String(responsePacket.getData(),0,responsePacket.getLength());
            System.out.printf("req: %s,resp: %s\n",request,response);
        }
    }
    public static void main(String[] args) throws IOException {
        UdpEchoClient udpEchoClient=new UdpEchoClient("127.0.0.1",1234);
        udpEchoClient.start();
    }
}
