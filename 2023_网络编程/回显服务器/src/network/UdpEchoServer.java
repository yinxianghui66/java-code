package network;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
public class UdpEchoServer {
    //定义一个scoket对象
    private DatagramSocket socket=null;
    //定义构造方法,指定端口号    //如果绑定端口失败（被别的进程占用了）抛出异常
    public UdpEchoServer(int port) throws SocketException {
        socket=new DatagramSocket(port);
    }
    public void start() throws IOException {
        System.out.println("服务器启动");
        while (true){
            //服务器是24小时全程待命的，如果没有任务就阻塞等待，一有任务就立即执行
            //1.读取请求并解析
            DatagramPacket packet=new DatagramPacket(new byte[4096],4096);
            socket.receive(packet);  //把packet这个空的对象放入方法，方法内部进行操作之后，把读取的数据写入packet
            //receive方法，如果当前没有客户端没发数据报，他会进行阻塞等待，一来数据报就立即执行
            //为了方便处理请求，将数据报转换成String
            String request=new String(packet.getData(),0,packet.getLength()); //把数据拿出来放到String 中从0-数组长度
            //2.根据请求计算响应（回显服务器省略这个）
            String response=process(request);  //用来处理数据的方法，返回响应结果
            //3.把响应结果写回客户端  //根据response字符串构造 DatagramPacket来返回响应
            DatagramPacket responsePaket=new DatagramPacket(response.getBytes(),response.getBytes().length,packet.getSocketAddress());
            //发送的时候需要制定发送到哪里，IP地址和端口号(getSocketAddress())  此时packet就是从客户端发送来的数据，这里的就是客户端的IP地址和端口号
            socket.send(responsePaket); //发送数据报
            System.out.printf("[%s:%d] req: %s,resp: %s\n",packet.getAddress().toString()
            ,packet.getPort(),request,response);  //打印日志，IP地址，端口号 客户端的请求 响应返回内容
        }
    }

    public String process(String request) {
        return  request;  //这里是回显服务器，怎么处理并不实现，请求是什么 返回就是什么
    }

    public static void main(String[] args) throws IOException {
        UdpEchoServer udpEchoServer=new UdpEchoServer(1234);
        udpEchoServer.start();
    }
}
