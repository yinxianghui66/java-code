package network;

import java.io.IOException;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;

public class UdpDictServer extends UdpEchoServer{
    private Map<String,String> dict=new HashMap<>();  //存储要翻译的字符串和对应的翻译~

    public UdpDictServer(int port) throws SocketException {
        super(port);
        dict.put("dog","狗");
        dict.put("cat","猫");
        dict.put("fuck","草");
        //存储英文和翻译到Map里 翻译实际上就是查表
    }
    @Override
    public String process(String request){
        return dict.getOrDefault(request,"查找不到该单词");  //在表里查找该单词，找到就返回翻译，找不到就差找不到
    }

    public static void main(String[] args) throws IOException {
        UdpDictServer udpDictServer=new UdpDictServer(1234);
        udpDictServer.start();
    }
}
