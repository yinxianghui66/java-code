import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TcpDictionaryserver extends TcpEchoServer{
    private Map<String,String> dict=new HashMap<>();  //使用MAP存储输入的字符串和对应的翻译
    public TcpDictionaryserver(int port) throws IOException {
        super(port);
        dict.put("dog","狗");
        dict.put("cat","猫");
        dict.put("sb","傻逼");
    }
    @Override
    public String process(String request){
        return dict.getOrDefault(request,"查找不到");
    }

    public static void main(String[] args) throws IOException {
        TcpDictionaryserver tcpEchoDictionaryserver=new TcpDictionaryserver(1234);
        tcpEchoDictionaryserver.start();
    }
}
