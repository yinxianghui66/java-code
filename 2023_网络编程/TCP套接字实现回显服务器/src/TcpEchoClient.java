import java.io.*;
import java.net.Socket;
import java.util.Scanner;
public class TcpEchoClient {
    private Socket socket=null;
    public TcpEchoClient (String host,int port) throws IOException {
        socket=new Socket(host,port);  //建立连接
    }
    public void start(){
        System.out.println("客户端启动");
        Scanner scanner=new Scanner(System.in);
        try(InputStream inputStream=socket.getInputStream();
            OutputStream outputStream=socket.getOutputStream()) {
            PrintWriter printWriter=new PrintWriter(outputStream);
            Scanner scannerFromSocket=new Scanner(inputStream);
            while (true){
                //从控制台输入请求
                System.out.println("输入请求->");
                String request= scanner.next();
                //将请求发给服务器
                printWriter.println(request);   //这里的发送是带\n 换行的 用来区分多个请求
                printWriter.flush();   //将缓冲器区数据刷新 写入
                //从读取服务器返回响应
                String response =scannerFromSocket.next();
                //显示出来
                System.out.printf("req: %s;resp: %s\n",request,response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        TcpEchoClient tcpEchoClient=new TcpEchoClient("127.0.0.1",1234);
        tcpEchoClient.start();
    }
}
