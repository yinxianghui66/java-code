import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TcpEchoServer {
    // ServerSocket 负责外场拉客
    //accept() 返回的Socket就是负责服务的
    private ServerSocket serverSocket=null;
    public TcpEchoServer(int port) throws IOException {
        serverSocket=new ServerSocket(port);
    }
    public void start() throws IOException {
        ExecutorService executorService= Executors.newCachedThreadPool();  //使用线程池优化
        System.out.println("服务器启动！");
        while (true){
            Socket clientSocket= serverSocket.accept();  //接受连接 等待客户端发来请求
            //如果直接调用，这个方法只能让一个客户端调用，导致下一个客户端无法及时accept建立连接，这里使用多线程来使多个客户端
            //这里是按照客户端来的数据串行创建线程，创建完之后线程并发执行~
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        processConnection(clientSocket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }
        //读取请求
    //根据请求计算响应
    //将响应返回客户端
    private void processConnection(Socket clientSocket) throws IOException {
        System.out.printf("[%s:%d] 服务器上线~\n",clientSocket.getInetAddress().toString(),
                clientSocket.getPort());
        try (InputStream inputStream = clientSocket.getInputStream();
             OutputStream outputStream=clientSocket.getOutputStream()){   //创建多个输入输出流对象
            Scanner scanner=new Scanner(inputStream);  //给inputStream套一层Scanner（转为字符流了!!) 可以使用hasNext方便我们找到换行
            PrintWriter printWriter=new PrintWriter(outputStream);  //把outputStream也转为字符流，方便写回字符串
        while (true) {
                if(!scanner.hasNext()){
                    //读取的内容到结尾了 也就是找到换行了 或者是对端关闭了连接，就会返回false
                    System.out.printf("[%s:%d] 客户端连接断开了",clientSocket.getInetAddress(),
                            clientSocket.getPort());
                    break;
                }
                //如果不是换行（请求的结尾）
                String request=scanner.next();  //直接读取这一次的请求 next()是往后读取到空白符就结束 (空格 换行 制表符 翻页符 ...)
                //处理响应
                String response=process(request);  //这里是回显服务器输入什么就返回什么
                //将响应返回客户端
                printWriter.println(response);  //记得换行，处理多个请求(之前约定\n为多个请求的分割，之前的next读取是不带\n的，这里返回响应要加回去
                 printWriter.flush();   //将缓冲器区数据刷新 写入                            // 本来的OutputStream是字节流，这里转换为字符流方便处理
                System.out.printf("[%s:%d] rep: %s;resp: %s\n",clientSocket.getInetAddress().toString(),
                        clientSocket.getPort(),request,response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            clientSocket.close();  //因为多次请求 需要释放资源 是给一个连接服务的
        }
    }

    public String process(String request) {
            return request;
    }

    public static void main(String[] args) throws IOException {
        TcpEchoServer tcpEchoServer=new TcpEchoServer(1234);
        tcpEchoServer.start();
    }
}
