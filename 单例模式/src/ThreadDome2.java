class Singletons{
    volatile private static Singletons singletons=null;
    public static Singletons getSingletons(){
        //如果对象已经创建过了，就不需要再创建了，直接返回就可以~
        if(singletons==null){
            synchronized (Singleton.class){    //保证判断和new是原子的
                if(singletons==null){
                    singletons=new Singletons();
                }
            }
        }
        return singletons;
    }

    private Singletons(){};
}
public class ThreadDome2 {
    public static void main(String[] args) {
        Singletons s1=Singletons.getSingletons();
        Singletons s2=Singletons.getSingletons();
        System.out.println(s1 == s2);
    }
}
