//这个类设定成单例的

class Singleton{

    private static Singleton instance=new Singleton();   //创建实例  唯一的~

    public static Singleton getInstance(){     //获取实例的方法
        return instance;
    }
    private  Singleton(){};  //该类构造方法设置为 private禁止外部new实例~
        //这样就可以保证单例~



}
class Test {
    public static void main(String[] args) {
        //唯一实例都是同一个~
            Singleton s1=Singleton.getInstance();
            Singleton s2=Singleton.getInstance();
//            Singleton s3=new Singleton();   //这样就禁止了再次new对象了~
        System.out.println(s1 == s2);
    }
}
