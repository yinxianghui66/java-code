package Model;


import java.sql.Connection;
import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class GoodsDao {

    //查询Goods表所有数据
    public List<Goods> selectAll(){
        List<Goods> goods=new ArrayList<>();
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        try {
            //建立链接
            connection=DBUtil.getConnection();
            //构造SQL语句
            String sql="select * from Goods";
            statement=connection.prepareStatement(sql);
            //执行SQL语句
            resultSet=statement.executeQuery();
            while (resultSet.next()){
                Goods goods1=new Goods();
                goods1.setGoodsId(resultSet.getInt("goodsId"));
                goods1.setName(resultSet.getString("name"));
                goods1.setPrice(resultSet.getDouble("price"));
                goods1.setNum(resultSet.getInt("num"));
                goods.add(goods1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,resultSet);

        }
        return goods;
    }
    //给一个商品id 返回商品数据
    public Goods selectId(int goodId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Goods goods = new Goods();
        try {
            //建立链接
            connection = DBUtil.getConnection();
            //构造SQL语句
            String sql = "select * from Goods where goodsId=?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, goodId);
            //执行SQL语句
            resultSet = statement.executeQuery();
            //返回的只有一条数据 使用if即可
            if (resultSet.next()) {
                goods.setGoodsId(resultSet.getInt("goodsId"));
                goods.setName(resultSet.getString("name"));
                goods.setPrice(resultSet.getDouble("price"));
                goods.setNum(resultSet.getInt("num"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return goods;
    }

}


