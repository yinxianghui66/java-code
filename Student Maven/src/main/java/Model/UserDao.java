package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    //查询所有用户数据
    public List<User> selectAll(){
        List<User> users=new ArrayList<>();
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        try {
            //建立链接
            connection=DBUtil.getConnection();
            //构造SQL语句
            String sql="select * from User";
            statement=connection.prepareStatement(sql);
            //执行SQL语句
            resultSet=statement.executeQuery();
            while (resultSet.next()){
               User user=new User();
               user.setUserId(resultSet.getInt("userId"));
               user.setUsername(resultSet.getString("username"));
               user.setPassword(resultSet.getString("password"));
               users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return users;
    }
    public boolean verify(String username,String password){
        UserDao userDao=new UserDao();
        List<User> user=userDao.selectAll();
        for (int i = 0; i <user.size(); i++) {
            if(user.get(i).getUsername().equals(username)){
                if(user.get(i).getPassword().equals(password)){
                    return true;
                }
            }
        }
        return false;
    }
}
