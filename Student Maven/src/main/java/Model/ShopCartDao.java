package Model;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ShopCartDao {
    //给定一个商品id  添加到购物车
    public void Idadd(int goodId) {
        GoodsDao goodsDao=new GoodsDao();
        Goods goods=goodsDao.selectId(goodId);
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //建立链接
            connection = DBUtil.getConnection();
            //构造SQL语句
            String sql="insert into ShopCart values(null,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1,goods.getName());
            statement.setDouble(2,goods.getPrice());
            statement.setInt(3,goods.getNum());
            //执行SQL语句
            statement.executeLargeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);

        }
    }
    //添加购物车数据
    public void add(ShopCart shopCart){
        Connection connection=null;
        PreparedStatement statement=null;
        try {
            //建立链接
            connection=DBUtil.getConnection();
            //构造sql语句
            String sql="insert into ShopCart values(null,?,?,?)";
            statement=connection.prepareStatement(sql);
            statement.setString(1,shopCart.getName());
            statement.setDouble(2,shopCart.getPrice());
            statement.setInt(3,shopCart.getNum());
            //执行sql语句
            statement.executeLargeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
    //删除购物车数据  给定一个id 删除对应的购物车数据
    public void remove(int id){
        Connection connection=null;
        PreparedStatement statement=null;
        try {
            //建立链接
            connection=DBUtil.getConnection();
            //构造sql语句
            String sql="delete from ShopCart where shopCartId= ?";
            statement=connection.prepareStatement(sql);
            statement.setInt(1,id);
            //执行sql语句
            statement.executeLargeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
    //查询所有数据
    public List<ShopCart> selectAll(){
        List<ShopCart> shopCarts=new ArrayList<>();
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        try {
            //建立链接
            connection=DBUtil.getConnection();
            //构造SQL语句
            String sql="select * from ShopCart";
            statement=connection.prepareStatement(sql);
            //执行SQL语句
            resultSet=statement.executeQuery();
            while (resultSet.next()){
                ShopCart shopCart=new ShopCart();
                shopCart.setShopCartId(resultSet.getInt("shopCartId"));
                shopCart.setName(resultSet.getString("name"));
                shopCart.setPrice(resultSet.getDouble("price"));
                shopCart.setNum(resultSet.getInt("num"));
                shopCarts.add(shopCart);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return shopCarts;
    }


}

