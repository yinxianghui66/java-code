package Model;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//封装 数据库的连接操作
public class DBUtil {
    //这个类提供 DataSource  DataSource 对于一个项目来说一个就够了（单例模式）
    private static volatile DataSource dataSource=null;

    private static DataSource getDataSource(){
        //保证线程安全  加锁和双重if判断
        if (dataSource == null) {
            synchronized (DBUtil.class) {
                if (dataSource == null) {
                    dataSource = new MysqlDataSource();
                    ((MysqlDataSource) dataSource).setURL("jdbc:mysql://127.0.0.1:3306/centre?characterEncoding=utf8&useSSL=false");
                    ((MysqlDataSource) dataSource).setUser("root");
                    ((MysqlDataSource) dataSource).setPassword("20020401yxh");
                }
            }
        }
        return dataSource;
    }
    //建立连接方法
    public static Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }
    //释放资源
    public static void close(Connection connection , PreparedStatement statement, ResultSet resultSet){
        if(resultSet!=null){
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(connection!=null){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(statement!=null){
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


}
