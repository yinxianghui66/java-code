package Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/index")
public class index extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session=req.getSession(false);
        if(session==null) {
            resp.sendRedirect("blog.html");
            return;
        }
        String username=(String) session.getAttribute("username");
        if(username==null){
            resp.sendRedirect("blog.html");
            return;
        }
        //验证都通过 登陆成功
        resp.setContentType("text/html;charset=utf8");
        String s="已登录";
        resp.getWriter().write(s);

    }
}
