package Controller;


import Model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;


//登陆校验
@WebServlet("/blog")
public class blog extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //从请求中拿到用户名和密码 校验
        req.setCharacterEncoding("utf8");
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        UserDao userDao=new UserDao();
        if(username==null||password==null||username.equals(" ")||password.equals(" ")){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("用户名或者密码不能为空");
            return;
        }
        if(!userDao.verify(username,password)){
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("用户名或者密码错误");
            return;
        }
        //设置Session 运行创建 到 index界面取出
        HttpSession session=req.getSession(true);
        session.setAttribute("username",username);
        resp.sendRedirect("index.jsp");
    }
}
