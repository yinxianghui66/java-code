package Controller;

import Model.Goods;
import Model.GoodsDao;
import Model.ShopCartDao;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import java.util.List;

@WebServlet("/commodity")
public class commodity extends HttpServlet {

    //将数据返还给浏览器  使用JSON格式
    ObjectMapper objectMapper=new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String  id=req.getParameter("goodId");
        HttpSession session = req.getSession(false);
        if (session == null) {
            resp.sendRedirect("blog.html");
            return;
        }
        String username = (String) session.getAttribute("username");
        if (username == null) {
            resp.sendRedirect("blog.html");
            return;
        }
        //如果发送的是带Id的 就根据这个id  查找对应的商品数据 进行添加到购物车操作
        if(id!=null){
            int ids=Integer.parseInt(id);
            ShopCartDao shopCartDao=new ShopCartDao();
            shopCartDao.Idadd(ids);
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("添加成功");
            resp.sendRedirect("shopping.html");
        }else {
            GoodsDao goodsDao = new GoodsDao();
            //查询所有商品数据
            List<Goods> good = goodsDao.selectAll();
            //使用objectMapper  转换成json格式
            String respString = objectMapper.writeValueAsString(good);
            resp.setContentType("application/json;charset=utf8");
            resp.getWriter().write(respString);
        }
    }


}
