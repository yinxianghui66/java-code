package Controller;

import Model.ShopCart;
import Model.ShopCartDao;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/shopping")
public class shopping extends HttpServlet {
    ObjectMapper objectMapper=new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id=req.getParameter("shopCartId");
        HttpSession session = req.getSession(false);
        if (session == null) {
            resp.sendRedirect("blog.html");
            return;
        }
        String username = (String) session.getAttribute("username");
        if (username == null) {
            resp.sendRedirect("blog.html");
            return;
        }
        if(id!=null){
            int ids=Integer.parseInt(id);
            ShopCartDao shopCartDao=new ShopCartDao();
            shopCartDao.remove(ids);
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("删除成功");
            resp.sendRedirect("shopping.html");
        }else {
            ShopCartDao shopCartDao = new ShopCartDao();
            List<ShopCart> shopCarts = shopCartDao.selectAll();
            //转换格式
            String respString = objectMapper.writeValueAsString(shopCarts);
            resp.setContentType("application/json;charset=utf8");
            //写回浏览器
            resp.getWriter().write(respString);
            }
    }

}
