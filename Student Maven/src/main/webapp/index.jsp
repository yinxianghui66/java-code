<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>index</title>
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
</head>
<style>
    .td{
        width: 100%;
        height: 500px;
        text-align: left;
    }   
    .ts{
        width: 70%;
        height: 70%;
        text-align: left;
    }
</style>
<body>
    <div>
       <% String name=(String)session.getAttribute("username");%>
       欢迎你<%=name%>
    </div>
    <hr>
    <table class="td" id="" border="1">
        <tr>
            <th > <a href="commodity.html" id="com">商品列表</a><br>
                <a href="shopping.html" id="shop">购物车</a></th>
            </th>
            <th class="ts" id="" border="1">
                center
            </th>
        </tr>
    </table>


    <script>

        jQuery.ajax({
                type:'get',
                url:'index',
                success :function(body){
                        
                        function sleep(numberMillis) {
                        var now = new Date();
                        var exitTime = now.getTime() + numberMillis;
                        while (true) {
                        now = new Date();
                        if (now.getTime() > exitTime)
                        return true;
                            }
                        }
                        //判断当前是否是登录状态 如果不是跳转到blog界面
                        let str="已登录";
                        if(body!=str){
                            document.write("用户未登录");
                            sleep(2);
                             window.location.replace("blog.html");
                        }
                    
                    }
                })
    </script>
</body>

</html>