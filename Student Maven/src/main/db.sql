--一般对于建表的 sql 都会单独写个 .sql 文件保存
--后续程序可能在不同的电脑上运行部署 部署的时候也需要运行 创建对应的数据库
--把建表的 sql 保存好 方便在不同的主机上进行建表建库

--这里是是正式 复制粘贴到mysql 执行的代码
create database if not exists centre character set utf8;

use centre;

drop table if exists Goods;

create table Goods(
    goodsId int primary key auto_increment,
    name varchar(128),
    price double,
    num int
);

drop table if exists ShopCart;

create table ShopCart(
    shopCartId int primary key auto_increment,
    name varchar(128),
    price double,
    num int
);

drop table if exists user;

create table user(
    userId int primary key auto_increment,
    username varchar(50) unique,
    password varchar(50)
);

insert into user values(null,'zhangsan','123'),(null,'lisi',123);

insert into Goods values(null,'商品1',50,1),(null,'商品2',60,1);
insert into Goods values(null,'商品3',70,1),(null,'商品4',80,1);
insert into Goods values(null,'商品5',90,1),(null,'商品6',100,1);
insert into Goods values(null,'商品7',110,1),(null,'商品8',120,1);
insert into Goods values(null,'商品9',130,1),(null,'商品10',140,1);

drop database centre;